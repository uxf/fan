default: cs-fix phpstan paratest

test: cs phpstan paratest

cs:
	vendor/bin/ecs --ansi

cs-fix:
	vendor/bin/ecs --fix --ansi

phpstan:
	php -d memory_limit=-1 vendor/bin/phpstan --ansi

release:
	php ./scripts/release.php $(type)

push:
	git push --follow-tags

paratest:
	make -C packages/ares
	make -C packages/cms
	make -C packages/code-gen
	make -C packages/content
	make -C packages/core
	make -C packages/datagrid
	make -C packages/form
	make -C packages/gen
	make -C packages/graphql
	make -C packages/hydrator
	make -C packages/security
	make -C packages/storage

phpunit:
	make -C packages/ares phpunit
	make -C packages/cms phpunit
	make -C packages/code-gen phpunit
	make -C packages/content phpunit
	make -C packages/core phpunit
	make -C packages/datagrid phpunit
	make -C packages/form phpunit
	make -C packages/gen phpunit
	make -C packages/graphql phpunit
	make -C packages/hydrator phpunit
	make -C packages/security phpunit
	make -C packages/storage phpunit

clear:
	rm -rf packages/ares/var
	rm -rf packages/cms/var
	rm -rf packages/code-gen/var
	rm -rf packages/content/var
	rm -rf packages/core/var
	rm -rf packages/datagrid/var
	rm -rf packages/form/var
	rm -rf packages/gen/var
	rm -rf packages/gql/var
	rm -rf packages/graphql/var
	rm -rf packages/hydrator/var
	rm -rf packages/security/var
	rm -rf packages/storage/var
