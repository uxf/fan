<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\ControlStructure\TrailingCommaInMultilineFixer;
use PhpCsFixer\Fixer\FunctionNotation\FunctionTypehintSpaceFixer;
use PhpCsFixer\Fixer\FunctionNotation\MethodArgumentSpaceFixer;
use PhpCsFixer\Fixer\Import\OrderedImportsFixer;
use PhpCsFixer\Fixer\Phpdoc\NoEmptyPhpdocFixer;
use PhpCsFixer\Fixer\Whitespace\NoExtraBlankLinesFixer;
use SlevomatCodingStandard\Sniffs\Namespaces\ReferenceUsedNamesOnlySniff;
use SlevomatCodingStandard\Sniffs\TypeHints\LongTypeHintsSniff;
use SlevomatCodingStandard\Sniffs\TypeHints\ParameterTypeHintSniff;
use SlevomatCodingStandard\Sniffs\TypeHints\PropertyTypeHintSniff;
use SlevomatCodingStandard\Sniffs\TypeHints\ReturnTypeHintSniff;
use SlevomatCodingStandard\Sniffs\TypeHints\UselessConstantTypeHintSniff;
use Symfony\Component\Finder\Finder;
use Symplify\EasyCodingStandard\Config\ECSConfig;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

$dirs = (new Finder())
    ->directories()
    ->depth('== 1')
    ->in(__DIR__ . '/packages')
    ->name(['src', 'tests', 'config'])
    ->getIterator();

$paths = array_map(static fn (SplFileInfo $dir) => $dir->getPathname(), iterator_to_array($dirs));

return ECSConfig::configure()
    ->withPaths($paths)
    ->withSets([
        SetList::PSR_12,
        SetList::STRICT,
        SetList::CLEAN_CODE,
        SetList::ARRAY,
    ])
    ->withRules([
        LongTypeHintsSniff::class,
        UselessConstantTypeHintSniff::class,
        ParameterTypeHintSniff::class,
        PropertyTypeHintSniff::class,
        ReturnTypeHintSniff::class,
        NoEmptyPhpdocFixer::class,
        NoExtraBlankLinesFixer::class,
        FunctionTypehintSpaceFixer::class,
        ReferenceUsedNamesOnlySniff::class,
    ])
    ->withConfiguredRule(OrderedImportsFixer::class, [
        'sort_algorithm' => OrderedImportsFixer::SORT_ALPHA,
        'imports_order' => [
            OrderedImportsFixer::IMPORT_TYPE_CONST,
            OrderedImportsFixer::IMPORT_TYPE_CLASS,
            OrderedImportsFixer::IMPORT_TYPE_FUNCTION,
        ],
    ])
    ->withConfiguredRule(TrailingCommaInMultilineFixer::class, [
        'elements' => ['arrays', 'arguments', 'parameters', 'match'],
    ])
    ->withConfiguredRule(MethodArgumentSpaceFixer::class, [
        'attribute_placement' => 'ignore',
        'on_multiline' => 'ensure_fully_multiline',
    ]);
