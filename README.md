# Monorepo uxf/fan

## Packages

- [cms](https://gitlab.com/uxf/fan/-/tree/3.x/packages/cms)
- [code-gen](https://gitlab.com/uxf/fan/-/tree/3.x/packages/code-gen)
- [content](https://gitlab.com/uxf/fan/-/tree/3.x/packages/content)
- [core](https://gitlab.com/uxf/fan/-/tree/3.x/packages/core)
- [datagrid](https://gitlab.com/uxf/fan/-/tree/3.x/packages/datagrid)
- [form](https://gitlab.com/uxf/fan/-/tree/3.x/packages/form)
- [gen](https://gitlab.com/uxf/fan/-/tree/3.x/packages/gen)
- [gql](https://gitlab.com/uxf/fan/-/tree/3.x/packages/gql)
- [graphql](https://gitlab.com/uxf/fan/-/tree/3.x/packages/graphql)
- [hydrator](https://gitlab.com/uxf/fan/-/tree/3.x/packages/hydrator)
- [security](https://gitlab.com/uxf/fan/-/tree/3.x/packages/security)
- [storage](https://gitlab.com/uxf/fan/-/tree/3.x/packages/storage)

## Dependency tree

[![Tree](https://gitlab.com/uxf/fan/-/raw/3.x/doc/tree.jpeg)](https://miro.com/app/board/uXjVORBCtQA=)

## Dev tools

```shell
############# DEVELOP #############

# run all (cs + phpstan + paratest)
make

# run cs check
make cs

# run cs fix
make cs-fix

# run phpstan
make phpstan

# run paratest
make paratest

# run phpunit
make phpnuit

############# RELEASE #############

# prepare release
make release type=fix
make release type=feat
make release type=perf

# push with tag
make push
```
