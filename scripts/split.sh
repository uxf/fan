#!/usr/bin/env bash

set -ex

# > help
# ./split.sh 3.x cms [COMMIT_TAG]

BRANCH="$1"
NAME="$2"
TAG="$3"
PREFIX="packages/$NAME"
URL="git@gitlab.com:uxf/$NAME.git"

git remote add $NAME $URL || git remote set-url $NAME $URL
git fetch $NAME $BRANCH

SHA1=`splitsh-lite --prefix=$PREFIX`
SHA1_REMOTE=`git log -n 1 $NAME/$BRANCH --pretty=format:"%H"`

if [ "$SHA1" == "$SHA1_REMOTE" ]; then
  echo "Skipped"
  exit
fi

if [ ! -z "$TAG" ]; then
    git tag -d "$TAG"
    git tag -a "$TAG" $SHA1 -m "$TAG"
fi

git push --follow-tags $NAME "$SHA1:$BRANCH" -f
