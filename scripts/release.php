<?php

declare(strict_types=1);

use Nette\Utils\FileSystem;
use Nette\Utils\Json;
use Symfony\Component\Finder\Finder;

require __DIR__ . '/../vendor/autoload.php';

enum ReleaseType: string
{
    case FIX = 'fix';
    case FEAT = 'feat';
    case PERF = 'perf';
}

if ($argc !== 2 || !$argv[1]) {
    echo 'Missing release type';
    exit(1);
}

$type = ReleaseType::tryFrom($argv[1]);
if ($type === null) {
    echo 'Invalid release type';
    exit(1);
}

exec('git describe --tags --abbrev=0', $latestTagOutput);
$latestTag = $latestTagOutput[0];

exec("git diff --name-only $latestTag HEAD", $diffFiles);

// parse
preg_match('/^(\d+)\.(\d+)\.(\d+)$/', $latestTag, $m);
[$all, $major, $minor, $patch] = $m;

$version = match ($type) {
    ReleaseType::FIX => "$major.$minor." . ($patch + 1),
    ReleaseType::FEAT => "$major." . ($minor + 1) . '.0',
    ReleaseType::PERF => ($major + 1) . ".0.0",
};

$modifiedPackages = [];

foreach ($diffFiles as $diffFile) {
    if (preg_match('/^packages\/([a-z\-]+)\/.*/', $diffFile, $m) === 1) {
        $modifiedPackages[] = $m[1];
    }
}

$modifiedPackages = array_unique($modifiedPackages);
$changedPackages = $modifiedPackages;

foreach ($modifiedPackages as $package) {
    $packages = match ($package) {
        'ares', 'cms', 'gen', 'gql', 'graphql', 'code-gen' => [],
        'storage' => ['cms', 'content'],
        'content', 'security', 'datagrid', 'form' => ['cms'],
        'core' => ['ares', 'cms', 'code-gen', 'content', 'datagrid', 'form', 'gen', 'gql', 'graphql', 'security', 'storage'],
        'hydrator' => ['ares', 'cms', 'code-gen', 'content', 'core', 'datagrid', 'form', 'gen', 'gql', 'graphql', 'security', 'storage'],
        default => throw new LogicException(),
    };

    array_push($changedPackages, ...$packages);
}

$dirs = [];
foreach (array_unique($changedPackages) as $package) {
    $dirs[] = __DIR__ . "/../packages/$package";
}

$finder = new Finder();

foreach ($finder->files()->depth('== 0')->in($dirs)->name('composer.json') as $file) {
    $packageComposer = Json::decode($file->getContents(), forceArrays: true);

    foreach ($packageComposer['require'] as $packageName => $packageVersion) {
        if (str_starts_with($packageName, 'uxf/')) {
            $package = substr($packageName, 4);
            if (in_array($package, $changedPackages, true)) {
                $packageComposer['require'][$packageName] = $version;
            }
        }
    }

    FileSystem::write($file->getPathname(), Json::encode($packageComposer, pretty: true) . "\n");
}

shell_exec("git commit -a -m '$version'");
shell_exec("git tag -a '$version' -m '$version'");
