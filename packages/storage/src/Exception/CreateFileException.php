<?php

declare(strict_types=1);

namespace UXF\Storage\Exception;

use Exception;

final class CreateFileException extends Exception
{
}
