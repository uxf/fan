<?php

declare(strict_types=1);

namespace UXF\Storage\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

#[ORM\Entity]
class Image extends File
{
    #[ORM\Column]
    private int $width;

    #[ORM\Column]
    private int $height;

    public function __construct(
        UuidInterface $uuid,
        string $extension,
        string $type,
        string $name,
        string $namespace,
        int $size,
        int $width,
        int $height,
    ) {
        parent::__construct($uuid, $extension, $type, $name, $namespace, $size);
        $this->width = $width;
        $this->height = $height;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }
}
