<?php

declare(strict_types=1);

namespace UXF\Storage\Http\Request;

use Symfony\Component\Validator\Constraints as Assert;

final readonly class FileRequestBody
{
    public function __construct(
        #[Assert\NotBlank]
        public string $content,
        public ?string $type = null,
        public ?string $name = null,
    ) {
    }
}
