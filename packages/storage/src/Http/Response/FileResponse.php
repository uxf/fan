<?php

declare(strict_types=1);

namespace UXF\Storage\Http\Response;

use Ramsey\Uuid\UuidInterface;
use UXF\Core\Type\DateTime;
use UXF\Storage\Entity\File;

final readonly class FileResponse extends StorageResponse
{
    public function __construct(
        public int $id,
        public UuidInterface $uuid,
        public string $type,
        public string $extension,
        public string $name,
        public string $namespace,
        public int $size,
        public DateTime $createdAt,
        public string $fileType = 'file',
    ) {
    }

    public static function create(File $file): self
    {
        return new self(
            id: $file->getId(),
            uuid: $file->getUuid(),
            type: $file->getType(),
            extension: $file->getExtension(),
            name: $file->getName(),
            namespace: $file->getNamespace(),
            size: $file->getSize(),
            createdAt: $file->getCreatedAt(),
        );
    }

    public static function createNullable(?File $file): ?self
    {
        return $file !== null ? self::create($file) : null;
    }
}
