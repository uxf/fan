<?php

declare(strict_types=1);

namespace UXF\Storage\Http\Response;

use Ramsey\Uuid\UuidInterface;
use UXF\Core\Type\DateTime;
use UXF\Storage\Entity\Image;

final readonly class ImageResponse extends StorageResponse
{
    public function __construct(
        public int $id,
        public UuidInterface $uuid,
        public string $type,
        public string $extension,
        public string $name,
        public string $namespace,
        public int $width,
        public int $height,
        public int $size,
        public DateTime $createdAt,
        public string $fileType = 'image',
    ) {
    }

    public static function create(Image $image): self
    {
        return new self(
            id: $image->getId(),
            uuid: $image->getUuid(),
            type: $image->getType(),
            extension: $image->getExtension(),
            name: $image->getName(),
            namespace: $image->getNamespace(),
            width: $image->getWidth(),
            height: $image->getHeight(),
            size: $image->getSize(),
            createdAt: $image->getCreatedAt(),
        );
    }

    public static function createNullable(?Image $image): ?self
    {
        return $image !== null ? self::create($image) : null;
    }
}
