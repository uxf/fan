<?php

declare(strict_types=1);

namespace UXF\Storage\GQL\Type;

use Ramsey\Uuid\UuidInterface;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type as LegacyType;
use UXF\Core\Type\DateTime;
use UXF\GraphQL\Attribute\Type;
use UXF\Storage\Entity\File as FileEntity;

#[LegacyType]
#[Type]
final readonly class File
{
    public function __construct(
        #[Field] public int $id,
        #[Field] public UuidInterface $uuid,
        #[Field] public string $type,
        #[Field] public string $extension,
        #[Field] public string $name,
        #[Field] public string $namespace,
        #[Field] public int $size,
        #[Field] public DateTime $createdAt,
    ) {
    }

    public static function create(FileEntity $file): self
    {
        return new self(
            id: $file->getId(),
            uuid: $file->getUuid(),
            type: $file->getType(),
            extension: $file->getExtension(),
            name: $file->getName(),
            namespace: $file->getNamespace(),
            size: $file->getSize(),
            createdAt: $file->getCreatedAt(),
        );
    }

    public static function createNullable(?FileEntity $file): ?self
    {
        return $file !== null ? self::create($file) : null;
    }
}
