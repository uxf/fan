<?php

declare(strict_types=1);

namespace UXF\Storage\GQL\Type;

use Ramsey\Uuid\UuidInterface;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type as LegacyType;
use UXF\Core\Type\DateTime;
use UXF\GraphQL\Attribute\Type;
use UXF\Storage\Entity\Image as ImageEntity;

#[LegacyType]
#[Type]
final readonly class Image
{
    public function __construct(
        #[Field] public int $id,
        #[Field] public UuidInterface $uuid,
        #[Field] public string $type,
        #[Field] public string $extension,
        #[Field] public string $name,
        #[Field] public string $namespace,
        #[Field] public int $size,
        #[Field] public int $width,
        #[Field] public int $height,
        #[Field] public DateTime $createdAt,
    ) {
    }

    public static function create(ImageEntity $image): self
    {
        return new self(
            id: $image->getId(),
            uuid: $image->getUuid(),
            type: $image->getType(),
            extension: $image->getExtension(),
            name: $image->getName(),
            namespace: $image->getNamespace(),
            size: $image->getSize(),
            width: $image->getWidth(),
            height: $image->getHeight(),
            createdAt: $image->getCreatedAt(),
        );
    }

    public static function createNullable(?ImageEntity $image): ?self
    {
        return $image !== null ? self::create($image) : null;
    }
}
