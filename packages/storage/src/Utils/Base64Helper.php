<?php

declare(strict_types=1);

namespace UXF\Storage\Utils;

use Nette\Utils\Strings;
use function Safe\base64_decode;

final readonly class Base64Helper
{
    public static function decode(string $content): string
    {
        // data:application/pdf;base64,xxx
        // data:image/png;base64,xxx
        // data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,
        return base64_decode(Strings::replace($content, '/^data:[a-z\-.]+\/[a-z\-.]+;base64,/i'));
    }
}
