<?php

declare(strict_types=1);

namespace UXF\Storage;

use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemOperator;
use Ramsey\Uuid\Doctrine\UuidType;
use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use UXF\Storage\Service\FileCreator\FileCreator;
use UXF\Storage\Service\FilesystemFactory;

final class UXFStorageBundle extends AbstractBundle
{
    protected string $extensionAlias = 'uxf_storage';

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
                ->arrayNode('filesystems')
                    ->defaultValue([
                        'default' => 'local://default/%kernel.project_dir%/public/upload',
                    ])
                    ->scalarPrototype()->end()
                ->end()
            ->end();
    }

    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import(__DIR__ . '/../config/services.php');

        $services = $container->services();
        foreach ($config['filesystems'] as $name => $dsn) {
            $services->set(FilesystemOperator::class . " \${$name}Filesystem", Filesystem::class)
                ->factory([FilesystemFactory::class, 'createFilesystem'])
                ->arg('$dsn', $dsn);
        }

        $builder->registerForAutoconfiguration(FileCreator::class)
            ->addTag('uxf.storage.file_creator');
    }

    public function prependExtension(ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        // doctrine
        $container->extension('doctrine', [
            'dbal' => [
                'types' => [
                    'uuid' => UuidType::class,
                ],
            ],
        ]);
    }
}
