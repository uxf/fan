<?php

declare(strict_types=1);

namespace UXF\StorageTests\Integration;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UXF\Storage\Http\Request\FileRequestBody;
use UXF\Storage\Service\StorageFileService;

use function Safe\copy;

class StorageTest extends KernelTestCase
{
    public function testStorageService(): void
    {
        self::bootKernel();

        /** @var StorageFileService $storage */
        $storage = self::getContainer()->get(StorageFileService::class);

        $cacheDir = self::getContainer()->getParameter('kernel.cache_dir');
        assert(is_string($cacheDir));
        $filePath = "$cacheDir/file.png";
        copy(__DIR__ . '/../files/file.png', $filePath);

        $uploadedFile = new UploadedFile($filePath, 'file.png', 'image/png', null, true);
        $file = $storage->createFile($uploadedFile, 'default');

        self::assertSame('png', $file->getExtension());
        self::assertSame('file.png', $file->getName());
        self::assertSame(1, $file->getId());
        self::assertSame('image/png', $file->getType());
        self::assertSame('default', $file->getNamespace());

        $request = self::createImageRequest('test-image.png');
        $file = $storage->createFile($request, 'xxx');

        self::assertSame('png', $file->getExtension());
        self::assertSame('test-image.png', $file->getName());
        self::assertSame(2, $file->getId());
        self::assertSame('image/png', $file->getType());
        self::assertSame('xxx', $file->getNamespace());
        self::assertSame(0755, fileperms(__DIR__ . "/../public/upload/xxx") & 0777);

        $request = self::createTxtRequest('note.txt');
        $file = $storage->createFile($request, 'default');

        self::assertSame('txt', $file->getExtension());
        self::assertSame('note.txt', $file->getName());
        self::assertSame(3, $file->getId());
        self::assertSame('text/plain', $file->getType());
        self::assertSame('default', $file->getNamespace());
    }

    private static function createImageRequest(string $name): FileRequestBody
    {
        return new FileRequestBody(
            'data:image/png;base64,' .
            'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg==',
            'image/png',
            $name,
        );
    }

    private static function createTxtRequest(string $name): FileRequestBody
    {
        return new FileRequestBody(
            'data:text/plain;base64,VGVzdAo=',
            'text/plain',
            $name,
        );
    }
}
