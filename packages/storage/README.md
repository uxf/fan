# UXF Storage

## Install
```
$ composer req uxf/storage
```

## Config

```php
// config/packages/uxf.php
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension('uxf_storage', [
        'filesystems' => [
            'default' => '%env(STORAGE_DSN)%', // default local
        ],
    ]);
};
```

| Storage | DSN                                                       | composer                            |
|---------|-----------------------------------------------------------|-------------------------------------|
| Local   | local://default/%kernel.project_dir%/public/upload        |                                     |
| AWS S3  | aws://key:secret@domain.com/bucket                        | league/flysystem-async-aws-s3       |
| Azure   | azure://accountName:accountKey@core.windows.net/container | league/flysystem-azure-blob-storage |
| S3      | s3://key:secret@domain.com/bucket                         |                                     |
| S3 http | s3://key:secret@domain.com/bucket?schema=http             |                                     |

## Usage

### FileCreator
```php
use League\Flysystem\FilesystemOperator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UXF\Core\SystemProvider\Uuid;
use UXF\Storage\Entity\File;
use UXF\Storage\Entity\Image;
use UXF\Storage\Utils\DestinationDirHelper;
use UXF\Storage\Utils\MimeTypeHelper;

class UploadedFileCreator implements FileCreator
{
    public function __construct(private readonly FilesystemOperator $defaultFilesystem)
    {
    }

    public function createFile(object $file, ?string $namespace): ?File
    {
        if (!$file instanceof UploadedFile) {
            return null;
        }

        $uuid = Uuid::uuid4();

        $type = $file->getClientMimeType();
        $ext = $file->getClientOriginalExtension();
        $ext = $ext !== '' ? $ext : (MimeTypeHelper::convertToExtension($type) ?? 'jpg');
        $size = $file->getSize();
        $raw = $file->getContent();

        $destDir = DestinationDirHelper::getDir($uuid, $namespace);
        $this->defaultFilesystem->write("$destDir/$uuid.$ext", $file->getContent());

        $originalName = $file->getClientOriginalName();

        if (MimeTypeHelper::isImage($type)) {
            $result = getimagesizefromstring($raw);
            assert($result !== false);
            [$width, $height] = $result;
            return new Image($uuid, $ext, $type, $originalName, $namespace, $size, $width ?? 0, $height ?? 0);
        }

        return new File($uuid, $ext, $type, $originalName, $namespace, $size);
    }
}
```
