<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Core\Test\Client;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->public()
        ->autoconfigure()
        ->autowire();

    $services->alias('test.client', Client::class);

    $services->set(Client::class);

    $services->load('UXF\FormTests\DataFixtures\\', __DIR__ . '/../DataFixtures');

    $services->load('UXF\FormTests\FormType\\', __DIR__ . '/../FormType');

    $containerConfigurator->extension('framework', [
        'test' => true,
        'validation' => [
            'email_validation_mode' => 'html5',
        ],
        'http_method_override' => false,
    ]);

    $containerConfigurator->extension('doctrine', [
        'dbal' => [
            'url' => 'sqlite:///%kernel.project_dir%/var/db.sqlite',
        ],
        'orm' => [
            'naming_strategy' => 'doctrine.orm.naming_strategy.underscore_number_aware',
            'auto_mapping' => true,
            'report_fields_where_declared' => true,
            'enable_lazy_ghost_objects' => true,
            'mappings' => [
                'x' => [
                    'type' => 'attribute',
                    'dir' => '%kernel.project_dir%/tests/Entity',
                    'prefix' => 'UXF\FormTests\Entity',
                ],
            ],
        ],
    ]);
};
