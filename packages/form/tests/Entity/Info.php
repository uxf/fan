<?php

declare(strict_types=1);

namespace UXF\FormTests\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Time;

#[ORM\Embeddable]
class Info
{
    #[ORM\Column]
    private int $integer = 1000;

    #[ORM\Column(type: 'float')]
    private float $float = 1000.0;

    #[ORM\Column]
    private string $string = '100';

    #[ORM\Column(type: 'text')]
    private string $text = '100!!!';

    #[ORM\Column(type: Date::class)]
    private Date $date;

    #[ORM\Column(type: DateTime::class)]
    private DateTime $datetime;

    #[ORM\Column(type: Time::class)]
    private Time $time;

    public function __construct()
    {
        $this->date = new Date('2030-01-01');
        $this->datetime = new DateTime('2030-02-02 10:00');
        $this->time = new Time('10:00');
    }

    public function getInteger(): int
    {
        return $this->integer;
    }

    public function setInteger(int $integer): void
    {
        $this->integer = $integer;
    }

    public function getFloat(): float
    {
        return $this->float;
    }

    public function setFloat(float $float): void
    {
        $this->float = $float;
    }

    public function getString(): string
    {
        return $this->string;
    }

    public function setString(string $string): void
    {
        $this->string = $string;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getDate(): Date
    {
        return $this->date;
    }

    public function setDate(Date $date): void
    {
        $this->date = $date;
    }

    public function getDatetime(): DateTime
    {
        return $this->datetime;
    }

    public function setDatetime(DateTime $datetime): void
    {
        $this->datetime = $datetime;
    }

    public function getTime(): Time
    {
        return $this->time;
    }

    public function setTime(Time $time): void
    {
        $this->time = $time;
    }
}
