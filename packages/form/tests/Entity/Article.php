<?php

declare(strict_types=1);

namespace UXF\FormTests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Country;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;

#[ORM\Entity]
class Article
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue]
    private ?int $id = 0;

    #[ORM\Column]
    private int $integer = 0;

    #[ORM\Column]
    private float $float = 0.0;

    #[ORM\Column]
    private string $string;

    #[ORM\Column(type: 'text')]
    private string $text = '';

    #[ORM\Column(type: Date::class)]
    private Date $date;

    #[ORM\Column(type: DateTime::class)]
    private DateTime $datetime;

    #[ORM\Column(type: Time::class)]
    private Time $time;

    #[ORM\Column(type: Email::class, nullable: true)]
    private ?Email $email = null;

    #[ORM\Column(type: Email::class, nullable: true)]
    private ?Phone $phone = null;

    #[ORM\Column(type: BankAccountNumberCze::class, nullable: true)]
    private ?BankAccountNumberCze $ban = null;

    #[ORM\Column(type: NationalIdentificationNumberCze::class, nullable: true)]
    private ?NationalIdentificationNumberCze $nin = null;

    #[ORM\Column(nullable: true)]
    private ?Country $country = null;

    #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
    private Tag $manyToOne;

    /**
     * @var Collection<int, Tag>
     */
    #[ORM\ManyToMany(targetEntity: Tag::class)]
    private Collection $manyToMany;

    /**
     * @var Collection<int, Comment>
     */
    #[ORM\OneToMany(mappedBy: 'article', targetEntity: Comment::class, indexBy: 'id')]
    private Collection $oneToMany;

    #[ORM\OneToOne]
    private ?ArticleUniqInfo $oneToOne = null;

    #[ORM\OneToOne(cascade: ['persist']), ORM\JoinColumn(nullable: false)]
    private ArticleFieldsetInfo $oneToOneFieldset;

    #[ORM\Embedded]
    private Info $info;

    #[ORM\Column]
    private Status $status;

    public function __construct(
        string $string,
        Tag $manyToOne,
        Date $date,
        DateTime $datetime,
        Time $time,
        Status $status,
    ) {
        $this->string = $string;
        $this->date = $date;
        $this->datetime = $datetime;
        $this->time = $time;
        $this->manyToOne = $manyToOne;
        $this->manyToMany = new ArrayCollection();
        $this->oneToMany = new ArrayCollection();
        $this->oneToOneFieldset = new ArticleFieldsetInfo($manyToOne);
        $this->info = new Info();
        $this->status = $status;
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getInteger(): int
    {
        return $this->integer;
    }

    public function setInteger(int $integer): void
    {
        $this->integer = $integer;
    }

    public function getFloat(): float
    {
        return $this->float;
    }

    public function setFloat(float $float): void
    {
        $this->float = $float;
    }

    public function getString(): string
    {
        return $this->string;
    }

    public function setString(string $string): void
    {
        $this->string = $string;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getDate(): Date
    {
        return $this->date;
    }

    public function setDate(Date $date): void
    {
        $this->date = $date;
    }

    public function getDatetime(): DateTime
    {
        return $this->datetime;
    }

    public function setDatetime(DateTime $datetime): void
    {
        $this->datetime = $datetime;
    }

    public function getTime(): Time
    {
        return $this->time;
    }

    public function setTime(Time $time): void
    {
        $this->time = $time;
    }

    public function getEmail(): ?Email
    {
        return $this->email;
    }

    public function setEmail(?Email $email): void
    {
        $this->email = $email;
    }

    public function getPhone(): ?Phone
    {
        return $this->phone;
    }

    public function setPhone(?Phone $phone): void
    {
        $this->phone = $phone;
    }

    public function getBan(): ?BankAccountNumberCze
    {
        return $this->ban;
    }

    public function setBan(?BankAccountNumberCze $ban): void
    {
        $this->ban = $ban;
    }

    public function getNin(): ?NationalIdentificationNumberCze
    {
        return $this->nin;
    }

    public function setNin(?NationalIdentificationNumberCze $nin): void
    {
        $this->nin = $nin;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): void
    {
        $this->country = $country;
    }

    public function getManyToOne(): Tag
    {
        return $this->manyToOne;
    }

    public function setManyToOne(Tag $manyToOne): void
    {
        $this->manyToOne = $manyToOne;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getManyToMany(): Collection
    {
        return $this->manyToMany;
    }

    /**
     * @param Collection<int, Tag> $manyToMany
     */
    public function setManyToMany(Collection $manyToMany): void
    {
        $this->manyToMany = $manyToMany;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getOneToMany(): Collection
    {
        return $this->oneToMany;
    }

    /**
     * @param Collection<int, Comment> $oneToMany
     */
    public function setOneToMany(Collection $oneToMany): void
    {
        $this->oneToMany = $oneToMany;
    }

    public function getOneToOne(): ?ArticleUniqInfo
    {
        return $this->oneToOne;
    }

    public function setOneToOne(?ArticleUniqInfo $oneToOne): void
    {
        $this->oneToOne = $oneToOne;
    }

    public function getOneToOneFieldset(): ArticleFieldsetInfo
    {
        return $this->oneToOneFieldset;
    }

    public function setOneToOneFieldset(ArticleFieldsetInfo $oneToOneFieldset): void
    {
        $this->oneToOneFieldset = $oneToOneFieldset;
    }

    public function getInfo(): Info
    {
        return $this->info;
    }

    public function setInfo(Info $info): void
    {
        $this->info = $info;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): void
    {
        $this->status = $status;
    }
}
