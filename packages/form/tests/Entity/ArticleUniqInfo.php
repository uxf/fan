<?php

declare(strict_types=1);

namespace UXF\FormTests\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class ArticleUniqInfo
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue]
    private int $id = 0;

    #[ORM\Column]
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
