# UXF Form

Symfony Form bundle

## Install

```
$ composer require uxf/form
```

## Config
```php
// config/routes/uxf.php
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return static function (RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->import('@UXFFormBundle/config/routes.php');
};
```

### Usage

`OneToMany` must have `indexBy="id"` and child entity must have property named `$parent` in constructor.
