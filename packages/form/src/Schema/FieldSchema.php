<?php

declare(strict_types=1);

namespace UXF\Form\Schema;

final readonly class FieldSchema
{
    /**
     * @param OptionSchema[]|null $options
     * @param FieldSchema[] $fields
     */
    public function __construct(
        public string $name,
        public string $type,
        public string $label,
        public bool $required,
        public bool $readOnly,
        public bool $editable,
        public ?string $autocomplete,
        public ?array $options,
        public array $fields,
    ) {
    }
}
