<?php

declare(strict_types=1);

namespace UXF\Form\Schema;

final readonly class FormSchema
{
    /**
     * @param FieldSchema[] $fields
     */
    public function __construct(public array $fields)
    {
    }
}
