<?php

declare(strict_types=1);

namespace UXF\Form\Field;

use Closure;

trait LabelFieldTrait
{
    private string $targetField;

    /** @phpstan-var class-string */
    private string $targetClassName;

    private string $autocomplete;

    /** @var (Closure(mixed $item): string)|null */
    private ?Closure $labelCallback = null;

    public function getTargetClassName(): string
    {
        return $this->targetClassName;
    }

    public function getTargetField(): string
    {
        return $this->targetField;
    }

    public function getAutocomplete(): string
    {
        return $this->autocomplete;
    }

    public function getLabelCallback(): ?Closure
    {
        return $this->labelCallback;
    }

    public function setLabelCallback(?Closure $labelCallback): void
    {
        $this->labelCallback = $labelCallback;
    }
}
