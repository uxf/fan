<?php

declare(strict_types=1);

namespace UXF\Form\Field;

interface Fieldset
{
    /**
     * @return array<string, Field>
     */
    public function getFields(): array;

    /**
     * @phpstan-return class-string
     */
    public function getClassName(): string;
}
