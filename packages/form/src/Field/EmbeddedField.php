<?php

declare(strict_types=1);

namespace UXF\Form\Field;

use UXF\Form\Mapper\EmbeddedMapper;

final class EmbeddedField extends Field implements Fieldset
{
    /** @var array<string, Field> */
    private array $fields = [];
    /** @var class-string */
    private string $className;

    /**
     * @param class-string $phpType
     * @param Field[] $fields
     */
    public function __construct(string $name, string $phpType, array $fields)
    {
        parent::__construct($name, $phpType, 'embedded');
        foreach ($fields as $field) {
            $this->fields[$field->getName()] = $field;
        }
        $this->className = $phpType;
    }

    /**
     * @return array<string, Field>
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @inheritDoc
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @param array<string, Field> $fields
     */
    public function setFields(array $fields): void
    {
        $this->fields = $fields;
    }

    public function getDefaultMapperServiceId(): string
    {
        return EmbeddedMapper::SERVICE_ID;
    }
}
