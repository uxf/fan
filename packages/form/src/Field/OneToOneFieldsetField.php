<?php

declare(strict_types=1);

namespace UXF\Form\Field;

use UXF\Form\Mapper\EmbeddedMapper;

final class OneToOneFieldsetField extends Field implements Fieldset
{
    /** @var array<string, Field> */
    private array $fields = [];
    /** @var class-string */
    private string $className;

    /**
     * @param class-string $phpType
     * @param Field[] $fields
     */
    public function __construct(string $name, string $phpType, array $fields)
    {
        parent::__construct($name, $phpType, 'oneToOneFieldset');
        foreach ($fields as $field) {
            $this->fields[$field->getName()] = $field;
        }
        $this->className = $phpType;
    }

    /**
     * @return array<string, Field>
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @inheritDoc
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    public function getDefaultMapperServiceId(): string
    {
        return EmbeddedMapper::SERVICE_ID;
    }
}
