<?php

declare(strict_types=1);

namespace UXF\Form\Field;

use UXF\Form\Mapper\ManyToManyMapper;

/**
 * @implements LabelField<mixed>
 */
final class ManyToManyField extends Field implements LabelField
{
    use LabelFieldTrait;

    /**
     * @phpstan-param class-string $phpType
     */
    public function __construct(string $name, string $phpType, string $targetField, string $autocomplete)
    {
        parent::__construct($name, $phpType, 'manyToMany');
        $this->targetClassName = $phpType;
        $this->targetField = $targetField;
        $this->autocomplete = $autocomplete;
    }

    public function getDefaultMapperServiceId(): string
    {
        return ManyToManyMapper::SERVICE_ID;
    }
}
