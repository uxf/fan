<?php

declare(strict_types=1);

namespace UXF\Form;

use Closure;
use UXF\Form\Field\Field;
use UXF\Form\Field\Fieldset;

/**
 * @template T of object
 */
final readonly class Form implements Fieldset
{
    /**
     * @phpstan-param class-string<T> $className
     * @param array<string, Field> $fields
     * @param Closure(array<string, mixed> $values): T|null $instantiator
     */
    public function __construct(
        public string $className,
        public array $fields,
        public ?Closure $instantiator,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @inheritDoc
     */
    public function getClassName(): string
    {
        return $this->className;
    }
}
