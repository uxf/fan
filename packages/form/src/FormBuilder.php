<?php

declare(strict_types=1);

namespace UXF\Form;

use Closure;
use UXF\Form\Field\EnumField;
use UXF\Form\Field\Field;
use UXF\Form\Field\Fieldset;
use UXF\Form\Field\LabelField;
use UXF\Form\Schema\FieldSchema;
use UXF\Form\Schema\FormSchema;

/**
 * @template T of object
 */
final class FormBuilder
{
    /** @var class-string<T>|null */
    private ?string $className = null;

    /** @var array<string, Field> */
    private array $fields = [];

    /** @var Closure(array<string, mixed> $values): T|null */
    private ?Closure $instantiator = null;

    /**
     * @return class-string<T>|null
     */
    public function getClassName(): ?string
    {
        return $this->className;
    }

    /**
     * @param class-string<T> $className
     */
    public function setClassName(string $className): void
    {
        $this->className = $className;
    }

    /**
     * @return array<string, Field>
     */
    public function getFields(bool $sorted = true): array
    {
        if ($sorted) {
            uasort($this->fields, static function (Field $cA, Field $cb) {
                return $cA->getOrder() <=> $cb->getOrder();
            });
        }

        return $this->fields;
    }

    public function getField(string $name): Field
    {
        return $this->fields[$name];
    }

    /**
     * @return self<T>
     */
    public function removeField(string $name): self
    {
        unset($this->fields[$name]);
        return $this;
    }

    public function addField(Field $field): Field
    {
        return $this->fields[$field->getName()] = $field;
    }

    /**
     * @return Closure(array<string, mixed> $values): T|null
     */
    public function getInstantiator(): ?Closure
    {
        return $this->instantiator;
    }

    /**
     * @param Closure(array<string, mixed> $instantiator): T|null $instantiator
     */
    public function setInstantiator(?Closure $instantiator): void
    {
        $this->instantiator = $instantiator;
    }

    public function createSchema(): FormSchema
    {
        $fields = array_filter($this->fields, static function (Field $field) {
            return !$field->isHidden();
        });

        usort($fields, static function (Field $cA, Field $cB) {
            return $cA->getOrder() <=> $cB->getOrder();
        });

        $schemas = [];
        foreach ($fields as $field) {
            $schemas[] = $this->createFieldSchema($field);
        }

        return new FormSchema($schemas);
    }

    private function createFieldSchema(Field $field): FieldSchema
    {
        $childrenFields = $field instanceof Fieldset
            ? array_values(array_map($this->createFieldSchema(...), $field->getFields()))
            : [];

        return new FieldSchema(
            name: $field->getName(),
            type: $field->getType(),
            label: $field->getLabel(),
            required: $field->isRequired(),
            readOnly: $field->isReadOnly(),
            editable: $field->isEditable(),
            autocomplete: $field instanceof LabelField ? $field->getAutocomplete() : null,
            options: $field instanceof EnumField ? $field->getOptions() : null,
            fields: $childrenFields,
        );
    }
}
