<?php

declare(strict_types=1);

namespace UXF\Form\Http;

final readonly class FormAutocompleteRequestQuery
{
    public function __construct(
        public string $term = '',
    ) {
    }
}
