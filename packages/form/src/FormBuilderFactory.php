<?php

declare(strict_types=1);

namespace UXF\Form;

/**
 * @template T of object
 */
interface FormBuilderFactory
{
    /**
     * @return FormBuilder<T>|null
     */
    public function tryCreateBuilder(string $entityAlias): ?FormBuilder;
}
