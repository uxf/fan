<?php

declare(strict_types=1);

namespace UXF\Form\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use UXF\Form\FormType;

/**
 * @author Jakub Janata <jakubjanata@gmail.com>
 */
final readonly class FormTypePass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        foreach ($container->findTaggedServiceIds('uxf.form.type') as $id => $tag) {
            $definition = $container->getDefinition($id)
                ->setPublic(true)
                ->setAutowired(true);

            $class = $definition->getClass();
            assert(is_string($class) && is_a($class, FormType::class, true));

            $container->setAlias('form.' . $class::getName(), $id)->setPublic(true);
        }
    }
}
