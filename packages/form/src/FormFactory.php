<?php

declare(strict_types=1);

namespace UXF\Form;

use Psr\Container\ContainerInterface;
use UXF\Form\Exception\FormException;
use UXF\Form\Schema\FormSchema;

/**
 * @author Jakub Janata <jakubjanata@gmail.com>
 */
final readonly class FormFactory
{
    /**
     * @param FormBuilderFactory<object> $annotationFormBuilderFactory
     */
    public function __construct(
        private ContainerInterface $container,
        private FormBuilderFactory $annotationFormBuilderFactory,
    ) {
    }

    /**
     * @phpstan-return Form<object>
     */
    public function createForm(string $name): Form
    {
        $builder = $this->createBuilder($name);

        /** @phpstan-var class-string<object>|null $className */
        $className = $builder->getClassName();
        if ($className === null || !class_exists($className)) {
            throw new FormException("Form $name: Missing or invalid \$className '$className'");
        }

        return new Form($className, $builder->getFields(), $builder->getInstantiator());
    }

    public function createSchema(string $name): FormSchema
    {
        return $this->createBuilder($name)->createSchema();
    }

    /**
     * @return FormBuilder<object>
     */
    private function createBuilder(string $name): FormBuilder
    {
        $builder = $this->annotationFormBuilderFactory->tryCreateBuilder($name) ?? new FormBuilder();

        $name = "form.$name";

        if ($this->container->has($name)) {
            /** @var FormType<object> $formType */
            $formType = $this->container->get($name);
            $formType->buildForm($builder);
        }

        return $builder;
    }
}
