<?php

declare(strict_types=1);

namespace UXF\Form\Mapper;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Form\Exception\FormException;
use UXF\Form\Field\Field;
use UXF\Form\Field\Fieldset;
use UXF\Form\Field\OneToManyField;
use UXF\Form\Service\EntityInstantiator;
use UXF\Form\Service\MapperResolver;

final readonly class OneToManyMapper implements Mapper
{
    public const string SERVICE_ID = 'uxf_form.one_to_many_mapper';

    public function __construct(
        private MapperResolver $mapperResolver,
        private PropertyAccessorInterface $propertyAccessor,
        private EntityManagerInterface $entityManager,
        private EntityInstantiator $entityInstantiator,
    ) {
    }

    public function mapToEntityConstructor(Field $field, mixed $value): mixed
    {
        throw new FormException('Unsupported method');
    }

    public function mapToEntity(Field $field, object $entity, mixed $value): void
    {
        if (!$field instanceof OneToManyField) {
            throw new FormException('Unsupported field');
        }

        /// indexed by id
        $originalRecords = $this->propertyAccessor->getValue($entity, $field->getName());
        $finalRecords = new ArrayCollection();
        $usedIds = [];

        foreach ($value as $recordValues) {
            $recordId = $recordValues['id'] ?? null;
            $recordEntity = $recordId !== null ? $originalRecords[$recordId] : null;

            if ($recordEntity === null) {
                // create new record
                $recordEntity = $this->entityInstantiator->instantiate(
                    $field,
                    $recordValues + [
                        'parent' => $entity,
                    ],
                );
                $this->entityManager->persist($recordEntity);
            } else {
                $usedIds[] = $recordId;
            }

            $finalRecords->add($recordEntity);

            foreach ($recordValues as $fieldName => $v) {
                $innerField = $field->getFields()[$fieldName] ?? null;
                if ($innerField === null || $innerField->isReadOnly() || $innerField->isHidden()) {
                    continue;
                }

                $this->mapperResolver->resolveMapper($innerField)->mapToEntity($innerField, $recordEntity, $v);
            }
        }

        // cleanup non-used
        foreach ($originalRecords as $id => $originalRecord) {
            if (!in_array($id, $usedIds, true)) {
                $this->entityManager->remove($originalRecord);
            }
        }

        $this->propertyAccessor->setValue($entity, $field->getName(), $finalRecords);
    }

    public function mapFromEntity(Field $field, object $entity): mixed
    {
        if (!$field instanceof Fieldset) {
            throw new FormException('Unsupported field');
        }

        /// indexed by id
        $originalRecords = $this->propertyAccessor->getValue($entity, $field->getName());

        $result = [];
        foreach ($originalRecords as $originalRecord) {
            $tmp = [];
            foreach ($field->getFields() as $innerField) {
                $value = $this->mapperResolver->resolveMapper($innerField)->mapFromEntity($innerField, $originalRecord);
                $tmp[$innerField->getName()] = $value;
            }
            $result[] = $tmp;
        }
        return $result;
    }
}
