<?php

declare(strict_types=1);

namespace UXF\Form\Mapper;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Form\Exception\FormException;
use UXF\Form\Field\Field;
use UXF\Form\Field\LabelField;

final readonly class ManyToOneMapper implements Mapper
{
    public const string SERVICE_ID = 'uxf_form.many_to_one_mapper';

    public function __construct(
        private PropertyAccessorInterface $propertyAccessor,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function mapToEntityConstructor(Field $field, mixed $value): mixed
    {
        if (!$field instanceof LabelField) {
            throw new FormException('Unsupported field');
        }

        $id = $value['id'] ?? null;
        if ($id !== null) {
            /** @var class-string<object> $targetClassName */
            $targetClassName = $field->getTargetClassName();
            return $this->entityManager->find($targetClassName, $id);
        }

        return null;
    }

    public function mapToEntity(Field $field, object $entity, mixed $value): void
    {
        if (!$field instanceof LabelField) {
            throw new FormException('Unsupported field');
        }

        if ($this->propertyAccessor->isWritable($entity, $field->getName())) {
            $id = $value['id'] ?? null;
            if ($id !== null) {
                /** @var class-string<object> $targetClassName */
                $targetClassName = $field->getTargetClassName();
                $item = $this->entityManager->find($targetClassName, $id);
                $this->propertyAccessor->setValue($entity, $field->getName(), $item);
            } else {
                $this->propertyAccessor->setValue($entity, $field->getName(), null);
            }
        }
    }

    public function mapFromEntity(Field $field, object $entity): mixed
    {
        if (!$field instanceof LabelField) {
            throw new FormException('Unsupported field');
        }

        if ($this->propertyAccessor->isReadable($entity, $field->getName())) {
            $item = $this->propertyAccessor->getValue($entity, $field->getName());
            if ($item !== null) {
                $labelCallback = $field->getLabelCallback();
                return [
                    'id' => $this->propertyAccessor->getValue($item, 'id'),
                    'label' => is_callable($labelCallback) ?
                        $labelCallback($item) :
                        $this->propertyAccessor->getValue($item, $field->getTargetField()),
                ];
            }
        }

        return null;
    }
}
