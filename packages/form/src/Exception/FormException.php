<?php

declare(strict_types=1);

namespace UXF\Form\Exception;

use Exception;
use Throwable;

final class FormException extends Exception
{
    public function __construct(string $message, ?Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }
}
