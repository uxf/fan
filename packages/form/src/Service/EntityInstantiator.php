<?php

declare(strict_types=1);

namespace UXF\Form\Service;

use ReflectionClass;
use UXF\Core\Exception\ValidationException;
use UXF\Form\Exception\FormException;
use UXF\Form\Field\Fieldset;
use UXF\Form\Field\OneToManyField;

final readonly class EntityInstantiator
{
    public function __construct(private MapperResolver $mapperResolver)
    {
    }

    /**
     * @param mixed[] $data
     */
    public function instantiate(Fieldset $fieldset, array $data): mixed
    {
        $reflection = new ReflectionClass($fieldset->getClassName());
        $constructor = $reflection->getConstructor();

        if ($constructor === null) {
            return $reflection->newInstanceWithoutConstructor();
        }

        $fields = $fieldset->getFields();
        $constructorNewParameters = [];

        foreach ($constructor->getParameters() as $constructorParameter) {
            // property name
            $name = $constructorParameter->getName();

            if (!isset($data[$name]) && !$constructorParameter->isOptional() && !$constructorParameter->allowsNull()) {
                throw new ValidationException([[
                    'field' => $name,
                    'message' => 'missing',
                ]]);
            }

            if ($fieldset instanceof OneToManyField && $name === 'parent') {
                $constructorNewParameters[] = $data['parent'];
                continue;
            }

            $field = $fields[$name] ?? null;
            if ($field === null) {
                if (!$constructorParameter->isDefaultValueAvailable()) {
                    throw new FormException('Unknown field');
                }

                $constructorNewParameters[] = $constructorParameter->getDefaultValue();
                continue;
            }

            $value = $this->mapperResolver->resolveMapper($field)->mapToEntityConstructor($field, $data[$name] ?? null);

            // resolve value
            if ($value === null) {
                if (!$constructorParameter->allowsNull()) {
                    throw new ValidationException([[
                        'field' => $name,
                        'message' => 'null value',
                    ]]);
                }
                $constructorNewParameters[] = null;
            } else {
                $constructorNewParameters[] = $value;
            }
        }

        return $reflection->newInstanceArgs($constructorNewParameters);
    }
}
