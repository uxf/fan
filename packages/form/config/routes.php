<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\Form\Controller\FormController;

return static function (RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->add('form_autocomplete', '/api/cms/form/_autocomplete/{name}')
        ->controller([FormController::class, 'autocomplete'])
        ->methods(['GET']);

    $routingConfigurator->add('form_schema', '/api/cms/form/{name}/schema')
        ->controller([FormController::class, 'getSchema'])
        ->methods(['GET']);

    $routingConfigurator->add('form_create', '/api/cms/form/{name}')
        ->controller([FormController::class, 'create'])
        ->methods(['POST']);

    $routingConfigurator->add('form_get', '/api/cms/form/{name}/{id}')
        ->controller([FormController::class, 'get'])
        ->methods(['GET']);

    $routingConfigurator->add('form_update', '/api/cms/form/{name}/{id}')
        ->controller([FormController::class, 'update'])
        ->methods(['PUT']);

    $routingConfigurator->add('form_remove', '/api/cms/form/{name}/{id}')
        ->controller([FormController::class, 'remove'])
        ->methods(['DELETE']);
};
