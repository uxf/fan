<?php

declare(strict_types=1);

namespace UXF\GraphQL\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
final readonly class Query
{
    public function __construct(public string $name)
    {
    }
}
