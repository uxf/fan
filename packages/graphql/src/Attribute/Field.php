<?php

declare(strict_types=1);

namespace UXF\GraphQL\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY | Attribute::TARGET_METHOD | Attribute::TARGET_PARAMETER)]
final readonly class Field
{
    public function __construct(
        public ?string $outputType = null,
        public ?string $inputType = null,
    ) {
    }
}
