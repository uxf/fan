<?php

declare(strict_types=1);

namespace UXF\GraphQL\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
final readonly class Input
{
    public function __construct(
        public string $name = '',
        public ?bool $generateFake = null,
    ) {
    }
}
