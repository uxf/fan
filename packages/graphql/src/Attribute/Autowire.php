<?php

declare(strict_types=1);

namespace UXF\GraphQL\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_PARAMETER)]
final readonly class Autowire
{
}
