<?php

declare(strict_types=1);

namespace UXF\GraphQL\Generator;

final readonly class Method
{
    public function __construct(
        public string $body,
        public string $returnType,
    ) {
    }
}
