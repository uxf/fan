<?php

declare(strict_types=1);

namespace UXF\GraphQL\Generator;

use BackedEnum;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use LogicException;
use Nette\PhpGenerator\Dumper;
use Nette\Utils\Strings;
use Ramsey\Uuid\UuidInterface;
use ReflectionNamedType;
use ReflectionProperty;
use UXF\Core\Attribute\Entity;
use UXF\Core\Attribute\Security;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\Utils\ClassNameHelper;
use UXF\Core\Utils\Lst;
use UXF\Core\Utils\ReflectionHelper;
use UXF\GraphQL\Attribute\Autowire;
use UXF\GraphQL\Attribute\Field;
use UXF\GraphQL\Attribute\Inject;
use UXF\GraphQL\Exception\GraphQLException;
use UXF\GraphQL\Inspector\Schema\InputSpecial;
use UXF\GraphQL\Inspector\Schema\InputTypeFieldSchema;
use UXF\GraphQL\Inspector\Schema\InputTypeSchema;
use UXF\GraphQL\Inspector\Schema\OutputTypeFieldSchema;
use UXF\GraphQL\Inspector\Schema\OutputTypeSchema;
use UXF\GraphQL\Inspector\Schema\ScalarTypeSchema;
use UXF\GraphQL\Inspector\Schema\TypeModifier;
use UXF\GraphQL\Inspector\Schema\TypeSchema;
use UXF\GraphQL\Inspector\Schema\UnionTypeSchema;
use UXF\GraphQL\Plugin\SchemaModifier;
use UXF\GraphQL\Type\Definition\EnumType;
use UXF\GraphQL\Utils\AttributeHelper;

final readonly class TypeGenerator
{
    /**
     * @param array<string, string> $injected
     * @param class-string<SchemaModifier>[] $modifiers
     */
    public function __construct(
        private array $injected,
        private array $modifiers,
        private Dumper $dumper = new Dumper(),
    ) {
    }

    public function generate(TypeSchema $typeSchema): Method
    {
        return match ($typeSchema::class) {
            InputTypeSchema::class => $this->generateInputType($typeSchema),
            OutputTypeSchema::class => $this->generateOutputType($typeSchema),
            UnionTypeSchema::class => $this->generateUnionType($typeSchema),
            ScalarTypeSchema::class => $this->generateScalarType($typeSchema),
            default => throw new LogicException(),
        };
    }

    private function generateInputType(InputTypeSchema $typeSchema): Method
    {
        $body = 'return new \\' . InputObjectType::class . "([\n";
        $body .= "    'name' => '$typeSchema->name',\n";

        // fields start
        $body .= "    'fields' => fn () => [\n";

        /** @var InputTypeFieldSchema $field */
        foreach ($typeSchema->fields as $field) {
            $type = $this->generateInputFieldType($field);
            if ($type === null) {
                continue;
            }

            $fieldBody = "'$field->name' => [\n";
            $fieldBody .= "    'type' => $type,\n";

            if ($field->hasDefaultValue && !$field->defaultValue instanceof NotSet) {
                $fieldBody .= "    'defaultValue' => {$this->dumper->dump($field->defaultValue)},\n";
            }

            $fieldBody .= "],\n";
            $body .= Strings::indent($fieldBody, 8, ' ');
        }

        $body .= "    ],\n";
        // fields end

        // parse
        $output = null;
        foreach ($this->modifiers as $modifier) {
            $output = $modifier::modifyParseValueFn($typeSchema);
            if ($output !== null) {
                break;
            }
        }
        $output ??= "fn (array \$values) => \$this->hydrator(\$values, \\{$typeSchema->phpType}::class)";
        $body .= "    'parseValue' => $output,\n";

        $body .= "]);";

        return new Method($body, InputObjectType::class);
    }

    private function generateOutputType(OutputTypeSchema $typeSchema): Method
    {
        $root = in_array($typeSchema->name, ['Query', 'Mutation'], true);

        $body = 'return new \\' . ObjectType::class . "([\n";
        $body .= "    'name' => '$typeSchema->name',\n";

        // fields start
        $body .= "    'fields' => fn () => [\n";

        foreach ($typeSchema->fields as $field) {
            $fieldBody = "'$field->name' => [\n";
            $fieldBody .= "    'type' => " . $this->generateOutputFieldType($field) . ",\n";
            $fieldBody .= $this->generateArgs($field);

            if ($root) {
                $fieldBody .= $this->generateRootResolveFn($field);
            }

            $fieldBody .= "],\n";
            $body .= Strings::indent($fieldBody, 8, ' ');
        }

        $body .= "    ],\n";
        // fields end

        // resolve fields
        if (!$root) {
            $body .= $this->generateOutputResolveFieldFn($typeSchema);
        }

        $body .= "]);";

        return new Method($body, ObjectType::class);
    }

    private function generateUnionType(UnionTypeSchema $typeSchema): Method
    {
        $types = Lst::from($typeSchema->types)
            ->sort(fn (OutputTypeSchema $a, OutputTypeSchema $b) => $a->name <=> $b->name)
            ->getValues();

        $body = 'return new \\' . UnionType::class . "([\n";
        $body .= "    'name' => '$typeSchema->name',\n";

        $body .= "    'types' => fn () => [\n";
        foreach ($types as $type) {
            $body .= "        \$this->get('{$type->name}'),\n";
        }
        $body .= "    ],\n";

        $body .= "    'resolveType' => fn (\$value) => match (\$value::class) {\n";

        foreach ($types as $type) {
            $body .= "        \\{$type->phpType}::class => \$this->get('{$type->name}'),\n";
        }
        $body .= "        default => throw new \Exception('Unexpected {$typeSchema->name}'),\n";
        $body .= "    },\n";

        $body .= "]);\n";

        return new Method($body, UnionType::class);
    }

    private function generateScalarType(ScalarTypeSchema $typeSchema): Method
    {
        if (enum_exists($typeSchema->phpType)) {
            return new Method(
                'return new \\' . EnumType::class . "(\\$typeSchema->phpType::class);",
                EnumType::class,
            );
        }

        if ($typeSchema->definition === null) {
            $name = lcfirst($typeSchema->name);
            return new Method("return Type::{$name}();", Type::class);
        }

        $body = "return \$this->container->get(\\{$typeSchema->definition->className}::class);";

        return new Method($body, $typeSchema->definition->className);
    }

    public function generateInputFieldType(InputTypeFieldSchema $fieldSchema): ?string
    {
        $typeSchema = $fieldSchema->type;
        $body = "\$this->get('{$typeSchema->name}')";

        $fieldAttribute = $fieldSchema->attribute(Field::class);
        if ($fieldAttribute !== null) {
            if ($fieldAttribute->inputType !== null) {
                $body = "\$this->get('{$fieldAttribute->inputType}')";
            }
        } else {
            if (
                $fieldSchema->attribute(Inject::class) !== null ||
                $fieldSchema->attribute(Autowire::class) !== null
            ) {
                return null;
            }

            $entity = $fieldSchema->attribute(Entity::class);
            if ($entity !== null) {
                $ref = ReflectionHelper::findReflectionProperty($typeSchema->phpType, $entity->property);
                assert($ref?->getType() instanceof ReflectionNamedType);

                $entityType = $ref->getType()->getName();
                if (class_exists($entityType) && ($ref = ReflectionHelper::findReflectionProperty($entityType, 'id')) !== null) {
                    assert($ref->getType() instanceof ReflectionNamedType);
                    $entityType = $ref->getType()->getName();
                }

                $body = match (true) {
                    $entityType === 'int' => 'Type::int()',
                    is_a($entityType, UuidInterface::class, true) => '$this->get(\'UUID\')',
                    is_a($entityType, BackedEnum::class, true) => "\$this->get('" . ClassNameHelper::shortName($entityType) . "')",
                    default => 'Type::string()',
                };
            } elseif ($typeSchema instanceof InputTypeSchema && $typeSchema->special === InputSpecial::Magic) {
                throw new GraphQLException("{$typeSchema->phpType} without #[Input] attribute");
            }
        }

        return self::printType($body, $fieldSchema->modifier);
    }

    public function generateOutputFieldType(OutputTypeFieldSchema $fieldSchema): string
    {
        $fieldAttribute = $fieldSchema->attribute(Field::class);
        if ($fieldAttribute?->outputType !== null) {
            $body = "\$this->get('{$fieldAttribute->outputType}')";
            return self::printType($body, $fieldSchema->modifier);
        }

        return self::printType("\$this->get('{$fieldSchema->type->name}')", $fieldSchema->modifier);
    }

    public function generateArgs(OutputTypeFieldSchema $fieldSchema): string
    {
        if ($fieldSchema->arguments === []) {
            return '';
        }

        $body = '';
        foreach ($fieldSchema->arguments as $argument) {
            $type = $this->generateInputFieldType($argument);
            if ($type === null) {
                continue;
            }

            $innerBody = "'$argument->name' => [\n";
            $innerBody .= "    'type' => $type,\n";

            if ($argument->hasDefaultValue && !$argument->defaultValue instanceof NotSet) {
                $innerBody .= "    'defaultValue' => {$this->dumper->dump($argument->defaultValue)},\n";
            }

            $innerBody .= "],\n";
            $body .= Strings::indent($innerBody, 4, ' ');
        }

        if ($body === '') {
            return '';
        }

        $body = "'args' => [\n$body],\n";

        return Strings::indent($body, 4, ' ');
    }

    public function generateOutputResolveFieldFn(OutputTypeSchema $typeSchema): string
    {
        $valueType = $typeSchema->phpType !== null ? "\\$typeSchema->phpType" : 'mixed';

        $body = "'resolveField' => function ($valueType \$item, array \$args, mixed \$contextValue, \\" . ResolveInfo::class . " \$info): mixed {\n";
        $body .= "    return match (\$info->fieldName) {\n";

        foreach ($typeSchema->fields as $field) {
            if ($field->reflection instanceof ReflectionProperty) {
                $innerBody = "'$field->name' => \$item->$field->name,\n";
            } else {
                $name = $field->reflection->getName();

                if ($field->arguments === []) {
                    $innerBody = "'$field->name' => \$item->{$name}(),\n";
                } else {
                    $innerBody = "'$field->name' => \$item->{$name}(\n";
                    $innerBody .= Strings::indent($this->generateArguments($field), 4, ' ');
                    $innerBody .= "),\n";
                }
            }

            $body .= Strings::indent($innerBody, 8, ' ');
        }

        $body .= "        default => throw new \LogicException(\$info->fieldName),\n";
        $body .= "    };\n";
        $body .= "},\n";

        return Strings::indent($body, 4, ' ');
    }

    public function generateRootResolveFn(OutputTypeFieldSchema $fieldSchema): string
    {
        $rootRef = $fieldSchema->rootReflection ?? throw new LogicException();

        $body = "'resolve' => function (mixed \$rootValue, array \$args, mixed \$contextValue, \\" . ResolveInfo::class . " \$info): mixed {\n";

        $security = AttributeHelper::find($rootRef, Security::class);
        if ($security !== null) {
            $roles = $this->dumper->dump($security->getRoles());
            $body .= "    \$this->container->get('uxf.role_checker')->check(new \\" . Security::class . "($roles));\n";
        }

        $body .= "    \$ctrl = \$this->container->get(\\{$rootRef->getDeclaringClass()->getName()}::class);\n";

        $args = $this->generateArguments($fieldSchema);

        if ($args === '') {
            $body .= "    return \$ctrl();\n";
        } else {
            $body .= "    return \$ctrl(\n";
            $body .= Strings::indent($this->generateArguments($fieldSchema), 8, ' ');
            $body .= "    );\n";
        }

        $body .= "},\n";

        return Strings::indent($body, 4, ' ');
    }

    private function generateArguments(OutputTypeFieldSchema $fieldSchema): string
    {
        $body = '';

        foreach ($fieldSchema->arguments as $argument) {
            if (($entity = $argument->attribute(Entity::class)) !== null) {
                $innerBody = self::printDoctrine($argument->type->phpType, $entity->property, $argument);
            } elseif ($argument->attribute(Inject::class) !== null) {
                $service = $this->injected[$argument->type->phpType] ?? throw new GraphQLException("{$argument->type->phpType} does not have injected service");
                $nullable = $argument->modifier->nullable() ? 'true' : 'false';
                $argInfo = "new InjectedArgument(\\{$argument->type->phpType}::class, $nullable)";
                $innerBody = "\$this->container->get(\\{$service}::class)(\$this->container->get('request_stack')->getMainRequest(), $argInfo)";
            } elseif ($argument->attribute(Autowire::class) !== null) {
                $innerBody = "\$this->container->get(\\{$argument->type->phpType}::class)";
            } else {
                if ($argument->hasDefaultValue) {
                    $fallback = $this->dumper->dump($argument->defaultValue);
                } elseif ($argument->modifier->nullable()) {
                    $fallback = 'null';
                } else {
                    $fallback = "throw new Error('{$argument->name} does not have value')";
                }

                $innerBody = "\$args['{$argument->name}'] ?? $fallback";
            }

            $body .= "{$argument->name}: $innerBody,\n";
        }

        return $body;
    }

    private static function printDoctrine(string $className, string $propertyName, InputTypeFieldSchema $argument): string
    {
        $argumentName = $argument->name;

        return $argument->modifier->array()
            ? "\$this->doctrineArray(\\$className::class, '$propertyName', \$args['{$argumentName}'] ?? null, '$argumentName')"
            : "\$this->doctrine(\\$className::class, '$propertyName', \$args['{$argumentName}'] ?? null, '$argumentName')";
    }
    
    private static function printType(string $body, TypeModifier $modifier): string
    {
        return match ($modifier) {
            TypeModifier::SIMPLE => $body,
            TypeModifier::SIMPLE_NON_NULL => self::nonNull($body),
            TypeModifier::ARRAY => self::listOf($body),
            TypeModifier::ARRAY_NON_NULL => self::nonNull(self::listOf($body)),
            TypeModifier::ARRAY_ITEM_NON_NULL => self::listOf(self::nonNull($body)),
            TypeModifier::ARRAY_AND_ITEM_NON_NULL => self::nonNull(self::listOf(self::nonNull($body))),
        };
    }

    private static function nonNull(string $inner): string
    {
        return "Type::nonNull($inner)";
    }

    private static function listOf(string $inner): string
    {
        return "Type::listOf($inner)";
    }
}
