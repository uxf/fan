<?php

declare(strict_types=1);

namespace UXF\GraphQL\Generator;

use GraphQL\Error\Error;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\NullableType;
use GraphQL\Type\Definition\Type;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\PsrPrinter;
use Nette\Utils\FileSystem;
use Psr\Container\ContainerInterface;
use UXF\Core\Exception\CoreException;
use UXF\Core\Exception\ValidationException;
use UXF\GraphQL\Inspector\Schema\TypeSchema;
use UXF\GraphQL\Inspector\TypeMap;
use UXF\GraphQL\Service\Injector\InjectedArgument;
use UXF\GraphQL\Type\TypeRegistryInterface;
use UXF\Hydrator\Exception\HydratorException;
use UXF\Hydrator\ObjectHydrator;

final readonly class TypeRegistryGenerator
{
    public function __construct(
        private string $namespace,
        private string $destinationDir,
        private TypeGenerator $typeGenerator,
    ) {
    }

    public function generateFile(TypeMap $typeMap): void
    {
        $class = new ClassType('TypeRegistry');
        $class->addImplement(TypeRegistryInterface::class)->setFinal();
        $class->addProperty('types')
            ->setType('array')
            ->setValue([])
            ->setPrivate()
            ->setComment('@var array<string, Type & NullableType>');

        $construct = $class->addMethod('__construct');
        $construct->addPromotedParameter('container')
            ->setType(ContainerInterface::class)
            ->setReadOnly()
            ->setPrivate();
        $construct->addPromotedParameter('hydrator')
            ->setType(ObjectHydrator::class)
            ->setReadOnly()
            ->setPrivate();

        $castMethod = $class->addMethod('get')
            ->setReturnType('?object')
            ->setBody('return $this->types[$name] ??= method_exists($this, $name) ? $this->{$name}() : null;');
        $castMethod->addParameter('name')
            ->setType('string');

        $this->generateHydratorHelper($class);
        $this->generateDoctrineHelper($class);

        foreach ($typeMap->scalars as $typeSchema) {
            $this->generateTypeMethod($class, $typeSchema);
        }
        foreach ($typeMap->outputs as $typeSchema) {
            if ($typeSchema->fields === []) {
                continue;
            }
            $this->generateTypeMethod($class, $typeSchema);
        }
        foreach ($typeMap->unions as $typeSchema) {
            $this->generateTypeMethod($class, $typeSchema);
        }
        foreach ($typeMap->inputs as $typeSchema) {
            $this->generateTypeMethod($class, $typeSchema);
        }

        $namespace = new PhpNamespace($this->namespace);
        $namespace->add($class);
        $namespace->addUse(InjectedArgument::class);
        $namespace->addUse(TypeRegistryInterface::class);
        $namespace->addUse(ContainerInterface::class);
        $namespace->addUse(ObjectHydrator::class);
        $namespace->addUse(HydratorException::class);
        $namespace->addUse(ValidationException::class);
        $namespace->addUse(CoreException::class);
        $namespace->addUse(Error::class);
        $namespace->addUse(UserError::class);
        $namespace->addUse(Type::class);
        $namespace->addUse(NullableType::class);

        $file = new PhpFile();
        $file->setStrictTypes();
        $file->addNamespace($namespace);

        $printer = new PsrPrinter();
        $output = $printer->printFile($file);

        $typeCasterName = $class->getName();
        FileSystem::createDir($this->destinationDir);
        FileSystem::write($this->destinationDir . "/$typeCasterName.php", $output);
    }

    private function generateTypeMethod(ClassType $class, TypeSchema $typeSchema): void
    {
        $method = $this->typeGenerator->generate($typeSchema);

        $class->addMethod($typeSchema->name())
            ->setPrivate()
            ->setReturnType($method->returnType)
            ->setBody($method->body);
    }

    private function generateHydratorHelper(ClassType $class): void
    {
        $method = $class->addMethod('hydrator');
        $method->addParameter('data')->setType('array');
        $method->addParameter('class')->setType('string');
        $method->setReturnType('object');
        $method->setBody('try {
    $result = $this->hydrator->hydrateArray($data, $class);
} catch (HydratorException $e) {
    throw new Error(previous: ValidationException::fromHydratorErrors($e->errors));
} catch (CoreException $e) {
    throw new Error(previous: $e);
}

if ($this->container->has(\'validator\')) {
    $e = ValidationException::fromValidator($this->container->get(\'validator\'), $result);
    if ($e !== null) {
        throw new Error(previous: $e);
    }
}

return $result;
');
    }

    private function generateDoctrineHelper(ClassType $class): void
    {
        $single = $class->addMethod('doctrine');
        $single->addParameter('className')->setType('string');
        $single->addParameter('propertyName')->setType('string');
        $single->addParameter('value')->setType('mixed');
        $single->addParameter('arg')->setType('string');
        $single->setReturnType('?object');
        $single->setBody('if ($value === null) {
    return null;
}

return $this->container->get(\'doctrine\')->getManager()->getRepository($className)->findOneBy([$propertyName => $value]) ?? throw new UserError("$arg:$value not found.");');

        $multi = $class->addMethod('doctrineArray');
        $multi->addParameter('className')->setType('string');
        $multi->addParameter('propertyName')->setType('string');
        $multi->addParameter('value')->setType('?array');
        $multi->addParameter('arg')->setType('string');
        $multi->setReturnType('?array');
        $multi->setBody('    if ($value === null) {
    return null;
}
if ($value === []) {
    return [];
}
$result = [];
$missing = [];
foreach ($value as $v) {
    $result[] = $entity = $this->container->get(\'doctrine\')->getManager()->getRepository($className)->findOneBy([$propertyName => $v]);
    if ($entity === null) {
        $missing[] = $v;
    }
}
if ($missing !== []) {
    throw new UserError("$arg:" . implode(\',\', $missing). " not found.");
}
return $result;');
    }
}
