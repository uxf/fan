<?php

declare(strict_types=1);

namespace UXF\GraphQL\Controller;

use GraphQL\Executor\ExecutionResult;
use GraphQL\Server\Helper;
use GraphQL\Server\ServerConfig;
use LogicException;
use Nette\Utils\Json;
use Psr\EventDispatcher\EventDispatcherInterface;
use RuntimeException;
use Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UXF\GraphQL\EventSubscriber\OperationPreExecutionEvent;
use UXF\GraphQL\Service\ResponseCallbackModifier;

final readonly class GQLController
{
    public function __construct(
        private ServerConfig $serverConfig,
        private HttpMessageFactoryInterface $httpMessageFactory,
        private ResponseCallbackModifier $responseModifier,
        private EventDispatcherInterface $eventDispatcher,
    ) {
    }

    public function __invoke(Request $request): Response
    {
        $psr7Request = $this->httpMessageFactory->createRequest($request);

        if ($request->getMethod() === 'POST') {
            $parsedBody = Json::decode($psr7Request->getBody()->getContents(), forceArrays: true);
            $psr7Request = $psr7Request->withParsedBody($parsedBody);
        }

        $helper = new Helper();
        $operation = $helper->parsePsrRequest($psr7Request);
        if (is_array($operation)) {
            throw new LogicException('Batch is not supported');
        }

        $this->eventDispatcher->dispatch(new OperationPreExecutionEvent($operation));
        $result = $helper->executeOperation($this->serverConfig, $operation);

        if ($result instanceof ExecutionResult) {
            $response = new JsonResponse();
            $response->headers->set('Content-Type', 'application/json; charset=utf-8');
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            $response->setData($result->toArray($this->serverConfig->getDebugFlag()));

            $this->responseModifier->modify($response);

            return $response;
        }

        throw new RuntimeException('Only ExecutionResult is supported');
    }
}
