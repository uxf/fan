<?php

declare(strict_types=1);

namespace UXF\GraphQL\Command;

use GraphQL\Type\Schema;
use GraphQL\Utils\SchemaPrinter;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use UXF\GraphQL\FakeGenerator\FakeConfig;
use UXF\GraphQL\FakeGenerator\FakeGenerator;
use function Safe\file_put_contents;

#[AsCommand(name: 'uxf:gql-gen')]
final class GenCommand extends Command
{
    /**
     * @param string[] $destinations
     */
    public function __construct(
        private readonly array $destinations,
        private readonly FakeConfig $fakeConfig,
        private readonly FakeGenerator $fakeGenerator,
        private readonly Schema $schema,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->schema->assertValid();

        $out = SchemaPrinter::doPrint($this->schema, [
            'sortTypes' => true,
        ]);

        foreach ($this->destinations as $destination) {
            (new Filesystem())->mkdir(pathinfo($destination, PATHINFO_DIRNAME));
            file_put_contents($destination, $out);
        }

        $this->fakeGenerator->generateFiles($this->fakeConfig);

        return 0;
    }
}
