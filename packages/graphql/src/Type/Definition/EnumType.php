<?php

declare(strict_types=1);

namespace UXF\GraphQL\Type\Definition;

use BackedEnum;
use GraphQL\Error\InvariantViolation;
use GraphQL\Type\Definition\EnumType as GraphQLEnumType;
use GraphQL\Utils\Utils;
use UXF\Core\Utils\ClassNameHelper;

final class EnumType extends GraphQLEnumType
{
    /**
     * @param class-string<BackedEnum> $enumClassName
     */
    public function __construct(string $enumClassName)
    {
        $values = [];

        /** @var BackedEnum $case */
        foreach ($enumClassName::cases() as $case) {
            $values[$this->serialize($case)] = [
                'value' => $case,
            ];
        }

        parent::__construct([
            'name' => ClassNameHelper::shortName($enumClassName),
            'values' => $values,
        ]);
    }

    public function serialize(mixed $value): string
    {
        if (!$value instanceof BackedEnum) {
            throw new InvariantViolation($this->name . ' is not an instance of BackedEnum: ' . Utils::printSafe($value));
        }

        return is_string($value->value) ? $value->value : $value->name;
    }
}
