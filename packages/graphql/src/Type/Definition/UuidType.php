<?php

declare(strict_types=1);

namespace UXF\GraphQL\Type\Definition;

use Exception;
use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Error\UserError;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class UuidType extends ScalarType
{
    public const string NAME = 'UUID';

    public function __construct()
    {
        parent::__construct([
            'name' => self::NAME,
        ]);
    }

    public function serialize(mixed $value): string
    {
        if (!$value instanceof UuidInterface) {
            throw new InvariantViolation('UUID is not an instance of UuidInterface: ' . Utils::printSafe($value));
        }

        return $value->__toString();
    }

    public function parseValue(mixed $value): ?UuidInterface
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof UuidInterface) {
            return $value;
        }

        try {
            return Uuid::fromString($value);
        } catch (Exception) {
            throw new UserError('Invalid uuid format');
        }
    }

    /**
     * @inheritDoc
     */
    public function parseLiteral($valueNode, ?array $variables = null): mixed
    {
        if ($valueNode instanceof StringValueNode) {
            return $valueNode->value;
        }

        throw new Error();
    }
}
