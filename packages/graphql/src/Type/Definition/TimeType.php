<?php

declare(strict_types=1);

namespace UXF\GraphQL\Type\Definition;

use Exception;
use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Error\UserError;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;
use UXF\Core\Type\Time;

final class TimeType extends ScalarType
{
    public const string NAME = 'Time';

    public function __construct()
    {
        parent::__construct([
            'name' => self::NAME,
        ]);
    }

    public function serialize(mixed $value): string
    {
        if (!$value instanceof Time) {
            throw new InvariantViolation('DateTime is not an instance of DateTimeImmutable: ' . Utils::printSafe($value));
        }

        return $value->__toString();
    }

    public function parseValue(mixed $value): ?Time
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof Time) {
            return $value;
        }

        try {
            return new Time($value);
        } catch (Exception) {
            throw new UserError('Invalid time format');
        }
    }

    /**
     * @inheritDoc
     */
    public function parseLiteral($valueNode, ?array $variables = null): mixed
    {
        if ($valueNode instanceof StringValueNode) {
            return $valueNode->value;
        }

        throw new Error();
    }
}
