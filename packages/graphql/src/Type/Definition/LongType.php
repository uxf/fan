<?php

declare(strict_types=1);

namespace UXF\GraphQL\Type\Definition;

use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Error\UserError;
use GraphQL\Language\AST\IntValueNode;
use GraphQL\Language\Printer;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;

final class LongType extends ScalarType
{
    public const string NAME = 'Long';

    public function __construct()
    {
        parent::__construct([
            'name' => self::NAME,
        ]);
    }

    public function serialize(mixed $value): int
    {
        if (!is_int($value)) {
            throw new InvariantViolation('Long is not type of int: ' . Utils::printSafe($value));
        }

        return $value;
    }

    public function parseValue(mixed $value): ?int
    {
        if ($value === null) {
            return null;
        }

        if (is_int($value)) {
            return $value;
        }

        if (!is_numeric($value)) {
            throw new UserError('Invalid long format');
        }

        return (int) $value;
    }

    /**
     * @inheritDoc
     */
    public function parseLiteral($valueNode, ?array $variables = null): int
    {
        if ($valueNode instanceof IntValueNode) {
            $val = (int) $valueNode->value;
            if ($valueNode->value === (string) $val) {
                return $val;
            }
        }

        $notInt = Printer::doPrint($valueNode);
        throw new Error("Long cannot represent non-integer value: {$notInt}", $valueNode);
    }
}
