<?php

declare(strict_types=1);

namespace UXF\GraphQL\Type\Definition;

use Exception;
use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Error\UserError;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;
use UXF\Core\Type\Decimal;

final class DecimalType extends ScalarType
{
    public const string NAME = 'Decimal';

    public function __construct()
    {
        parent::__construct([
            'name' => self::NAME,
        ]);
    }

    public function serialize(mixed $value): string
    {
        if (!$value instanceof Decimal) {
            throw new InvariantViolation('Decimal is not an instance of Decimal: ' . Utils::printSafe($value));
        }

        return $value->__toString();
    }

    public function parseValue(mixed $value): ?Decimal
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof Decimal) {
            return $value;
        }

        try {
            return Decimal::of($value);
        } catch (Exception) {
            throw new UserError('Invalid decimal format');
        }
    }

    /**
     * @inheritDoc
     */
    public function parseLiteral($valueNode, ?array $variables = null): mixed
    {
        if ($valueNode instanceof StringValueNode) {
            return $valueNode->value;
        }

        throw new Error();
    }
}
