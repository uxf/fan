<?php

declare(strict_types=1);

namespace UXF\GraphQL\Type\Definition;

use Exception;
use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Error\UserError;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;
use UXF\Core\Type\BankAccountNumberCze;

final class BanCzeType extends ScalarType
{
    public const string NAME = 'BanCze';

    public function __construct()
    {
        parent::__construct([
            'name' => self::NAME,
        ]);
    }

    public function serialize(mixed $value): string
    {
        if (!$value instanceof BankAccountNumberCze) {
            throw new InvariantViolation('BanCze is not an instance of BankAccountNumberCze: ' . Utils::printSafe($value));
        }

        return $value->__toString();
    }

    public function parseValue(mixed $value): ?BankAccountNumberCze
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof BankAccountNumberCze) {
            return $value;
        }

        try {
            return BankAccountNumberCze::of($value);
        } catch (Exception) {
            throw new UserError('Invalid bank account number format');
        }
    }

    /**
     * @inheritDoc
     */
    public function parseLiteral($valueNode, ?array $variables = null): mixed
    {
        if ($valueNode instanceof StringValueNode) {
            return $valueNode->value;
        }

        throw new Error();
    }
}
