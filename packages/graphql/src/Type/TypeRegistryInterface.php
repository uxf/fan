<?php

declare(strict_types=1);

namespace UXF\GraphQL\Type;

interface TypeRegistryInterface
{
    public function get(string $name): ?object;
}
