<?php

declare(strict_types=1);

namespace UXF\GraphQL\Type;

use JsonSerializable;

/**
 * @final
 */
readonly class Json implements JsonSerializable
{
    public function __construct(public mixed $content)
    {
    }

    public function jsonSerialize(): mixed
    {
        return $this->content;
    }
}
