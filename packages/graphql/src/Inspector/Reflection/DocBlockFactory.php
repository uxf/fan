<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Reflection;

use phpDocumentor\Reflection\DocBlock;
use phpDocumentor\Reflection\DocBlockFactory as DocBlockFactoryConcrete;
use phpDocumentor\Reflection\DocBlockFactoryInterface;
use phpDocumentor\Reflection\Types\Context;
use phpDocumentor\Reflection\Types\ContextFactory;
use ReflectionMethod;

final readonly class DocBlockFactory
{
    public function __construct(
        private DocBlockFactoryInterface $docBlockFactory,
        private ContextFactory $contextFactory,
    ) {
    }

    public static function default(): self
    {
        return new self(
            DocBlockFactoryConcrete::createInstance(),
            new ContextFactory(),
        );
    }

    public function create(ReflectionMethod $reflectionMethod, ?Context $context = null): DocBlock
    {
        $docBlock = $reflectionMethod->getDocComment();
        $docBlock = $docBlock !== false ? $docBlock : '/** */';

        return $this->docBlockFactory->create(
            $docBlock,
            $context ?? $this->createContext($reflectionMethod),
        );
    }

    public function createContext(ReflectionMethod $reflectionMethod): Context
    {
        return $this->contextFactory->createFromReflector($reflectionMethod);
    }
}
