<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Schema;

final class OutputTypeSchema implements TypeSchema
{
    /**
     * @param array<string, OutputTypeFieldSchema> $fields
     */
    public function __construct(
        public string $name,
        public ?string $phpType,
        public array $fields = [],
    ) {
    }

    public function name(): string
    {
        return $this->name;
    }

    public function addField(OutputTypeFieldSchema $field): OutputTypeFieldSchema
    {
        return $this->fields[$field->name] = $field;
    }
}
