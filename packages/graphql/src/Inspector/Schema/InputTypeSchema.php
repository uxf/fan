<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Schema;

final class InputTypeSchema implements TypeSchema
{
    /**
     * @param array<string, InputTypeFieldSchema> $fields
     */
    public function __construct(
        public string $name,
        public string $phpType,
        public array $fields = [],
        public ?InputSpecial $special = null,
    ) {
    }

    public function name(): string
    {
        return $this->name;
    }

    public function addField(InputTypeFieldSchema $field): InputTypeFieldSchema
    {
        return $this->fields[$field->name] = $field;
    }
}
