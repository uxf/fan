<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Schema;

use UXF\Core\Utils\Lst;
use UXF\GraphQL\Exception\GraphQLException;

final class UnionTypeSchema implements TypeSchema
{
    /**
     * @param OutputTypeSchema[] $types
     */
    public function __construct(
        public string $name,
        public array $types,
    ) {
    }

    /**
     * @param (OutputTypeSchema|ScalarTypeSchema)[] $types
     */
    public static function tryCreate(string $name, array $types): self
    {
        foreach ($types as $type) {
            if (!$type instanceof OutputTypeSchema) {
                throw new GraphQLException("Union $name contains scalar type {$type->name}");
            }
        }

        return new self($name, $types); // @phpstan-ignore argument.type
    }

    public function name(): string
    {
        return $this->name;
    }

    /**
     * @param (OutputTypeSchema|ScalarTypeSchema)[] $types
     */
    public function check(array $types): void
    {
        $other = self::tryCreate('', $types);

        $selfTypes = Lst::from($this->types)->map(fn (OutputTypeSchema $t) => $t->phpType)->sort()->getValues();
        $otherTypes = Lst::from($other->types)->map(fn (OutputTypeSchema $t) => $t->phpType)->sort()->getValues();

        if ($selfTypes !== $otherTypes) {
            throw new GraphQLException(
                "Union {$this->name} type mismatch:\n" .
                implode(', ', $selfTypes) . "\n" .
                "vs\n" .
                implode(', ', $otherTypes),
            );
        }
    }
}
