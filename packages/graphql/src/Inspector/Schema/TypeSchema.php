<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Schema;

interface TypeSchema
{
    public function name(): string;
}
