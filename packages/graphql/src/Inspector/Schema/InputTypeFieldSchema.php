<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Schema;

final readonly class InputTypeFieldSchema
{
    /**
     * @param array<class-string, object> $attributes
     */
    public function __construct(
        public string $name,
        public InputTypeSchema | ScalarTypeSchema $type,
        public TypeModifier $modifier,
        public bool $hasDefaultValue,
        public mixed $defaultValue,
        public array $attributes = [],
    ) {
    }

    /**
     * @template T of object
     * @param class-string<T> $name
     * @return T|null
     */
    public function attribute(string $name): ?object
    {
        $attribute = $this->attributes[$name] ?? null;
        assert($attribute === null || $attribute instanceof $name);
        return $attribute;
    }
}
