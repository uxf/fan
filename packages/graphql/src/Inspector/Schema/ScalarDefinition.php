<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Schema;

final readonly class ScalarDefinition
{
    /**
     * @param class-string $className
     */
    public function __construct(
        public string $className,
        public ?string $generatedFakeType = null,
    ) {
    }
}
