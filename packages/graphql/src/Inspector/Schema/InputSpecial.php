<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Schema;

enum InputSpecial
{
    case Magic;
    case GenerateFake;
    case NotGenerateFake;
}
