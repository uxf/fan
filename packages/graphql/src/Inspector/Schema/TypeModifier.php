<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Schema;

enum TypeModifier
{
    case SIMPLE;
    case SIMPLE_NON_NULL;
    case ARRAY;
    case ARRAY_NON_NULL;
    case ARRAY_ITEM_NON_NULL;
    case ARRAY_AND_ITEM_NON_NULL;

    public static function create(bool $isList, bool $nullable, bool $itemsNullable): self
    {
        return match ($isList) {
            true => match ($nullable) {
                true => match ($itemsNullable) {
                    true => self::ARRAY,
                    false => self::ARRAY_ITEM_NON_NULL,
                },
                false => match ($itemsNullable) {
                    true => self::ARRAY_NON_NULL,
                    false => self::ARRAY_AND_ITEM_NON_NULL,
                },
            },
            false => match ($nullable) {
                true => self::SIMPLE,
                false => self::SIMPLE_NON_NULL,
            },
        };
    }

    public function array(): bool
    {
        return match ($this) {
            self::SIMPLE => false,
            self::SIMPLE_NON_NULL => false,
            self::ARRAY => true,
            self::ARRAY_NON_NULL => true,
            self::ARRAY_ITEM_NON_NULL => true,
            self::ARRAY_AND_ITEM_NON_NULL => true,
        };
    }

    public function nullable(): bool
    {
        return match ($this) {
            self::SIMPLE => true,
            self::SIMPLE_NON_NULL => false,
            self::ARRAY => true,
            self::ARRAY_NON_NULL => false,
            self::ARRAY_ITEM_NON_NULL => true,
            self::ARRAY_AND_ITEM_NON_NULL => false,
        };
    }
}
