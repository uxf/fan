<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Schema;

final class ScalarTypeSchema implements TypeSchema
{
    public function __construct(
        public string $name,
        public string $phpType,
        public ?ScalarDefinition $definition = null,
    ) {
    }

    public function name(): string
    {
        return $this->name;
    }
}
