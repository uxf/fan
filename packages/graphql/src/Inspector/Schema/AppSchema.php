<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Schema;

use UXF\GraphQL\Inspector\TypeMap;

final readonly class AppSchema
{
    /**
     * @param string[] $controllers
     */
    public function __construct(
        public TypeMap $typeMap,
        public array $controllers,
    ) {
    }
}
