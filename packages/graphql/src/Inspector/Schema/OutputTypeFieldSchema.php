<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector\Schema;

use ReflectionMethod;
use ReflectionProperty;

final class OutputTypeFieldSchema
{
    /**
     * @param array<class-string, object> $attributes
     * @param array<string, InputTypeFieldSchema> $arguments
     */
    public function __construct(
        public string $name,
        public OutputTypeSchema|UnionTypeSchema|ScalarTypeSchema $type,
        public TypeModifier $modifier,
        public ReflectionMethod|ReflectionProperty $reflection,
        public ?ReflectionMethod $rootReflection = null,
        public array $attributes = [],
        public array $arguments = [],
    ) {
    }

    public function addArgument(InputTypeFieldSchema $argument): InputTypeFieldSchema
    {
        return $this->arguments[$argument->name] = $argument;
    }

    /**
     * @template T of object
     * @param class-string<T> $name
     * @return T|null
     */
    public function attribute(string $name): ?object
    {
        $attribute = $this->attributes[$name] ?? null;
        assert($attribute === null || $attribute instanceof $name);
        return $attribute;
    }
}
