<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector;

use LogicException;
use Ramsey\Uuid\UuidInterface;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\Core\Utils\ClassNameHelper;
use UXF\GraphQL\Inspector\Schema\InputTypeSchema;
use UXF\GraphQL\Inspector\Schema\OutputTypeSchema;
use UXF\GraphQL\Inspector\Schema\ScalarDefinition;
use UXF\GraphQL\Inspector\Schema\ScalarTypeSchema;
use UXF\GraphQL\Inspector\Schema\TypeSchema;
use UXF\GraphQL\Inspector\Schema\UnionTypeSchema;
use UXF\GraphQL\Plugin\SchemaModifier;
use UXF\GraphQL\Type\Definition\BanCzeType;
use UXF\GraphQL\Type\Definition\DateTimeType;
use UXF\GraphQL\Type\Definition\DateType;
use UXF\GraphQL\Type\Definition\DecimalType;
use UXF\GraphQL\Type\Definition\EmailType;
use UXF\GraphQL\Type\Definition\JsonType;
use UXF\GraphQL\Type\Definition\LongType;
use UXF\GraphQL\Type\Definition\NinCzeType;
use UXF\GraphQL\Type\Definition\PhoneType;
use UXF\GraphQL\Type\Definition\TimeType;
use UXF\GraphQL\Type\Definition\UrlType;
use UXF\GraphQL\Type\Definition\UuidType;
use UXF\GraphQL\Type\Json;

// indexed primary by php type
final class TypeMap
{
    /** @var array<string, InputTypeSchema> */
    public array $inputs = [];
    /** @var array<string, OutputTypeSchema> */
    public array $outputs;
    /** @var array<string, UnionTypeSchema> */
    public array $unions = [];
    /** @var array<string, ScalarTypeSchema> */
    public array $scalars;
    /** @var array<string, string> */
    public array $scalarAliases;

    /**
     * @param class-string<SchemaModifier>[] $modifiers
     */
    public function __construct(array $modifiers)
    {
        $this->scalars = [
            'int' => new ScalarTypeSchema('Int', 'int'),
            'float' => new ScalarTypeSchema('Float', 'float'),
            'string' => new ScalarTypeSchema('String', 'string'),
            'bool' => new ScalarTypeSchema('Boolean', 'bool'),
            BankAccountNumberCze::class => new ScalarTypeSchema('BanCze', BankAccountNumberCze::class, new ScalarDefinition(BanCzeType::class)),
            DateTime::class => new ScalarTypeSchema('DateTime', DateTime::class, new ScalarDefinition(DateTimeType::class)),
            Date::class => new ScalarTypeSchema('Date', Date::class, new ScalarDefinition(DateType::class)),
            Decimal::class => new ScalarTypeSchema('Decimal', Decimal::class, new ScalarDefinition(DecimalType::class)),
            Email::class => new ScalarTypeSchema('Email', Email::class, new ScalarDefinition(EmailType::class)),
            Json::class => new ScalarTypeSchema('JSON', Json::class, new ScalarDefinition(JsonType::class)),
            'Long' => new ScalarTypeSchema('Long', 'int', new ScalarDefinition(LongType::class)),
            NationalIdentificationNumberCze::class => new ScalarTypeSchema('NinCze', NationalIdentificationNumberCze::class, new ScalarDefinition(NinCzeType::class)),
            Phone::class => new ScalarTypeSchema('Phone', Phone::class, new ScalarDefinition(PhoneType::class)),
            Time::class => new ScalarTypeSchema('Time', Time::class, new ScalarDefinition(TimeType::class)),
            Url::class => new ScalarTypeSchema('Url', Url::class, new ScalarDefinition(UrlType::class)),
            UuidInterface::class => new ScalarTypeSchema('UUID', UuidInterface::class, new ScalarDefinition(UuidType::class)),
        ];

        $this->scalarAliases = [
            'true' => 'bool',
            'false' => 'bool',
        ];

        $this->outputs = [
            'Query' => new OutputTypeSchema('Query', null),
            'Mutation' => new OutputTypeSchema('Mutation', null),
        ];

        foreach ($modifiers as $modifier) {
            $modifier::modifyTypeMap($this);
        }
    }

    /**
     * @param class-string<SchemaModifier>[] $modifiers
     */
    public static function create(array $modifiers): self
    {
        usort($modifiers, static fn ($a, $b) => $b::getDefaultPriority() <=> $a::getDefaultPriority());

        return new self($modifiers);
    }

    /**
     * @template T of TypeSchema
     * @param T $type
     * @return T
     */
    public function add(TypeSchema $type): TypeSchema
    {
        if ($type instanceof InputTypeSchema) {
            if (isset($this->inputs[$type->phpType])) {
                throw new LogicException($type::class . " {$type->phpType} already added");
            }
            $this->inputs[$type->phpType] = $type;
        } elseif ($type instanceof OutputTypeSchema) {
            if (isset($this->outputs[$type->phpType])) {
                throw new LogicException($type::class . " {$type->phpType} already added");
            }
            $this->outputs[$type->phpType] = $type;
        } elseif ($type instanceof UnionTypeSchema) {
            if (isset($this->unions[$type->name])) {
                throw new LogicException($type::class . " {$type->name} already added");
            }
            $this->unions[$type->name] = $type;
        } elseif ($type instanceof ScalarTypeSchema) {
            if (isset($this->scalars[$type->phpType])) {
                throw new LogicException($type::class . " {$type->phpType} already added");
            }
            $this->scalars[$type->phpType] = $type;
        } else {
            throw new LogicException();
        }

        return $type;
    }

    public function getInput(string $name): InputTypeSchema | ScalarTypeSchema | null
    {
        return $this->getScalar($name) ?? $this->inputs[$name] ?? null;
    }

    public function getOutput(string $name): OutputTypeSchema | ScalarTypeSchema | null
    {
        return $this->getScalar($name) ?? $this->outputs[$name] ?? null;
    }

    public function getUnion(string $name): ?UnionTypeSchema
    {
        return $this->unions[$name] ?? null;
    }

    public function getScalar(string $name): ?ScalarTypeSchema
    {
        if (isset($this->scalarAliases[$name])) {
            return $this->scalars[$this->scalarAliases[$name]];
        }

        $scalar = $this->scalars[$name] ?? null;

        if ($scalar === null && enum_exists($name)) {
            $scalar = $this->add(new ScalarTypeSchema(ClassNameHelper::shortName($name), $name));
        }

        return $scalar;
    }
}
