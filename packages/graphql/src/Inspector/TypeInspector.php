<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector;

use BackedEnum;
use LogicException;
use Nette\Utils\Strings;
use phpDocumentor\Reflection\DocBlock\Tags\Param;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use phpDocumentor\Reflection\Type as PhpDocType;
use phpDocumentor\Reflection\Types\Array_;
use phpDocumentor\Reflection\Types\Compound;
use phpDocumentor\Reflection\Types\Null_;
use ReflectionClass;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionProperty;
use ReflectionType;
use ReflectionUnionType;
use UXF\Core\Attribute\Entity;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\Utils\ClassNameHelper;
use UXF\Core\Utils\Lst;
use UXF\Core\Utils\ReflectionHelper;
use UXF\GraphQL\Attribute\Ignore;
use UXF\GraphQL\Attribute\Input;
use UXF\GraphQL\Attribute\Type;
use UXF\GraphQL\Attribute\Union;
use UXF\GraphQL\Exception\GraphQLException;
use UXF\GraphQL\Inspector\Reflection\DocBlockFactory;
use UXF\GraphQL\Inspector\Schema\InputSpecial;
use UXF\GraphQL\Inspector\Schema\InputTypeFieldSchema;
use UXF\GraphQL\Inspector\Schema\InputTypeSchema;
use UXF\GraphQL\Inspector\Schema\OutputTypeFieldSchema;
use UXF\GraphQL\Inspector\Schema\OutputTypeSchema;
use UXF\GraphQL\Inspector\Schema\ScalarTypeSchema;
use UXF\GraphQL\Inspector\Schema\TypeModifier;
use UXF\GraphQL\Inspector\Schema\UnionTypeSchema;
use UXF\GraphQL\Utils\AttributeHelper;

final readonly class TypeInspector
{
    public function __construct(
        private DocBlockFactory $docBlockFactory,
        private TypeMap $typeMap,
    ) {
    }

    public function getQuery(): OutputTypeSchema
    {
        $query = $this->typeMap->getOutput('Query');
        assert($query instanceof OutputTypeSchema);
        return $query;
    }

    public function getMutation(): OutputTypeSchema
    {
        $mutation = $this->typeMap->getOutput('Mutation');
        assert($mutation instanceof OutputTypeSchema);
        return $mutation;
    }

    public function getTypeMap(): TypeMap
    {
        return $this->typeMap;
    }

    public function inspectInputType(string $phpTypeName): InputTypeSchema | ScalarTypeSchema
    {
        $inputType = $this->typeMap->getInput($phpTypeName);
        if ($inputType !== null) {
            return $inputType;
        }

        $scalarType = $this->typeMap->getScalar($phpTypeName);
        if ($scalarType !== null) {
            return $scalarType;
        }

        if (!class_exists($phpTypeName) && !interface_exists($phpTypeName)) {
            throw new GraphQLException("$phpTypeName is not class or interface");
        }

        $reflectionClass = new ReflectionClass($phpTypeName);

        $inputAttribute = AttributeHelper::find($reflectionClass, Input::class);
        if ($inputAttribute === null) {
            return new InputTypeSchema($phpTypeName, $phpTypeName, special: InputSpecial::Magic);
        }

        $name = $inputAttribute->name;
        if ($name === '') {
            $name = ClassNameHelper::shortName($reflectionClass->getName());
        }

        $special = null;
        if ($inputAttribute->generateFake !== null) {
            $special = $inputAttribute->generateFake ? InputSpecial::GenerateFake : InputSpecial::NotGenerateFake;
        }

        $inputType = new InputTypeSchema(
            name: $name,
            phpType: $phpTypeName,
            special: $special,
        );

        $this->typeMap->add($inputType);

        $constructor = $reflectionClass->getConstructor() ?? throw new GraphQLException("$phpTypeName without constructor");

        foreach ($constructor->getParameters() as $reflectionParameter) {
            $field = $this->inspectParameter($reflectionParameter);
            $inputType->addField($field);
        }

        return $inputType;
    }

    public function inspectOutputType(string $phpTypeName): OutputTypeSchema | ScalarTypeSchema
    {
        $outputType = $this->typeMap->getOutput($phpTypeName);
        if ($outputType !== null) {
            return $outputType;
        }

        $scalarType = $this->typeMap->getScalar($phpTypeName);
        if ($scalarType !== null) {
            return $scalarType;
        }

        if (!class_exists($phpTypeName)) {
            throw new GraphQLException("$phpTypeName is not class");
        }

        $reflectionClass = new ReflectionClass($phpTypeName);

        $typeAttribute = AttributeHelper::find($reflectionClass, Type::class);
        if ($typeAttribute === null) {
            throw new GraphQLException("{$reflectionClass->getName()} without #[Type] attribute");
        }

        $name = $typeAttribute->name;
        if ($name === '') {
            $name = ClassNameHelper::shortName($reflectionClass->getName());
        }

        $outputType = new OutputTypeSchema($name, $phpTypeName);
        $this->typeMap->add($outputType);

        foreach ($reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC) as $reflectionProperty) {
            if ($reflectionProperty->getAttributes(Ignore::class) !== []) {
                continue;
            }

            $outputType->addField($this->inspectProperty($reflectionProperty));
        }

        foreach ($reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC) as $reflectionMethod) {
            if ($reflectionMethod->name === '__construct' || $reflectionMethod->isStatic()) {
                continue;
            }
            if ($reflectionMethod->getAttributes(Ignore::class) !== []) {
                continue;
            }

            $outputType->addField($this->inspectMethod($reflectionMethod, null));
        }

        return $outputType;
    }

    public function inspectQueryOrMutation(ReflectionMethod $reflectionMethod, string $name): OutputTypeFieldSchema
    {
        return $this->inspectMethod($reflectionMethod, $name);
    }

    private function inspectParameter(ReflectionParameter $reflectionParameter): InputTypeFieldSchema
    {
        $type = $reflectionParameter->getType();
        $types = match (true) {
            $type instanceof ReflectionNamedType => [$type->getName()],
            $type instanceof ReflectionUnionType => array_map(static fn (ReflectionNamedType $t) => $t->getName(), $type->getTypes()),
            default => [],
        };

        $isList = in_array('array', $types, true);
        $itemsNullable = false;

        if ($isList) {
            $ref = $reflectionParameter->getDeclaringFunction();
            assert($ref instanceof ReflectionMethod);
            [$types, $itemsNullable] = $this->inspectDocBlock($ref, $reflectionParameter->name);
        } else {
            $types = Lst::from($types)
                ->filter(fn (string $type) => $type !== 'null' && $type !== NotSet::class)
                ->getValues();
        }

        if (count($types) !== 1) {
            $className = $reflectionParameter->getDeclaringClass()?->name;
            throw new LogicException("$className::\$$reflectionParameter->name has invalid type");
        }

        $inputType = $this->inspectInputType($types[0]);
        $hasDefaultValue = $reflectionParameter->isDefaultValueAvailable();

        // register enum
        $entity = AttributeHelper::find($reflectionParameter, Entity::class);
        if ($entity instanceof Entity) {
            $ref = ReflectionHelper::findReflectionProperty($types[0], $entity->property);
            assert($ref?->getType() instanceof ReflectionNamedType);
            $entityType = $ref->getType()->getName();
            if (is_a($entityType, BackedEnum::class, true)) {
                $this->typeMap->getScalar($entityType);
            }
        }

        return new InputTypeFieldSchema(
            name: $reflectionParameter->name,
            type: $inputType,
            modifier: TypeModifier::create($isList, $reflectionParameter->allowsNull(), $itemsNullable),
            hasDefaultValue: $hasDefaultValue,
            defaultValue: $hasDefaultValue ? $reflectionParameter->getDefaultValue() : null,
            attributes: AttributeHelper::all($reflectionParameter),
        );
    }

    private function inspectMethod(ReflectionMethod $reflectionMethod, ?string $name): OutputTypeFieldSchema
    {
        $returnType = $reflectionMethod->getReturnType();
        $declaringClassName = $reflectionMethod->getDeclaringClass()->getName();
        if ($returnType === null) {
            throw new GraphQLException("{$declaringClassName}::{$reflectionMethod->name}() missing return type");
        }

        $types = match (true) {
            $returnType instanceof ReflectionNamedType => [$returnType->getName()],
            $returnType instanceof ReflectionUnionType => array_map(static fn (ReflectionNamedType $t) => $t->getName(), $returnType->getTypes()),
            default => [],
        };

        $isList = $returnType instanceof ReflectionNamedType && $returnType->getName() === 'array';
        $itemsNullable = false;

        if ($isList) {
            $docBlock = $this->docBlockFactory->create($reflectionMethod);

            $returnTypeTag = $docBlock->getTagsByName('return')[0] ?? null;
            if (!$returnTypeTag instanceof Return_) {
                throw new GraphQLException("{$declaringClassName}::{$reflectionMethod->name}() missing return tag");
            }

            [$types, $itemsNullable] = $this->inspectTag($returnTypeTag->getType());
        } else {
            $types = Lst::from($types)
                ->filter(fn (string $type) => $type !== 'null')
                ->map(fn (string $type) => $type !== 'self' ? $type : $declaringClassName)
                ->getValues();
        }

        $outputSchemas = array_map($this->inspectOutputType(...), $types);

        if ($outputSchemas === []) {
            throw new GraphQLException("{$declaringClassName}::{$reflectionMethod->name} without type");
        }

        if (count($outputSchemas) > 1) {
            $union = AttributeHelper::find($reflectionMethod, Union::class);
            if ($union === null) {
                throw new GraphQLException("{$declaringClassName}::{$reflectionMethod->name} without #[Union] attribute");
            }

            $outputType = $this->typeMap->getUnion($union->name);
            if ($outputType === null) {
                $outputType = $this->typeMap->add(UnionTypeSchema::tryCreate($union->name, $outputSchemas));
            } else {
                $outputType->check($outputSchemas);
            }
        } else {
            $outputType = $outputSchemas[0];
        }

        if ($name === null) {
            $name = Strings::replace($reflectionMethod->name, '/^(is|get)/');
            $name = lcfirst($name);
        }

        $field = new OutputTypeFieldSchema(
            name: $name,
            type: $outputType,
            modifier: TypeModifier::create($isList, $returnType->allowsNull(), $itemsNullable),
            reflection: $reflectionMethod,
            attributes: AttributeHelper::all($reflectionMethod),
        );

        foreach ($reflectionMethod->getParameters() as $reflectionParameter) {
            $argument = $this->inspectParameter($reflectionParameter);
            $field->addArgument($argument);
        }

        return $field;
    }

    private function inspectProperty(ReflectionProperty $reflectionProperty): OutputTypeFieldSchema
    {
        $propertyType = $reflectionProperty->getType();
        $declaringClass = $reflectionProperty->getDeclaringClass();

        $types = match (true) {
            $propertyType instanceof ReflectionNamedType => [$propertyType->getName()],
            $propertyType instanceof ReflectionUnionType => array_map(static fn (ReflectionNamedType $t) => $t->getName(), $propertyType->getTypes()),
            default => throw new LogicException('Not supported'),
        };

        $isList = in_array('array', $types, true);

        $types = Lst::from($types)
            ->filter(fn (string $type) => $type !== 'null')
            ->map(fn (string $type) => $type !== 'self' ? $type : $declaringClass->name)
            ->getValues();

        assert($propertyType instanceof ReflectionType);
        $nullable = $propertyType->allowsNull();
        $itemsNullable = false;

        if ($isList) {
            if (!$declaringClass->hasMethod('__construct')) {
                throw new GraphQLException("{$declaringClass->name} without constructor");
            }

            [$types, $itemsNullable] = $this->inspectDocBlock($declaringClass->getMethod('__construct'), $reflectionProperty->name);
        }

        $outputSchemas = array_map($this->inspectOutputType(...), $types);

        if ($outputSchemas === []) {
            throw new LogicException("{$declaringClass->getName()}::\$$reflectionProperty->name has no type");
        }

        if (count($outputSchemas) > 1) {
            $union = AttributeHelper::find($reflectionProperty, Union::class);
            if ($union === null) {
                throw new GraphQLException("{$declaringClass->getName()}::\${$reflectionProperty->name} without #[Union] attribute");
            }

            $outputType = $this->typeMap->getUnion($union->name);
            if ($outputType === null) {
                $outputType = $this->typeMap->add(UnionTypeSchema::tryCreate($union->name, $outputSchemas));
            } else {
                $outputType->check($outputSchemas);
            }
        } else {
            $outputType = $outputSchemas[0];
        }

        return new OutputTypeFieldSchema(
            name: $reflectionProperty->name,
            type: $outputType,
            modifier: TypeModifier::create($isList, $nullable, $itemsNullable),
            reflection: $reflectionProperty,
            attributes: AttributeHelper::all($reflectionProperty),
        );
    }

    /**
     * @return array{string[], bool}
     */
    private function inspectDocBlock(ReflectionMethod $reflectionMethod, string $name): array
    {
        /** @var Param[] $paramTags */
        $paramTags = $this->docBlockFactory->create($reflectionMethod)->getTagsByName('param');
        foreach ($paramTags as $paramTag) {
            if ($paramTag->getVariableName() === $name) {
                return $this->inspectTag($paramTag->getType());
            }
        }

        return [[], false];
    }

    /**
     * @return array{string[], bool}
     */
    private function inspectTag(?PhpDocType $refType): array
    {
        $types = [];
        $itemsNullable = false;

        if ($refType instanceof Compound) {
            $refType = $refType->get(0);
        }

        if ($refType instanceof Array_) {
            $innerType = $refType->getValueType();
            if ($innerType instanceof Compound) {
                foreach ($innerType->getIterator() as $unionType) {
                    if ($unionType instanceof Null_) {
                        $itemsNullable = true;
                    } else {
                        $types[] = self::trim($unionType->__toString());
                    }
                }
            } else {
                $types[] = self::trim($refType->getValueType()->__toString());
            }
        }

        return [$types, $itemsNullable];
    }

    private static function trim(string $className): string
    {
        return ltrim($className, '\\');
    }
}
