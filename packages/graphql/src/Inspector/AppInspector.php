<?php

declare(strict_types=1);

namespace UXF\GraphQL\Inspector;

use ReflectionClass;
use ReflectionMethod;
use UXF\Core\Utils\ClassFinder;
use UXF\Core\Utils\Lst;
use UXF\GraphQL\Attribute\Mutation;
use UXF\GraphQL\Attribute\Query;
use UXF\GraphQL\Exception\GraphQLException;
use UXF\GraphQL\Inspector\Schema\AppSchema;
use UXF\GraphQL\Inspector\Schema\InputTypeSchema;
use UXF\GraphQL\Inspector\Schema\OutputTypeSchema;
use UXF\GraphQL\Inspector\Schema\ScalarTypeSchema;
use UXF\GraphQL\Inspector\Schema\UnionTypeSchema;
use UXF\GraphQL\Utils\AttributeHelper;

final readonly class AppInspector
{
    /**
     * @param string[] $dirs
     */
    public function __construct(
        private array $dirs,
        private TypeInspector $typeInspector,
    ) {
    }

    public function inspect(): AppSchema
    {
        $classNames = ClassFinder::findAllClassNames($this->dirs);

        $controllers = [];

        foreach ($classNames as $className) {
            if (!class_exists($className)) {
                continue;
            }

            $reflectionClass = new ReflectionClass($className);

            if ($reflectionClass->hasMethod('__invoke')) {
                $reflectionMethod = $reflectionClass->getMethod('__invoke');
                $controller = $this->inspectController($reflectionMethod);
                if ($controller !== null) {
                    $controllers[] = $controller;
                }
            }
        }

        $typeMap = $this->typeInspector->getTypeMap();

        // fix sort
        ksort($typeMap->outputs['Query']->fields);
        ksort($typeMap->outputs['Mutation']->fields);
        sort($controllers);

        $this->check($typeMap);

        return new AppSchema($typeMap, $controllers);
    }

    private function inspectController(ReflectionMethod $reflectionMethod): ?string
    {
        $name = null;
        $parentType = null;

        // query
        $queryAttribute = AttributeHelper::find($reflectionMethod, Query::class);
        if ($queryAttribute !== null) {
            $name = $queryAttribute->name;
            $parentType = $this->typeInspector->getQuery();
        }

        // mutation
        $mutationAttribute = AttributeHelper::find($reflectionMethod, Mutation::class);
        if ($mutationAttribute !== null) {
            $name = $mutationAttribute->name;
            $parentType = $this->typeInspector->getMutation();
        }

        if ($name === null || $parentType === null) {
            return null;
        }

        $field = $this->typeInspector->inspectQueryOrMutation($reflectionMethod, $name);
        $field->rootReflection = $reflectionMethod;
        $parentType->addField($field);
        return $reflectionMethod->getDeclaringClass()->name;
    }

    private function check(TypeMap $typeMap): void
    {
        $outputNames = Lst::from($typeMap->outputs)->map(fn (OutputTypeSchema $t) => $t->name)->getValues();
        $inputNames = Lst::from($typeMap->inputs)->map(fn (InputTypeSchema $t) => $t->name)->getValues();
        $unionNames = Lst::from($typeMap->unions)->map(fn (UnionTypeSchema $t) => $t->name)->getValues();
        $scalarNames = Lst::from($typeMap->scalars)->map(fn (ScalarTypeSchema $t) => $t->name)->getValues();

        $names = [...$outputNames, ...$inputNames, ...$unionNames, ...$scalarNames];
        $duplicities = array_unique(array_diff_assoc($names, array_unique($names)));

        if ($duplicities !== []) {
            $errors = [];

            foreach ($duplicities as $name) {
                $error = Lst::from($typeMap->outputs)->find(fn (OutputTypeSchema $t) => $t->name === $name)?->phpType;
                if ($error !== null) {
                    $errors[$name][] = "output $error";
                }
                $error = Lst::from($typeMap->inputs)->find(fn (InputTypeSchema $t) => $t->name === $name)?->phpType;
                if ($error !== null) {
                    $errors[$name][] = "input $error";
                }
                $error = Lst::from($typeMap->unions)->find(fn (UnionTypeSchema $t) => $t->name === $name)?->name;
                if ($error !== null) {
                    $errors[$name][] = "union $error";
                }
                $error = Lst::from($typeMap->scalars)->find(fn (ScalarTypeSchema $t) => $t->name === $name)?->phpType;
                if ($error !== null) {
                    $errors[$name][] = "scalar $error";
                }
            }

            $msg = '';
            foreach ($errors as $name => $phpTypes) {
                $msg .= "'$name' in " . implode(' + ', $phpTypes) . "\n";
            }

            throw new GraphQLException("Duplicate names:\n$msg");
        }
    }
}
