<?php

declare(strict_types=1);

namespace UXF\GraphQL\DependencyInjection;

use RuntimeException;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use UXF\Core\Contract\Permission\RoleChecker;
use UXF\GraphQL\Attribute\Autowire;
use UXF\GraphQL\Generator\TypeGenerator;
use UXF\GraphQL\Generator\TypeRegistryGenerator;
use UXF\GraphQL\Inspector\AppInspector;
use UXF\GraphQL\Inspector\Reflection\DocBlockFactory;
use UXF\GraphQL\Inspector\TypeInspector;
use UXF\GraphQL\Inspector\TypeMap;
use UXF\GraphQL\Plugin\SchemaModifier;
use UXF\GraphQL\Security\DummyRoleChecker;

final readonly class SchemaCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        /** @var string $namespace */
        $namespace = $container->getParameter('uxf_graphql.namespace');
        /** @var string[] $sources */
        $sources = $container->getParameter('uxf_graphql.sources');
        /** @var array<string, string> $injected */
        $injected = $container->getParameter('uxf_graphql.injected');
        /** @var class-string<SchemaModifier>[] $modifiers */
        $modifiers = $container->getParameter('uxf_graphql.modifiers');

        /** @var string $generatorDirRaw */
        $generatorDirRaw = $container->getParameter('uxf_graphql.type_registry_dir');
        $generatorDir = $container->resolveEnvPlaceholders($generatorDirRaw, true);

        usort($modifiers, static fn ($a, $b) => $b::getDefaultPriority() <=> $a::getDefaultPriority());

        $typeInspector = new TypeInspector(DocBlockFactory::default(), new TypeMap($modifiers));
        $appInspect = new AppInspector($sources, $typeInspector);
        $appSchema = $appInspect->inspect();

        $container->setParameter('uxf_graphql.controller_names', $appSchema->controllers);

        // controllers
        foreach ($appSchema->controllers as $controller) {
            $this->makePublic($container, $controller, true);
        }

        // register autowire
        foreach ($appSchema->typeMap->outputs as $output) {
            foreach ($output->fields as $outputField) {
                foreach ($outputField->arguments as $argument) {
                    if ($argument->attribute(Autowire::class) !== null) {
                        $this->makePublic($container, $argument->type->phpType);
                    }
                }
            }
        }

        // register injected services as public
        foreach ($injected as $injectedServiceId) {
            $this->makePublic($container, $injectedServiceId);
        }

        $generator = new TypeRegistryGenerator($namespace, $generatorDir, new TypeGenerator($injected, $modifiers));
        $generator->generateFile($appSchema->typeMap);

        // security
        if (!$container->hasDefinition(RoleChecker::class)) {
            $container->register(RoleChecker::class, DummyRoleChecker::class);
        }

        // validator
        $this->makePublic($container, 'validator');
    }

    private function makePublic(ContainerBuilder $container, string $className, bool $autoRegister = false): void
    {
        if ($container->hasAlias($className)) {
            $container->getAlias($className)->setPublic(true);
        } elseif ($container->hasDefinition($className)) {
            $container->getDefinition($className)->setPublic(true);
        } elseif ($autoRegister) {
            $container->register($className, $className)->setAutowired(true)->setPublic(true);
        } else {
            throw new RuntimeException("Service $className is not registered in container");
        }
    }
}
