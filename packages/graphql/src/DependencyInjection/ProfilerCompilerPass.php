<?php

declare(strict_types=1);

namespace UXF\GraphQL\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final readonly class ProfilerCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if ($container->hasDefinition('profiler_listener')) {
            $container->getDefinition('profiler_listener')->setPublic(true);
        }
    }
}
