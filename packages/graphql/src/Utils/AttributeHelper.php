<?php

declare(strict_types=1);

namespace UXF\GraphQL\Utils;

use Error;
use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;
use ReflectionProperty;

final readonly class AttributeHelper
{
    /**
     * @template T of object
     * @param ReflectionParameter|ReflectionMethod|ReflectionProperty|ReflectionClass<object> $reflection
     * @param class-string<T> $name
     * @return T|null
     */
    public static function find(
        ReflectionParameter|ReflectionMethod|ReflectionProperty|ReflectionClass $reflection,
        string $name,
    ): ?object {
        $attributes = $reflection->getAttributes($name);
        if ($attributes === []) {
            return null;
        }

        return $attributes[0]->newInstance();
    }

    /**
     * @param ReflectionParameter|ReflectionMethod|ReflectionProperty|ReflectionClass<object> $reflection
     * @return array<class-string, object>
     */
    public static function all(ReflectionParameter|ReflectionMethod|ReflectionProperty|ReflectionClass $reflection): array
    {
        $result = [];
        foreach ($reflection->getAttributes() as $attribute) {
            try {
                $result[$attribute->getName()] = $attribute->newInstance();
            } catch (Error) {
            }
        }
        return $result;
    }
}
