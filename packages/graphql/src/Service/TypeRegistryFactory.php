<?php

declare(strict_types=1);

namespace UXF\GraphQL\Service;

use LogicException;
use Psr\Container\ContainerInterface;
use UXF\GraphQL\Type\TypeRegistryInterface;
use UXF\Hydrator\ObjectHydrator;

final readonly class TypeRegistryFactory
{
    public function __construct(
        private string $namespace,
        private string $typeRegistryDir,
        private ContainerInterface $container,
        private ObjectHydrator $hydrator,
    ) {
    }

    public function create(): TypeRegistryInterface
    {
        if (!file_exists($this->typeRegistryDir . '/TypeRegistry.php')) {
            throw new LogicException('Please clear cache');
        }

        require_once $this->typeRegistryDir . '/TypeRegistry.php';

        $className = "\\{$this->namespace}\\TypeRegistry";
        return new $className($this->container, $this->hydrator); // @phpstan-ignore return.type
    }
}
