<?php

declare(strict_types=1);

namespace UXF\GraphQL\Service;

use GraphQL\Type\Schema;
use GraphQL\Type\SchemaConfig;
use UXF\GraphQL\Type\TypeRegistryInterface;

final readonly class SchemaFactory
{
    public function __construct(private TypeRegistryInterface $typeRegistry)
    {
    }

    public function createSchema(): Schema
    {
        // @phpstan-ignore-next-line
        $config = SchemaConfig::create([
            'query' => $this->typeRegistry->get('Query'),
            'mutation' => $this->typeRegistry->get('Mutation'),
            'typeLoader' => $this->typeRegistry->get(...),
        ]);

        return new Schema($config);
    }
}
