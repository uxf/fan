<?php

declare(strict_types=1);

namespace UXF\GraphQL\Service\Injector;

final readonly class InjectedArgument
{
    /**
     * @param class-string $type
     */
    public function __construct(
        public string $type,
        public bool $nullable,
    ) {
    }
}
