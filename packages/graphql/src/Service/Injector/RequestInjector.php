<?php

declare(strict_types=1);

namespace UXF\GraphQL\Service\Injector;

use Symfony\Component\HttpFoundation\Request;

final readonly class RequestInjector
{
    public function __invoke(Request $request, InjectedArgument $argument): Request
    {
        return $request;
    }
}
