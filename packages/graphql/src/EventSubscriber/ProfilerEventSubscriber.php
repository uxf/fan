<?php

declare(strict_types=1);

namespace UXF\GraphQL\EventSubscriber;

use Psr\Container\ContainerInterface;
use ReflectionProperty;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Throwable;

// ugly hack
final class ProfilerEventSubscriber implements EventSubscriberInterface
{
    public ?string $operationName = null;

    public function __construct(
        private readonly ContainerInterface $container,
    ) {
    }

    public function onOperationPreExecutionEvent(OperationPreExecutionEvent $event): void
    {
        $this->operationName = $event->operationParams->operation;
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        if ($this->operationName !== null) {
            try {
                $profilerListener = $this->container->get('profiler_listener');
                $reflectionProperty = new ReflectionProperty($profilerListener, 'profiles');
                $reflectionProperty->setAccessible(true);
                $reflectionProperty->getValue($profilerListener)->getInfo()->setUrl('GQL ' . $this->operationName);
            } catch (Throwable) {
                // ignore
            }
        }
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            OperationPreExecutionEvent::class => 'onOperationPreExecutionEvent',
            ResponseEvent::class => ['onKernelResponse', -101], // after -100
        ];
    }
}
