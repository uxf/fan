<?php

declare(strict_types=1);

namespace UXF\GraphQL\EventSubscriber;

use GraphQL\Server\OperationParams;

final readonly class OperationPreExecutionEvent
{
    public function __construct(public OperationParams $operationParams)
    {
    }
}
