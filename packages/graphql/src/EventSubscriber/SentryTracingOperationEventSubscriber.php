<?php

declare(strict_types=1);

namespace UXF\GraphQL\EventSubscriber;

use Sentry\State\HubInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final readonly class SentryTracingOperationEventSubscriber implements EventSubscriberInterface
{
    public function __construct(private ?HubInterface $hub = null)
    {
    }

    public function onOperationPreExecutionEvent(OperationPreExecutionEvent $event): void
    {
        $transaction = $this->hub?->getTransaction();

        if ($transaction !== null) {
            $transaction->setName(trim('GQL ' . $event->operationParams->operation));
            $transaction->setOp('http.server.gql');
        }
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            OperationPreExecutionEvent::class => 'onOperationPreExecutionEvent',
        ];
    }
}
