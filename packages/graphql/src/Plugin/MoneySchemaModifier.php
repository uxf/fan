<?php

declare(strict_types=1);

namespace UXF\GraphQL\Plugin;

use ReflectionProperty;
use UXF\Core\Type\Currency;
use UXF\Core\Type\Money;
use UXF\GraphQL\Inspector\Schema\InputSpecial;
use UXF\GraphQL\Inspector\Schema\InputTypeFieldSchema;
use UXF\GraphQL\Inspector\Schema\InputTypeSchema;
use UXF\GraphQL\Inspector\Schema\OutputTypeFieldSchema;
use UXF\GraphQL\Inspector\Schema\OutputTypeSchema;
use UXF\GraphQL\Inspector\Schema\ScalarTypeSchema;
use UXF\GraphQL\Inspector\Schema\TypeModifier;
use UXF\GraphQL\Inspector\TypeMap;

final class MoneySchemaModifier implements SchemaModifier
{
    public static function modifyTypeMap(TypeMap $typeMap): void
    {
        $amountPropRef = new ReflectionProperty(Money::class, 'amount');
        $currencyPropRef = new ReflectionProperty(Money::class, 'currency');

        $string = $typeMap->scalars['string'];

        $typeMap->scalars[Currency::class] = $currency = new ScalarTypeSchema('Currency', Currency::class);
        $typeMap->outputs[Money::class] = new OutputTypeSchema('Money', Money::class, [
            'amount' => new OutputTypeFieldSchema('amount', $string, TypeModifier::SIMPLE_NON_NULL, $amountPropRef),
            'currency' => new OutputTypeFieldSchema('currency', $currency, TypeModifier::SIMPLE_NON_NULL, $currencyPropRef),
        ]);
        $typeMap->inputs[Money::class] = new InputTypeSchema('MoneyInput', Money::class, [
            'amount' => new InputTypeFieldSchema('amount', $string, TypeModifier::SIMPLE_NON_NULL, false, null),
            'currency' => new InputTypeFieldSchema('currency', $currency, TypeModifier::SIMPLE_NON_NULL, false, null),
        ], InputSpecial::NotGenerateFake);
    }

    public static function modifyParseValueFn(InputTypeSchema $inputType): ?string
    {
        if ($inputType->phpType === Money::class) {
            return 'function (array $values) {
        try {
            return \\' . Money::class . "::of(\$values['amount'], \$values['currency']);
        } catch (\Exception) {
            throw new Error('Invalid money format', previous: new UserError('Invalid money format'));
        }
    }";
        }

        return null;
    }

    public static function getDefaultPriority(): int
    {
        return 0;
    }
}
