<?php

declare(strict_types=1);

namespace UXF\GraphQL\Plugin;

use UXF\GraphQL\Inspector\Schema\InputTypeSchema;
use UXF\GraphQL\Inspector\TypeMap;

interface SchemaModifier
{
    public static function modifyTypeMap(TypeMap $typeMap): void;

    public static function modifyParseValueFn(InputTypeSchema $inputType): ?string;

    public static function getDefaultPriority(): int;
}
