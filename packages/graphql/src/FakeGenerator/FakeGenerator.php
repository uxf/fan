<?php

declare(strict_types=1);

namespace UXF\GraphQL\FakeGenerator;

use JsonSerializable;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Literal;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\PsrPrinter;
use Nette\PhpGenerator\Type;
use Nette\Utils\FileSystem;
use ReflectionNamedType;
use Symfony\Component\Finder\Finder;
use UXF\Core\Attribute\Entity;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\Http\Request\NotSetTrait;
use UXF\Core\Utils\ClassNameHelper;
use UXF\Core\Utils\Lst;
use UXF\Core\Utils\ReflectionHelper;
use UXF\GraphQL\Inspector\AppInspector;
use UXF\GraphQL\Inspector\Schema\InputSpecial;
use UXF\GraphQL\Inspector\Schema\InputTypeSchema;
use UXF\GraphQL\Inspector\Schema\ScalarTypeSchema;

final readonly class FakeGenerator
{
    public function __construct(
        private bool $default,
        private AppInspector $appInspector,
        private PsrPrinter $printer = new PsrPrinter(),
    ) {
    }

    public function generateFiles(FakeConfig $config): void
    {
        if (!$config->enabled) {
            return;
        }

        FileSystem::createDir($config->destination);

        $appSchema = $this->appInspector->inspect();
        $typeMap = $appSchema->typeMap;
        $generatedClassNames = [];

        foreach ($typeMap->inputs as $input) {
            if ($this->shouldSkip($input->special)) {
                continue;
            }

            $usedClasses = [];

            $class = new ClassType("T{$input->name}");
            $class->setFinal()->setReadOnly();

            $construct = $class->addMethod('__construct');
            $arrayParameters = [];

            foreach ($input->fields as $field) {
                $parameter = $construct->addPromotedParameter($field->name);
                $phpType = $field->type->phpType;
                $notSet = $field->hasDefaultValue && $field->defaultValue instanceof NotSet;
                if ($notSet) {
                    $usedClasses[] = NotSet::class;
                }

                $entity = $field->attribute(Entity::class);
                if ($entity !== null) {
                    $ref = ReflectionHelper::findReflectionProperty($phpType, $entity->property);
                    assert($ref?->getType() instanceof ReflectionNamedType);

                    $entityType = $ref->getType()->getName();
                    if (class_exists($entityType) && ($ref = ReflectionHelper::findReflectionProperty($entityType, 'id')) !== null) {
                        assert($ref->getType() instanceof ReflectionNamedType);
                        $entityType = $ref->getType()->getName();
                    }

                    $phpType = $entityType;
                } elseif ($field->type instanceof InputTypeSchema && !$this->shouldSkip($field->type->special)) {
                    $phpType = "{$config->namespace}\\T{$field->type->name}";
                } elseif ($field->type instanceof ScalarTypeSchema && $field->type->definition?->generatedFakeType !== null) {
                    $phpType = $field->type->definition->generatedFakeType;
                }

                if (class_exists($phpType) || interface_exists($phpType)) {
                    $usedClasses[] = $phpType;
                }

                if ($field->modifier->array()) {
                    $shortName = ClassNameHelper::shortName($phpType);

                    $phpDoc = "@param {$shortName}[]";

                    if ($field->modifier->nullable()) {
                        $phpDoc = "$phpDoc|null";
                    }

                    if ($notSet) {
                        $phpDoc = "$phpDoc|NotSet";
                        $parameter->setType(Type::union('array', 'null', NotSet::class));
                    } else {
                        $parameter->setType('array');
                    }

                    $arrayParameters[] = "$phpDoc \${$field->name}";
                } elseif (!$notSet) {
                    $parameter->setType($phpType);
                } else {
                    $parameter->setType(Type::union($phpType, 'null', NotSet::class));
                }

                if ($field->modifier->nullable()) {
                    $parameter->setNullable();
                }

                if ($field->hasDefaultValue) {
                    if ($notSet) {
                        $parameter->setDefaultValue(new Literal('new NotSet()'));
                    } else {
                        $parameter->setDefaultValue($field->defaultValue);
                    }
                }
            }

            $construct->addComment(implode("\n", $arrayParameters));

            $usedClasses = Lst::from($usedClasses)->unique()->sort()->getValues();

            $phpNamespace = new PhpNamespace($config->namespace);
            $phpNamespace->add($class);

            if (in_array(NotSet::class, $usedClasses, true)) {
                $class->addImplement(JsonSerializable::class);
                $class->addTrait(NotSetTrait::class);
                $phpNamespace->addUse(JsonSerializable::class);
                $phpNamespace->addUse(NotSetTrait::class);
            }

            foreach ($usedClasses as $usedClass) {
                $phpNamespace->addUse($usedClass);
            }

            $file = new PhpFile();
            $file->setStrictTypes();
            $file->addNamespace($phpNamespace);

            $output = $this->printer->printFile($file);
            FileSystem::write($config->destination . "/{$class->getName()}.php", $output);
            $generatedClassNames[] = $class->getName();
        }

        // cleanup
        foreach ((new Finder())->files()->in($config->destination) as $file) {
            if (!in_array($file->getBasename('.php'), $generatedClassNames, true)) {
                FileSystem::delete($file->getRealPath());
            }
        }
    }

    private function shouldSkip(?InputSpecial $special): bool
    {
        return ($this->default === false && $special !== InputSpecial::GenerateFake) || $special === InputSpecial::NotGenerateFake;
    }
}
