<?php

declare(strict_types=1);

namespace UXF\GraphQL\FakeGenerator;

final readonly class FakeConfig
{
    public function __construct(
        public bool $enabled,
        public string $namespace,
        public string $destination,
    ) {
    }

    public static function create(bool $enabled, string $namespace, string $destination): self
    {
        return new self(enabled: $enabled, namespace: $namespace, destination: $destination);
    }
}
