<?php

declare(strict_types=1);

namespace UXF\GraphQL\Security;

use UXF\Core\Attribute\Security;
use UXF\Core\Contract\Permission\RoleChecker;

class DummyRoleChecker implements RoleChecker
{
    public function check(Security $access): void
    {
    }
}
