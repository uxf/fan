<?php

declare(strict_types=1);

namespace UXF\GraphQL;

use GraphQL\Error\DebugFlag;
use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use UXF\GraphQL\DependencyInjection\ProfilerCompilerPass;
use UXF\GraphQL\DependencyInjection\SchemaCompilerPass;
use UXF\GraphQL\Plugin\MoneySchemaModifier;
use UXF\GraphQL\Service\Injector\RequestInjector;

final class UXFGraphQLBundle extends AbstractBundle
{
    private const array DefaultHydratorOptions = [
        'allow_trim_string' => true,
        'nullable_optional' => true,
    ];

    protected string $extensionAlias = 'uxf_graphql';

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
                ->arrayNode('sources')
                    ->isRequired()->scalarPrototype()->end()
                ->end()
                ->arrayNode('injected')
                    ->scalarPrototype()->end()
                ->end()
                ->arrayNode('modifiers')
                    ->scalarPrototype()->end()
                ->end()
                ->integerNode('debug_flag')
                    ->defaultValue(DebugFlag::INCLUDE_DEBUG_MESSAGE)
                ->end()
                ->arrayNode('destination')
                    ->beforeNormalization()
                        ->ifString()
                        ->then(fn (string $v) => [$v])
                    ->end()
                    ->scalarPrototype()->isRequired()->end()
                ->end()
                ->arrayNode('fake')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('enabled')->defaultValue(false)->end()
                        ->booleanNode('default_strategy')->defaultValue(false)->end()
                        ->stringNode('namespace')->defaultValue('App\\Tests\\TInput')->end()
                        ->stringNode('destination')->defaultValue('%kernel.project_dir%/tests/TInput')->end()
                    ->end()
                ->end()
                ->arrayNode('hydrator_options')
                    ->scalarPrototype()->end()
                ->end()
                ->stringNode('namespace')->defaultValue('UxfGraphql')->end()
                ->booleanNode('register_psr_17')->defaultTrue()->end();
    }

    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import(__DIR__ . '/../config/services.php');

        $parameters = $container->parameters();
        $parameters->set('uxf_graphql.sources', $config['sources']);
        $parameters->set('uxf_graphql.destinations', $config['destination']);
        $parameters->set('uxf_graphql.debug_flag', $config['debug_flag']);
        $parameters->set('uxf_graphql.namespace', $config['namespace']);
        $parameters->set('uxf_graphql.hydrator_options', $config['hydrator_options'] + self::DefaultHydratorOptions);

        $parameters->set('uxf_graphql.fake.enabled', $config['fake']['enabled']);
        $parameters->set('uxf_graphql.fake.default_strategy', $config['fake']['default_strategy']);
        $parameters->set('uxf_graphql.fake.namespace', $config['fake']['namespace']);
        $parameters->set('uxf_graphql.fake.destination', $config['fake']['destination']);

        $modifiers = $config['modifiers'] ?? [];
        $modifiers[] = MoneySchemaModifier::class;
        $parameters->set('uxf_graphql.modifiers', $modifiers);

        $injected = $config['injected'] ?? [];
        $injected[Request::class] = RequestInjector::class;
        $parameters->set('uxf_graphql.injected', $injected);

        if ($config['register_psr_17']) {
            $container->import(__DIR__ . '/../config/services_psr17.php');
        }
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new SchemaCompilerPass());
        $container->addCompilerPass(new ProfilerCompilerPass());
    }
}
