<?php

declare(strict_types=1);

namespace UXF\GraphQL\Exception;

use Exception;

final class GraphQLException extends Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
