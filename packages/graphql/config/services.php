<?php

declare(strict_types=1);

use GraphQL\Server\ServerConfig;
use GraphQL\Type\Schema;
use Psr\Container\ContainerInterface;
use Symfony\Component\Cache\Psr16Cache;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Core\Contract\Permission\RoleChecker;
use UXF\GraphQL\Command\GenCommand;
use UXF\GraphQL\Controller\GQLController;
use UXF\GraphQL\Error\DefaultErrorHandler;
use UXF\GraphQL\Error\ErrorHandler;
use UXF\GraphQL\EventSubscriber\ProfilerEventSubscriber;
use UXF\GraphQL\EventSubscriber\SentryTracingOperationEventSubscriber;
use UXF\GraphQL\FakeGenerator\FakeConfig;
use UXF\GraphQL\FakeGenerator\FakeGenerator;
use UXF\GraphQL\Inspector\AppInspector;
use UXF\GraphQL\Inspector\Reflection\DocBlockFactory;
use UXF\GraphQL\Inspector\TypeInspector;
use UXF\GraphQL\Inspector\TypeMap;
use UXF\GraphQL\Service\Injector\RequestInjector;
use UXF\GraphQL\Service\ResponseCallbackModifier;
use UXF\GraphQL\Service\SchemaFactory;
use UXF\GraphQL\Service\TypeRegistryFactory;
use UXF\GraphQL\Type\Definition\BanCzeType;
use UXF\GraphQL\Type\Definition\DateTimeType;
use UXF\GraphQL\Type\Definition\DateType;
use UXF\GraphQL\Type\Definition\DecimalType;
use UXF\GraphQL\Type\Definition\EmailType;
use UXF\GraphQL\Type\Definition\JsonType;
use UXF\GraphQL\Type\Definition\LongType;
use UXF\GraphQL\Type\Definition\NinCzeType;
use UXF\GraphQL\Type\Definition\PhoneType;
use UXF\GraphQL\Type\Definition\TimeType;
use UXF\GraphQL\Type\Definition\UrlType;
use UXF\GraphQL\Type\Definition\UuidType;
use UXF\GraphQL\Type\TypeRegistryInterface;
use UXF\Hydrator\ObjectHydrator;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $containerConfigurator): void {
    $parameters = $containerConfigurator->parameters();
    $parameters->set('uxf_graphql.type_registry_dir', '%kernel.cache_dir%/graphql');

    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $services->alias(ContainerInterface::class, 'service_container');

    $services->set(GQLController::class)->public();
    $services->set(GenCommand::class)
        ->arg('$destinations', param('uxf_graphql.destinations'))
        ->public();

    $services->set(ErrorHandler::class, DefaultErrorHandler::class)
        ->arg('$debug', '%kernel.debug%');

    $services->set('uxf_graphql.cache', Psr16Cache::class);

    $services->set('uxf_graphql.hydrator', ObjectHydrator::class)
        ->factory([ObjectHydrator::class, 'create'])
        ->arg('$options', param('uxf_graphql.hydrator_options'))
        ->arg('$name', 'GraphGL');

    $services->set(TypeRegistryFactory::class)
        ->arg('$namespace', param('uxf_graphql.namespace'))
        ->arg('$typeRegistryDir', param('uxf_graphql.type_registry_dir'))
        ->arg('$hydrator', service('uxf_graphql.hydrator'));

    $services->set(TypeRegistryInterface::class)
        ->factory([service(TypeRegistryFactory::class), 'create']);

    $services->set(SchemaFactory::class);

    $services->set(Schema::class)
        ->factory([service(SchemaFactory::class), 'createSchema']);

    $services->set(ServerConfig::class)
        ->call('setSchema', [service(Schema::class)])
        ->call('setDebugFlag', [param('uxf_graphql.debug_flag')])
        ->call('setErrorsHandler', [service(ErrorHandler::class)]);

    // fake generator
    $services->set(DocBlockFactory::class)
        ->factory([null, 'default']);

    $services->set(TypeMap::class)
        ->factory([null, 'create'])
        ->arg('$modifiers', param('uxf_graphql.modifiers'));

    $services->set(TypeInspector::class);
    $services->set(AppInspector::class)
        ->arg('$dirs', param('uxf_graphql.sources'));

    $services->set(FakeGenerator::class)
        ->arg('$default', param('uxf_graphql.fake.default_strategy'));

    $services->set(FakeConfig::class)
        ->factory([null, 'create'])
        ->arg('$enabled', param('uxf_graphql.fake.enabled'))
        ->arg('$namespace', param('uxf_graphql.fake.namespace'))
        ->arg('$destination', param('uxf_graphql.fake.destination'));

    // security
    $services->alias('uxf.role_checker', RoleChecker::class)->public();

    // injectors
    $services->set(RequestInjector::class)->public();

    // sentry
    $services->set(SentryTracingOperationEventSubscriber::class);

    // profiler
    if ($containerConfigurator->env() === 'dev') {
        $services->set(ProfilerEventSubscriber::class);
    }
    $services->set(ResponseCallbackModifier::class);

    // types
    $services->set(BanCzeType::class)->public();
    $services->set(DateTimeType::class)->public();
    $services->set(DateType::class)->public();
    $services->set(DecimalType::class)->public();
    $services->set(EmailType::class)->public();
    $services->set(JsonType::class)->public();
    $services->set(LongType::class)->public();
    $services->set(NinCzeType::class)->public();
    $services->set(PhoneType::class)->public();
    $services->set(TimeType::class)->public();
    $services->set(UrlType::class)->public();
    $services->set(UuidType::class)->public();
};
