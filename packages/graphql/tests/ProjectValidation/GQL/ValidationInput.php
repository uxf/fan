<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectValidation\GQL;

use Symfony\Component\Validator\Constraints as Assert;
use UXF\GraphQL\Attribute\Input;

#[Input]
final readonly class ValidationInput
{
    public function __construct(
        #[Assert\NotBlank] public string $string,
        #[Assert\Positive] public int $number,
    ) {
    }
}
