<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectValidation\GQL;

use UXF\GraphQL\Attribute\Query;

final readonly class ValidationQuery
{
    #[Query('validation')]
    public function __invoke(ValidationInput $input): bool
    {
        return true;
    }
}
