<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectValidation;

use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\Core\UXFCoreBundle;
use UXF\GraphQL\UXFGraphQLBundle;
use UXF\GraphQLTests\ProjectScalar\Service\SuperScalarSchemaModifier;
use UXF\GraphQLTests\ProjectShared\KernelHelper;
use UXF\Hydrator\UXFHydratorBundle;

class ValidationKernel extends BaseKernel
{
    use MicroKernelTrait;

    /**
     * @inheritDoc
     */
    public function registerBundles(): iterable
    {
        $bundles = [
            FrameworkBundle::class,
            MonologBundle::class,
            UXFCoreBundle::class,
            UXFHydratorBundle::class,
            UXFGraphQLBundle::class,
        ];

        foreach ($bundles as $bundle) {
            yield new $bundle();
        }
    }

    public function getCacheDir(): string
    {
        return __DIR__ . '/../../var/cache/test-validation';
    }

    protected function configureContainer(ContainerConfigurator $container): void
    {
        KernelHelper::configure($container);

        $container->extension('uxf_graphql', [
            'destination' => __DIR__ . '/../generated/validation.graphql',
            'sources' => [
                __DIR__ . '/../../tests/ProjectValidation',
            ],
            'modifiers' => [SuperScalarSchemaModifier::class],
            'namespace' => 'UxfGraphqlValidation',
        ]);
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import(__DIR__ . '/../../config/routes.php');
    }
}
