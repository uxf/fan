<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectUnion\GQL;

use UXF\GraphQL\Attribute\Type;

#[Type]
final readonly class Banana
{
    public function __construct(
        public int $id,
        public string $type,
    ) {
    }
}
