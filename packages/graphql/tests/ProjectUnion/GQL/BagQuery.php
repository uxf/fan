<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectUnion\GQL;

use UXF\GraphQL\Attribute\Query;

final readonly class BagQuery
{
    #[Query('bag')]
    public function __invoke(): BagType
    {
        return new BagType(
            itemA: new Apple(1, 3.14),
            itemB: null,
            itemsC: [new Banana(2, 'B')],
            itemsD: null,
        );
    }
}
