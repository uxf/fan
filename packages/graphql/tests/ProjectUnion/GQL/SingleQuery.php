<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectUnion\GQL;

use UXF\GraphQL\Attribute\Query;
use UXF\GraphQL\Attribute\Union;

final readonly class SingleQuery
{
    #[Query('single')] // @phpstan-ignore return.unusedType
    #[Union('Fruit')]
    public function __invoke(): Apple|Banana
    {
        return new Apple(1, 3.14);
    }
}
