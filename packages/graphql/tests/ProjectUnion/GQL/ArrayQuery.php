<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectUnion\GQL;

use UXF\GraphQL\Attribute\Query;
use UXF\GraphQL\Attribute\Union;

final readonly class ArrayQuery
{
    /**
     * @return (Apple|Banana)[]
     */
    #[Query('array')]
    #[Union('Fruit')]
    public function __invoke(): array
    {
        return [new Apple(1, 3.14)];
    }
}
