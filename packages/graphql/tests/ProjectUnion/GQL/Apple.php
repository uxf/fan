<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectUnion\GQL;

use UXF\GraphQL\Attribute\Type;

#[Type]
final readonly class Apple
{
    public function __construct(
        public int $id,
        public float $quality,
    ) {
    }
}
