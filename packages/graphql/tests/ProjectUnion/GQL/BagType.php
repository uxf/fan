<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectUnion\GQL;

use UXF\GraphQL\Attribute\Type;
use UXF\GraphQL\Attribute\Union;

#[Type('Bag')]
final readonly class BagType
{
    /**
     * @param (Apple|Banana)[] $itemsC
     * @param (Apple|Banana)[]|null $itemsD
     */
    public function __construct(
        #[Union('Fruit')] public Apple|Banana $itemA,
        #[Union('Fruit')] public Apple|Banana|null $itemB,
        #[Union('Fruit')] public array $itemsC,
        #[Union('Fruit')] public ?array $itemsD,
    ) {
    }

    #[Union('Fruit')] // @phpstan-ignore return.unusedType
    public function getItem1(): Apple|Banana
    {
        return new Apple(1, 3.14);
    }

    #[Union('Fruit')] // @phpstan-ignore-line
    public function getItem2(): Apple|Banana|null
    {
        return null;
    }

    /**
     * @return (Apple|Banana)[]
     */
    #[Union('Fruit')]
    public function getItems3(): array
    {
        return [new Banana(2, 'C')];
    }

    /**
     * @return (Apple|Banana)[]|null
     */
    #[Union('Fruit')] // @phpstan-ignore return.unusedType
    public function getItems4(): ?array
    {
        return [new Banana(2, 'C')];
    }
}
