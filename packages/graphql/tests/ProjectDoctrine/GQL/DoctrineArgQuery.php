<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectDoctrine\GQL;

use UXF\Core\Attribute\Entity;
use UXF\GraphQL\Attribute\Query;
use UXF\GraphQLTests\ProjectShared\Entity\Donald;
use UXF\GraphQLTests\ProjectShared\Entity\Minnie;

final readonly class DoctrineArgQuery
{
    /**
     * @param Donald[] $donalds
     * @param Donald[] $donaldNames
     * @param Donald[] $donaldUuids
     * @param Donald[] $donaldRefs
     * @param Donald[]|null $donaldsNullable
     * @param Donald[]|null $donaldsNullableOptional
     * @param Donald[] $donaldsOptional
     */
    #[Query('args')]
    public function __invoke(
        #[Entity] Donald $donald,
        #[Entity('name')] Donald $donaldName,
        #[Entity('uuid')] Donald $donaldUuid,
        #[Entity('minnie')] Donald $donaldRef,
        #[Entity('enum')] Donald $donaldEnum,
        #[Entity('enumInt')] Donald $donaldEnumInt,
        #[Entity('hidden')] Minnie $minnie,
        #[Entity] array $donalds,
        #[Entity('name')] array $donaldNames,
        #[Entity('uuid')] array $donaldUuids,
        #[Entity('minnie')] array $donaldRefs,
        #[Entity('name')] ?Donald $donaldNullable,
        #[Entity('uuid')] ?array $donaldsNullable,
        #[Entity('name')] ?Donald $donaldNullableOptional = null,
        #[Entity('name')] ?array $donaldsNullableOptional = null,
        #[Entity('name')] array $donaldsOptional = [],
    ): bool {
        return true;
    }
}
