<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectDoctrine\GQL;

use UXF\Core\Attribute\Entity;
use UXF\GraphQL\Attribute\Input;
use UXF\GraphQLTests\ProjectShared\Entity\Donald;
use UXF\GraphQLTests\ProjectShared\Entity\Minnie;

#[Input]
final readonly class DoctrineInput
{
    /**
     * @param Donald[] $donalds
     * @param Donald[] $donaldNames
     * @param Donald[] $donaldUuids
     * @param Donald[] $donaldRefs
     * @param Donald[]|null $donaldsNullable
     * @param Donald[]|null $donaldsNullableOptional
     * @param Donald[] $donaldsOptional
     */
    public function __construct(
        #[Entity] public Donald $donald,
        #[Entity('name')] public Donald $donaldName,
        #[Entity('uuid')] public Donald $donaldUuid,
        #[Entity('minnie')] public Donald $donaldRef,
        #[Entity('enum')] public Donald $donaldEnum,
        #[Entity('enumInt')] public Donald $donaldEnumInt,
        #[Entity('hidden')] public Minnie $minnie,
        #[Entity] public array $donalds,
        #[Entity('name')] public array $donaldNames,
        #[Entity('uuid')] public array $donaldUuids,
        #[Entity('minnie')] public array $donaldRefs,
        #[Entity('name')] public ?Donald $donaldNullable,
        #[Entity('uuid')] public ?array $donaldsNullable,
        #[Entity('name')] public ?Donald $donaldNullableOptional = null,
        #[Entity('name')] public ?array $donaldsNullableOptional = null,
        #[Entity('name')] public array $donaldsOptional = [],
    ) {
    }
}
