<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectDoctrine\GQL;

use UXF\GraphQL\Attribute\Query;

final readonly class DoctrineInputQuery
{
    #[Query('item')]
    public function __invoke(DoctrineInput $input): bool
    {
        return true;
    }
}
