<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\Functional\Validation;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpKernel\KernelInterface;
use UXF\GraphQLTests\Functional\BaseGraphQLTestCase;
use UXF\GraphQLTests\ProjectValidation\ValidationKernel;

class ValidationSmokeTest extends BaseGraphQLTestCase
{
    public function testValid(): void
    {
        $this->gql(__DIR__ . '/doc/ValidationQuery.graphql', [
            'input' => [
                'string' => 'OK',
                'number' => 1,
            ],
        ]);
    }

    public function testInvalid(): void
    {
        $this->gql(__DIR__ . '/doc/ValidationQuery.graphql', [
            'input' => [
                'string' => '',
                'number' => 0,
            ],
        ]);
    }

    public function testGen(): void
    {
        $app = new Application(self::bootKernel());
        $cmd = $app->find('uxf:gql-gen');
        $tester = new CommandTester($cmd);
        $tester->execute([]);
        $tester->assertCommandIsSuccessful();

        self::assertFileEquals(__DIR__ . '/expected/schema.graphql', __DIR__ . '/../../generated/validation.graphql');
    }

    /**
     * @param array<mixed> $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        return new ValidationKernel('test', true);
    }
}
