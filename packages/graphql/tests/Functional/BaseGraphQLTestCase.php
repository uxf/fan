<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\Functional;

use UXF\Core\Test\Client;
use UXF\Core\Test\WebTestCase;

abstract class BaseGraphQLTestCase extends WebTestCase
{
    protected Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = self::createClient();
    }

    /**
     * @param array<mixed> $variables
     */
    protected function gql(string $queryPath, array $variables = [], bool $logged = false, ?string $operationName = null): void
    {
        $headers = [];
        if ($logged) {
            $headers['HTTP_AUTH_TOKEN'] = 'testUser';
        }

        $gql = file_get_contents($queryPath);

        $this->client->post('/graphql', [
            'query' => $gql,
            'variables' => $variables,
            'operationName' => $operationName,
        ], $headers);
        self::assertResponseIsSuccessful();

        $this->assertSnapshot($this->client->getResponseData());
    }
}
