<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\Functional\Basic;

use stdClass;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpKernel\KernelInterface;
use UXF\GraphQLTests\Functional\BaseGraphQLTestCase;
use UXF\GraphQLTests\ProjectBasic\BasicKernel;
use UXF\GraphQLTests\ProjectShared\Enum\GoofyEnum;
use UXF\GraphQLTests\ProjectShared\Enum\PlutoEnum;

class BasicSmokeTest extends BaseGraphQLTestCase
{
    public function test(): void
    {
        $this->gql(__DIR__ . '/doc/Query.graphql', [
            'json' => [
                'hello' => [
                    'myFriend' => [1, 2, 3],
                ],
            ],
        ], operationName: 'Query');

        $this->gql(__DIR__ . '/doc/Login.graphql');
        self::assertResponseHeaderSame('Auth-Token', '1');

        $this->gql(__DIR__ . '/doc/Mutation.graphql', [
            'json' => [
                'hello' => [
                    'myFriend' => [1, 2, 3],
                ],
            ],
        ], true);

        $this->gql(__DIR__ . '/doc/GoofyQuery.graphql', [], true);

        $this->gql(__DIR__ . '/doc/DonaldQuery.graphql', [
            'id' => 1,
            'ids' => [1],
        ]);

        $this->gql(__DIR__ . '/doc/DonaldQuery.graphql', [
            'id' => 2,
            'ids' => [2],
        ]);

        $this->gql(__DIR__ . '/doc/GoofyMutation.graphql');

        $this->gql(__DIR__ . '/doc/GoofyMutation2.graphql', [
            'goofy' => [],
        ]);

        $this->gql(__DIR__ . '/doc/MickeyQuery.graphql', [
            'uuid' => '00000000-0000-0000-0000-000000000001',
            'minnie' => 2,
            'enum' => PlutoEnum::OK_WOW,
            'enumInt' => GoofyEnum::FIRST->name,
            'names' => ['WOW!'],
        ]);

        $this->gql(__DIR__ . '/doc/DaisyQuery.graphql', [
            'string' => 'x',
            'array' => ['x'],
            'date' => '2020-01-02',
            'dateTime' => '2020-01-02T10:00:00',
            'time' => '10:00:00',
            'phone' => '777666777',
            'email' => 'info@uxf.cz',
            'ninCze' => '930201/3545',
            'banCze' => '123/0300',
            'uuid' => '00000000-0000-0000-0000-000000000001',
            'money' => [
                'amount' => '1',
                'currency' => 'CZK',
            ],
            'decimal' => '3.14',
            'url' => 'https://uxf.cz',
            'enum' => 'OK',
            'enumInt' => 'FIRST',
            'json' => new stdClass(),
        ]);
    }

    public function testInvalid(): void
    {
        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(date: '2010-01-50'),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(dateTime: '2010-01-50'),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(time: '25:00:00'),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(phone: 'xx'),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(uuid: 'b477e7cd-5b6f-46e5-aeda-a88467e9c7f'),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(money: [
                'amount' => '1,0',
                'currency' => 'CZK',
            ]),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(enum: 'XX'),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(long: 'XX'),
        ]);

        $this->gql(__DIR__ . '/doc/Daisy2Query.graphql');

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(ninCze: 'xx'),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(banCze: 'xx'),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(email: 'xx'),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(decimal: 'xx'),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(url: 'xx'),
        ]);

        $this->gql(__DIR__ . '/doc/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(dateTime: '2010-01-29'),
        ]);
    }

    public function testJson(): void
    {
        $this->gql(__DIR__ . '/doc/JsonQuery.graphql', [
            'json' => 1,
        ]);

        $this->gql(__DIR__ . '/doc/JsonQuery.graphql', [
            'json' => [],
        ]);

        $this->gql(__DIR__ . '/doc/JsonQuery.graphql', [
            'json' => new stdClass(),
        ]);

        $this->gql(__DIR__ . '/doc/JsonQuery.graphql', [
            'json' => "hello",
        ]);

        $this->gql(__DIR__ . '/doc/JsonQuery.graphql', [
            'json' => '{vrrr}',
        ]);
    }

    public function testGen(): void
    {
        $kernel = self::bootKernel();

        $app = new Application($kernel);
        $cmd = $app->find('uxf:gql-gen');
        $tester = new CommandTester($cmd);
        $tester->execute([]);
        $tester->assertCommandIsSuccessful();

        self::assertFileEquals(__DIR__ . '/expected/schema.graphql', __DIR__ . '/../../generated/basic.graphql');
    }

    /**
     * @return array<string, mixed>
     */
    private static function createPlutoInput(
        mixed $string = 'hello',
        mixed $array = ['kitty'],
        mixed $date = '2010-01-28',
        mixed $dateTime = '2010-01-28T11:27:25+01:00',
        mixed $time = '15:30:50',
        mixed $phone = '+420777666777',
        mixed $email = 'info@uxf.cz',
        mixed $ninCze = '930201/3545',
        mixed $banCze = '123/0300',
        mixed $uuid = 'b477e7cd-5b6f-46e5-aeda-a88467e9c7fc',
        mixed $money = [
            'amount' => '123.45',
            'currency' => 'CZK',
        ],
        mixed $decimal = '3.14',
        mixed $url = 'https://uxf.cz',
        mixed $enum = 'OK',
        mixed $enumInt = 'FIRST',
        mixed $json = [],
        mixed $long = 5147483647,
    ): array {
        return [
            'string' => $string,
            'array' => $array,
            'date' => $date,
            'dateTime' => $dateTime,
            'time' => $time,
            'phone' => $phone,
            'email' => $email,
            'ninCze' => $ninCze,
            'banCze' => $banCze,
            'uuid' => $uuid,
            'money' => $money,
            'decimal' => $decimal,
            'url' => $url,
            'enum' => $enum,
            'enumInt' => $enumInt,
            'json' => $json,
            'long' => $long,
        ];
    }

    /**
     * @param array<mixed> $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        return new BasicKernel('test', true);
    }
}
