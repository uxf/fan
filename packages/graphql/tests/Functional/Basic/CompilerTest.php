<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\Functional\Basic;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;
use UXF\GraphQLTests\ProjectBasic\BasicKernel;
use UXF\GraphQLTests\ProjectBasic\GQL\Mutation\GoofyMutation;
use UXF\GraphQLTests\ProjectBasic\GQL\Mutation\LoginMutation;
use UXF\GraphQLTests\ProjectBasic\GQL\Mutation\PlutoMutation;
use UXF\GraphQLTests\ProjectBasic\GQL\Query\DaysiQuery;
use UXF\GraphQLTests\ProjectBasic\GQL\Query\DonaldQuery;
use UXF\GraphQLTests\ProjectBasic\GQL\Query\GoofyQuery;
use UXF\GraphQLTests\ProjectBasic\GQL\Query\MickeyQuery;
use UXF\GraphQLTests\ProjectBasic\GQL\Query\PlutoQuery;

class CompilerTest extends KernelTestCase
{
    public function test(): void
    {
        $expected = [
            GoofyMutation::class,
            LoginMutation::class,
            PlutoMutation::class,
            DaysiQuery::class,
            DonaldQuery::class,
            GoofyQuery::class,
            MickeyQuery::class,
            PlutoQuery::class,
        ];

        $names = self::getContainer()->getParameter('uxf_graphql.controller_names');
        self::assertSame($expected, $names);
    }


    /**
     * @param array<mixed> $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        return new BasicKernel('test', true);
    }
}
