<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\Functional\Doctrine;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpKernel\KernelInterface;
use UXF\GraphQLTests\Functional\BaseGraphQLTestCase;
use UXF\GraphQLTests\ProjectDoctrine\DoctrineKernel;

class DoctrineSmokeTest extends BaseGraphQLTestCase
{
    public function testInput(): void
    {
        $this->gql(__DIR__ . '/doc/ItemQuery.graphql', [
            'input' => [
                'donald' => 1,
                'donaldName' => 'WOW!',
                'donaldUuid' => '00000000-0000-0000-0000-000000000001',
                'donaldRef' => 2,
                'donaldEnum' => 'OK',
                'donaldEnumInt' => 'FIRST',
                'donalds' => [1],
                'minnie' => 0,
                'donaldNames' => ['WOW!'],
                'donaldUuids' => ['00000000-0000-0000-0000-000000000001'],
                'donaldRefs' => [2],
                'donaldNullable' => null,
                'donaldsNullable' => null,
            ],
        ]);
    }

    public function testArgs(): void
    {
        $this->gql(__DIR__ . '/doc/ArgsQuery.graphql', [
            'donald' => 1,
            'donaldName' => 'WOW!',
            'donaldUuid' => '00000000-0000-0000-0000-000000000001',
            'donaldRef' => 2,
            'donaldEnum' => 'OK',
            'donaldEnumInt' => 'FIRST',
            'minnie' => 0,
            'donalds' => [1],
            'donaldNames' => ['WOW!'],
            'donaldUuids' => ['00000000-0000-0000-0000-000000000001'],
            'donaldRefs' => [2],
            'donaldNullable' => null,
            'donaldsNullable' => null,
        ]);
    }

    public function testArgsNotFoundSingle(): void
    {
        $this->gql(__DIR__ . '/doc/ArgsQuery.graphql', [
            'donald' => 0,
            'donaldName' => 'WOW!',
            'donaldUuid' => '00000000-0000-0000-0000-000000000001',
            'donaldRef' => 2,
            'donaldEnum' => 'OK',
            'donaldEnumInt' => 'FIRST',
            'minnie' => 0,
            'donalds' => [1],
            'donaldNames' => ['WOW!'],
            'donaldUuids' => ['00000000-0000-0000-0000-000000000001'],
            'donaldRefs' => [2],
            'donaldNullable' => null,
            'donaldsNullable' => null,
        ]);
    }

    public function testArgsNotFoundInArray(): void
    {
        $this->gql(__DIR__ . '/doc/ArgsQuery.graphql', [
            'donald' => 1,
            'donaldName' => 'WOW!',
            'donaldUuid' => '00000000-0000-0000-0000-000000000001',
            'donaldRef' => 2,
            'donaldEnum' => 'OK',
            'donaldEnumInt' => 'FIRST',
            'minnie' => 0,
            'donalds' => [0, 100],
            'donaldNames' => ['WOW!'],
            'donaldUuids' => ['00000000-0000-0000-0000-000000000001'],
            'donaldRefs' => [2],
            'donaldNullable' => null,
            'donaldsNullable' => null,
        ]);
    }

    public function testGen(): void
    {
        $app = new Application(self::bootKernel());
        $cmd = $app->find('uxf:gql-gen');
        $tester = new CommandTester($cmd);
        $tester->execute([]);
        $tester->assertCommandIsSuccessful();

        self::assertFileEquals(__DIR__ . '/expected/schema.graphql', __DIR__ . '/../../generated/doctrine.graphql');
    }

    /**
     * @param array<mixed> $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        return new DoctrineKernel('test', true);
    }
}
