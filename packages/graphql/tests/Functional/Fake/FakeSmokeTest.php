<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\Functional\Fake;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpKernel\KernelInterface;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\GraphQL\Type\Json;
use UXF\GraphQLTests\Functional\BaseGraphQLTestCase;
use UXF\GraphQLTests\ProjectFake\FakeKernel;

class FakeSmokeTest extends BaseGraphQLTestCase
{
    public function testGen(): void
    {
        $app = new Application(self::bootKernel());
        $cmd = $app->find('uxf:gql-gen');
        $tester = new CommandTester($cmd);
        $tester->execute([]);
        $tester->assertCommandIsSuccessful();

        require __DIR__ . '/../../generated/fake/TGolfInput.php';
        $class = 'App\Tests\TGolfInput';
        self::assertTrue(class_exists($class));

        $this->gql(__DIR__ . '/doc/FakeQuery.graphql', [
            'input' => new $class(
                string: '',
                array: [],
                date: new Date('2000-01-01'),
                dateTime: new DateTime('2000-01-01 10:00'),
                time: new Time('10:00'),
                phone: Phone::of('731000999'),
                email: Email::of('test@test.cz'),
                ninCze: null,
                banCze: null,
                uuid: null,
                money: null,
                decimal: null,
                url: null,
                enum: null,
                enumInt: null,
                json: new Json([1]),
                long: null,
                climb: null,
                donald: 1,
            ),
        ]);
    }

    /**
     * @param array<mixed> $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        return new FakeKernel('test', true);
    }
}
