<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\Functional\Modifier;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpKernel\KernelInterface;
use UXF\GraphQLTests\Functional\BaseGraphQLTestCase;
use UXF\GraphQLTests\ProjectModifier\ModifierKernel;

class ModifierSmokeTest extends BaseGraphQLTestCase
{
    public function test(): void
    {
        $this->gql(__DIR__ . '/doc/PalmexQuery.graphql');
    }

    public function testGen(): void
    {
        $app = new Application(self::bootKernel());
        $cmd = $app->find('uxf:gql-gen');
        $tester = new CommandTester($cmd);
        $tester->execute([]);
        $tester->assertCommandIsSuccessful();

        self::assertFileEquals(__DIR__ . '/expected/schema.graphql', __DIR__ . '/../../generated/modifier.graphql');
    }

    /**
     * @param array<mixed> $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        return new ModifierKernel('test', true);
    }
}
