<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\Functional\NotSet;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpKernel\KernelInterface;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\Type\Date;
use UXF\GraphQLTests\Functional\BaseGraphQLTestCase;
use UXF\GraphQLTests\ProjectNotSet\GQL\PatchArrayInput;
use UXF\GraphQLTests\ProjectNotSet\GQL\PatchInnerInput;
use UXF\GraphQLTests\ProjectNotSet\GQL\PatchInput;
use UXF\GraphQLTests\ProjectNotSet\InputCollector;
use UXF\GraphQLTests\ProjectNotSet\NotSetKernel;

class NotSetSmokeTest extends BaseGraphQLTestCase
{
    public function testEmpty(): void
    {
        $this->gql(__DIR__ . '/doc/PatchMutation.graphql', [
            'input' => [],
        ]);

        $collector = self::getContainer()->get(InputCollector::class);
        self::assertInstanceOf(InputCollector::class, $collector);
        self::assertEquals([[
            'input' => new PatchInput(),
            'inputs' => new NotSet(),
        ]], $collector->items);
    }

    public function testInner(): void
    {
        $this->gql(__DIR__ . '/doc/PatchMutation.graphql', [
            'input' => [
                'string' => 'X',
                'int' => null,
                'inner' => [
                    'date' => '2000-01-31',
                ],
            ],
            'inputs' => [
                [
                    'dates' => ['2000-01-31'],
                ],
            ],
        ]);

        $collector = self::getContainer()->get(InputCollector::class);
        self::assertInstanceOf(InputCollector::class, $collector);
        self::assertEquals([[
            'input' => new PatchInput(
                string: 'X',
                int: null,
                inner: new PatchInnerInput(new Date('2000-01-31')),
            ),
            'inputs' => [
                new PatchArrayInput([new Date('2000-01-31')]),
            ],
        ]], $collector->items);
    }

    public function testGen(): void
    {
        $app = new Application(self::bootKernel());
        $cmd = $app->find('uxf:gql-gen');
        $tester = new CommandTester($cmd);
        $tester->execute([]);
        $tester->assertCommandIsSuccessful();

        self::assertFileEquals(__DIR__ . '/expected/schema.graphql', __DIR__ . '/../../generated/not-set.graphql');
    }

    /**
     * @param array<mixed> $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        return new NotSetKernel('test', true);
    }
}
