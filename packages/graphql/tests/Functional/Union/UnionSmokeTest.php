<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\Functional\Union;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpKernel\KernelInterface;
use UXF\GraphQLTests\Functional\BaseGraphQLTestCase;
use UXF\GraphQLTests\ProjectUnion\UnionKernel;

class UnionSmokeTest extends BaseGraphQLTestCase
{
    public function testSingle(): void
    {
        $this->gql(__DIR__ . '/doc/SingleQuery.graphql');
    }

    public function testArray(): void
    {
        $this->gql(__DIR__ . '/doc/ArrayQuery.graphql');
    }

    public function testBag(): void
    {
        $this->gql(__DIR__ . '/doc/BagQuery.graphql');
    }

    public function testGen(): void
    {
        $app = new Application(self::bootKernel());
        $cmd = $app->find('uxf:gql-gen');
        $tester = new CommandTester($cmd);
        $tester->execute([]);
        $tester->assertCommandIsSuccessful();

        self::assertFileEquals(__DIR__ . '/expected/schema.graphql', __DIR__ . '/../../generated/union.graphql');
    }

    /**
     * @param array<mixed> $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        return new UnionKernel('test', true);
    }
}
