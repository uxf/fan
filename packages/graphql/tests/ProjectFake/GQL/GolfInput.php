<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectFake\GQL;

use Ramsey\Uuid\UuidInterface;
use UXF\Core\Attribute\Entity;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\GraphQL\Attribute\Field;
use UXF\GraphQL\Attribute\Input;
use UXF\GraphQL\Type\Json;
use UXF\GraphQLTests\ProjectShared\Entity\Donald;
use UXF\GraphQLTests\ProjectShared\Enum\GoofyEnum;
use UXF\GraphQLTests\ProjectShared\Enum\PlutoEnum;

#[Input(generateFake: true)]
final readonly class GolfInput
{
    /**
     * @param string[] $array
     * @param Donald[] $donalds
     * @param Donald[]|null $donalds2
     * @param string[]|null|NotSet $notSets
     */
    public function __construct(
        public string $string,
        public array $array,
        public Date $date,
        public DateTime $dateTime,
        public Time $time,
        public Phone $phone,
        public Email $email,
        public ?NationalIdentificationNumberCze $ninCze,
        public ?BankAccountNumberCze $banCze,
        public ?UuidInterface $uuid,
        public ?Money $money,
        public ?Decimal $decimal,
        public ?Url $url,
        public ?PlutoEnum $enum,
        public ?GoofyEnum $enumInt,
        public ?Json $json,
        #[Field(inputType: 'Long')] public ?int $long,
        public ?ClimbInput $climb,
        #[Entity] public Donald $donald,
        #[Entity] public ?Donald $donald2 = null,
        #[Entity] public array $donalds = [],
        #[Entity] public ?array $donalds2 = null,
        public string|null|NotSet $notSet = new NotSet(),
        public array|null|NotSet $notSets = new NotSet(),
    ) {
    }
}
