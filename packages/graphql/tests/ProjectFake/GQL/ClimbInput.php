<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectFake\GQL;

use UXF\GraphQL\Attribute\Input;

#[Input(generateFake: true)]
final readonly class ClimbInput
{
    public function __construct(
        public int $id,
    ) {
    }
}
