<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectFake\GQL;

use UXF\GraphQL\Attribute\Query;

final readonly class FakeQuery
{
    #[Query('fake')]
    public function __invoke(GolfInput $input): bool
    {
        return true;
    }
}
