<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectShared;

use Ramsey\Uuid\Doctrine\UuidType;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\DependencyInjection\Loader\Configurator\ServicesConfigurator;
use UXF\Core\Test\Client;

final readonly class KernelHelper
{
    public static function configure(ContainerConfigurator $container, bool $withDoctrine = false): ServicesConfigurator
    {
        $services = $container->services();

        $services->defaults()
            ->autowire()
            ->autoconfigure();

        $services->set(Client::class);
        $services->alias('test.client', Client::class)->public();

        $services->set(Fixtures::class);

        $container->extension('framework', [
            'test' => true,
            'validation' => [
                'email_validation_mode' => 'html5',
            ],
            'http_method_override' => false,
        ]);

        if ($withDoctrine) {
            $container->extension('doctrine', [
                'dbal' => [
                    'url' => 'sqlite:///%kernel.project_dir%/var/db.sqlite',
                    'types' => [
                        'uuid' => UuidType::class,
                    ],
                ],
                'orm' => [
                    'naming_strategy' => 'doctrine.orm.naming_strategy.underscore_number_aware',
                    'auto_mapping' => true,
                    'report_fields_where_declared' => true,
                    'enable_lazy_ghost_objects' => true,
                    'mappings' => [
                        'test' => [
                            'type' => 'attribute',
                            'dir' => __DIR__ . '/../ProjectShared/Entity',
                            'prefix' => 'UXF\GraphQLTests\ProjectShared\Entity',
                        ],
                    ],
                ],
            ]);
        }

        return $services;
    }
}
