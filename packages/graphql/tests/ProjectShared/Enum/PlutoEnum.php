<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectShared\Enum;

enum PlutoEnum: string
{
    case OK_WOW = 'OK';
    case ERROR = 'ERROR';
}
