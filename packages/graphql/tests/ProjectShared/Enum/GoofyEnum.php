<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectShared\Enum;

enum GoofyEnum: int
{
    case FIRST = 1;
    case SECOND = 2;
}
