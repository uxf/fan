<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectShared\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity]
class User implements UserInterface
{
    #[ORM\Id, ORM\Column]
    public string $id = '1';

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    public function eraseCredentials(): void
    {
    }

    public function getUserIdentifier(): string
    {
        assert($this->id !== '');
        return $this->id;
    }
}
