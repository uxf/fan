<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectShared\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use UXF\GraphQLTests\ProjectShared\Enum\GoofyEnum;
use UXF\GraphQLTests\ProjectShared\Enum\PlutoEnum;

#[ORM\Entity]
readonly class Donald
{
    public function __construct(
        #[ORM\Id, ORM\Column]
        public int $id,
        #[ORM\Column(type: 'uuid')]
        public UuidInterface $uuid,
        #[ORM\Column]
        public string $name,
        #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
        public Minnie $minnie,
        #[ORM\Column]
        public PlutoEnum $enum,
        #[ORM\Column]
        public GoofyEnum $enumInt,
    ) {
    }
}
