<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectShared\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Minnie extends MinnieParent
{
}
