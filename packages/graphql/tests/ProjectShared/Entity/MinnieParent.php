<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectShared\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'minnie')]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn('type', 'string')]
#[ORM\DiscriminatorMap([
    'm' => Minnie::class,
    'p' => MinnieParent::class,
])]
abstract class MinnieParent
{
    #[ORM\Id, ORM\Column]
    private int $id;

    #[ORM\Column]
    private int $hidden;

    public function __construct(int $id, int $hidden)
    {
        $this->id = $id;
        $this->hidden = $hidden;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getHidden(): int
    {
        return $this->hidden;
    }
}
