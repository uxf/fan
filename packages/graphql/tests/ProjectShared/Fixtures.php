<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectShared;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use UXF\GraphQLTests\ProjectShared\Entity\Donald;
use UXF\GraphQLTests\ProjectShared\Entity\Minnie;
use UXF\GraphQLTests\ProjectShared\Enum\GoofyEnum;
use UXF\GraphQLTests\ProjectShared\Enum\PlutoEnum;

final class Fixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $minnie = new Minnie(2, 0);
        $manager->persist($minnie);
        $donald = new Donald(1, Uuid::fromInteger('1'), 'WOW!', $minnie, PlutoEnum::OK_WOW, GoofyEnum::FIRST);
        $manager->persist($donald);
        $manager->flush();
    }
}
