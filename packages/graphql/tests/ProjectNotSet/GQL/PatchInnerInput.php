<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectNotSet\GQL;

use Ramsey\Uuid\UuidInterface;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\GraphQL\Attribute\Input;
use UXF\GraphQL\Type\Json;
use UXF\GraphQLTests\ProjectShared\Enum\GoofyEnum;
use UXF\GraphQLTests\ProjectShared\Enum\PlutoEnum;

#[Input]
final readonly class PatchInnerInput
{
    public function __construct(
        public Date|null|NotSet $date = new NotSet(),
        public DateTime|null|NotSet $dateTime = new NotSet(),
        public Time|null|NotSet $time = new NotSet(),
        public Phone|null|NotSet $phone = new NotSet(),
        public Email|null|NotSet $email = new NotSet(),
        public NationalIdentificationNumberCze|null|NotSet $ninCze = new NotSet(),
        public BankAccountNumberCze|null|NotSet $banCze = new NotSet(),
        public UuidInterface|null|NotSet $uuid = new NotSet(),
        public Money|null|NotSet $money = new NotSet(),
        public Decimal|null|NotSet $decimal = new NotSet(),
        public Url|null|NotSet $url = new NotSet(),
        public PlutoEnum|null|NotSet $enum = new NotSet(),
        public GoofyEnum|null|NotSet $enumInt = new NotSet(),
        public Json|null|NotSet $json = new NotSet(),
    ) {
    }
}
