<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectNotSet\GQL;

use UXF\Core\Http\Request\NotSet;
use UXF\GraphQL\Attribute\Mutation;
use UXF\GraphQLTests\ProjectNotSet\InputCollector;

final readonly class PatchMutation
{
    public function __construct(
        private InputCollector $collector,
    ) {
    }

    /**
     * @param PatchArrayInput[]|null|NotSet $inputs
     */
    #[Mutation('patch')]
    public function __invoke(PatchInput $input, array|null|NotSet $inputs = new NotSet()): bool
    {
        $this->collector->items[] = [
            'input' => $input,
            'inputs' => $inputs,
        ];

        return true;
    }
}
