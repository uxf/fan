<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectNotSet\GQL;

use UXF\GraphQL\Attribute\Query;

final readonly class HealthQuery
{
    #[Query('health')]
    public function __invoke(): bool
    {
        return true;
    }
}
