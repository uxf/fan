<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectNotSet\GQL;

use UXF\Core\Attribute\Entity;
use UXF\Core\Http\Request\NotSet;
use UXF\GraphQL\Attribute\Input;
use UXF\GraphQLTests\ProjectShared\Entity\Donald;

#[Input]
final readonly class PatchInput
{
    /**
     * @param string[]|null|NotSet $stringArray
     * @param int[]|null|NotSet $intArray
     * @param float[]|null|NotSet $floatArray
     * @param bool[]|null|NotSet $boolArray
     * @param Donald[]|null|NotSet $entityArray
     * @param PatchInnerInput[]|null|NotSet $innerArray
     */
    public function __construct(
        public string|null|NotSet $string = new NotSet(),
        public int|null|NotSet $int = new NotSet(),
        public float|null|NotSet $float = new NotSet(),
        public bool|null|NotSet $bool = new NotSet(),
        #[Entity] public Donald|null|NotSet $entity = new NotSet(),
        public PatchInnerInput|null|NotSet $inner = new NotSet(),
        public array|null|NotSet $stringArray = new NotSet(),
        public array|null|NotSet $intArray = new NotSet(),
        public array|null|NotSet $floatArray = new NotSet(),
        public array|null|NotSet $boolArray = new NotSet(),
        #[Entity] public array|null|NotSet $entityArray = new NotSet(),
        public array|null|NotSet $innerArray = new NotSet(),
    ) {
    }
}
