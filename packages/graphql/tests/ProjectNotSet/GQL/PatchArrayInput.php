<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectNotSet\GQL;

use Ramsey\Uuid\UuidInterface;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\GraphQL\Attribute\Input;
use UXF\GraphQL\Type\Json;
use UXF\GraphQLTests\ProjectShared\Enum\GoofyEnum;
use UXF\GraphQLTests\ProjectShared\Enum\PlutoEnum;

#[Input]
final readonly class PatchArrayInput
{
    /**
     * @param Date[]|null|NotSet $dates
     * @param DateTime[]|null|NotSet $dateTimes
     * @param Time[]|null|NotSet $times
     * @param Phone[]|null|NotSet $phones
     * @param Email[]|null|NotSet $emails
     * @param NationalIdentificationNumberCze[]|null|NotSet $ninCzes
     * @param BankAccountNumberCze[]|null|NotSet $banCzes
     * @param UuidInterface[]|null|NotSet $uuids
     * @param Money[]|null|NotSet $moneys
     * @param Decimal[]|null|NotSet $decimals
     * @param Url[]|null|NotSet $urls
     * @param PlutoEnum[]|null|NotSet $enums
     * @param GoofyEnum[]|null|NotSet $enumInts
     * @param Json[]|null|NotSet $jsons
     */
    public function __construct(
        public array|null|NotSet $dates = new NotSet(),
        public array|null|NotSet $dateTimes = new NotSet(),
        public array|null|NotSet $times = new NotSet(),
        public array|null|NotSet $phones = new NotSet(),
        public array|null|NotSet $emails = new NotSet(),
        public array|null|NotSet $ninCzes = new NotSet(),
        public array|null|NotSet $banCzes = new NotSet(),
        public array|null|NotSet $uuids = new NotSet(),
        public array|null|NotSet $moneys = new NotSet(),
        public array|null|NotSet $decimals = new NotSet(),
        public array|null|NotSet $urls = new NotSet(),
        public array|null|NotSet $enums = new NotSet(),
        public array|null|NotSet $enumInts = new NotSet(),
        public array|null|NotSet $jsons = new NotSet(),
    ) {
    }
}
