<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectNotSet;

final class InputCollector
{
    /** @var array<string, mixed>[] */
    public array $items = [];
}
