<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectModifier\GQL;

use UXF\GraphQL\Attribute\Type;

#[Type('Palmex')]
final readonly class PalmexType
{
    /**
     * @param (int|null)[]|null $c
     * @param (int|null)[] $d
     * @param int[]|null $e
     * @param int[] $f
     */
    public function __construct(
        public ?int $a, // SIMPLE
        public int $b, // SIMPLE_NON_NULL
        public ?array $c, // ARRAY
        public array $d, // ARRAY_NON_NULL
        public ?array $e, // ARRAY_ITEM_NON_NULL
        public array $f, // ARRAY_AND_ITEM_NON_NULL
    ) {
    }

    public function xA(): ?int
    {
        return $this->a;
    }

    public function xB(): int
    {
        return $this->b;
    }

    /**
     * @return (int|null)[]|null
     */
    public function xC(): ?array
    {
        return $this->c;
    }

    /**
     * @return (int|null)[]
     */
    public function xD(): array
    {
        return $this->d;
    }

    /**
     * @return int[]|null
     */
    public function xE(): ?array
    {
        return $this->e;
    }

    /**
     * @return int[]
     */
    public function xF(): array
    {
        return $this->f;
    }
}
