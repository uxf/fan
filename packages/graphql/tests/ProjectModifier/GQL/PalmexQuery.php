<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectModifier\GQL;

use UXF\GraphQL\Attribute\Query;

final readonly class PalmexQuery
{
    #[Query('palmex')]
    public function __invoke(): PalmexType
    {
        return new PalmexType(
            a: null,
            b: 0,
            c: null,
            d: [],
            e: null,
            f: [],
        );
    }
}
