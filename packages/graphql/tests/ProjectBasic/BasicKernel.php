<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use Symfony\Component\Security\Core\User\InMemoryUser;
use UXF\Core\UXFCoreBundle;
use UXF\GraphQL\UXFGraphQLBundle;
use UXF\GraphQLTests\ProjectBasic\Security\TestAuthenticator;
use UXF\GraphQLTests\ProjectBasic\Staff\Coyote;
use UXF\GraphQLTests\ProjectBasic\Staff\CoyoteResolver;
use UXF\GraphQLTests\ProjectBasic\Staff\MickeyService;
use UXF\GraphQLTests\ProjectBasic\Staff\UserResolver;
use UXF\GraphQLTests\ProjectShared\Entity\User;
use UXF\GraphQLTests\ProjectShared\KernelHelper;
use UXF\Hydrator\UXFHydratorBundle;
use UXF\Security\Service\AuthUserProvider;
use UXF\Security\UXFSecurityBundle;
use UXF\SecurityTests\Project\TestAuthUserProvider;

class BasicKernel extends BaseKernel
{
    use MicroKernelTrait;

    /**
     * @inheritDoc
     */
    public function registerBundles(): iterable
    {
        $bundles = [
            FrameworkBundle::class,
            DoctrineBundle::class,
            DoctrineFixturesBundle::class,
            TwigBundle::class,
            MonologBundle::class,
            SecurityBundle::class,
            UXFCoreBundle::class,
            UXFHydratorBundle::class,
            UXFGraphQLBundle::class,
            UXFSecurityBundle::class,
        ];

        foreach ($bundles as $bundle) {
            yield new $bundle();
        }
    }

    public function getCacheDir(): string
    {
        return __DIR__ . '/../../var/cache/test-basic';
    }

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $services = KernelHelper::configure($container, withDoctrine: true);

        $container->extension('security', [
            'providers' => [
                'in_memory' => [
                    'memory' => [
                        'users' => [
                            'testUser' => [
                                'password' => 'pass',
                                'roles' => ['ROLE_USER'],
                            ],
                        ],
                    ],
                ],
            ],
            'firewalls' => [
                'main' => [
                    'custom_authenticators' => [
                        TestAuthenticator::class,
                    ],
                ],
            ],
        ]);

        $container->extension('uxf_security', [
            'user_class' => User::class,
            'base_url' => 'https://uxf.cz',
            'public_key' => '',
            'private_key' => '',
            // frozen time
            'access_token_lifetime' => 'P10Y',
        ]);
        $services->set(AuthUserProvider::class, TestAuthUserProvider::class);

        $container->extension('uxf_graphql', [
            'destination' => __DIR__ . '/../generated/basic.graphql',
            'sources' => [
                __DIR__ . '/../../tests/ProjectBasic',
            ],
            'injected' => [
                Coyote::class => CoyoteResolver::class,
                InMemoryUser::class => UserResolver::class,
            ],
        ]);

        $services->set(TestAuthenticator::class);
        $services->set(MickeyService::class);
        $services->set(CoyoteResolver::class);
        $services->set(UserResolver::class);
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import(__DIR__ . '/../../config/routes.php');
    }
}
