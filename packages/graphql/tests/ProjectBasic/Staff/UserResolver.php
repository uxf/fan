<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\Staff;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\InMemoryUser;
use UXF\Core\Exception\BasicException;
use UXF\GraphQL\Service\Injector\InjectedArgument;

final readonly class UserResolver
{
    public function __construct(
        private Security $security,
    ) {
    }

    public function __invoke(Request $request, InjectedArgument $argument): InMemoryUser
    {
        $user = $this->security->getUser();
        if (!$user instanceof InMemoryUser) {
            throw BasicException::unauthorized();
        }
        return $user;
    }
}
