<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\Staff;

use Symfony\Component\HttpFoundation\Request;
use UXF\GraphQL\Service\Injector\InjectedArgument;

class CoyoteResolver
{
    public function __invoke(Request $request, InjectedArgument $argument): Coyote
    {
        assert($argument->type === Coyote::class);
        return new Coyote();
    }
}
