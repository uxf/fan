<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\Staff;

class MickeyService
{
    public function get(): string
    {
        return 'mickey mouse';
    }
}
