<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\GQL\Query;

use UXF\Core\Attribute\Entity;
use UXF\GraphQL\Attribute\Query;
use UXF\GraphQLTests\ProjectBasic\GQL\Type\Donald as DonaldType;
use UXF\GraphQLTests\ProjectShared\Entity\Donald;

class MickeyQuery
{
    /**
     * @param Donald[] $donalds
     * @param Donald[]|null $donaldsNullable
     * @param Donald[]|null $donaldsNullableOptional
     * @param Donald[] $donaldsOptional
     */
    #[Query(name: 'mickey')]
    public function __invoke(
        #[Entity('uuid')] Donald $donald,
        #[Entity('minnie')] Donald $donaldX,
        #[Entity('enum')] Donald $donaldY,
        #[Entity('enumInt')] Donald $donaldZ,
        #[Entity('name')] array $donalds,
        #[Entity('name')] ?Donald $donaldNullable,
        #[Entity('uuid')] ?array $donaldsNullable,
        #[Entity('name')] ?Donald $donaldNullableOptional = null,
        #[Entity('name')] ?array $donaldsNullableOptional = null,
        #[Entity('name')] array $donaldsOptional = [],
    ): DonaldType {
        return new DonaldType($donald);
    }
}
