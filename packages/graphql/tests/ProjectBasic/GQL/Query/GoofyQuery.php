<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\GQL\Query;

use UXF\Core\Attribute\Security;
use UXF\GraphQL\Attribute\Query;
use UXF\GraphQLTests\ProjectBasic\GQL\Input\PlutoInput;
use UXF\GraphQLTests\ProjectBasic\GQL\Type\Goofy;
use UXF\GraphQLTests\ProjectBasic\Security\UserRole;

class GoofyQuery
{
    /**
     * @param PlutoInput[] $plutos
     * @return Goofy[]|null
     */
    #[Security(UserRole::ROLE_ROOT)]
    #[Query(name: 'goofy')]
    public function __invoke(array $plutos): ?array
    {
        return null;
    }
}
