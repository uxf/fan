<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\GQL\Query;

use UXF\GraphQL\Attribute\Inject;
use UXF\GraphQL\Attribute\Query;
use UXF\GraphQLTests\ProjectBasic\GQL\Input\PlutoInput;
use UXF\GraphQLTests\ProjectBasic\GQL\Type\Pluto;
use UXF\GraphQLTests\ProjectBasic\Staff\Coyote;

class PlutoQuery
{
    #[Query(name: 'pluto')]
    public function __invoke(PlutoInput $pluto, #[Inject] Coyote $coyote): Pluto
    {
        return new Pluto(
            string: $pluto->string,
            array: $pluto->array,
            date: $pluto->date,
            dateTime: $pluto->dateTime,
            time: $pluto->time,
            phone: $pluto->phone,
            email: $pluto->email,
            ninCze: $pluto->ninCze,
            banCze: $pluto->banCze,
            uuid: $pluto->uuid,
            money: $pluto->money,
            decimal: $pluto->decimal,
            url: $pluto->url,
            enum: $pluto->enum,
            enumInt: $pluto->enumInt,
            json: $pluto->json,
            long: $pluto->long,
        );
    }
}
