<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\GQL\Query;

use UXF\Core\Attribute\Entity;
use UXF\GraphQL\Attribute\Query;
use UXF\GraphQLTests\ProjectBasic\GQL\Type\Donald as DonaldType;
use UXF\GraphQLTests\ProjectShared\Entity\Donald;

class DonaldQuery
{
    /**
     * @param Donald[] $donalds
     * @param Donald[]|null $donaldsNullable
     * @param Donald[]|null $donaldsNullableOptional
     * @param Donald[] $donaldsOptional
     */
    #[Query(name: 'donald')]
    public function __invoke(
        #[Entity] Donald $donald,
        #[Entity] array $donalds,
        #[Entity] ?Donald $donaldNullable,
        #[Entity] ?array $donaldsNullable,
        #[Entity] ?Donald $donaldNullableOptional = null,
        #[Entity] ?array $donaldsNullableOptional = null,
        #[Entity] array $donaldsOptional = [],
    ): DonaldType {
        return new DonaldType($donald);
    }
}
