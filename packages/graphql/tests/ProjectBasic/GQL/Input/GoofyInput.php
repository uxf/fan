<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\GQL\Input;

use Ramsey\Uuid\UuidInterface;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\GraphQL\Attribute\Field;
use UXF\GraphQL\Attribute\Input;
use UXF\GraphQL\Type\Json;
use UXF\GraphQLTests\ProjectShared\Enum\GoofyEnum;
use UXF\GraphQLTests\ProjectShared\Enum\PlutoEnum;

#[Input]
final readonly class GoofyInput
{
    /**
     * @param string[]|null $array
     */
    public function __construct(
        public ?string $string,
        public ?array $array,
        public ?Date $date,
        public ?DateTime $dateTime,
        public ?Time $time,
        public ?Phone $phone,
        public ?Email $email,
        public ?NationalIdentificationNumberCze $ninCze,
        public ?BankAccountNumberCze $banCze,
        public ?UuidInterface $uuid,
        public ?Money $money,
        public ?Decimal $decimal,
        public ?Url $url,
        public ?PlutoEnum $enum,
        public ?GoofyEnum $enumInt,
        public ?Json $json,
        #[Field(inputType: 'Long')] public ?int $long,
    ) {
    }
}
