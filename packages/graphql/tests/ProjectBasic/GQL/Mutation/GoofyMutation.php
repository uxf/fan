<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\GQL\Mutation;

use UXF\GraphQL\Attribute\Mutation;
use UXF\GraphQLTests\ProjectBasic\GQL\Input\GoofyInput;

class GoofyMutation
{
    #[Mutation(name: 'goofy')]
    public function __invoke(GoofyInput $goofy): bool
    {
        return true;
    }
}
