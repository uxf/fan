<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\GQL\Mutation;

use Symfony\Component\HttpFoundation\Response;
use UXF\GraphQL\Attribute\Mutation;
use UXF\GraphQL\Service\ResponseCallbackModifier;

final readonly class LoginMutation
{
    public function __construct(private ResponseCallbackModifier $responseModifier)
    {
    }

    #[Mutation(name: 'login')]
    public function __invoke(string $username, string $password): bool
    {
        $this->responseModifier->add(fn (Response $response) => $response->headers->set('Auth-Token', '1'));
        return true;
    }
}
