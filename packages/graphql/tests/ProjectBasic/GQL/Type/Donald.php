<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\GQL\Type;

use UXF\GraphQL\Attribute\Type;
use UXF\GraphQLTests\ProjectShared\Entity\Donald as AppDonald;

#[Type]
final readonly class Donald
{
    public function __construct(private AppDonald $donald)
    {
    }

    public function getId(): int
    {
        return $this->donald->id;
    }

    public function getName(): string
    {
        return $this->donald->name;
    }
}
