<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\GQL\Type;

use Ramsey\Uuid\UuidInterface;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\GraphQL\Attribute\Autowire;
use UXF\GraphQL\Attribute\Field;
use UXF\GraphQL\Attribute\Ignore;
use UXF\GraphQL\Attribute\Inject;
use UXF\GraphQL\Attribute\Type;
use UXF\GraphQL\Type\Json;
use UXF\GraphQLTests\ProjectBasic\Staff\Coyote;
use UXF\GraphQLTests\ProjectBasic\Staff\MickeyService;
use UXF\GraphQLTests\ProjectShared\Enum\GoofyEnum;
use UXF\GraphQLTests\ProjectShared\Enum\PlutoEnum;

#[Type]
final readonly class Pluto
{
    /**
     * @param string[] $array
     */
    public function __construct(
        public string $string,
        public array $array,
        public Date $date,
        public DateTime $dateTime,
        public Time $time,
        public Phone $phone,
        public Email $email,
        public NationalIdentificationNumberCze $ninCze,
        public BankAccountNumberCze $banCze,
        public UuidInterface $uuid,
        public Money $money,
        public Decimal $decimal,
        public Url $url,
        public PlutoEnum $enum,
        public GoofyEnum $enumInt,
        public Json $json,
        #[Field(outputType: 'Long')] public int $long,
        #[Ignore] public bool $ignoredProperty = false,
        protected bool $private = false,
    ) {
    }

    public static function create(): null
    {
        return null;
    }

    /**
     * @return string[]
     */
    public function getArray(): array
    {
        return $this->array;
    }

    #[Field(outputType: 'Long')]
    public function getLong2(): int
    {
        return -1;
    }

    /**
     * @return Goofy[]
     */
    public function children(): array
    {
        return [];
    }

    /**
     * @return string[]|null
     */
    public function nodes(#[Autowire] MickeyService $mickeyService, #[Inject] Coyote $coyote): ?array
    {
        $tmp = $mickeyService->get();
        return $tmp !== '' ? [$mickeyService->get()] : null;
    }

    #[Ignore]
    public function ignored(): bool
    {
        return true;
    }
}
