<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectBasic\GQL\Type;

use Ramsey\Uuid\UuidInterface;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\GraphQL\Attribute\Type;
use UXF\GraphQLTests\ProjectShared\Enum\GoofyEnum;
use UXF\GraphQLTests\ProjectShared\Enum\PlutoEnum;

#[Type]
class Goofy
{
    /**
     * @return int[]
     */
    public function getInts(): array
    {
        return [];
    }

    /**
     * @return Date[]
     */
    public function getDates(): array
    {
        return [];
    }

    /**
     * @return DateTime[]
     */
    public function getDateTimes(): array
    {
        return [];
    }

    /**
     * @return Time[]
     */
    public function getTimes(): array
    {
        return [];
    }

    /**
     * @return Phone[]
     */
    public function getPhones(): array
    {
        return [];
    }

    /**
     * @return Email[]
     */
    public function getEmails(): array
    {
        return [];
    }

    /**
     * @return NationalIdentificationNumberCze[]
     */
    public function getNins(): array
    {
        return [];
    }

    /**
     * @return BankAccountNumberCze[]
     */
    public function getBans(): array
    {
        return [];
    }

    /**
     * @return UuidInterface[]
     */
    public function getUuids(): array
    {
        return [];
    }

    /**
     * @return Money[]
     */
    public function getMoneys(): array
    {
        return [];
    }

    /**
     * @return Decimal[]
     */
    public function getDecimals(): array
    {
        return [];
    }

    /**
     * @return Url[]
     */
    public function getUrls(): array
    {
        return [];
    }

    /**
     * @return PlutoEnum[]
     */
    public function getEnums(): array
    {
        return [];
    }

    /**
     * @return GoofyEnum[]
     */
    public function getEnumInts(): array
    {
        return [];
    }
}
