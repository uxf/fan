<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectScalar\Service;

final readonly class SuperScalar
{
    public function __construct(
        public int $id,
    ) {
    }
}
