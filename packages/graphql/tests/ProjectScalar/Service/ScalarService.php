<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectScalar\Service;

final readonly class ScalarService
{
    public function get(int $id): SuperScalar
    {
        return new SuperScalar($id);
    }
}
