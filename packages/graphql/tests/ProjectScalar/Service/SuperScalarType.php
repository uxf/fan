<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectScalar\Service;

use GraphQL\Language\AST\Node;
use GraphQL\Type\Definition\ScalarType;
use LogicException;

final class SuperScalarType extends ScalarType
{
    public function __construct(
        private readonly ScalarService $service,
    ) {
        parent::__construct();
    }

    public function serialize(mixed $value): string
    {
        throw new LogicException();
    }

    public function parseValue(mixed $value): ?SuperScalar
    {
        if ($value === null) {
            return null;
        }

        return $this->service->get($value);
    }

    public function parseLiteral(Node $valueNode, ?array $variables = null): void
    {
        throw new LogicException();
    }
}
