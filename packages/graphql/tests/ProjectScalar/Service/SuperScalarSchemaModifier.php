<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectScalar\Service;

use UXF\GraphQL\Inspector\Schema\InputTypeSchema;
use UXF\GraphQL\Inspector\Schema\ScalarDefinition;
use UXF\GraphQL\Inspector\Schema\ScalarTypeSchema;
use UXF\GraphQL\Inspector\TypeMap;
use UXF\GraphQL\Plugin\SchemaModifier;

class SuperScalarSchemaModifier implements SchemaModifier
{
    public static function modifyTypeMap(TypeMap $typeMap): void
    {
        $typeMap->scalars[SuperScalar::class] = new ScalarTypeSchema(
            name: 'SuperScalar',
            phpType: SuperScalar::class,
            definition: new ScalarDefinition(SuperScalarType::class),
        );
    }

    public static function modifyParseValueFn(InputTypeSchema $inputType): ?string
    {
        return null;
    }

    public static function getDefaultPriority(): int
    {
        return 10;
    }
}
