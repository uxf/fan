<?php

declare(strict_types=1);

namespace UXF\GraphQLTests\ProjectScalar\GQL;

use UXF\GraphQL\Attribute\Query;
use UXF\GraphQLTests\ProjectScalar\Service\SuperScalar;

final readonly class ScalarQuery
{
    #[Query('scalar')]
    public function __invoke(SuperScalar $scalar, ?SuperScalar $scalarNullable): int
    {
        return $scalar->id;
    }
}
