# UXF GraphQL

## Install
```
$ composer req uxf/graphql
```

## Config

```php
// config/packages/uxf.php
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension('uxf_graphql', [
        'destination' => __DIR__ . '/../generated/schema.graphql', // or array
        'fake' => [
            'enabled' => true, // default false
            'default_strategy' => true, // default false
            'namespace' => 'App\\Tests\\TInput', // optional
            'destination' => __DIR__ . '/../generated/fake', // optional
        ],
        'sources' => [
            __DIR__ . '/../src',
        ],
        'injected' => [
            // class => resolver
            Coyote::class => CoyoteResolver::class,
        ],
        'modifiers' => [
            AppTypeMapModifier::class,
        ],
        'hydrator_options' => [
            'allow_trim_string' => false, // default true
            'nullable_optional' => false, // default true
        ],
        'register_psr_17' => false, // default true
        'debug_flag' => DebugFlag::INCLUDE_DEBUG_MESSAGE, // default
    ]);
};
```

## Query

```php
use UXF\GraphQL\Attribute\Query;

final readonly class ArticlesQuery
{
    public function __construct(
        private ArticleProvider $articleProvider
    ) {
    }

    /**
     * @return ArticleType[]
     */
    #[Query('articles')]
    public function __invoke(int $limit, int $offset): array
    {
        return $this->articleProvider->list($limit, $offset);
    }
}
```

```graphql
query Articles($limit: Int!, $offset: Int!) {
    articles(limit: $limit, offset: $offset) {
        id
        name
    }
}
```

## Mutation

```php
use UXF\GraphQL\Attribute\Mutation;

final readonly class ArticleCreateMutation
{
    public function __construct(
        private ArticleCreator $articleCreator,
        private ArticleProvider $articleProvider,
    ) {
    }

    #[Mutation('articleCreate')]
    public function __invoke(ArticleInput $input): ArticleType
    {
        $article = $this->articleCreator->create($input);
        return $this->articleProvider->get($article);
    }
}
```

```graphql
mutation ArticleCreate($input: ArticleInput!) {
    articleCreate(input: $input) {
        id
        name
    }
}
```

## Type

```php
use UXF\GraphQL\Attribute\Type;

#[Type('Article')]
final readonly class ArticleType
{
    public function __construct(
        private Article $article,
        public int $id,
        public string $name,
    ) {
    }

    /**
     * @return TagType[]
     */
    public function tags(): array
    {
        return $this->article->getTags()->map(TagType::create(...))->getValues();
    }
}
```

```graphql
type Article {
    id: Int
    name: String
    tags: [Tag!]!
}
```

## Input

```php
use UXF\GraphQL\Attribute\Input;

#[Input('ArticleInput')]
final readonly class ArticleInput
{
    /**
     * @param int[] $tags
     */
    public function __construct(
        public string $name,
        public ?Date $published,
        public array $tags,
        public int $score = 1,
    ) {
    }
}
```

```graphql
input ArticleInput {
    name: String!
    published: Date
    tags: [Int!]!
    score: Int = 1
}
```

### Limitations

**⚠️ Nullable array items are not supported in inputs for now!** (Supported only in output types + args)

```php
#[Input]
final readonly class InvalidInput
{
    /**
     * @param (int|null)[] $a       -> does not work !!!
     * @param (int|null)[]|null $b  -> does not work !!!
     */
    public function __construct(
        public array $a,
        public ?array $b,
    ) {
    }
}
```

### Validation

```php
use Symfony\Component\Validator\Constraints as Assert;
use UXF\GraphQL\Attribute\Input;

#[Input]
final readonly class ValidationInput
{
    public function __construct(
        #[Assert\NotBlank] public string $string,
        #[Assert\Positive] public int $number,
    ) {
    }
}
```

### NotSet

```php

use UXF\Core\Http\Request\NotSet;
use UXF\GraphQL\Attribute\Input;

#[Input]
final readonly class PatchInput
{
    /**
     * @param int[]|null|NotSet $intArray
     */
    public function __construct(
        public string|null|NotSet $string = new NotSet(),
        public array|null|NotSet $intArray = new NotSet(),
    ) {
    }
}

```

```graphql
input PatchInput {
  string: String
  intArray: [Int!]
}
```

### Generate fake input

```php
use App\Entity\Donald;
use UXF\Core\Attribute\Entity;
use UXF\GraphQL\Attribute\Input;

#[Input(generateFake: true)]
final readonly class DoctrineInput
{
    /**
     * @param Donald[] $donalds
     */
    public function __construct(
        #[Entity] public Donald $donald,
        #[Entity] public array $donalds,
    ) {
    }
}
```

with

```php
    $containerConfigurator->extension('uxf_graphql', [
        'fake' => [
            'enabled' => true,
            'default_strategy' => false,
            'namespace' => 'App\\Tests\\FakeInput',
            'destination' => __DIR__ . '/../tests/FakeInput',
        ],
        ...
    ]);
```

generates

```php
namespace App\Tests\FakeInput;

final readonly class FakeDoctrineInput
{
    /**
     * @param int[] $donalds
     */
    public function __construct(
        public int $donald,
        public array $donalds,
    ) {
    }
}
```

## Entity

### Arguments

```php
use App\Entity\Donald;
use UXF\Core\Attribute\Entity;
use UXF\GraphQL\Attribute\Query;

final readonly class DonaldQuery
{
    /**
     * @param Donald[] $donalds
     */
    #[Query('donald')]
    public function __invoke(
        #[Entity] Donald $donald,
        #[Entity('uuid')] Donald $donald2,
        #[Entity] array $donalds,
    ): DonaldType {
        ...
    }
}
```

```graphql
query Donald($donald: Int!, $donald2: UUID!, $donalds: [Int!]!) {
    donald(
        donald: $donald
        donald2: $donald2
        donalds: $donalds
    ) {
        id
        name
    }
}
```

### Input

```php
use App\Entity\Donald;
use UXF\Core\Attribute\Entity;
use UXF\GraphQL\Attribute\Input;

#[Input]
final readonly class DoctrineInput
{
    /**
     * @param Donald[] $donalds
     * @param Donald[] $donaldNames
     * @param Donald[] $donaldUuids
     * @param Donald[] $donaldRefs
     * @param Donald[]|null $donaldsNullable
     * @param Donald[]|null $donaldsNullableOptional
     * @param Donald[] $donaldsOptional
     */
    public function __construct(
        #[Entity] public Donald $donald,
        #[Entity('name')] public Donald $donaldName,
        #[Entity('uuid')] public Donald $donaldUuid,
        #[Entity('minnie')] public Donald $donaldRef,
        #[Entity('enum')] public Donald $donaldEnum,
        #[Entity('enumInt')] public Donald $donaldEnumInt,
        #[Entity] public array $donalds,
        #[Entity('name')] public array $donaldNames,
        #[Entity('uuid')] public array $donaldUuids,
        #[Entity('minnie')] public array $donaldRefs,
        #[Entity('name')] public ?Donald $donaldNullable,
        #[Entity('uuid')] public ?array $donaldsNullable,
        #[Entity('name')] public ?Donald $donaldNullableOptional = null,
        #[Entity('name')] public ?array $donaldsNullableOptional = null,
        #[Entity('name')] public array $donaldsOptional = [],
    ) {
    }
}
```

```graphql
input DoctrineInput {
  donald: Int!
  donaldName: String!
  donaldUuid: UUID!
  donaldRef: Int!
  donaldEnum: PlutoEnum!
  donaldEnumInt: GoofyEnum!
  donalds: [Int!]!
  donaldNames: [String!]!
  donaldUuids: [UUID!]!
  donaldRefs: [Int!]!
  donaldNullable: String
  donaldsNullable: [UUID!]
  donaldNullableOptional: String = null
  donaldsNullableOptional: [String!] = null
  donaldsOptional: [String!]! = []
}
```

## Union

### Property

```php
use UXF\GraphQL\Attribute\Type;
use UXF\GraphQL\Attribute\Union;

#[Type('Bag')]
final readonly class BagType
{
    public function __construct(
        #[Union('Fruit')] public Apple|Banana $item,
    ) {
    }
}
```

```graphql
union Fruit = Apple | Banana

type Bag {
    item: Fruit!
}
```

### Method

```php
use UXF\GraphQL\Attribute\Type;
use UXF\GraphQL\Attribute\Union;

#[Type('Bag')]
final readonly class BagType
{
    /**
     * @return (Apple|Banana)[]
     */
    #[Union('Fruit')]
    public function getItems(): array
    {
        return [new Banana(2, 'C')];
    }
}
```

```graphql
union Fruit = Apple | Banana

type Bag {
    items: [Fruit!]!
}
```

## Inject

- Symfony Request injector is registered by default

### Query/Mutation

```php
use UXF\GraphQL\Attribute\Inject;

final readonly class PlutoQuery
{
    #[Query(name: 'pluto')]
    public function __invoke(#[Inject] Coyote $coyote): Pluto
    {
        ...
    }
}

```

### Type method

```php
use UXF\GraphQL\Attribute\Inject;

#[Type('Pluto')]
final readonly class PlutoQuery
{
    /**
     * @return string[]
     */
    public function getItems(#[Inject] Coyote $coyote): array
    {
        ...
    }
}
```

### Injector service

```php
use Symfony\Component\HttpFoundation\Request;
use UXF\GraphQL\Service\Injector\InjectedArgument;

final readonly class CoyoteInjector
{
    public function __invoke(Request $request, InjectedArgument $argument): Coyote
    {
        assert($argument->type === Coyote::class);
        return new Coyote();
    }
}
```

## Autowire

```php
use UXF\GraphQL\Attribute\Autowire;

#[Type('Pluto')]
final readonly class PlutoQuery
{
    /**
     * @return string[]
     */
    public function getItems(#[Autowire] ResponseProvider $provider): array
    {
        ...
    }
}
```

```graphql
type Pluto {
    items: [String!]!
}
```

## Field

### Input

```php
use UXF\GraphQL\Attribute\Field;

#[Input]
final readonly class PlutoInput
{
    public function __construct(
        #[Field(inputType: 'Long')] public int $long,
    ) {
    }
}
```

```graphql
input PlutoInput {
    long: Long!
}
```

### Output

```php
use UXF\GraphQL\Attribute\Field;

#[Type]
final readonly class Pluto
{
    public function __construct(
        public string $string,
        #[Field(outputType: 'Long')] public int $long,
    ) {
    }

    #[Field(outputType: 'Long')]
    public function getLong2(): int
    {
        return -1;
    }
}
```

```graphql
type Pluto {
    string: String!
    long: Long!
    long2: Long!
}
```

## Ignore

```php
use UXF\GraphQL\Attribute\Ignore;

#[Type]
final readonly class Pluto
{
    public function __construct(
        public string $string,
        #[Ignore] public bool $ignoredProperty,
    ) {
    }

    #[Ignore]
    public function ignored(): bool
    {
        return true;
    }
}
```

```graphql
type Pluto {
    string: String!
}
```

## SchemaModifier

```php
// config/packages/uxf.php

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension('uxf_graphql', [
        'modifiers' => [
            AppTypeMapModifier::class,
        ],
        ...
    ]);
};
```

```php
use UXF\GraphQL\Plugin\SchemaModifier;

final class AppSchemaModifier implements SchemaModifier
{
    // add custom types
    public static function modifyTypeMap(TypeMap $typeMap): void
    {
        // simple scalar
        $typeMap->scalars[ResultTime::class] = new ScalarTypeSchema(
            name: 'ResultTime',
            phpType: ResultTime::class,
            definition: new ScalarDefinition(ResultTimeType::class),
        );

        // scalar with dependency
        $typeMap->scalars[SuperScalar::class] = new ScalarTypeSchema(
            name: 'SuperScalar',
            phpType: SuperScalar::class,
            definition: new ScalarDefinition(SuperScalarType::class, 'int'),
        );
    }

    // modify input parse function
    public static function modifyParseValueFn(InputTypeSchema $inputType): ?string
    {
        if ($inputType->phpType === Money::class) {
            return 'function (array $values) {
    try {
        return \\' . Money::class . "::of(\$values['amount'], \$values['currency']);
    } catch (\Exception) {
        throw new Error('Invalid money format', previous: new UserError('Invalid money format'));
    }
}";
        }

        return null;
    }

    public static function getDefaultPriority(): int
    {
        return 10;
    }
}
```