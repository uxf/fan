<?php

declare(strict_types=1);

namespace UXF\CoreTests\Story;

use UXF\Core\Test\WebTestCase;
use UXF\CoreTests\Integration\DbHelper;

class WebTest extends WebTestCase
{
    public function testResponseModifier(): void
    {
        $client = self::createClient();

        $client->get('/modify-response');

        self::assertResponseStatusCodeSame(509);
        self::assertResponseHeaderSame('X-Test', 'hello');
        self::assertResponseHeaderSame('Content-type', 'application/json; charset=utf-8');
    }

    public function testPublicPropsResponse(): void
    {
        $client = self::createClient();

        $client->get('/modify-response');

        self::assertSame([
            'dateTime' => '2020-03-04T00:00:00+01:00',
            'phone' => '+420777666777',
        ], $client->getResponseData());
    }

    public function testParameterConverter(): void
    {
        $client = self::createClient();

        DbHelper::initDatabase(self::getContainer());

        // /convert/{date}/{dateTime}/{time}/{email}/{phone}/{nin}/{enum}/{uuid}/{hello1}/{hello2}/{wizard}
        $client->get('/convert/2020-01-02/2020-03-04T00:00:00+01:00/10:00/test@test.com/731444555/9302013545/NEW/9e396235-1cca-4fe1-9d7c-94f848a186e7/1/63845671-09bb-4e64-ba32-5a19f29254db/0');

        self::assertResponseIsSuccessful();
        self::assertSame([
            'injectedObject' => 'en',
            'date' => '2020-01-02',
            'dateTime' => '2020-03-04T00:00:00+01:00',
            'time' => '10:00:00',
            'email' => 'test@test.com',
            'phone' => '+420731444555',
            'nin' => '930201/3545',
            'enum' => 'NEW',
            'uuid' => '9e396235-1cca-4fe1-9d7c-94f848a186e7',
            'hello1' => [
                'id' => 1,
                'uuid' => '00000000-0000-0000-0000-000000000000',
                'createdAt' => '2020-01-01T01:00:00+01:00',
                'publishedAt' => '2020-01-01',
                'openAt' => '10:00:00',
                'phone' => '+420777666777',
                'ninCze' => '930201/3545',
                'banCze' => '123/0300',
            ],
            'hello2' => [
                'id' => 2,
                'uuid' => '63845671-09bb-4e64-ba32-5a19f29254db',
                'createdAt' => '2020-01-01T02:00:00+01:00',
                'publishedAt' => '2020-01-01',
                'openAt' => '10:00:00',
                'phone' => '+420777666777',
                'ninCze' => '930201/3545',
                'banCze' => '123/0300',
            ],
            'wizard' => 'WOW!',
        ], $client->getResponseData());

        // error
        // /convert/{date}/{dateTime}/{time}/{email}/{phone}/{nin}/{enum}/{uuid}/{hello1}/{hello2}/{wizard}
        $client->get('/convert/2020-01-02/2020-03-04T00:00:00+01:00/10:00/test@test.com/731444555/9302013545/XXX/9e396235-1cca-4fe1-9d7c-94f848a186e7/1/2/0');

        self::assertResponseStatusCodeSame(400);
        self::assertSame([
            'error' => [
                'code' => 'BAD_USER_INPUT',
                'message' => 'INVALID INPUT',
            ],
            'validationErrors' => [
                [
                    'field' => 'url:enum',
                    'message' => 'Invalid enum value \'XXX\'',
                ],
            ],
        ], $client->getResponseData());
    }

    public function testMixedBody(): void
    {
        $client = self::createClient();

        // valid
        $client->request('POST', '/body', [], [], [], '{}');
        self::assertResponseIsSuccessful();
        self::assertSame([], $client->getResponseData());

        $client->request('POST', '/body', [], [], [], '[]');
        self::assertResponseIsSuccessful();
        self::assertSame([], $client->getResponseData());

        $client->request('POST', '/body', [], [], [], 'true');
        self::assertResponseIsSuccessful();
        self::assertTrue($client->getResponseData());

        $client->request('POST', '/body', [], [], [], '"text"');
        self::assertResponseIsSuccessful();
        self::assertSame('text', $client->getResponseData());

        $client->request('POST', '/body', [], [], [], '{"ok": "go"}');
        self::assertResponseIsSuccessful();
        self::assertSame([
            'ok' => 'go',
        ], $client->getResponseData());

        $client->request('POST', '/body', [], [], [], '');
        self::assertResponseStatusCodeSame(400);
    }

    public function testHeader(): void
    {
        $client = self::createClient();

        // valid
        $client->get('/header', [
            'HTTP_X_Tenant' => 'd22fddfe-ad4e-4da5-95e7-8309dc37f27d',
        ]);
        self::assertResponseIsSuccessful();
        self::assertSame([
            'xTenant' => 'd22fddfe-ad4e-4da5-95e7-8309dc37f27d',
        ], $client->getResponseData());

        // missing but optional
        $client->get('/header');
        self::assertResponseIsSuccessful();
        self::assertSame([
            'xTenant' => null,
        ], $client->getResponseData());

        // invalid
        $client->get('/header', [
            'HTTP_X_Tenant' => 'x',
        ]);
        self::assertResponseStatusCodeSame(400);
        self::assertSame([
            'error' => [
                'code' => 'BAD_USER_INPUT',
                'message' => 'INVALID INPUT',
            ],
            'validationErrors' => [
                [
                    'field' => 'header:xTenant',
                    'message' => 'Invalid uuid format [Supported format is GUID => \'x\' value given]',
                ],
            ],
        ], $client->getResponseData());
    }
}
