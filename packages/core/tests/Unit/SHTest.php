<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit;

use PHPUnit\Framework\TestCase;
use Throwable;
use UXF\Core\Exception\BasicException;
use UXF\Core\Utils\SH;

class SHTest extends TestCase
{
    public function testS2b(): void
    {
        self::assertTrue(SH::s2b('true'));
        self::assertFalse(SH::s2b('false'));
        self::assertFalse(SH::s2b('dskadskdsa'));
        self::assertFalse(SH::s2b(''));
    }

    public function testS2i(): void
    {
        self::assertSame(-3, SH::s2i('-3'));
        self::assertSame(0, SH::s2i('0'));
        self::assertSame(9_999_999, SH::s2i('9999999'));

        $this->expectException(BasicException::class);
        SH::s2i('eeee');
        SH::s2i('99999999999999999999999999');
        SH::s2i('9.9999999999999999999999999');
        SH::s2i('-9.9999999999999999999999999');
        SH::s2i('+9.9999999999999999999999999');
        SH::s2i('+9');
    }

    public function testS2f(): void
    {
        self::assertEquals(-3, SH::s2f('-3'));
        self::assertEquals(0, SH::s2f('0'));
        self::assertEquals(9_999_999, SH::s2f('9999999'));
        self::assertSame(-3.14, SH::s2f('-3.14'));
        self::assertSame(3.14, SH::s2f('3.14'));
        self::assertEquals(1, SH::s2f('1.0'));
        self::assertEquals(-3, SH::s2f('-3.000'));
        self::assertSame(3.004, SH::s2f('3.004'));

        $this->expectException(BasicException::class);
        SH::s2f('eeee');
        SH::s2f('99999999999999999999999999');
    }

    public function testS2dt(): void
    {
        SH::s2dt('2018-01-01T23:22:11');
        SH::s2dt('2018-01-01T23:22:11CEST');
        SH::s2dt('2018-01-01 23:22:11');
        SH::s2dt('2018-01-01');
        SH::s2dt('now');

        $this->expectException(Throwable::class);
        SH::s2dt('2018-01-43');
        SH::s2dt('dsadsa');
    }

    public function testPurge(): void
    {
        self::assertSame('a', SH::purge(' a '));
        self::assertSame('a', SH::purge("\na\n"));
        self::assertNull(SH::purge(' '));
        self::assertNull(SH::purge(''));
        self::assertNull(SH::purge(null));
    }

    public function testIsEmpty(): void
    {
        self::assertFalse(SH::isEmpty(' a ')); // @phpstan-ignore staticMethod.impossibleType
        self::assertTrue(SH::isEmpty("\n")); // @phpstan-ignore staticMethod.impossibleType
        self::assertTrue(SH::isEmpty(' ')); // @phpstan-ignore staticMethod.impossibleType
        self::assertTrue(SH::isEmpty('')); // @phpstan-ignore staticMethod.alreadyNarrowedType
        self::assertTrue(SH::isEmpty(null)); // @phpstan-ignore staticMethod.alreadyNarrowedType
    }

    public function testIsNotEmpty(): void
    {
        self::assertTrue(SH::isNotEmpty(' a ')); // @phpstan-ignore staticMethod.alreadyNarrowedType
        self::assertFalse(SH::isNotEmpty("\n")); // @phpstan-ignore staticMethod.alreadyNarrowedType
        self::assertFalse(SH::isNotEmpty(' ')); // @phpstan-ignore staticMethod.alreadyNarrowedType
        self::assertFalse(SH::isNotEmpty('')); // @phpstan-ignore staticMethod.impossibleType
        self::assertFalse(SH::isNotEmpty(null)); // @phpstan-ignore staticMethod.impossibleType
    }
}
