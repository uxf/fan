<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\SystemProvider;

use PHPUnit\Framework\TestCase;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\DateTime;

class ClockTest extends TestCase
{
    public function test(): void
    {
        $now = new DateTime();
        self::assertSame((string) $now, (string) Clock::now());

        // freeze
        Clock::$frozenTime = new DateTime('2022-01-13T22:43:51+00:00');
        self::assertSame((string) new DateTime('2022-01-13T22:43:51+00:00'), (string) Clock::now());

        // unfreeze
        Clock::$frozenTime = null;
        self::assertSame((string) $now, (string) Clock::now());
    }
}
