<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Currency;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;
use UXF\Core\Type\Equals;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;

class EqualsTest extends TestCase
{
    #[DataProvider('getData')]
    public function testInvalid(Equals $item): void
    {
        self::assertTrue($item->equals($item));
    }

    /**
     * @return Equals[][]
     */
    public static function getData(): array
    {
        return [
            [new DateTime()],
            [new Date('2020-01-02')],
            [new Time('10:00')],
            [Phone::of('731444555')],
            [NationalIdentificationNumberCze::of('930201/3545')],
            [Money::of(10, Currency::CZK)],
            [Email::of('info@uxf.cz')],
            [BankAccountNumberCze::of('123/0300')],
        ];
    }
}
