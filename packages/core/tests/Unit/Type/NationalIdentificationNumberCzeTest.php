<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Core\SystemProvider\Calendar;
use UXF\Core\Type\Date;
use UXF\Core\Type\NationalIdentificationNumberCze;

class NationalIdentificationNumberCzeTest extends TestCase
{
    #[DataProvider('getData')]
    public function test(string $input, string $birthDate): void
    {
        $nin = NationalIdentificationNumberCze::of($input);
        self::assertSame($birthDate, $nin->getBirthDate()->__toString());
    }

    public function testSimple(): void
    {
        $nin = NationalIdentificationNumberCze::of('9958077723');
        self::assertNull(NationalIdentificationNumberCze::tryParse('9958077724'));
        self::assertNull(NationalIdentificationNumberCze::parseNullable(null));
        self::assertSame('1999-08-07', $nin->getBirthDate()->__toString());
        self::assertSame('9958077723', $nin->withoutSlash());
        self::assertSame('995807/7723', $nin->toString());
        self::assertSame('995807', $nin->beforeSlash());
        self::assertSame('7723', $nin->afterSlash());

        Calendar::$frozenDate = new Date('2000-08-06');
        self::assertSame(0, $nin->getAge());
        Calendar::$frozenDate = new Date('2000-08-07');
        self::assertSame(1, $nin->getAge());
        Calendar::$frozenDate = new Date('2000-08-08');
        self::assertSame(1, $nin->getAge());
        Calendar::$frozenDate = new Date('2020-08-08');
        self::assertSame(21, $nin->getAge());
        Calendar::$frozenDate = null;

        $nin2 = NationalIdentificationNumberCze::of('995807/7723');
        self::assertTrue($nin->equals($nin2));
        self::assertTrue($nin->any($nin2));

        $nin3 = NationalIdentificationNumberCze::of('507807/1724');
        self::assertFalse($nin->equals($nin3));
        self::assertFalse($nin->any($nin3));
    }

    /**
     * @return array<mixed>
     */
    public static function getData(): array
    {
        return [
            ['000807537', '1900-08-07'],
            ['500807070', '1950-08-07'],
            ['9958077723', '1999-08-07'],
            ['2058074161', '2020-08-07'],
            ['5058071722', '2050-08-07'],
            ['507807/1724', '2050-08-07'],
        ];
    }
}
