<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Core\Exception\DecimalException;
use UXF\Core\Type\Decimal;

class DecimalTest extends TestCase
{
    public function test(): void
    {
        self::assertSame('1', Decimal::of(1)->toString());
        self::assertSame('1.01', Decimal::of(1.01)->toString());
        self::assertSame('1.001', Decimal::of(1.001)->toString());
        self::assertSame('-1.001', Decimal::of(-1.001)->toString());
        self::assertSame('-1', Decimal::of(-1)->toString());
        self::assertSame('-1.12', Decimal::of(-1.12)->toString());
        self::assertSame('0', Decimal::of('0.0000')->toString());
        self::assertSame('0', Decimal::of('-0.0000')->toString());
        self::assertSame('10', Decimal::of('10.0000')->toString());
        self::assertSame('-10', Decimal::of('-10.0000')->toString());
        self::assertSame('-10.1', Decimal::of('-10.1000')->toString());
        self::assertSame('1000000000000.01', Decimal::of('1000000000000.01')->toString());
        self::assertSame('-1000000000000.01', Decimal::of('-1000000000000.01')->toString());

        // round
        self::assertSame('1.05', Decimal::of(1.046)->toScale(2)->toString());
        self::assertSame('1.05', Decimal::of(1.045)->toScale(2)->toString());
        self::assertSame('1.04', Decimal::of(1.044)->toScale(2)->toString());
        self::assertSame('-1.05', Decimal::of(-1.046)->toScale(2)->toString());
        self::assertSame('-1.05', Decimal::of(-1.045)->toScale(2)->toString());
        self::assertSame('-1.04', Decimal::of(-1.044)->toScale(2)->toString());

        self::assertTrue(Decimal::of(10) > Decimal::of(5));
        self::assertTrue(Decimal::of(10) >= Decimal::of(5));
        self::assertTrue(Decimal::of(10.1) < Decimal::of(100.1));
        self::assertTrue(Decimal::of(0.1) > Decimal::zero());
        self::assertTrue(Decimal::of(0) >= Decimal::zero());
        self::assertTrue(Decimal::of('-1') < Decimal::zero());
        self::assertTrue(Decimal::of(-1) < Decimal::of(-0.99));
        self::assertTrue(Decimal::of(-0.99) > Decimal::of(-1));
        self::assertTrue(Decimal::of(-1) < Decimal::of(1));
        self::assertTrue(Decimal::of(1) > Decimal::of(-1));
        self::assertTrue(Decimal::of(9) > Decimal::of(-9.99));
        self::assertTrue(Decimal::of(-9) > Decimal::of(-9.99));

        self::assertSame('7.99', Decimal::of(3.14)->plus(Decimal::of(4.85))->toString());
        self::assertSame('-1.71', Decimal::of(3.14)->minus(Decimal::of(4.85))->toString());
        self::assertSame('9.42', Decimal::of(3.14)->multipliedBy(3)->toString());
        self::assertSame('0.79', Decimal::of(3.14)->dividedBy(4)->toString());
        self::assertSame(3, Decimal::of(3.14)->toInt());
        self::assertSame(3.14, Decimal::of(3.14)->toFloat());

        self::assertTrue(Decimal::zero()->isZero());
        self::assertFalse(Decimal::of(1)->isZero());

        self::assertTrue(Decimal::of(1)->isPositive());
        self::assertTrue(Decimal::of(0)->isPositiveOrZero());
        self::assertTrue(Decimal::of(1)->isPositiveOrZero());
        self::assertTrue(Decimal::of(-1)->isNegative());
        self::assertTrue(Decimal::of(0)->isNegativeOrZero());
        self::assertTrue(Decimal::of(-1)->isNegativeOrZero());

        self::assertTrue(Decimal::zero()->equals(Decimal::zero()));
        self::assertTrue(Decimal::zero()->any(Decimal::zero()));
        self::assertFalse(Decimal::zero()->any(Decimal::of(1)));
        self::assertTrue(Decimal::of('0')->equals(Decimal::of('-0')));
        self::assertTrue(Decimal::of(3.14)->equals(Decimal::of(3.14)));
        self::assertFalse(Decimal::of(3.14)->equals(Decimal::of(3.13)));
        self::assertFalse(Decimal::of(3.14)->equals(null));
        self::assertFalse(Decimal::of(3.14)->equals(Decimal::of(3.13)));

        self::assertSame('0', Decimal::of(666)->minus(Decimal::of(666))->toString());
    }

    #[DataProvider('getInvalidData')]
    public function testInvalid(string $amount): void
    {
        $this->expectException(DecimalException::class);
        $this->expectExceptionMessage('Invalid format');
        Decimal::of($amount);
    }

    /**
     * @return string[][]
     */
    public static function getInvalidData(): array
    {
        return [
            [''],
            ['-'],
            ['a'],
            ['1.0.'],
            ['1..0'],
            ['+10'],
            ['--10'],
        ];
    }

    public function testPhpstanPure(): void
    {
        $a = Decimal::of(1);
        $a->plus($a); // @phpstan-ignore-line
        $a->minus($a); // @phpstan-ignore-line
        $a->multipliedBy(0); // @phpstan-ignore-line
        $a->dividedBy(1); // @phpstan-ignore-line
        self::assertSame('1', $a->toString());
    }
}
