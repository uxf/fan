<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Core\Type\Phone;

class PhoneTest extends TestCase
{
    public function testSimple(): void
    {
        $phone = Phone::of('+420771222333');
        $phone2 = Phone::of('+420771222333');
        self::assertTrue($phone->equals($phone2));
        self::assertTrue($phone->any($phone2));

        $phone3 = Phone::of('+420771222331');
        self::assertFalse($phone->equals($phone3));
        self::assertFalse($phone->any($phone3));
    }

    #[DataProvider('getData')]
    public function test(string $actual, string $expected, int $countryCode, string $nationalNumber): void
    {
        $phone = Phone::of($actual);
        self::assertSame($expected, $phone->toString());
        self::assertSame($countryCode, $phone->getCountryCode());
        self::assertSame($nationalNumber, $phone->getNationalNumber());
    }

    /**
     * @return array<mixed>
     */
    public static function getData(): array
    {
        return [
            ['771222333', '+420771222333', 420, '771222333'],
            ['+420723456789', '+420723456789', 420, '723456789'],
            ['00420723456789', '+420723456789', 420, '723456789'],
            ['00420602123456', '+420602123456', 420, '602123456'],
            ['0902641750', '+421902641750', 421, '902641750'],
            ['+420 731 123 456', '+420731123456', 420, '731123456'],
        ];
    }

    #[DataProvider('getData2')]
    public function testValid(string $actual, bool $expected): void
    {
        self::assertSame($expected, Phone::tryParse($actual) !== null);
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function getData2(): array
    {
        return [
            ['12345678', false],
            ['+420723456789', true],
            ['723456789', true],
        ];
    }
}
