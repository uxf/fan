<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use Exception;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Core\Type\Time;
use function Safe\json_encode;

class TimeTest extends TestCase
{
    public function testCreate(): void
    {
        $timeA = new Time('12:13');
        self::assertSame(12, $timeA->hours);
        self::assertSame(13, $timeA->minutes);
        self::assertSame(0, $timeA->seconds);

        $timeB = new Time('13:14:15');
        self::assertSame(13, $timeB->hours);
        self::assertSame(14, $timeB->minutes);
        self::assertSame(15, $timeB->seconds);

        $timeC = new Time('0:00:00');
        self::assertSame(0, $timeC->hours);
        self::assertSame(0, $timeC->minutes);
        self::assertSame(0, $timeC->seconds);

        $timeD = Time::fromSeconds(0);
        self::assertSame(0, $timeD->hours);
        self::assertSame(0, $timeD->minutes);
        self::assertSame(0, $timeD->seconds);

        $timeE = Time::fromSeconds(86399);
        self::assertSame(23, $timeE->hours);
        self::assertSame(59, $timeE->minutes);
        self::assertSame(59, $timeE->seconds);

        $timeF = new Time('12:13');
        self::assertTrue($timeA->equals($timeF));
        self::assertTrue($timeA->any($timeF));
        self::assertFalse($timeA->equals($timeB));
        self::assertFalse($timeA->any($timeB));

        // output
        $time = new Time('23:59:59');
        self::assertEquals($time, unserialize(serialize($time)));
        self::assertSame('23:59:59', (string) $time);
        self::assertSame('["23:59:59"]', json_encode([$time]));
    }

    public function testCompare(): void
    {
        $timeA = new Time('12:13:45');
        $timeB = new Time('13:13:45');
        $timeC = new Time('13:13:45');

        // ===
        self::assertTrue($timeB->equals($timeC));
        self::assertFalse($timeA->equals($timeB));
        self::assertFalse($timeA->equals(null));

        // >
        self::assertTrue($timeB > $timeA);
        self::assertTrue($timeB > null);
        self::assertFalse($timeA > $timeB);
        self::assertFalse($timeC > $timeB);

        // <
        self::assertTrue($timeA < $timeB);
        self::assertTrue(null < $timeB);
        self::assertFalse($timeB < $timeA);
        self::assertFalse($timeB < $timeC);

        // >=
        self::assertTrue($timeB >= $timeA);
        self::assertTrue($timeB >= null);
        self::assertFalse($timeA >= $timeB);

        // <=
        self::assertTrue($timeA <= $timeB);
        self::assertTrue(null <= $timeB);
        self::assertFalse($timeB <= $timeA);
    }

    public function testModify(): void
    {
        $time = new Time('12:13:45');
        self::assertSame('12:13:46', (string) $time->addSeconds(1));
        self::assertSame('12:13:44', (string) $time->subSeconds(1));
    }

    #[DataProvider('getInvalidData')]
    public function testInvalid(string $date): void
    {
        $this->expectException(Exception::class);
        new Time($date);
    }

    /**
     * @return string[][]
     */
    public static function getInvalidData(): array
    {
        return [
            ['1.1.2020'],
            ['2020-02-30'],
            ['2020-2-3'],
            ['first day of month'],
            ['24:00:00'],
            ['23:0:00'],
            ['23:60'],
            ['23:00:60'],
            ['12:100:56'],
        ];
    }
}
