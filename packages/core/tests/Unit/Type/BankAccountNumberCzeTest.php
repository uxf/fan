<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use PHPUnit\Framework\TestCase;
use UXF\Core\Type\BankAccountNumberCze;

class BankAccountNumberCzeTest extends TestCase
{
    public function test(): void
    {
        self::assertSame('19-2000145399/0800', BankAccountNumberCze::of('19-2000145399/0800')->toString());
        self::assertSame('145254386/2100', BankAccountNumberCze::of('145254386/2100')->toString());
        self::assertSame('123/0300', BankAccountNumberCze::of('123/0300')->toString());

        self::assertSame('123/0300', BankAccountNumberCze::of(' 0-00123/0300 ')->toString());

        $bankAccountNumber = BankAccountNumberCze::of('19-2000145399/0800');
        self::assertSame(['19', '2000145399', '0800'], $bankAccountNumber->getParts());
        self::assertSame('19', $bankAccountNumber->getPrefix());
        self::assertSame('2000145399', $bankAccountNumber->getAccount());
        self::assertSame('19-2000145399', $bankAccountNumber->getAccountWithPrefix());
        self::assertSame('0800', $bankAccountNumber->getBankCode());
        self::assertSame('Česká spořitelna, a.s.', $bankAccountNumber->getBankName());
        self::assertSame('CZ6508000000192000145399', $bankAccountNumber->getIBAN());

        $bankAccountNumber2 = BankAccountNumberCze::of('19-2000145399/0800');
        self::assertTrue($bankAccountNumber->equals($bankAccountNumber2));
        self::assertTrue($bankAccountNumber->any($bankAccountNumber2));

        $bankAccountNumber3 = BankAccountNumberCze::of('145254386/2100');
        self::assertFalse($bankAccountNumber->equals($bankAccountNumber3));
        self::assertFalse($bankAccountNumber->any($bankAccountNumber3));

        self::assertNull(BankAccountNumberCze::tryParse('x'));
        self::assertNull(BankAccountNumberCze::parseNullable(null));
    }
}
