<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit;

use InvalidArgumentException;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Utils\DateHelper;

class DateHelperTest extends TestCase
{
    #[DataProvider('getDayData')]
    public function testDay(string $expected, Date|DateTime|int $date): void
    {
        self::assertSame($expected, DateHelper::dayName($date));
    }

    #[DataProvider('getMonthData')]
    public function testMonth(string $expected, Date|DateTime|int $date): void
    {
        self::assertSame($expected, DateHelper::monthName($date));
    }

    public function testInvalidDay(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid day 0');
        DateHelper::dayName(0);
    }

    public function testInvalidMonth(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid month 0');
        DateHelper::monthName(0);
    }

    /**
     * @return array<mixed>
     */
    public static function getDayData(): array
    {
        return [
            ['Pondělí', new Date('2024-05-20')],
            ['Úterý', new Date('2024-05-21')],
            ['Středa', new Date('2024-05-22')],
            ['Čtvrtek', new Date('2024-05-23')],
            ['Pátek', new Date('2024-05-24')],
            ['Sobota', new Date('2024-05-25')],
            ['Neděle', new Date('2024-05-26')],
            //
            ['Pondělí', new DateTime('2024-05-20')],
            ['Úterý', new DateTime('2024-05-21')],
            ['Středa', new DateTime('2024-05-22')],
            ['Čtvrtek', new DateTime('2024-05-23')],
            ['Pátek', new DateTime('2024-05-24')],
            ['Sobota', new DateTime('2024-05-25')],
            ['Neděle', new DateTime('2024-05-26')],
            //
            ['Pondělí', 1],
            ['Úterý', 2],
            ['Středa', 3],
            ['Čtvrtek', 4],
            ['Pátek', 5],
            ['Sobota', 6],
            ['Neděle', 7],
        ];
    }

    /**
     * @return array<mixed>
     */
    public static function getMonthData(): array
    {
        return [
            ['Leden', new Date('2024-01-01')],
            ['Únor', new Date('2024-02-01')],
            ['Březen', new Date('2024-03-01')],
            ['Duben', new Date('2024-04-01')],
            ['Květen', new Date('2024-05-01')],
            ['Červen', new Date('2024-06-01')],
            ['Červenec', new Date('2024-07-01')],
            ['Srpen', new Date('2024-08-01')],
            ['Září', new Date('2024-09-01')],
            ['Říjen', new Date('2024-10-01')],
            ['Listopad', new Date('2024-11-01')],
            ['Prosinec', new Date('2024-12-01')],
            //
            ['Leden', new DateTime('2024-01-01')],
            ['Únor', new DateTime('2024-02-01')],
            ['Březen', new DateTime('2024-03-01')],
            ['Duben', new DateTime('2024-04-01')],
            ['Květen', new DateTime('2024-05-01')],
            ['Červen', new DateTime('2024-06-01')],
            ['Červenec', new DateTime('2024-07-01')],
            ['Srpen', new DateTime('2024-08-01')],
            ['Září', new DateTime('2024-09-01')],
            ['Říjen', new DateTime('2024-10-01')],
            ['Listopad', new DateTime('2024-11-01')],
            ['Prosinec', new DateTime('2024-12-01')],
            //
            ['Leden', 1],
            ['Únor', 2],
            ['Březen', 3],
            ['Duben', 4],
            ['Květen', 5],
            ['Červen', 6],
            ['Červenec', 7],
            ['Srpen', 8],
            ['Září', 9],
            ['Říjen', 10],
            ['Listopad', 11],
            ['Prosinec', 12],
        ];
    }
}
