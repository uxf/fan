<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use UXF\Core\EventSubscriber\ObjectResponseSubscriber;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Phone;
use UXF\CoreTests\Project\Controller\ModifiedResponseController;
use UXF\CoreTests\Project\Http\TestResponseBody;

class ObjectResponseSubscriberTest extends TestCase
{
    public function test(): void
    {
        $instance = new ObjectResponseSubscriber();

        $kernel = $this->createMock(KernelInterface::class);
        $request = new Request();
        $request->attributes->set('_controller', ModifiedResponseController::class);
        $result = [new TestResponseBody(new DateTime('2020-03-04'), Phone::of('+420777666777'))];

        $event = new ViewEvent($kernel, $request, HttpKernelInterface::MAIN_REQUEST, $result);

        $instance->processResponse($event);

        self::assertSame('[{"dateTime":"2020-03-04T00:00:00+01:00","phone":"+420777666777"}]', $event->getResponse()?->getContent());
    }
}
