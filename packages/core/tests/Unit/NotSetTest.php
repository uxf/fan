<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit;

use PHPUnit\Framework\TestCase;
use UXF\CoreTests\Project\Http\TestPatchABody;
use function Safe\json_encode;

class NotSetTest extends TestCase
{
    public function test(): void
    {
        self::assertSame('{}', json_encode(new TestPatchABody()));
    }
}
