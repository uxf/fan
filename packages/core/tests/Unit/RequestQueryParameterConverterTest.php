<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use UXF\Core\Hydrator\Hydrator;
use UXF\Core\RequestConverter\QueryParameterConverter;
use UXF\CoreTests\Project\Entity\NumericEnum;
use UXF\CoreTests\Project\Http\TestRequestEmptyQuery;
use UXF\CoreTests\Project\HttpQuery\FakeQuery;
use UXF\Hydrator\Inspector\SimpleArrayPhpDocParser;
use function Safe\json_decode;
use function Safe\json_encode;

class RequestQueryParameterConverterTest extends TestCase
{
    public function test(): void
    {
        $classMetadataFactory = $this->createMock(ClassMetadataFactory::class);
        $classMetadataFactory->method('isTransient')
            ->willReturn(false);

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->method('getMetadataFactory')
            ->willReturn($classMetadataFactory);

        $converter = new QueryParameterConverter(
            hydrator: $this->createMock(Hydrator::class),
            validator: $this->createMock(ValidatorInterface::class),
            arrayPhpDocParser: new SimpleArrayPhpDocParser(),
            entityManager: $entityManager,
        );

        $result = $converter->retype([
            'int' => '1',
            'float' => '2.2',
            'bool' => '1',
            'enum' => '1',
            'intArray' => ['3', '4'],
            'floatArray' => ['5.5', '6.6'],
            'boolArray' => ['true', 'false'],
        ], FakeQuery::class);

        $query = new FakeQuery(
            int: 1,
            float: 2.2,
            bool: true,
            enum: NumericEnum::ONE,
            intArray: [3, 4],
            floatArray: [5.5, 6.6],
            boolArray: [true, false],
        );
        $expected = json_decode(json_encode($query), true);

        self::assertSame($expected, $result);

        // empty
        $result = $converter->retype([
            'language' => '',
        ], TestRequestEmptyQuery::class);
        self::assertNull($result['language']);
    }
}
