<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\CoreTests\Project\Entity\Hello;

class DoctrineDateTimeTest extends KernelTestCase
{
    // special for Europe/Prague
    public function test(): void
    {
        self::bootKernel();

        $container = self::getContainer();
        DbHelper::initDatabase($container);

        $registry = $container->get('doctrine');
        assert($registry instanceof Registry);
        $em = $registry->getManager();

        $hello = new Hello(
            id: 3,
            uuid: Uuid::fromString('00000000-0000-4000-0000-000000000000'),
            createdAt: new DateTime('2022-11-18T12:47:40Z'),
            publishedAt: new Date('2022-11-18'),
            openAt: new Time('10:00:00'),
            phone: Phone::of('777666555'),
            ninCze: NationalIdentificationNumberCze::of('930201/3545'),
            banCze: BankAccountNumberCze::of('123/0300'),
        );
        $em->persist($hello);
        $em->flush();
        $em->clear();

        $hello = $em->find(Hello::class, 3);
        self::assertSame('2022-11-18T13:47:40+01:00', $hello?->createdAt->format(DATE_ATOM));
    }

    public function testDql(): void
    {
        self::bootKernel();

        $container = self::getContainer();
        DbHelper::initDatabase($container);

        /** @var EntityManagerInterface $em */
        $em = $container->get(EntityManagerInterface::class);

        $result = $em->createQueryBuilder()
            ->select([
                'DATE(hello.createdAt) AS d',
                'TIME(hello.createdAt) AS t',
                'CAST(hello.createdAt AS TEXT) AS u',
            ])
            ->from(Hello::class, 'hello')
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_SCALAR);

        self::assertSame([
            [
                'd' => '2020-01-01',
                't' => '01:00:00',
                'u' => '2020-01-01 01:00:00',
            ],
            [
                'd' => '2020-01-01',
                't' => '02:00:00',
                'u' => '2020-01-01 02:00:00',
            ],
        ], $result);
    }
}
