<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration;

use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\SystemProvider\Calendar;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Currency;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\CoreTests\Project\Dto\BanCzeDto;
use UXF\CoreTests\Project\Dto\DateDto;
use UXF\CoreTests\Project\Dto\DateTimeDto;
use UXF\CoreTests\Project\Dto\DecimalDto;
use UXF\CoreTests\Project\Dto\DoctrineDto;
use UXF\CoreTests\Project\Dto\EmailDto;
use UXF\CoreTests\Project\Dto\MoneyDto;
use UXF\CoreTests\Project\Dto\NinCzeDto;
use UXF\CoreTests\Project\Dto\PhoneDto;
use UXF\CoreTests\Project\Dto\TimeDto;
use UXF\CoreTests\Project\Dto\UrlDto;
use UXF\CoreTests\Project\Dto\UuidDto;
use UXF\CoreTests\Project\Entity\Language;
use UXF\CoreTests\Project\Entity\NumericEnum;
use UXF\CoreTests\Project\Entity\State;
use UXF\CoreTests\Project\FunnyService;
use UXF\CoreTests\Project\Http\TestNullableRequestBody;
use UXF\CoreTests\Project\Http\TestPhp8RequestBody;
use UXF\CoreTests\Project\Http\WizardRequestBody;
use UXF\Hydrator\Exception\HydratorException;
use UXF\Hydrator\ObjectHydrator;
use UXF\Hydrator\Options;

class HydratorTest extends KernelTestCase
{
    public function test(): void
    {
        $hydrator = $this->init();

        $errors = null;
        try {
            $hydrator->hydrateArray([
                'emptyString' => '',
                'emptyArray' => [100],
                'language' => 100,
                'rawEmail' => 'xx',
                'int' => 0,
                'uuid' => 'xx',
                'date' => 'xx',
                'dateTime' => 'xx',
                'time' => 'xx',
                'phone' => 'xx',
                'email' => 'xx',
                'ninCze' => 'xx',
                'banCze' => 'xx',
                'money' => [
                    'amount' => '',
                    'currency' => '',
                ],
                'decimal' => 'x',
                'url' => 'x',
            ], TestPhp8RequestBody::class);
        } catch (HydratorException $e) {
            $errors = $e->errors;
        }

        self::assertSame([
            'emptyArray[0]' => ["Not found [int given]"],
            'language' => ["Not found [int given]"],
            'uuid' => ["Invalid uuid format [Supported format is GUID => 'xx' value given]"],
            'date' => ["Invalid date format [Supported format is Y-m-d => 'xx' value given]"],
            'dateTime' => ["Invalid date time format [Supported format is ISO8601 => 'xx' value given]"],
            'time' => ["Invalid time format [Supported format is H:i => 'xx' value given]"],
            'phone' => ["Invalid phone format [Supported format is +420123456789 => 'xx' value given]"],
            'email' => ["Invalid email format [Supported format is joe@black.com => 'xx' value given]"],
            'ninCze' => ["Invalid national identification number [Supported format is 9302013545 or 930201/3545 => 'xx' value given]"],
            'banCze' => ["Invalid bank account number [Supported format is 123/0300 => 'xx' value given]"],
            'money.amount' => ["Invalid money amount format [Supported format is -100.00 => '' value given]"],
            'money.currency' => ["Invalid money currency format [Supported format is ISO 4217 => '' value given]"],
            'decimal' => ["Invalid number format [Supported format is -100.00 => 'x' value given]"],
            'url' => ["Invalid url format [Supported format is https://google.com => 'x' value given]"],
        ], $errors);

        // without cast
        $object = new TestPhp8RequestBody(
            emptyString: '',
            emptyArray: [],
            language: Language::create(1, 'x'),
            rawEmail: '',
            int: 1,
            uuid: Uuid::uuid4(),
            date: Calendar::today(),
            dateTime: Clock::now(),
            time: new Time('10:00'),
            phone: Phone::of('+420777666777'),
            email: Email::of('joe@black.cz'),
            ninCze: NationalIdentificationNumberCze::of('930201/3545'),
            banCze: BankAccountNumberCze::of('123/0300'),
            money: Money::of(1, Currency::CZK),
            decimal: Decimal::of(3.14),
            url: Url::of('https://uxf.cz'),
        );
        $result = $hydrator->hydrateArray((array)$object, TestPhp8RequestBody::class);
        self::assertSame(1, $result->int);

        // doctrine
        $data = [
            'a1' => 1,
            'a2' => [1],
            'b1' => '00000000-0000-0000-0000-000000000000',
            'b2' => ['00000000-0000-0000-0000-000000000000'],
        ];
        $result = $hydrator->hydrateArray($data, DoctrineDto::class);
        self::assertSame(1, $result->a1->id);
        self::assertCount(1, $result->a2);
        self::assertEquals('00000000-0000-0000-0000-000000000000', $result->b1?->uuid);
        self::assertCount(1, $result->b2 ?? []);

        // money
        $data = [
            'a1' => Money::of(1, Currency::CZK),
            'a2' => [Money::of(5.5, Currency::EUR)],
            'b1' => null,
            'b2' => null,
        ];
        $result = $hydrator->hydrateArray($data, MoneyDto::class);
        self::assertEquals(Money::of(1, Currency::CZK), $result->a1);
        self::assertEquals([Money::of(5.5, Currency::EUR)], $result->a2);

        // uuid
        $data = [
            'a1' => 'ec7821fb-b41a-4ccc-88ad-e0e491305f2e',
            'a2' => ['ec7821fb-b41a-4ccc-88ad-e0e491305f2e'],
            'b1' => null,
            'b2' => null,
        ];
        $result = $hydrator->hydrateArray($data, UuidDto::class);
        self::assertEquals('ec7821fb-b41a-4ccc-88ad-e0e491305f2e', $result->a1);
        self::assertEquals(['ec7821fb-b41a-4ccc-88ad-e0e491305f2e'], $result->a2);

        // date
        $data = [
            'a1' => '2020-01-02',
            'a2' => ['2020-02-03'],
            'b1' => null,
            'b2' => null,
        ];
        $result = $hydrator->hydrateArray($data, DateDto::class);
        self::assertEquals('2020-01-02', $result->a1);
        self::assertEquals(['2020-02-03'], $result->a2);

        // date time
        $data = [
            'a1' => '2020-01-02 10:00:55',
            'a2' => ['2020-02-03 10:00:59'],
            'b1' => null,
            'b2' => null,
        ];
        $result = $hydrator->hydrateArray($data, DateTimeDto::class);
        self::assertSame('2020-01-02T10:00:55+01:00', $result->a1->__toString());
        self::assertSame('2020-02-03T10:00:59+01:00', $result->a2[0]->__toString());

        // time
        $data = [
            'a1' => '10:22:33',
            'a2' => ['01:02:03'],
            'b1' => null,
            'b2' => null,
        ];
        $result = $hydrator->hydrateArray($data, TimeDto::class);
        self::assertEquals('10:22:33', $result->a1);
        self::assertEquals(['01:02:03'], $result->a2);

        // phone
        $data = [
            'a1' => '777666555',
            'a2' => ['731000999'],
            'b1' => null,
            'b2' => null,
        ];
        $result = $hydrator->hydrateArray($data, PhoneDto::class);
        self::assertEquals('+420777666555', $result->a1);
        self::assertEquals(['+420731000999'], $result->a2);

        // email
        $data = [
            'a1' => 'a@uxf.cz',
            'a2' => ['b@uxf.cz'],
            'b1' => null,
            'b2' => null,
        ];
        $result = $hydrator->hydrateArray($data, EmailDto::class);
        self::assertEquals('a@uxf.cz', $result->a1);
        self::assertEquals(['b@uxf.cz'], $result->a2);

        // ninCze
        $data = [
            'a1' => '930201/3545',
            'a2' => ['9302013545'],
            'b1' => null,
            'b2' => null,
        ];
        $result = $hydrator->hydrateArray($data, NinCzeDto::class);
        self::assertEquals('930201/3545', $result->a1);
        self::assertEquals(['930201/3545'], $result->a2);

        // banCze
        $data = [
            'a1' => '123/0300',
            'a2' => ['1230/0300'],
            'b1' => null,
            'b2' => null,
        ];
        $result = $hydrator->hydrateArray($data, BanCzeDto::class);
        self::assertEquals('123/0300', $result->a1);
        self::assertEquals(['1230/0300'], $result->a2);

        // decimal
        $data = [
            'a1' => '1',
            'a2' => ['2.2'],
            'b1' => null,
            'b2' => null,
        ];
        $result = $hydrator->hydrateArray($data, DecimalDto::class);
        self::assertEquals('1', $result->a1);
        self::assertEquals(['2.2'], $result->a2);

        // url
        $data = [
            'a1' => 'appio.dev',
            'a2' => ['uxf.cz'],
            'b1' => null,
            'b2' => null,
        ];
        $result = $hydrator->hydrateArray($data, UrlDto::class);
        self::assertEquals('https://appio.dev', $result->a1);
        self::assertEquals(['https://uxf.cz'], $result->a2);
    }

    public function testNullableOptional(): void
    {
        $hydrator = $this->init();

        $options = new Options('N', [
            'nullable_optional' => true,
        ]);
        $result = $hydrator->hydrateArray([], TestNullableRequestBody::class, $options);
        self::assertInstanceOf(NotSet::class, $result->notSet);
    }

    public function testDoctrine(): void
    {
        $hydrator = $this->init();

        $data = [
            'wizard' => 1,
            'wizardName' => 'WOW!',
            'wizardUuid' => Uuid::fromInteger('1'),
            'wizardRef' => 1,
            'wizardEnum' => State::NEW,
            'wizardEnumInt' => NumericEnum::ONE,
            'wizardHidden' => 0,
            'wizards' => [1],
            'wizardNames' => ['WOW!'],
            'wizardUuids' => [Uuid::fromInteger('1')],
            'wizardRefs' => [1],
            'wizardNullable' => null,
            'wizardsNullable' => null,
        ];

        $result = $hydrator->hydrateArray($data, WizardRequestBody::class);
        self::assertNotEmpty($result->wizards);
    }

    private function init(): ObjectHydrator
    {
        self::bootKernel();

        $container = self::getContainer();
        DbHelper::initDatabase($container);

        $service = $container->get(FunnyService::class);
        assert($service instanceof FunnyService);

        return $service->getObjectHydrator();
    }
}
