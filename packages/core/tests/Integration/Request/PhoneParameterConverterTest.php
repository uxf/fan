<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration\Request;

use PHPUnit\Framework\TestCase;
use UXF\Core\Exception\ValidationException;
use UXF\Core\RequestConverter\PhoneParameterConverter;
use UXF\Core\Type\Phone;

class PhoneParameterConverterTest extends TestCase
{
    public function testValid(): void
    {
        $actual = PhoneParameterConverter::convert('731444555', '');
        self::assertEquals(Phone::of('+420731444555'), $actual);
    }

    public function testInvalid(): void
    {
        $this->expectException(ValidationException::class);
        PhoneParameterConverter::convert('1992-04-31', '');
    }
}
