<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration\Request;

use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use UXF\Core\Exception\ValidationException;
use UXF\Core\RequestConverter\UuidParameterConverter;

class UuidParamConverterTest extends TestCase
{
    public function testValid(): void
    {
        $actual = UuidParameterConverter::convert('9e396235-1cca-4fe1-9d7c-94f848a186e7', '');
        self::assertTrue(Uuid::fromString('9e396235-1cca-4fe1-9d7c-94f848a186e7')->equals($actual));
    }

    public function testInvalid(): void
    {
        $this->expectException(ValidationException::class);
        UuidParameterConverter::convert('xxx', '');
    }
}
