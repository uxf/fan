<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration\Request;

use LogicException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use UXF\Core\Exception\ValidationException;
use UXF\Core\RequestConverter\QueryParameterConverter;
use UXF\CoreTests\Integration\DbHelper;
use UXF\CoreTests\Project\Entity\Language;
use UXF\CoreTests\Project\Entity\State;
use UXF\CoreTests\Project\Http\TestRequestEmptyQuery;
use UXF\CoreTests\Project\Http\TestRequestQuery;

class RequestQueryParameterConverterTest extends KernelTestCase
{
    private QueryParameterConverter $converter;

    protected function setUp(): void
    {
        self::bootKernel();
        DbHelper::initDatabase(self::getContainer());

        /** @var QueryParameterConverter $converter */
        $converter = self::getContainer()->get(QueryParameterConverter::class);
        $this->converter = $converter;
    }

    public function testValid(): void
    {
        $params = [
            'stringParam' => 'x',
            'stringNullParam' => null,
            'stringRequiredParam' => 'z',
            'intParam' => 10,
            'intNullParam' => '',
            'intRequiredParam' => 20,
            'boolParam' => 1,
            'boolNullParam' => false,
            'boolRequiredParam' => true,
            'language' => 1,
            'languages' => [1, 2],
            'state' => 'NEW',
            'filter' => [
                'hello' => 'ok',
                'hell' => 666,
            ],
        ];

        $request = Request::create('?' . http_build_query($params));
        $queryObject = $this->converter->convert($request, TestRequestQuery::class);

        self::assertInstanceOf(TestRequestQuery::class, $queryObject);
        self::assertSame('x', $queryObject->stringParam);
        self::assertNull($queryObject->stringNullParam);
        self::assertSame('z', $queryObject->stringRequiredParam);
        self::assertSame(10, $queryObject->intParam);
        self::assertNull($queryObject->intNullParam);
        self::assertSame(20, $queryObject->intRequiredParam);
        self::assertTrue($queryObject->boolParam);
        self::assertFalse($queryObject->boolNullParam);
        self::assertTrue($queryObject->boolRequiredParam);
        self::assertInstanceOf(Language::class, $queryObject->language);
        self::assertCount(2, $queryObject->languages);
        self::assertSame(1, $queryObject->languages[0]->getId());
        self::assertSame(2, $queryObject->languages[1]->getId());
        self::assertSame(State::NEW, $queryObject->state);
    }

    public function testEmptyStringEntity(): void
    {
        $params = [
            'language' => '',
        ];

        $request = Request::create('?' . http_build_query($params));
        $queryObject = $this->converter->convert($request, TestRequestEmptyQuery::class);

        self::assertNull($queryObject->language);
    }

    public function testInvalid(): void
    {
        $request = Request::create('?language=1');
        $this->expectException(ValidationException::class);
        $this->converter->convert($request, TestRequestQuery::class);
    }

    public function testInvalidInner(): void
    {
        $params = [
            'stringRequiredParam' => 'z',
            'intRequiredParam' => 20,
            'boolRequiredParam' => true,
            'language' => 1,
        ];

        $request = Request::create('?' . http_build_query($params));

        try {
            $this->converter->convert($request, TestRequestQuery::class);
        } catch (ValidationException $e) {
            $errors = $e->getValidationErrors() ?? throw new LogicException();
            self::assertSame('filter.hello', $errors[0]['field']);
        }
    }

    public function testInvalidNullInner(): void
    {
        $request = Request::create('?filter=');

        $this->expectException(ValidationException::class);
        $this->converter->convert($request, TestRequestQuery::class);
    }
}
