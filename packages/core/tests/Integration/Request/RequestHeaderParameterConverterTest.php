<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration\Request;

use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use UXF\Core\Exception\ValidationException;
use UXF\Core\RequestConverter\HeaderParameterConverter;
use UXF\CoreTests\Integration\DbHelper;
use UXF\CoreTests\Project\HttpHeader\TestRequestHeader;

class RequestHeaderParameterConverterTest extends KernelTestCase
{
    private HeaderParameterConverter $converter;

    protected function setUp(): void
    {
        self::bootKernel();
        DbHelper::initDatabase(self::getContainer());

        /** @var HeaderParameterConverter $converter */
        $converter = self::getContainer()->get(HeaderParameterConverter::class);
        $this->converter = $converter;
    }

    public function testValid(): void
    {
        $request = Request::create('', 'POST', server: [
            'HTTP_X_TENANT' => '00000000-0000-4000-0000-000000000001',
        ]);

        self::assertEquals(
            new TestRequestHeader(Uuid::fromString('00000000-0000-4000-0000-000000000001')),
            $this->converter->convert($request, TestRequestHeader::class),
        );
    }

    public function testInvalid(): void
    {
        $request = Request::create('', 'POST', server: [
            'HTTP_X_TENANT' => '00000000-0000-4000-0000-00000000000x',
        ]);

        $this->expectException(ValidationException::class);
        $this->converter->convert($request, TestRequestHeader::class);
    }
}
