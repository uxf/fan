<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration\Request;

use PHPUnit\Framework\TestCase;
use UXF\Core\Exception\ValidationException;
use UXF\Core\RequestConverter\EnumParameterConverter;
use UXF\CoreTests\Project\Entity\NumericEnum;
use UXF\CoreTests\Project\Entity\State;

class EnumParameterConverterTest extends TestCase
{
    public function testValid(): void
    {
        $actual = EnumParameterConverter::convert('NEW', State::class, '');
        self::assertInstanceOf(State::class, $actual);
        self::assertSame(State::NEW, $actual);
    }

    public function testValidInt(): void
    {
        $actual = EnumParameterConverter::convert('1', NumericEnum::class, '');
        self::assertInstanceOf(NumericEnum::class, $actual);
        self::assertSame(NumericEnum::ONE, $actual);
    }

    public function testInvalid(): void
    {
        $this->expectException(ValidationException::class);
        EnumParameterConverter::convert('UNKNOWN', State::class, '');
    }
}
