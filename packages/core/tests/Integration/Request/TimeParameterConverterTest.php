<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration\Request;

use PHPUnit\Framework\TestCase;
use UXF\Core\Exception\ValidationException;
use UXF\Core\RequestConverter\TimeParameterConverter;
use UXF\Core\Type\Time;

class TimeParameterConverterTest extends TestCase
{
    public function testValid(): void
    {
        $actual = TimeParameterConverter::convert('10:11:12', '');
        self::assertEquals(new Time('10:11:12'), $actual);
    }

    public function testInvalid(): void
    {
        $this->expectException(ValidationException::class);
        TimeParameterConverter::convert('1992-04-31', '');
    }
}
