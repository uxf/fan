<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use UXF\CoreTests\Project\Entity\Hospital;
use function Safe\json_encode;

class JsonbContainsTest extends KernelTestCase
{
    public function testSQL(): void
    {
        /** @var EntityManagerInterface $em */
        $em = self::getContainer()->get(EntityManagerInterface::class);

        $sql = $em->createQueryBuilder()
            ->select('l')
            ->from(Hospital::class, 'l')
            ->where('JSONB_CONTAINS(l.data, :value) = TRUE')
            ->setParameter('value', json_encode([1]))
            ->getQuery()
            ->getSQL();

        self::assertSame(
            'SELECT h0_.id AS id_0, h0_.data AS data_1 FROM hospital h0_ WHERE jsonb_contains(h0_.data, ?) = 1',
            $sql,
        );
    }

    public function test(): void
    {
        self::bootKernel();
        DbHelper::initDatabase(self::getContainer());

        /** @var EntityManagerInterface $em */
        $em = self::getContainer()->get(EntityManagerInterface::class);

        $qb = $em->createQueryBuilder()
            ->select('l')
            ->from(Hospital::class, 'l')
            ->where('JSONB_CONTAINS(l.data, :value) = TRUE');

        $qb->setParameter('value', json_encode([1]));
        self::assertInstanceOf(Hospital::class, $qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', json_encode(['1']));
        self::assertNull($qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', json_encode(['2']));
        self::assertInstanceOf(Hospital::class, $qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', json_encode([2]));
        self::assertNull($qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', json_encode([100]));
        self::assertInstanceOf(Hospital::class, $qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', json_encode([100, 101]));
        self::assertInstanceOf(Hospital::class, $qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', json_encode([99, 100]));
        self::assertNull($qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', json_encode([true]));
        self::assertInstanceOf(Hospital::class, $qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', json_encode([
            'name' => 'NAME',
        ]));
        self::assertInstanceOf(Hospital::class, $qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', json_encode([
            'name' => 'NOT',
        ]));
        self::assertNull($qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', json_encode([[
            'name' => 'XXX',
        ]]));
        self::assertInstanceOf(Hospital::class, $qb->getQuery()->getOneOrNullResult());
    }
}
