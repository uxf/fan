<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use UXF\CoreTests\Project\Entity\Display;
use function Safe\json_encode;

class JsonbContainsSubstringTest extends KernelTestCase
{
    public function testSQL(): void
    {
        /** @var EntityManagerInterface $em */
        $em = self::getContainer()->get(EntityManagerInterface::class);

        $sql = $em->createQueryBuilder()
            ->select('l')
            ->from(Display::class, 'l')
            ->where('JSONB_CONTAINS_SUBSTRING(l.data, :value) = TRUE')
            ->setParameter('value', json_encode([1]))
            ->getQuery()
            ->getSQL();

        self::assertSame(
            'SELECT d0_.id AS id_0, d0_.data AS data_1 FROM display d0_ WHERE jsonb_contains_substring(d0_.data, ?) = 1',
            $sql,
        );
    }

    public function test(): void
    {
        self::bootKernel();
        DbHelper::initDatabase(self::getContainer());

        /** @var EntityManagerInterface $em */
        $em = self::getContainer()->get(EntityManagerInterface::class);

        $qb = $em->createQueryBuilder()
            ->select('l')
            ->from(Display::class, 'l')
            ->where('JSONB_CONTAINS_SUBSTRING(l.data, :value) = TRUE');

        $qb->setParameter('value', 'HELLO');
        self::assertInstanceOf(Display::class, $qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', 'HELL%');
        self::assertInstanceOf(Display::class, $qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', '%ELLO');
        self::assertInstanceOf(Display::class, $qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', '%ELL%');
        self::assertInstanceOf(Display::class, $qb->getQuery()->getOneOrNullResult());

        $qb->setParameter('value', 'XHELLO');
        self::assertNull($qb->getQuery()->getOneOrNullResult());
    }
}
