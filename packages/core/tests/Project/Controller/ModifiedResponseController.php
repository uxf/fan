<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Controller;

use Symfony\Component\HttpFoundation\Response;
use UXF\Core\Http\ResponseModifier;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Phone;
use UXF\CoreTests\Project\Http\TestResponseBody;

/**
 * @implements ResponseModifier<TestResponseBody>
 */
final class ModifiedResponseController implements ResponseModifier
{
    public function __invoke(): TestResponseBody
    {
        return new TestResponseBody(
            dateTime: new DateTime('2020-03-04'),
            phone: Phone::of('+420777666777'),
        );
    }

    public static function modifyResponse(Response $response, mixed $data): void
    {
        $response->setStatusCode(509);
        $response->headers->set('X-Test', 'hello');
    }
}
