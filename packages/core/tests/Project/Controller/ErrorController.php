<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Controller;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use UXF\Core\Exception\BasicException;
use UXF\Core\Exception\ValidationException;
use UXF\Hydrator\Exception\HydratorCollectionException;
use UXF\Hydrator\Exception\HydratorException;

class ErrorController
{
    public function __invoke(string $type): void
    {
        match ($type) {
            'basic' => throw BasicException::badRequest('message', 'ERROR_CODE'),
            'validation' => throw new ValidationException([[
                'field' => 'x',
                'message' => 'problem',
            ]]),
            'validation-array' => $this->errors(),
            'validation-list' => $this->errorList(),
            'http' => throw new HttpException(401),
            default => throw new Exception('message'),
        };
    }

    private function errors(): never
    {
        $collection = new HydratorCollectionException([
            'a' => new HydratorException([
                'X' => ['problem 1'],
            ]),
            'b' => new HydratorException([
                'Y' => ['problem 2', 'problem 3'],
            ]),
        ]);

        throw ValidationException::fromHydratorCollectionException($collection);
    }

    private function errorList(): never
    {
        $collection = new HydratorCollectionException([
            new HydratorException([
                'X' => ['problem 1'],
            ]),
            new HydratorException([
                'Y' => ['problem 2', 'problem 3'],
            ]),
        ]);

        throw ValidationException::fromHydratorCollectionException($collection);
    }
}
