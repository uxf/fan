<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

final readonly class InjectedObject
{
    public function __construct(public string $defaultLocale)
    {
    }
}
