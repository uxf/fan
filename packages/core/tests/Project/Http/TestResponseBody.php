<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

use UXF\Core\Type\DateTime;
use UXF\Core\Type\Phone;

final readonly class TestResponseBody
{
    public function __construct(
        public DateTime $dateTime,
        public Phone $phone,
    ) {
    }
}
