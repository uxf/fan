<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

use Symfony\Component\Validator\Constraints as Assert;
use UXF\CoreTests\Project\Entity\Language;
use UXF\CoreTests\Project\Entity\State;

final readonly class TestRequestBody
{
    /**
     * @param Language[] $languages
     */
    public function __construct(
        public Language $language,
        public ?Language $privateLanguage = null,
        public array $languages = [],
        public ?Language $nullableLanguage = null,
        public ?Language $hackingLanguage = null,
        public ?State $state = null,
        #[Assert\GreaterThan(value: 0)]
        public int $int = 0,
    ) {
    }
}
