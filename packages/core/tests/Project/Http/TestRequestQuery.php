<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

use Symfony\Component\Validator\Constraints as Assert;
use UXF\CoreTests\Project\Entity\Language;
use UXF\CoreTests\Project\Entity\State;

final readonly class TestRequestQuery
{
    /**
     * @param Language[] $languages
     */
    public function __construct(
        public string $stringParam = '',
        public ?string $stringNullParam = null,
        #[Assert\NotNull]
        public string $stringRequiredParam = '',
        public int $intParam = 0,
        public ?int $intNullParam = null,
        #[Assert\GreaterThan(value: 0)]
        public int $intRequiredParam = 0,
        public bool $boolParam = false,
        public ?bool $boolNullParam = null,
        #[Assert\IsTrue]
        public bool $boolRequiredParam = false,
        #[Assert\NotNull]
        public ?Language $language = null,
        public array $languages = [],
        public ?State $state = null,
        #[Assert\Valid]
        public ?TestRequestQueryInner $filter = new TestRequestQueryInner(),
    ) {
    }
}
