<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

use Ramsey\Uuid\UuidInterface;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\CoreTests\Project\Entity\Language;

class TestNullableRequestBody
{
    /**
     * @param Language[]|null $languages
     */
    public function __construct(
        public ?Language $language,
        public ?array $languages,
        public ?UuidInterface $uuid,
        public ?Date $date,
        public ?DateTime $dateTime,
        public ?Time $time,
        public ?Phone $phone,
        public ?Email $email,
        public ?NationalIdentificationNumberCze $ninCze,
        public ?BankAccountNumberCze $banCze,
        public ?Money $money,
        public ?Decimal $decimal,
        public ?Url $url,
        public string|null|NotSet $notSet = new NotSet(),
    ) {
    }
}
