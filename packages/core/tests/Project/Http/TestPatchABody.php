<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

use JsonSerializable;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\Http\Request\NotSetTrait;

final readonly class TestPatchABody implements JsonSerializable
{
    use NotSetTrait;

    /**
     * @param string[]|null|NotSet $array
     * @param string[]|null|NotSet $arrayNotSet
     * @param TestPatchBBody[]|null|NotSet $children
     * @param TestPatchBBody[]|null|NotSet $childrenNotSet
     */
    public function __construct(
        public string | null | NotSet $string = new NotSet(),
        public string | null | NotSet $stringNotSet = new NotSet(),
        public array | null | NotSet $array = new NotSet(),
        public array | null | NotSet $arrayNotSet = new NotSet(),
        public TestPatchBBody | null | NotSet $child = new NotSet(),
        public TestPatchBBody | null | NotSet $childNotSet = new NotSet(),
        public array | null | NotSet $children = new NotSet(),
        public array | null | NotSet $childrenNotSet = new NotSet(),
    ) {
    }
}
