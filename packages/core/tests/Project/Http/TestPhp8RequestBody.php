<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints\Email as AssertEmail;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\CoreTests\Project\Entity\Language;

class TestPhp8RequestBody
{
    /**
     * @param Language[] $emptyArray
     */
    public function __construct(
        #[NotBlank] public string $emptyString,
        #[NotBlank] public array $emptyArray,
        #[NotNull] public ?Language $language,
        #[AssertEmail] public string $rawEmail,
        #[GreaterThan(value: 0)] public int $int,
        public UuidInterface $uuid,
        public Date $date,
        public DateTime $dateTime,
        public Time $time,
        public Phone $phone,
        public Email $email,
        public NationalIdentificationNumberCze $ninCze,
        public BankAccountNumberCze $banCze,
        public Money $money,
        public Decimal $decimal,
        public Url $url,
    ) {
    }
}
