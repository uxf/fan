<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

use UXF\Core\Attribute\Entity;
use UXF\CoreTests\Project\Entity\Wizard;

final readonly class WizardRequestBody
{
    /**
     * @param Wizard[] $wizards
     * @param Wizard[] $wizardNames
     * @param Wizard[] $wizardUuids
     * @param Wizard[] $wizardRefs
     * @param Wizard[]|null $wizardsNullable
     * @param Wizard[]|null $wizardsNullableOptional
     * @param Wizard[] $wizardsOptional
     */
    public function __construct(
        #[Entity] public Wizard $wizard,
        #[Entity('name')] public Wizard $wizardName,
        #[Entity('uuid')] public Wizard $wizardUuid,
        #[Entity('item')] public Wizard $wizardRef,
        #[Entity('enum')] public Wizard $wizardEnum,
        #[Entity('enumInt')] public Wizard $wizardEnumInt,
        #[Entity('hidden')] public Wizard $wizardHidden,
        #[Entity] public array $wizards,
        #[Entity('name')] public array $wizardNames,
        #[Entity('uuid')] public array $wizardUuids,
        #[Entity('item')] public array $wizardRefs,
        #[Entity('name')] public ?Wizard $wizardNullable,
        #[Entity('uuid')] public ?array $wizardsNullable,
        #[Entity('name')] public ?Wizard $wizardNullableOptional = null,
        #[Entity('name')] public ?array $wizardsNullableOptional = null,
        #[Entity('name')] public array $wizardsOptional = [],
    ) {
    }
}
