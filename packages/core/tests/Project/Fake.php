<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project;

final readonly class Fake
{
    public function tester(): void
    {
        $fake = new FakeField(); // @phpstan-ignore-line uxf
        $fake->forbidden(); // @phpstan-ignore-line uxf
        FakeField::forbiddenStatic(); // @phpstan-ignore-line uxf
    }
}
