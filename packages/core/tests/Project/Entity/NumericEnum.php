<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Entity;

enum NumericEnum: int
{
    case ZERO = 0;
    case ONE = 1;
}
