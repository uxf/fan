<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

#[ORM\Entity]
class Wizard extends SuperWizard
{
    public function __construct(
        #[ORM\Column(type: 'uuid')]
        public UuidInterface $uuid,
        #[ORM\Column]
        public string $name,
        #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
        public Wizard $item,
        #[ORM\Column]
        public State $enum,
        #[ORM\Column]
        public NumericEnum $enumInt,
    ) {
    }
}
