<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Entity;

enum State: string
{
    case NEW = 'NEW';
    case OLD = 'OLD';
}
