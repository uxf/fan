<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Entity;

use Doctrine\ORM\Mapping as ORM;
use Stringable;

#[ORM\Entity]
class Language implements Stringable
{
    #[ORM\Column, ORM\Id]
    private int $id;

    #[ORM\Column]
    private string $name;

    private function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public static function create(int $id, string $name): self
    {
        return new self($id, $name);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    // for Lst tests
    public function __toString(): string
    {
        return (string) $this->id;
    }
}
