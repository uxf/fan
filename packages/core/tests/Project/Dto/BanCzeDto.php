<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use UXF\Core\Type\BankAccountNumberCze;

final readonly class BanCzeDto
{
    /**
     * @param BankAccountNumberCze[] $a2
     * @param BankAccountNumberCze[] $b2
     * @param BankAccountNumberCze[] $c2
     */
    public function __construct(
        public BankAccountNumberCze $a1,
        public array $a2,
        public ?BankAccountNumberCze $b1,
        public ?array $b2,
        public ?BankAccountNumberCze $c1 = null,
        public ?array $c2 = [],
    ) {
    }
}
