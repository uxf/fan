<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use UXF\Core\Type\Email;

final readonly class EmailDto
{
    /**
     * @param Email[] $a2
     * @param Email[] $b2
     * @param Email[] $c2
     */
    public function __construct(
        public Email $a1,
        public array $a2,
        public ?Email $b1,
        public ?array $b2,
        public ?Email $c1 = null,
        public ?array $c2 = [],
    ) {
    }
}
