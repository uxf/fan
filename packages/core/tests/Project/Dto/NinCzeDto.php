<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use UXF\Core\Type\NationalIdentificationNumberCze;

final readonly class NinCzeDto
{
    /**
     * @param NationalIdentificationNumberCze[] $a2
     * @param NationalIdentificationNumberCze[] $b2
     * @param NationalIdentificationNumberCze[] $c2
     */
    public function __construct(
        public NationalIdentificationNumberCze $a1,
        public array $a2,
        public ?NationalIdentificationNumberCze $b1,
        public ?array $b2,
        public ?NationalIdentificationNumberCze $c1 = null,
        public ?array $c2 = [],
    ) {
    }
}
