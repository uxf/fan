<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use UXF\Core\Type\Time;

final readonly class TimeDto
{
    /**
     * @param Time[] $a2
     * @param Time[] $b2
     * @param Time[] $c2
     */
    public function __construct(
        public Time $a1,
        public array $a2,
        public ?Time $b1,
        public ?array $b2,
        public ?Time $c1 = null,
        public ?array $c2 = [],
    ) {
    }
}
