<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\HttpHeader;

use Ramsey\Uuid\UuidInterface;

class TestRequestHeader
{
    public function __construct(public ?UuidInterface $xTenant = null)
    {
    }
}
