<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Core\EventSubscriber\ExceptionSubscriber;
use UXF\Core\EventSubscriber\ObjectResponseSubscriber;
use UXF\Core\Hydrator\Hydrator;
use UXF\Core\Hydrator\ParameterGenerator\BankAccountNumberCzeParameterGenerator;
use UXF\Core\Hydrator\ParameterGenerator\DateParameterGenerator;
use UXF\Core\Hydrator\ParameterGenerator\DateTimeParameterGenerator;
use UXF\Core\Hydrator\ParameterGenerator\DecimalParameterGenerator;
use UXF\Core\Hydrator\ParameterGenerator\DoctrineParameterGenerator;
use UXF\Core\Hydrator\ParameterGenerator\EmailParameterGenerator;
use UXF\Core\Hydrator\ParameterGenerator\MoneyParameterGenerator;
use UXF\Core\Hydrator\ParameterGenerator\NationalIdentificationNumberCzeParameterGenerator;
use UXF\Core\Hydrator\ParameterGenerator\PhoneParameterGenerator;
use UXF\Core\Hydrator\ParameterGenerator\TimeParameterGenerator;
use UXF\Core\Hydrator\ParameterGenerator\UrlParameterGenerator;
use UXF\Core\Hydrator\ParameterGenerator\UuidParameterGenerator;
use UXF\Core\Hydrator\UxfHydrator;
use UXF\Core\RequestConverter\BodyParameterConverter;
use UXF\Core\RequestConverter\EntityParameterConverter;
use UXF\Core\RequestConverter\HeaderParameterConverter;
use UXF\Core\RequestConverter\QueryParameterConverter;
use UXF\Core\ValueResolver\CoreValueResolver;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $services->set(ExceptionSubscriber::class)
        ->arg('$env', '%kernel.environment%')
        ->arg('$debug', '%kernel.debug%');

    $services->set(ObjectResponseSubscriber::class);

    $services->set(CoreValueResolver::class)
        ->tag('controller.argument_value_resolver', [
            'priority' => 1024,
        ]);

    $services->set(BodyParameterConverter::class);
    $services->set(EntityParameterConverter::class);
    $services->set(HeaderParameterConverter::class);
    $services->set(QueryParameterConverter::class);

    $services->set(Hydrator::class, UxfHydrator::class);
    $services->set(DateParameterGenerator::class);
    $services->set(DateTimeParameterGenerator::class);
    $services->set(DoctrineParameterGenerator::class);
    $services->set(MoneyParameterGenerator::class);
    $services->set(DecimalParameterGenerator::class);
    $services->set(UrlParameterGenerator::class);
    $services->set(PhoneParameterGenerator::class);
    $services->set(EmailParameterGenerator::class);
    $services->set(NationalIdentificationNumberCzeParameterGenerator::class);
    $services->set(BankAccountNumberCzeParameterGenerator::class);
    $services->set(TimeParameterGenerator::class);
    $services->set(UuidParameterGenerator::class);
};
