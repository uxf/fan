<?php

declare(strict_types=1);

namespace UXF\Core\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
final readonly class AutocompleteFields
{
    /**
     * @param array<literal-string> & non-empty-array $fields
     */
    public function __construct(
        public string $name,
        public array $fields,
    ) {
    }
}
