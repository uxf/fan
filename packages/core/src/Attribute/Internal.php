<?php

declare(strict_types=1);

namespace UXF\Core\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
final readonly class Internal
{
    /**
     * @param class-string $className
     */
    public function __construct(
        public string $className,
    ) {
    }
}
