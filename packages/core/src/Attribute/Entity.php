<?php

declare(strict_types=1);

namespace UXF\Core\Attribute;

use Attribute;

/**
 * @final
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
readonly class Entity
{
    /**
     * @param literal-string $property
     */
    public function __construct(public string $property = 'id')
    {
    }
}
