<?php

declare(strict_types=1);

namespace UXF\Core\Attribute;

use Attribute;

/**
 * @final
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
readonly class FromBody
{
    /**
     * @phpstan-param class-string $arrayClassName - specify array item type
     */
    public function __construct(public ?string $arrayClassName = null)
    {
    }
}
