<?php

declare(strict_types=1);

namespace UXF\Core\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS_CONSTANT)]
final readonly class Label
{
    public function __construct(
        public string $label,
        public ?string $color = null,
    ) {
    }
}
