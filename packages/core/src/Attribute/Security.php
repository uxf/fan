<?php

declare(strict_types=1);

namespace UXF\Core\Attribute;

use Attribute;
use BackedEnum;

#[Attribute(Attribute::TARGET_METHOD)]
final readonly class Security
{
    /**
     * @param BackedEnum|BackedEnum[] $role
     */
    public function __construct(
        public BackedEnum|array $role,
    ) {
    }

    /**
     * @return BackedEnum[]
     */
    public function getRoles(): array
    {
        return is_array($this->role) ? $this->role : [$this->role];
    }
}
