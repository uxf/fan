<?php

declare(strict_types=1);

namespace UXF\Core\Http\Request;

use Attribute;
use UXF\Core\Attribute\FromBody as CoreFromBody;

/**
 * @deprecated Please use UXF\Core\Attribute\FromBody
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
final readonly class FromBody extends CoreFromBody
{
}
