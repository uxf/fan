<?php

declare(strict_types=1);

namespace UXF\Core\Http\Request;

use Attribute;
use UXF\Core\Attribute\FromHeader as CoreFromHeader;

/**
 * @deprecated Please use UXF\Core\Attribute\FromHeader
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
final readonly class FromHeader extends CoreFromHeader
{
}
