<?php

declare(strict_types=1);

namespace UXF\Core\Http\Request;

trait NotSetTrait
{
    public function jsonSerialize(): object
    {
        $properties = (array) $this;
        foreach ($properties as $name => $value) {
            if ($value instanceof NotSet) {
                unset($properties[$name]);
            }
        }
        return (object) $properties;
    }
}
