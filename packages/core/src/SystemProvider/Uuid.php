<?php

declare(strict_types=1);

namespace UXF\Core\SystemProvider;

use Ramsey\Uuid\Uuid as RamseyUuid;
use Ramsey\Uuid\UuidInterface;

final class Uuid
{
    public static ?int $seq = null;

    public static function uuid4(): UuidInterface
    {
        if (self::$seq === null) {
            return RamseyUuid::uuid4();
        }

        return RamseyUuid::fromString(sprintf('00000000-0000-4000-0000-%012s', dechex(self::$seq++)));
    }

    public static function uuid7(): UuidInterface
    {
        if (self::$seq === null) {
            return RamseyUuid::uuid7(Clock::now());
        }

        // The first 36 bits have been dedicated to the Unix Timestamp (unixts)
        // All 12 bits of scenario subsec_a is fully dedicated to millisecond information (msec).
        [$a, $b] = str_split(sprintf('%012x', Clock::now()->format('Uv')), 8);
        return RamseyUuid::fromString(sprintf('%s-%s-7000-0000-%012s', $a, $b, dechex(self::$seq++)));
    }
}
