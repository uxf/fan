<?php

declare(strict_types=1);

namespace UXF\Core\PHPStanRules;

use PhpParser\Node;
use PhpParser\Node\Expr\StaticCall;
use PHPStan\Analyser\Scope;
use PHPStan\Reflection\ReflectionProvider;
use PHPStan\Rules\Rule;
use ReflectionException;

/**
 * @implements Rule<StaticCall>
 */
final class InternalEnforcementStaticRule implements Rule
{
    public function __construct(
        private ReflectionProvider $reflectionProvider,
    ) {
    }

    public function getNodeType(): string
    {
        return StaticCall::class;
    }

    /**
     * @inheritDoc
     */
    public function processNode(Node $node, Scope $scope): array
    {
        if (!$node->class instanceof Node\Name) {
            return []; // TODO eg. $this->enum::cases()
        }

        $methodClass = $node->class->name ?? '';
        if (!$node->name instanceof Node\Identifier || !class_exists($methodClass)) {
            return [];
        }

        $methodName = $node->name->name;

        try {
            $reflectionMethod = $this->reflectionProvider->getClass($methodClass)->getNativeReflection()->getMethod($methodName);
        } catch (ReflectionException) {
            return [];
        }

        return InternalEnforcementCheck::check($reflectionMethod, $scope);
    }
}
