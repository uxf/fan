<?php

declare(strict_types=1);

namespace UXF\Core\Utils;

use Closure;
use JsonSerializable;

/**
 * Experimental list class
 *
 * @template T
 */
final readonly class Lst implements JsonSerializable
{
    /**
     * @param list<T> $values
     */
    public function __construct(private array $values)
    {
    }

    /**
     * @template TX
     * @param iterable<TX> $values
     * @return self<TX>
     */
    public static function from(iterable $values): self
    {
        if (!is_array($values)) {
            $values = iterator_to_array($values);
        }

        if (array_is_list($values)) {
            return new self($values);
        }

        return new self(array_values($values));
    }

    /**
     * @phpstan-param Closure(T $item): bool $fn
     * @return T|null
     */
    public function find(Closure $fn): mixed
    {
        foreach ($this->values as $value) {
            if ($fn($value)) {
                return $value;
            }
        }

        return null;
    }

    /**
     * @phpstan-pure
     * @template T2
     * @phpstan-param Closure(T $item, int $index): T2 $fn
     * @return self<T2>
     */
    public function map(Closure $fn): self
    {
        $result = [];
        foreach ($this->values as $index => $item) {
            $result[] = $fn($item, $index);
        }
        return new self($result);
    }

    /**
     * @phpstan-pure
     * @phpstan-param Closure(T $item): bool $fn
     * @return self<T>
     */
    public function filter(Closure $fn): self
    {
        return new self(array_values(array_filter($this->values, $fn)));
    }

    /**
     * @phpstan-pure
     * @phpstan-param (Closure(T $a, T $b): int)|null $fn
     * @return self<T>
     */
    public function sort(?Closure $fn = null): self
    {
        $tmp = $this->values;

        if ($fn === null) {
            sort($tmp);
        } else {
            usort($tmp, $fn);
        }

        return new self($tmp);
    }

    /**
     * @phpstan-pure
     * @param SORT_REGULAR|SORT_NUMERIC|SORT_STRING|SORT_LOCALE_STRING $flags
     * @return self<T>
     */
    public function unique(int $flags = SORT_REGULAR): self
    {
        return self::from(array_unique($this->values, $flags));
    }

    /**
     * @phpstan-pure
     * @param self<T> $other
     * @return self<T>
     */
    public function concat(self $other): self
    {
        return new self([...$this->values, ...$other->values]);
    }

    /**
     * @phpstan-pure
     * @param T $item
     * @return self<T>
     */
    public function push(mixed $item): self
    {
        return new self([...$this->values, $item]);
    }

    /**
     * @phpstan-pure
     * @param T $item
     * @return self<T>
     */
    public function unshift(mixed $item): self
    {
        return new self([$item, ...$this->values]);
    }

    /**
     * @phpstan-pure
     * @return self<T>
     */
    public function slice(int $offset, ?int $length = null): self
    {
        return new self(array_slice($this->values, $offset, $length));
    }

    public function join(string $separator = ''): string
    {
        return implode($separator, $this->values);
    }

    /**
     * @template TAcc
     * @param TAcc $acc
     * @param Closure(TAcc, T): TAcc $fn
     * @return TAcc
     */
    public function aggregate(mixed $acc, Closure $fn): mixed
    {
        foreach ($this->values as $item) {
            $acc = $fn($acc, $item);
        }
        return $acc;
    }

    /**
     * @param Closure(T $fn, int $index): mixed $fn
     */
    public function forEach(Closure $fn): void
    {
        foreach ($this->values as $index => $item) {
            $fn($item, $index);
        }
    }

    /**
     * @template TKey
     * @template TValue
     * @param Closure(T): TKey $keyFn
     * @param Closure(T): TValue $valueFn
     * @return array<TKey, TValue>
     */
    public function dictionary(Closure $keyFn, Closure $valueFn): array
    {
        $result = [];
        foreach ($this->values as $item) {
            $result[$keyFn($item)] = $valueFn($item);
        }
        return $result;
    }

    /**
     * @template TKey
     * @template TValue
     * @param Closure(T): TKey $keyFn
     * @param Closure(T): TValue $valueFn
     * @return array<TKey, TValue[]>
     */
    public function groupBy(Closure $keyFn, Closure $valueFn): array
    {
        $result = [];
        foreach ($this->values as $item) {
            $result[$keyFn($item)][] = $valueFn($item);
        }
        return $result;
    }

    /**
     * @return T|null
     */
    public function first(): mixed
    {
        return $this->values[0] ?? null;
    }

    /**
     * @return T|null
     */
    public function last(): mixed
    {
        return $this->values[count($this->values) - 1] ?? null;
    }

    public function count(): int
    {
        return count($this->values);
    }

    public function isEmpty(): bool
    {
        return $this->values === [];
    }

    /**
     * @param T $element
     */
    public function contains(mixed $element): bool
    {
        return in_array($element, $this->values, true);
    }

    /**
     * @return list<T>
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @return list<T>
     */
    public function jsonSerialize(): array
    {
        return $this->values;
    }
}
