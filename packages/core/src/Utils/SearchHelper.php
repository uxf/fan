<?php

declare(strict_types=1);

namespace UXF\Core\Utils;

use Nette\Utils\Strings;

/**
 * @deprecated user native DQL UNACCENT function (eg. UNACCENT(l.name) LIKE UNACCENT(:text))
 */
final readonly class SearchHelper
{
    public static function unaccentLike(string $value): string
    {
        return '%' . self::unaccent($value) . '%';
    }

    public static function unaccent(string $value): string
    {
        return Strings::lower(Strings::toAscii($value));
    }
}
