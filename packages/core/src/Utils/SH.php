<?php

declare(strict_types=1);

namespace UXF\Core\Utils;

use BackedEnum;
use DateTimeInterface;
use Exception;
use Nette\Utils\Strings;
use UXF\Core\Exception\BasicException;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use function Safe\preg_replace;

/**
 * String helper
 */
final readonly class SH
{
    public static function s2b(string $string): bool
    {
        return $string === 'true' || $string === 'TRUE' || $string === '1';
    }

    public static function s2i(string $value): int
    {
        if ((string) (int) $value === $value) {
            return (int) $value;
        }
        throw new BasicException("Value $value isn't valid integer.");
    }

    public static function s2f(string $value): float
    {
        $possibleResult = (string) (float) $value;
        if ($possibleResult === $value || $possibleResult === preg_replace('/\.0+/', '', $value)) {
            return (float) $value;
        }
        throw new BasicException("Value $value isn't valid float.");
    }

    public static function s2d(string $value): Date
    {
        try {
            return new Date($value);
        } catch (Exception) {
            throw new BasicException("Value $value isn't valid date.");
        }
    }

    public static function s2dt(string $value): DateTime
    {
        try {
            return (new DateTime($value))->normalizeTz();
        } catch (Exception) {
            throw new BasicException("Value $value isn't valid datetime.");
        }
    }

    public static function m2s(mixed $value): string
    {
        if ($value instanceof DateTimeInterface) {
            return $value->format('Y-m-d H:i:s');
        }

        if ($value instanceof BackedEnum) {
            return (string) $value->value;
        }

        return (string) $value;
    }

    public static function purge(?string $string): ?string
    {
        $string = Strings::trim($string ?? '');
        return $string !== '' ? $string : null;
    }

    /**
     * @phpstan-assert-if-false non-empty-string $string
     */
    public static function isEmpty(?string $string): bool
    {
        return Strings::trim($string ?? '') === '';
    }

    /**
     * @phpstan-assert-if-true non-empty-string $string
     */
    public static function isNotEmpty(?string $string): bool
    {
        return Strings::trim($string ?? '') !== '';
    }
}
