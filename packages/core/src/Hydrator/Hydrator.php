<?php

declare(strict_types=1);

namespace UXF\Core\Hydrator;

interface Hydrator
{
    /**
     * @template T of object
     * @param array<mixed> $data
     * @phpstan-param class-string<T> $class
     * @phpstan-return T
     */
    public function hydrateArray(array $data, string $class): mixed;

    /**
     * @template T of object
     * @param array<mixed> $data
     * @phpstan-param class-string<T> $class
     * @phpstan-return array<T>
     */
    public function hydrateArrays(array $data, string $class): array;
}
