<?php

declare(strict_types=1);

namespace UXF\Core\Hydrator\ParameterGenerator;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use UXF\Hydrator\Generator\GeneratorHelper;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class UuidParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        return GeneratorHelper::gen(
            generatorClass: __CLASS__,
            definition: $definition,
            constructor: "\\" . Uuid::class . "::fromString(\$data[\$name])",
            constructorArray: "\\" . Uuid::class . "::fromString(\$value)",
            errorMsgKey: 'uuid.invalid_format',
            errorArgs: "supportedFormat: 'GUID'",
        );
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        return !$definition->isUnion() && is_a($definition->getFirstType(), UuidInterface::class, true);
    }

    public static function getDefaultPriority(): int
    {
        return 200;
    }
}
