<?php

declare(strict_types=1);

namespace UXF\Core\Hydrator\ParameterGenerator;

use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use UXF\Core\Attribute\Entity;
use UXF\Core\Utils\ReflectionHelper;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class DoctrineParameterGenerator implements ParameterGenerator
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function generate(ParameterDefinition $definition, Options $options): string
    {
        $typeName = $definition->getFirstType();
        $name = $definition->name;
        $propertyName = 'id';

        foreach ($definition->attributes as $attribute) {
            if (is_a($attribute->getName(), Entity::class, true)) {
                $attributeInstance = $attribute->newInstance();
                assert($attributeInstance instanceof Entity);
                $propertyName = $attributeInstance->property;
                if (ReflectionHelper::findReflectionProperty($typeName, $propertyName) === null) {
                    throw new LogicException("Invalid property $typeName::\$$propertyName");
                }
                break;
            }
        }

        $body = '// ' . __CLASS__ . "\n";

        if (!$definition->array) {
            $body .= "if (\$data[\$name] instanceof \\$typeName) {\n";
            $body .= "    \$_$name = \$data[\$name];\n";
            $body .= "} elseif (\$data[\$name] !== null && (is_scalar(\$data[\$name]) || \$data[\$name] instanceof \Stringable || \$data[\$name] instanceof \BackedEnum)) {\n";
            $body .= "    \$_$name = \$this->container->get('doctrine')->getManager()->getRepository(\\$typeName::class)->findOneBy(['$propertyName' => \$data[\$name]]);\n";
            $body .= "    if (\$_$name === null) {\n";
            $body .= "        \$errors[\$path . \$name][] = \$this->translator->trans('doctrine.not_found', new ErrorInfo(\$data[\$name]));\n";
            $body .= "    }\n";
        } else {
            $body .= "if (is_array(\$data[\$name])) {\n";
            $body .= "    \$_$name = [];\n";
            $body .= "    foreach (\$data[\$name] as \$key => \$id) {\n";
            $body .= "        \$_{$name}[] = \$_tmp = \$this->container->get('doctrine')->getManager()->getRepository(\\$typeName::class)->findOneBy(['$propertyName' => \$id]);\n";
            $body .= "        if (\$_tmp === null) {\n";
            $body .= "            \$errors[\$path . \"{\$name}[\$key]\"][] = \$this->translator->trans('doctrine.not_found', new ErrorInfo(\$id));\n";
            $body .= "        }\n";
            $body .= "    }\n";
        }

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";
        }

        $msg = $definition->array ? 'array_invalid_value' : 'invalid_value';

        $body .= "} else {\n";
        $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.$msg', new ErrorInfo(\$data[\$name]));\n";
        $body .= "}\n";

        return $body;
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        return
            !$definition->isUnion() &&
            class_exists($definition->getFirstType()) &&
            !$this->entityManager->getMetadataFactory()->isTransient($definition->getFirstType());
    }

    public static function getDefaultPriority(): int
    {
        return 10;
    }
}
