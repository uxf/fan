<?php

declare(strict_types=1);

namespace UXF\Core\Hydrator\ParameterGenerator;

use UXF\Core\Type\DateTime;
use UXF\Hydrator\Generator\GeneratorHelper;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class DateTimeParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        return GeneratorHelper::gen(
            generatorClass: __CLASS__,
            definition: $definition,
            constructor: "(new \\" . DateTime::class . "(\$data[\$name]))->normalizeTz()",
            constructorArray: "(new \\" . DateTime::class . "(\$value))->normalizeTz()",
            errorMsgKey: 'date_time.invalid_format',
            errorArgs: "supportedFormat: 'ISO8601'",
        );
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        return !$definition->isUnion() && $definition->getFirstType() === DateTime::class;
    }

    public static function getDefaultPriority(): int
    {
        return 200;
    }
}
