<?php

declare(strict_types=1);

namespace UXF\Core\Hydrator\ParameterGenerator;

use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Hydrator\Generator\GeneratorHelper;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

class NationalIdentificationNumberCzeParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        return GeneratorHelper::of(
            generatorClass: __CLASS__,
            definition: $definition,
            errorMsgKey: 'nin_cze.invalid_format',
            errorArgs: "supportedFormat: '9302013545 or 930201/3545'",
        );
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        return !$definition->isUnion() && $definition->getFirstType() === NationalIdentificationNumberCze::class;
    }

    public static function getDefaultPriority(): int
    {
        return 200;
    }
}
