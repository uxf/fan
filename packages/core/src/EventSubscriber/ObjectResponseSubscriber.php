<?php

declare(strict_types=1);

namespace UXF\Core\EventSubscriber;

use stdClass;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use UXF\Core\Http\ResponseModifier;

final readonly class ObjectResponseSubscriber implements EventSubscriberInterface
{
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ViewEvent::class => 'processResponse',
        ];
    }

    public function processResponse(ViewEvent $event): void
    {
        $result = $event->getControllerResult();
        $controller = $event->getRequest()->attributes->get('_controller');

        $response = new JsonResponse();
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $response->setData($result ?? new stdClass());

        if (is_array($controller)) {
            $controller = $controller[0];
        }

        if (class_exists($controller) && is_a($controller, ResponseModifier::class, true)) {
            $controller::modifyResponse($response, $result);
        }

        $event->setResponse($response);
    }
}
