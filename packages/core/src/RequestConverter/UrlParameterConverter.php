<?php

declare(strict_types=1);

namespace UXF\Core\RequestConverter;

use Exception;
use UXF\Core\Exception\ValidationException;
use UXF\Core\Type\Url;

final readonly class UrlParameterConverter
{
    public static function convert(mixed $value, string $name): Url
    {
        try {
            return Url::of($value);
        } catch (Exception $e) {
            throw new ValidationException([[
                'field' => "url:$name",
                'message' => $e->getMessage(),
            ]]);
        }
    }
}
