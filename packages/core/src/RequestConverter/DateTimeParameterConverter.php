<?php

declare(strict_types=1);

namespace UXF\Core\RequestConverter;

use Exception;
use UXF\Core\Exception\ValidationException;
use UXF\Core\Type\DateTime;

final readonly class DateTimeParameterConverter
{
    public static function convert(mixed $value, string $name): DateTime
    {
        try {
            return (new DateTime($value))->normalizeTz();
        } catch (Exception $e) {
            throw new ValidationException([[
                'field' => "url:$name",
                'message' => $e->getMessage(),
            ]]);
        }
    }
}
