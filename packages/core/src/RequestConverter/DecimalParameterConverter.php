<?php

declare(strict_types=1);

namespace UXF\Core\RequestConverter;

use Exception;
use UXF\Core\Exception\ValidationException;
use UXF\Core\Type\Decimal;

final readonly class DecimalParameterConverter
{
    public static function convert(mixed $value, string $name): Decimal
    {
        try {
            return Decimal::of($value);
        } catch (Exception $e) {
            throw new ValidationException([[
                'field' => "url:$name",
                'message' => $e->getMessage(),
            ]]);
        }
    }
}
