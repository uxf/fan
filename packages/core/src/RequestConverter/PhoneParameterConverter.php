<?php

declare(strict_types=1);

namespace UXF\Core\RequestConverter;

use Exception;
use UXF\Core\Exception\ValidationException;
use UXF\Core\Type\Phone;

final readonly class PhoneParameterConverter
{
    public static function convert(mixed $value, string $name): Phone
    {
        try {
            return Phone::of($value);
        } catch (Exception $e) {
            throw new ValidationException([[
                'field' => "url:$name",
                'message' => $e->getMessage(),
            ]]);
        }
    }
}
