<?php

declare(strict_types=1);

namespace UXF\Core\RequestConverter;

use BackedEnum;
use ReflectionEnum;
use UXF\Core\Exception\ValidationException;

final readonly class EnumParameterConverter
{
    /**
     * @param class-string<BackedEnum> $class
     */
    public static function convert(mixed $value, string $class, string $name): ?BackedEnum
    {
        if ($value instanceof BackedEnum || $value === null) {
            return $value;
        }

        if (is_string($value) && (string) (new ReflectionEnum($class))->getBackingType() === 'int') {
            $value = (int) $value;
        }

        return $class::tryFrom($value) ?? throw new ValidationException([[
            'field' => "url:$name",
            'message' => "Invalid enum value '$value'",
        ]]);
    }
}
