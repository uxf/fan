<?php

declare(strict_types=1);

namespace UXF\Core\Contract\Autocomplete;

use Ramsey\Uuid\UuidInterface;

final readonly class AutocompleteResult
{
    public function __construct(
        public int|string|UuidInterface $id,
        public string $label,
    ) {
    }
}
