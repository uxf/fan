<?php

declare(strict_types=1);

namespace UXF\Core\Contract\Permission;

use UXF\Core\Attribute\Security;

interface RoleChecker
{
    public function check(Security $access): void;
}
