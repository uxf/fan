<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use UXF\Core\Type\Decimal;

final class DoctrineDecimalType extends StringType
{
    public static function register(): void
    {
        if (self::hasType(Decimal::class)) {
            return;
        }

        self::addType(Decimal::class, self::class);
    }

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        $column['precision'] ??= 24;
        $column['scale'] ??= 4;
        return $platform->getDecimalTypeDeclarationSQL($column);
    }

    public function convertToPHPValue(mixed $value, AbstractPlatform $platform): ?Decimal
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof Decimal) {
            return $value;
        }

        return Decimal::of($value);
    }

    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform): ?string
    {
        if ($value instanceof Decimal) {
            return $value->toString();
        }

        return $value;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    public function getName(): string
    {
        return Decimal::class;
    }
}
