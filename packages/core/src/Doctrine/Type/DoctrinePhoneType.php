<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use UXF\Core\Type\Phone;

final class DoctrinePhoneType extends StringType
{
    public static function register(): void
    {
        if (self::hasType(Phone::class)) {
            return;
        }

        self::addType(Phone::class, self::class);
    }

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        $column['length'] ??= 16;
        return parent::getSQLDeclaration($column, $platform);
    }

    public function convertToPHPValue(mixed $value, AbstractPlatform $platform): ?Phone
    {
        if ($value === null) {
            return null;
        }

        return Phone::createFromTrustedSource($value);
    }

    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform): ?string
    {
        return $value?->toString();
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    public function getName(): string
    {
        return Phone::class;
    }
}
