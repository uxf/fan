<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Type;

use DateTimeZone;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use UXF\Core\Type\DateTime;

final class DoctrineDateTimeType extends Type
{
    private static ?DateTimeZone $tz = null;

    public static function register(): void
    {
        if (self::hasType(DateTime::class)) {
            return;
        }

        self::addType(DateTime::class, self::class);
    }

    public function getName(): string
    {
        return DateTime::class;
    }

    /**
     * @param array<mixed> $column
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDateTimeTypeDeclarationSQL($column);
    }

    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform): ?string
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof DateTime) {
            // convert input TZ to system TZ
            if (self::getTz()->getName() !== $value->getTimezone()->getName()) {
                $value = $value->setTimezone(self::getTz());
            }

            return $value->format($platform->getDateTimeFormatString());
        }

        throw new ConversionException();
    }

    public function convertToPHPValue(mixed $value, AbstractPlatform $platform): mixed
    {
        if ($value === null || $value instanceof DateTime) {
            return $value;
        }

        $dateTime = DateTime::createFromFormat($platform->getDateTimeFormatString(), $value);

        if ($dateTime === false) {
            throw new ConversionException();
        }

        return $dateTime;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    private static function getTz(): DateTimeZone
    {
        return self::$tz ??= new DateTimeZone(date_default_timezone_get());
    }
}
