<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\StringType;
use UXF\Core\Type\BankAccountNumberCze;

final class DoctrineBankAccountNumberCzeType extends StringType
{
    public static function register(): void
    {
        if (self::hasType(BankAccountNumberCze::class)) {
            return;
        }

        self::addType(BankAccountNumberCze::class, self::class);
    }

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        $column['length'] ??= 22;
        return parent::getSQLDeclaration($column, $platform);
    }

    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform): ?string
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof BankAccountNumberCze) {
            return (string) $value;
        }

        throw new ConversionException();
    }

    public function convertToPHPValue(mixed $value, AbstractPlatform $platform): ?BankAccountNumberCze
    {
        if ($value === null || $value instanceof BankAccountNumberCze) {
            return $value;
        }

        return BankAccountNumberCze::createFromTrustedSource($value);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    public function getName(): string
    {
        return BankAccountNumberCze::class;
    }
}
