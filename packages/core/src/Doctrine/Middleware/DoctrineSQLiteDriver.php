<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Middleware;

use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Driver\Middleware\AbstractDriverMiddleware;
use PDO;
use SensitiveParameter;
use UXF\Core\Test\SqlitePlatform;

class DoctrineSQLiteDriver extends AbstractDriverMiddleware
{
    /**
     * @inheritDoc
     */
    public function connect(#[SensitiveParameter] array $params): Connection
    {
        $connection = parent::connect($params);

        $native = $connection->getNativeConnection();
        if ($native instanceof PDO) {
            $native->sqliteCreateFunction('unaccent', SqlitePlatform::unaccent(...), 1);
            $native->sqliteCreateFunction('jsonb_contains', SqlitePlatform::jsonbContains(...), 2);
            $native->sqliteCreateFunction('jsonb_contains_substring', SqlitePlatform::jsonbContainsSubstring(...), 2);
        }

        return $connection;
    }
}
