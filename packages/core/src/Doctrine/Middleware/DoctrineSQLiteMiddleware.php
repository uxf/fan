<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Middleware;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsMiddleware;
use Doctrine\DBAL\Driver;
use Doctrine\DBAL\Driver\Middleware;

#[AsMiddleware]
class DoctrineSQLiteMiddleware implements Middleware
{
    public function wrap(Driver $driver): Driver
    {
        return new DoctrineSQLiteDriver($driver);
    }
}
