<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Func;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\TokenType;

// JSONB_CONTAINS_SUBSTRING(root.items, :value) = TRUE
final class JsonbContainsSubstring extends FunctionNode
{
    public Node $column; // @phpstan-ignore-line
    public Node $value; // @phpstan-ignore-line

    public function getSql(SqlWalker $sqlWalker): string
    {
        $column = $this->column->dispatch($sqlWalker);
        $value = $this->value->dispatch($sqlWalker);

        if ($sqlWalker->getConnection()->getDatabasePlatform() instanceof PostgreSQLPlatform) {
            return "EXISTS (SELECT 1 FROM jsonb_array_elements_text($column) AS _tmp WHERE _tmp LIKE $value)";
        }

        return "jsonb_contains_substring($column, $value)";
    }

    public function parse(Parser $parser): void
    {
        $parser->match(TokenType::T_IDENTIFIER);
        $parser->match(TokenType::T_OPEN_PARENTHESIS);
        $this->column = $parser->StringPrimary();
        $parser->match(TokenType::T_COMMA);
        $this->value = $parser->StringPrimary();
        $parser->match(TokenType::T_CLOSE_PARENTHESIS);
    }
}
