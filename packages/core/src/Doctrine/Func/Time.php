<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Func;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\TokenType;
use RuntimeException;

final class Time extends FunctionNode
{
    public ?Node $string = null;

    public function getSql(SqlWalker $sqlWalker): string
    {
        if ($this->string === null) {
            throw new RuntimeException();
        }

        if ($sqlWalker->getConnection()->getDatabasePlatform() instanceof PostgreSQLPlatform) {
            // value::TIME
            return $this->string->dispatch($sqlWalker) . '::TIME';
        }

        // TIME(value)
        return 'TIME(' . $this->string->dispatch($sqlWalker) . ')';
    }

    public function parse(Parser $parser): void
    {
        $parser->match(TokenType::T_IDENTIFIER);
        $parser->match(TokenType::T_OPEN_PARENTHESIS);
        $this->string = $parser->StringPrimary();
        $parser->match(TokenType::T_CLOSE_PARENTHESIS);
    }
}
