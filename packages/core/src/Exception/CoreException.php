<?php

declare(strict_types=1);

namespace UXF\Core\Exception;

interface CoreException
{
    public function getHttpStatusCode(): int;
    public function getErrorCode(): string;
    public function getMessage(): string;
    /**
     * @return array<array{message: string, field: string}>|null
     */
    public function getValidationErrors(): ?array;
    /**
     * @return \Psr\Log\LogLevel::*
     */
    public function getLevel(): string;
}
