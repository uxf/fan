<?php

declare(strict_types=1);

namespace UXF\Core\Test;

use Nette\Utils\Json;
use Nette\Utils\Strings;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

class Client extends KernelBrowser
{
    private const array DEFAULT_HEADERS = [
        'CONTENT_TYPE' => 'application/json',
    ];

    /**
     * @param mixed[] $parameters
     * @param mixed[] $files
     * @param mixed[] $server
     */
    public function request(
        string $method,
        string $uri,
        array $parameters = [],
        array $files = [],
        array $server = [],
        ?string $content = null,
        bool $changeHistory = true,
    ): Crawler {
        return parent::request(
            $method,
            $uri,
            $parameters,
            $files,
            array_merge(self::DEFAULT_HEADERS, $server),
            $content,
            $changeHistory,
        );
    }

    /**
     * @param array<string, string> $headers
     */
    public function get(string $uri, array $headers = []): Crawler
    {
        return $this->request('GET', $uri, [], [], $headers);
    }

    /**
     * @param array<mixed>|object $body
     * @param array<string, string> $headers
     */
    public function post(string $uri, array|object $body, array $headers = []): Crawler
    {
        return $this->request('POST', $uri, [], [], $headers, Json::encode($body));
    }

    /**
     * @param array<mixed>|object $body
     * @param array<string, string> $headers
     */
    public function put(string $uri, array|object $body, array $headers = []): Crawler
    {
        return $this->request('PUT', $uri, [], [], $headers, Json::encode($body));
    }

    /**
     * @param array<mixed>|object $body
     * @param array<string, string> $headers
     */
    public function patch(string $uri, array|object $body, array $headers = []): Crawler
    {
        return $this->request('PATCH', $uri, [], [], $headers, Json::encode($body));
    }

    /**
     * @param array<string, string> $headers
     */
    public function delete(string $uri, array $headers = []): Crawler
    {
        return $this->request('DELETE', $uri, [], [], $headers);
    }

    public function login(string $username = 'root@uxf.cz', string $password = 'root'): Crawler
    {
        $response = $this->post('/api/auth/login', [
            'username' => $username,
            'password' => $password,
        ]);

        if ($this->getStatusCode() !== Response::HTTP_OK) {
            throw new RuntimeException("Login failed!\n" . Strings::truncate((string) $this->getResponse()->getContent(), 1000));
        }

        return $response;
    }

    public function logout(): Crawler
    {
        return $this->get('/api/auth/logout');
    }

    /**
     * @phpstan-impure
     */
    public function getResponseData(): mixed
    {
        return Json::decode((string) $this->getResponse()->getContent(), forceArrays: true);
    }

    public function getStatusCode(): int
    {
        return $this->getResponse()->getStatusCode();
    }
}
