<?php

declare(strict_types=1);

namespace UXF\Core\Test;

use Spatie\Snapshots\MatchesSnapshots;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as SymfonyWebTestCase;

abstract class WebTestCase extends SymfonyWebTestCase
{
    use MatchesSnapshots;

    /**
     * @param mixed[] $options
     * @param mixed[] $server
     */
    protected static function createClient(array $options = [], array $server = []): Client
    {
        /** @var Client $client */
        $client = parent::createClient($options, $server);

        return $client;
    }

    public function assertSnapshot(mixed $actual): void
    {
        $this->doSnapshotAssertion($actual, new JsonDriver());
    }
}
