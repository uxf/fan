<?php

declare(strict_types=1);

namespace UXF\Core\Test;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Platforms\SQLitePlatform;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\DefaultQuoteStrategy;
use Doctrine\ORM\Mapping\ManyToManyOwningSideMapping;

// hack for sqlite schemas
class SqliteDefaultQuoteStrategy extends DefaultQuoteStrategy
{
    public function getTableName(ClassMetadata $class, AbstractPlatform $platform): string
    {
        $tableName = $class->table['name'];

        if (($class->table['schema'] ?? '') !== '') {
            $tableName = $class->table['schema'] . '.' . $class->table['name'];

            if ($platform instanceof SQLitePlatform) {
                return $class->table['schema'] . '__' . $class->table['name'];
            }
        }

        return isset($class->table['quoted']) ? $platform->quoteIdentifier($tableName) : $tableName;
    }

    /**
     * @param ManyToManyOwningSideMapping|array<mixed> $association - BC with ORM 2
     */
    public function getJoinTableName(ManyToManyOwningSideMapping|array $association, ClassMetadata $class, AbstractPlatform $platform): string
    {
        if ($association instanceof ManyToManyOwningSideMapping) {
            $schema = $association->joinTable->schema;
            $tableName = $association->joinTable->name;
            $quoted = $association->joinTable->quoted;
        } else {
            $schema = $association['joinTable']['schema'] ?? null;
            $tableName = $association['joinTable']['name'];
            $quoted = $association['joinTable']['quoted'] ?? false;
        }

        if ($schema !== null) {
            if ($platform instanceof SQLitePlatform) {
                return $schema . '__' . $tableName;
            }

            $schema .= '.';
        }

        if ($quoted === true) {
            $tableName = $platform->quoteIdentifier($tableName);
        }

        return $schema . $tableName;
    }
}
