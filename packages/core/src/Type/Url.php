<?php

declare(strict_types=1);

namespace UXF\Core\Type;

use Exception;
use JsonSerializable;
use Stringable;
use UXF\Core\Exception\UrlException;
use function Safe\parse_url;

final readonly class Url implements JsonSerializable, Stringable, Equals
{
    private function __construct(
        private string $value,
    ) {
    }

    public static function of(string $value): self
    {
        if (!str_contains($value, 'localhost') && !str_contains($value, '.')) {
            throw new UrlException("Invalid url '{$value}'");
        }

        return self::parseWithoutDomainCheck($value);
    }

    public static function parseWithoutDomainCheck(string $value): self
    {
        $originalValue = $value;
        $value = trim($originalValue);

        if (filter_var($value, FILTER_VALIDATE_URL) === false) {
            $value = "https://$value";
            if (filter_var($value, FILTER_VALIDATE_URL) === false) {
                throw new UrlException("Invalid url '{$originalValue}'");
            }
        }

        return new self($value);
    }

    public static function parseNullable(?string $value): ?self
    {
        return $value === null ? null : self::of($value);
    }

    public static function tryParse(?string $value): ?self
    {
        if ($value === null) {
            return null;
        }

        try {
            return self::of($value);
        } catch (Exception) {
            return null;
        }
    }

    /**
     * pouziti pro doctrine
     * @internal
     */
    public static function createFromTrustedSource(string $value): self
    {
        return new self($value);
    }

    public function equals(?self $other): bool
    {
        return $this->value === $other?->value;
    }

    public function any(self ...$others): bool
    {
        foreach ($others as $other) {
            if ($other->equals($this)) {
                return true;
            }
        }

        return false;
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function getComponents(): UrlComponents
    {
        $components = parse_url($this->value);
        assert(is_array($components));
        if (isset($components['query'])) {
            parse_str($components['query'], $query);
            $components['query'] = $query;
        }

        return new UrlComponents(
            $components['scheme'],
            $components['host'],
            $components['port'] ?? null,
            $components['user'] ?? null,
            $components['pass'] ?? null,
            $components['path'] ?? null,
            $components['query'] ?? [],
            $components['fragment'] ?? null,
        );
    }

    public function jsonSerialize(): string
    {
        return $this->toString();
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    /**
     * @return array<mixed>
     */
    public function __serialize(): array
    {
        return [
            'value' => $this->value,
        ];
    }

    /**
     * @param array<mixed> $data
     */
    public function __unserialize(array $data): void
    {
        $this->value = $data['value'];
    }
}
