<?php

declare(strict_types=1);

namespace UXF\Core\Type;

use Brick\Math\Internal\Calculator;
use Exception;
use JsonSerializable;
use Stringable;

// https://www.cnb.cz/cs/platebni-styk/ucty-kody-bank/
final readonly class BankAccountNumberCze implements JsonSerializable, Stringable, Equals
{
    // Váhy pro kontrolu prefixu
    private const array PrefixWeights = [10, 5, 8, 4, 2, 1];

    // Váhy pro kontrolu základní části čísla
    private const array AccountWeights = [6, 3, 7, 9, 10, 5, 8, 4, 2, 1];

    // Výpis všech bank působících v ČR k datu 11. 02. 2023
    private const array Banks = [
        '0100' => 'Komerční banka, a.s.',
        '0300' => 'Československá obchodní banka, a. s.',
        '0600' => 'MONETA Money Bank, a.s.',
        '0710' => 'ČESKÁ NÁRODNÍ BANKA',
        '0800' => 'Česká spořitelna, a.s.',
        '2010' => 'Fio banka, a.s.',
        '2060' => 'Citfin, spořitelní družstvo',
        '2070' => 'TRINITY BANK a.s.',
        '2100' => 'Hypoteční banka, a.s.',
        '2200' => 'Peněžní dům, spořitelní družstvo',
        '2220' => 'Artesa, spořitelní družstvo',
        '2250' => 'Banka CREDITAS a.s.',
        '2260' => 'NEY spořitelní družstvo',
        '2275' => 'Podnikatelská družstevní záložna',
        '2600' => 'Citibank Europe plc, organizační složka',
        '2700' => 'UniCredit Bank Czech Republic and Slovakia, a.s.',
        '3030' => 'Air Bank a.s.',
        '3050' => 'BNP Paribas Personal Finance SA, odštěpný závod',
        '3060' => 'PKO BP S.A., Czech Branch',
        '3500' => 'ING Bank N.V.',
        '4000' => 'Max banka a.s.',
        '4300' => 'Národní rozvojová banka, a.s.',
        '5500' => 'Raiffeisenbank a.s.',
        '5800' => 'J&T BANKA, a.s.',
        '6000' => 'PPF banka a.s.',
        '6100' => 'Raiffeisenbank a.s. (do 31. 12. 2021 Equa bank a.s.)',
        '6200' => 'COMMERZBANK Aktiengesellschaft, pobočka Praha',
        '6210' => 'mBank S.A., organizační složka',
        '6300' => 'BNP Paribas S.A., pobočka Česká republika',
        '6363' => 'Partners Banka, a.s.',
        '6700' => 'Všeobecná úverová banka a.s., pobočka Praha',
        '6800' => 'Sberbank CZ, a.s. v likvidaci',
        '7910' => 'Deutsche Bank Aktiengesellschaft Filiale Prag, organizační složka',
        '7950' => 'Raiffeisen stavební spořitelna a.s.',
        '7960' => 'ČSOB Stavební spořitelna, a.s.',
        '7970' => 'MONETA Stavební Spořitelna, a.s.',
        '7990' => 'Modrá pyramida stavební spořitelna, a.s.',
        '8030' => 'Volksbank Raiffeisenbank Nordoberpfalz eG pobočka Cheb',
        '8040' => 'Oberbank AG pobočka Česká republika',
        '8060' => 'Stavební spořitelna České spořitelny, a.s.',
        '8090' => 'Česká exportní banka, a.s.',
        '8150' => 'HSBC Continental Europe, Czech Republic',
        '8190' => 'Sparkasse Oberlausitz-Niederschlesien',
        '8198' => 'FAS finance company s.r.o.',
        '8199' => 'MoneyPolo Europe s.r.o.',
        '8200' => 'PRIVAT BANK der Raiffeisenlandesbank Oberösterreich Aktiengesellschaft, pobočka Česká republika',
        '8220' => 'Payment execution s.r.o.',
        '8230' => 'ABAPAY s.r.o.',
        '8240' => 'Družstevní záložna Kredit, v likvidaci',
        '8250' => 'Bank of China (CEE) Ltd. Prague Branch',
        '8255' => 'Bank of Communications Co., Ltd., Prague Branch odštěpný závod',
        '8265' => 'Industrial and Commercial Bank of China Limited, Prague Branch, odštěpný závod',
        '8270' => 'Fairplay Pay s.r.o.',
        '8280' => 'B-Efekt a.s.',
        '8293' => 'Mercurius partners s.r.o.',
        '8299' => 'BESTPAY s.r.o.',
        '8500' => 'Multitude Bank p.l.c.',
    ];

    private function __construct(
        private string $value,
    ) {
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public static function of(string $value): self
    {
        [$prefix, $account, $bank] = self::parse($value);

        // Kontrola prefixu
        if ($prefix !== null) {
            // Doplnění na 6 číslic nulami zleva
            $prefixFull = str_pad($prefix, 6, '0', STR_PAD_LEFT);

            // Suma všech čísel pronásobených jejich váhami
            $sum = 0;
            foreach (self::PrefixWeights as $i => $v) {
                $sum += (int) $prefixFull[$i] * $v;
            }

            // Kontrola na dělitelnost 11
            if ($sum % 11 !== 0) {
                throw new Exception('Neplatné předčíslí');
            }
        }

        // Doplnění na 10 číslic nulami zleva
        $base = str_pad($account, 10, '0', STR_PAD_LEFT);

        // Suma všech číslic pronásobených jejich vahami
        $sum = 0;
        foreach (self::AccountWeights as $i => $v) {
            $sum += (int) $base[$i] * $v;
        }

        // Kontrola na dělitelnost 11
        if ($sum % 11 !== 0) {
            throw new Exception('Neplatné číslo účtu');
        }

        // Kontrola bankovního čísla
        if (!isset(self::Banks[$bank])) {
            throw new Exception('Neplatný kód banky');
        }

        return new self(($prefix > 0 ? "{$prefix}-" : '') . "{$account}/{$bank}");
    }

    public static function tryParse(string $value): ?self
    {
        try {
            return self::of($value);
        } catch (Exception) {
            return null;
        }
    }

    public static function parseNullable(?string $value): ?self
    {
        if ($value === null) {
            return null;
        }

        return self::of($value);
    }

    /**
     * @return array{string|null,string,string}
     */
    public function getParts(): array
    {
        return self::parse($this->value);
    }

    public function getPrefix(): ?string
    {
        return self::parse($this->value)[0];
    }

    public function getAccount(): string
    {
        return self::parse($this->value)[1];
    }

    public function getAccountWithPrefix(): string
    {
        $parts = self::parse($this->value);
        return trim("{$parts[0]}-{$parts[1]}", '-');
    }

    public function getBankCode(): string
    {
        return self::parse($this->value)[2];
    }

    public function getBankName(): string
    {
        return self::Banks[$this->getBankCode()];
    }

    public function getIBAN(): string
    {
        [$prefix, $account, $bank] = self::parse($this->value);

        $base = $bank .
            str_pad($prefix ?? '', 6, '0', STR_PAD_LEFT) .
            str_pad($account, 10, '0', STR_PAD_LEFT) .
            '123500';

        $xx = 98 - (int) Calculator::get()->divR(ltrim($base, '0'), '97');

        return sprintf('CZ%02d%04d%06d%010d', $xx, $bank, $prefix, $account);
    }

    public function toString(): string
    {
        return $this->value;
    }

    /**
     * pouziti pro doctrine
     * @internal
     */
    public static function createFromTrustedSource(string $value): self
    {
        return new self($value);
    }

    public function equals(?self $other): bool
    {
        return $this->value === $other?->value;
    }

    public function any(self ...$others): bool
    {
        foreach ($others as $other) {
            if ($other->equals($this)) {
                return true;
            }
        }

        return false;
    }

    public function jsonSerialize(): string
    {
        return $this->value;
    }

    /**
     * @return array{string|null,string,string}
     */
    private static function parse(string $value): array
    {
        if (preg_match('#^\s*((\d{0,6})-)?(\d{2,10})\/(\d{4})\s*$#', $value, $matches) !== 1) {
            throw new Exception('Neplatné číslo účtu');
        }

        [, , $prefix, $account, $bank] = $matches;
        return [$prefix !== '' ? ltrim($prefix, '0') : null, ltrim($account, '0'), $bank];
    }
}
