<?php

declare(strict_types=1);

namespace UXF\Core\Type;

use Exception;
use JsonSerializable;
use Nette\Utils\Strings;
use Stringable;

final readonly class Time implements JsonSerializable, Stringable, Equals
{
    private string $time;
    public int $hours;
    public int $minutes;
    public int $seconds;

    public function __construct(string $time)
    {
        $m = Strings::match($time, '/^([01]?[0-9]|2[0-3]):([0-5][0-9]):?([0-5][0-9])?$/') ?? throw new Exception("Invalid time format: '$time'");

        $this->hours = (int) $m[1];
        $this->minutes = (int) $m[2];
        $this->seconds = (int) ($m[3] ?? 0);
        $this->time = sprintf('%02d:%02d:%02d', $this->hours, $this->minutes, $this->seconds);
    }

    /**
     * @phpstan-param int<0, 86399> $seconds
     */
    public static function fromSeconds(int $seconds): self
    {
        return new self(gmdate('H:i:s', $seconds));
    }

    public function __toString(): string
    {
        return $this->time;
    }

    public function jsonSerialize(): string
    {
        return $this->time;
    }

    public function equals(?self $other): bool
    {
        return $this->time === $other?->time;
    }

    public function any(self ...$others): bool
    {
        foreach ($others as $other) {
            if ($other->equals($this)) {
                return true;
            }
        }

        return false;
    }

    public function getSeconds(): int
    {
        return $this->hours * 3600 + $this->minutes * 60 + $this->seconds;
    }

    /**
     * @phpstan-pure
     * @phpstan-param int<1, 86399> $seconds
     */
    public function addSeconds(int $seconds): self
    {
        $seconds += $this->getSeconds();
        if ($seconds < 0 || $seconds >= 86400) {
            throw new Exception("Seconds out of allowed range (0-86399) - $seconds given");
        }

        return self::fromSeconds($seconds);
    }

    /**
     * @phpstan-pure
     * @phpstan-param int<1, 86399> $seconds
     */
    public function subSeconds(int $seconds): self
    {
        $seconds = $this->getSeconds() - $seconds;
        if ($seconds < 0 || $seconds >= 86400) {
            throw new Exception("Seconds out of allowed range (0-86399) - $seconds given");
        }

        return self::fromSeconds($seconds);
    }
}
