<?php

declare(strict_types=1);

namespace UXF\Core\Type;

use Brick\Math\Exception\MathException;
use Brick\Money\Exception\MoneyMismatchException;
use Brick\Money\Money as BrickMoney;
use Stringable;
use UXF\Core\Exception\MoneyException;

final readonly class Money implements Stringable, Equals
{
    private function __construct(
        public string $amount,
        public Currency $currency,
        private BrickMoney $brick,
    ) {
    }

    public static function of(string|int|float|Decimal $amount, Currency $currency, RoundingMode $roundingMode = RoundingMode::HALF_UP): self
    {
        if ($amount instanceof Decimal) {
            $amount = $amount->toFloat();
        }

        if (is_string($amount)) {
            $amount = self::formatAmount($amount);
        }

        return self::fromBrick(BrickMoney::of($amount, $currency->value, roundingMode: $roundingMode->toBrick9()));
    }

    public static function zero(Currency $currency): self
    {
        return self::fromBrick(BrickMoney::of(0, $currency->value));
    }

    /**
     * @phpstan-pure
     */
    public function plus(self $other, RoundingMode $roundingMode = RoundingMode::HALF_UP): self
    {
        try {
            return self::fromBrick($this->brick->plus($other->brick, $roundingMode->toBrick9()));
        } catch (MoneyMismatchException $e) {
            throw new MoneyException('Currency mismatch', $e);
        }
    }

    /**
     * @phpstan-pure
     */
    public function minus(self $other, RoundingMode $roundingMode = RoundingMode::HALF_UP): self
    {
        try {
            return self::fromBrick($this->brick->minus($other->brick, $roundingMode->toBrick9()));
        } catch (MoneyMismatchException $e) {
            throw new MoneyException('Currency mismatch', $e);
        }
    }

    /**
     * @phpstan-pure
     */
    public function multipliedBy(string|int|float $amount, RoundingMode $roundingMode = RoundingMode::HALF_UP): self
    {
        try {
            return self::fromBrick($this->brick->multipliedBy($amount, $roundingMode->toBrick9()));
        } catch (MathException $e) {
            throw new MoneyException($e->getMessage(), $e);
        }
    }

    /**
     * @phpstan-pure
     */
    public function dividedBy(string|int|float $amount, RoundingMode $roundingMode = RoundingMode::HALF_UP): self
    {
        try {
            return self::fromBrick($this->brick->dividedBy($amount, $roundingMode->toBrick9()));
        } catch (MathException $e) {
            throw new MoneyException($e->getMessage(), $e);
        }
    }

    public function amountInt(RoundingMode $roundingMode = RoundingMode::HALF_UP): int
    {
        return $this->brick->getAmount()->toScale(0, $roundingMode->toBrick9())->toInt();
    }

    public function amountFloat(): float
    {
        return $this->brick->getAmount()->toFloat();
    }

    public function amountDecimal(): Decimal
    {
        return Decimal::of($this->amount);
    }

    public function isZero(): bool
    {
        return $this->brick->isZero();
    }

    public function isPositive(): bool
    {
        return $this->brick->isPositive();
    }

    public function isPositiveOrZero(): bool
    {
        return $this->brick->isPositiveOrZero();
    }

    public function isNegative(): bool
    {
        return $this->brick->isNegative();
    }

    public function isNegativeOrZero(): bool
    {
        return $this->brick->isNegativeOrZero();
    }

    public function equals(?self $other): bool
    {
        return $other !== null && $this->currency === $other->currency && $this->brick->isEqualTo($other->brick);
    }

    public function any(self ...$others): bool
    {
        foreach ($others as $other) {
            if ($other->equals($this)) {
                return true;
            }
        }

        return false;
    }

    public function __toString(): string
    {
        return "{$this->amount} {$this->currency->name}";
    }

    private static function fromBrick(BrickMoney $brick): self
    {
        return new self(
            amount: self::formatAmount($brick->getAmount()->__toString()),
            currency: Currency::from($brick->getCurrency()->getCurrencyCode()),
            brick: $brick,
        );
    }

    private static function formatAmount(string $amount): string
    {
        if (preg_match('/^(?<sign>\-)?(?<a>\d+)(\.(?<b>\d+))?$/', $amount, $m) !== 1) {
            throw new MoneyException('Invalid number format');
        }

        $b = rtrim($m['b'] ?? '', '0');
        return $m['sign'] . $m['a'] . ($b !== '' ? ".$b" : '');
    }
}
