<?php

declare(strict_types=1);

namespace UXF\Core\Type;

use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;
use Exception;
use JsonSerializable;
use Nette\Utils\Strings;
use Stringable;

final readonly class Email implements JsonSerializable, Stringable, Equals
{
    private function __construct(
        private string $value,
    ) {
    }

    public static function of(string $value): self
    {
        $value = Strings::lower(Strings::trim($value));

        // validate ĚŠČŘŽÝÁÍÉ, spaces etc.
        if (filter_var($value, FILTER_VALIDATE_EMAIL) === false) {
            throw new Exception("Invalid email '{$value}'");
        }

        // maybe we can use NoRFCWarningsValidation in future?
        $validator = new EmailValidator();
        if (!$validator->isValid($value, new RFCValidation())) {
            throw new Exception("Invalid email '{$value}' ({$validator->getError()?->description()})");
        }
        return new self($value);
    }

    public static function parseNullable(?string $value): ?self
    {
        return $value === null ? null : self::of($value);
    }

    public static function tryParse(?string $value): ?self
    {
        if ($value === null) {
            return null;
        }

        try {
            return self::of($value);
        } catch (Exception) {
            return null;
        }
    }

    /**
     * pouziti pro doctrine
     * @internal
     */
    public static function createFromTrustedSource(string $value): self
    {
        return new self($value);
    }

    public function equals(?self $other): bool
    {
        return $this->value === $other?->value;
    }

    public function any(self ...$others): bool
    {
        foreach ($others as $other) {
            if ($other->equals($this)) {
                return true;
            }
        }

        return false;
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function getLocalPart(): string
    {
        return $this->getParts()[0];
    }

    public function getDomain(): string
    {
        return $this->getParts()[1];
    }

    public function jsonSerialize(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @return array<mixed>
     */
    public function __serialize(): array
    {
        return [
            'value' => $this->value,
        ];
    }

    /**
     * @param array<mixed> $data
     */
    public function __unserialize(array $data): void
    {
        $this->value = $data['value'];
    }

    /**
     * @return array{string, string}
     */
    private function getParts(): array
    {
        $parts = explode('@', $this->value);
        return [$parts[0], $parts[1] ?? ''];
    }
}
