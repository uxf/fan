<?php

declare(strict_types=1);

namespace UXF\Core\Type;

use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use JsonSerializable;
use Stringable;
use function Safe\json_encode;

final class DateTime extends DateTimeImmutable implements JsonSerializable, Stringable, Equals
{
    public function __construct(string $datetime = 'now', ?DateTimeZone $timezone = null)
    {
        parent::__construct($datetime, $timezone);
        $errors = self::getLastErrors();
        if ($errors !== false && ($errors['warning_count'] > 0 || $errors['error_count'] > 0)) {
            throw new Exception("Invalid date-time format: '$datetime'" . json_encode($errors));
        }
    }

    public static function createFromDateTime(DateTimeInterface $dateTime): self
    {
        return new self($dateTime->format(self::ATOM));
    }

    public static function createFromDateTimeNullable(?DateTimeInterface $dateTime): ?self
    {
        return $dateTime !== null ? self::createFromDateTime($dateTime) : null;
    }

    public function resetToStartOfDay(): self
    {
        return $this->setTime(0, 0);
    }

    public function resetToEndOfDay(): self
    {
        return $this->setTime(23, 59, 59);
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function jsonSerialize(): string
    {
        return $this->__toString();
    }

    public function toString(): string
    {
        return $this->format(self::ATOM);
    }

    public function equals(?self $other): bool
    {
        return $this->getTimestamp() === $other?->getTimestamp();
    }

    public function any(self ...$others): bool
    {
        foreach ($others as $other) {
            if ($other->equals($this)) {
                return true;
            }
        }

        return false;
    }

    public function normalizeTz(): self
    {
        $timeZone = new DateTimeZone(date_default_timezone_get());

        // convert to system TZ
        if ($timeZone->getName() !== $this->getTimezone()->getName()) {
            return $this->setTimezone($timeZone);
        }

        return $this;
    }
}
