<?php

declare(strict_types=1);

namespace UXF\Core\Type;

use DateInterval;
use DateTimeInterface;
use Exception;
use JsonSerializable;
use Stringable;
use function Safe\json_encode;

final class Date implements JsonSerializable, Stringable, Equals
{
    private const string FORMAT = 'Y-m-d';

    public function __construct(private readonly string $date)
    {
        $dateTime = DateTime::createFromFormat(self::FORMAT, $date);
        if ($dateTime === false || $dateTime->format(self::FORMAT) !== $date) {
            throw new Exception("Invalid date format: '$date'");
        }
    }

    public static function createFromFormat(string $format, string $date): self
    {
        $dateTime = DateTime::createFromFormat($format, $date);
        $errors = DateTime::getLastErrors();
        if ($errors !== false && ($errors['warning_count'] > 0 || $errors['error_count'] > 0)) {
            throw new Exception("Invalid date format: '$date' (required: $format)" . json_encode($errors));
        }

        assert($dateTime instanceof DateTime);
        return self::createFromDateTime($dateTime);
    }

    public static function createFromDateTime(DateTimeInterface $date): self
    {
        return new self($date->format(self::FORMAT));
    }

    public function toDateTime(): DateTime
    {
        $dateTime = DateTime::createFromFormat('!' . self::FORMAT, $this->date);
        assert($dateTime instanceof DateTime);
        return $dateTime;
    }

    public function __toString(): string
    {
        return $this->date;
    }

    public function jsonSerialize(): string
    {
        return $this->date;
    }

    /**
     * @return array<mixed>
     */
    public function __serialize(): array
    {
        return [
            'date' => $this->date,
        ];
    }

    /**
     * @param array<mixed> $data
     */
    public function __unserialize(array $data): void
    {
        $this->date = $data['date'];
    }

    public function equals(?self $other): bool
    {
        return $this->date === $other?->date;
    }

    public function any(self ...$others): bool
    {
        foreach ($others as $other) {
            if ($other->equals($this)) {
                return true;
            }
        }

        return false;
    }

    public function format(string $format): string
    {
        return $this->toDateTime()->format($format);
    }

    public function diff(self $other): DateInterval
    {
        return $this->toDateTime()->diff($other->toDateTime());
    }

    // other is in future: return positive
    // other is in past:   return negative
    public function diffDays(self $other): int
    {
        return (int) $this->diff($other)->format('%r%a');
    }

    /**
     * @phpstan-pure
     */
    public function add(DateInterval $interval): self
    {
        return new self($this->toDateTime()->add($interval)->format(self::FORMAT));
    }

    /**
     * @phpstan-pure
     */
    public function sub(DateInterval $interval): self
    {
        return new self($this->toDateTime()->sub($interval)->format(self::FORMAT));
    }

    /**
     * @phpstan-pure
     * @phpstan-param int<1, max> $days
     */
    public function addDays(int $days): self
    {
        return new self($this->toDateTime()->add(new DateInterval("P{$days}D"))->format(self::FORMAT));
    }

    /**
     * @phpstan-pure
     * @phpstan-param int<1, max> $days
     */
    public function subDays(int $days): self
    {
        return new self($this->toDateTime()->sub(new DateInterval("P{$days}D"))->format(self::FORMAT));
    }
}
