<?php

declare(strict_types=1);

namespace UXF\Core\Type;

use Brick\Math\BigDecimal;
use Brick\Math\Exception\MathException;
use JsonSerializable;
use Stringable;
use UXF\Core\Exception\DecimalException;

final readonly class Decimal implements JsonSerializable, Stringable, Equals
{
    private function __construct(
        private string $value,
        private BigDecimal $brick,
    ) {
    }

    public static function of(string|int|float $value): self
    {
        if (is_string($value)) {
            $value = self::formatAmount($value);
        }

        return self::fromBrick(BigDecimal::of($value));
    }

    public static function zero(): self
    {
        return self::fromBrick(BigDecimal::zero());
    }

    /**
     * @phpstan-pure
     */
    public function plus(self $other): self
    {
        return self::fromBrick($this->brick->plus($other->brick));
    }

    /**
     * @phpstan-pure
     */
    public function minus(self $other): self
    {
        return self::fromBrick($this->brick->minus($other->brick));
    }

    /**
     * @phpstan-pure
     */
    public function multipliedBy(string|int|float $amount): self
    {
        try {
            return self::fromBrick($this->brick->multipliedBy($amount));
        } catch (MathException $e) {
            throw new DecimalException($e->getMessage(), $e);
        }
    }

    /**
     * @phpstan-pure
     */
    public function dividedBy(string|int|float $amount, RoundingMode $roundingMode = RoundingMode::HALF_UP): self
    {
        try {
            return self::fromBrick($this->brick->dividedBy($amount, null, $roundingMode->toBrick9()));
        } catch (MathException $e) {
            throw new DecimalException($e->getMessage(), $e);
        }
    }

    public function toScale(int $scale, RoundingMode $roundingMode = RoundingMode::HALF_UP): self
    {
        return self::fromBrick($this->brick->toScale($scale, $roundingMode->toBrick9()));
    }

    public function toInt(RoundingMode $roundingMode = RoundingMode::HALF_UP): int
    {
        return $this->brick->toScale(0, $roundingMode->toBrick9())->toInt();
    }

    public function toFloat(): float
    {
        return $this->brick->toFloat();
    }

    public function isZero(): bool
    {
        return $this->brick->isZero();
    }

    public function isPositive(): bool
    {
        return $this->brick->isPositive();
    }

    public function isPositiveOrZero(): bool
    {
        return $this->brick->isPositiveOrZero();
    }

    public function isNegative(): bool
    {
        return $this->brick->isNegative();
    }

    public function isNegativeOrZero(): bool
    {
        return $this->brick->isNegativeOrZero();
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function equals(?self $other): bool
    {
        return $other !== null && $this->brick->isEqualTo($other->brick);
    }

    public function any(self ...$others): bool
    {
        foreach ($others as $other) {
            if ($other->equals($this)) {
                return true;
            }
        }

        return false;
    }

    public function jsonSerialize(): string
    {
        return $this->toString();
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    private static function fromBrick(BigDecimal $brick): self
    {
        return new self(
            value: self::formatAmount($brick->__toString()),
            brick: $brick,
        );
    }

    private static function formatAmount(string $amount): string
    {
        if (preg_match('/^(?<sign>\-)?(?<a>\d+)(\.(?<b>\d+))?$/', $amount, $m) !== 1) {
            throw new DecimalException('Invalid format');
        }

        $b = rtrim($m['b'] ?? '', '0');
        return $m['sign'] . $m['a'] . ($b !== '' ? ".$b" : '');
    }
}
