<?php

declare(strict_types=1);

namespace UXF\Gen\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use UXF\Gen\Config\OpenApiConfig;
use UXF\Gen\Generator\OpenApi\OpenApiGenerator;

final readonly class OpenApiController
{
    /** @var array<string, OpenApiConfig> */
    private array $areas;

    /**
     * @param array<string, mixed> $areas
     */
    public function __construct(
        array $areas,
        private UrlGeneratorInterface $urlGenerator,
        private OpenApiGenerator $openApiGenerator,
    ) {
        array_walk($areas, static fn (&$config, $name) => $config = OpenApiConfig::create($config, $name));
        $this->areas = $areas;
    }

    public function ui(?string $area = null): Response
    {
        if (!isset($this->areas[$area])) {
            return new Response('Invalid area', 404);
        }

        $jsonUrl = $this->urlGenerator->generate('uxf_gen_json', [
            'area' => $area,
        ]);
        $baseUrl = 'https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/5.9.0/';
        return new Response('<html>
            <head>
            <meta charset="UTF-8">
            <link href="' . $baseUrl . 'swagger-ui.css" rel="stylesheet">
            </head>
            <body>
            <div id="swagger-ui"></div>
            <script src="' . $baseUrl . 'swagger-ui-bundle.js"></script>
            <script src="' . $baseUrl . 'swagger-ui-standalone-preset.js"></script>
            <script>
                window.onload = function() {
                  window.ui = SwaggerUIBundle({
                    dom_id: "#swagger-ui",
                    deepLinking: true,
                    presets: [
                      SwaggerUIBundle.presets.apis,
                      SwaggerUIStandalonePreset
                    ],
                    layout: "StandaloneLayout",
                    url: "' . $jsonUrl . '",
                  });
                }
            </script>
            </body>
            </html>
        ');
    }

    /**
     * @return mixed[]
     */
    public function json(string $area): array
    {
        if (!isset($this->areas[$area])) {
            return [];
        }

        return $this->openApiGenerator->generate($this->areas[$area]);
    }
}
