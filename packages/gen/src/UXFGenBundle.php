<?php

declare(strict_types=1);

namespace UXF\Gen;

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use UXF\Core\Attribute\Entity;
use UXF\Core\Attribute\FromBody;
use UXF\Core\Attribute\FromHeader;
use UXF\Core\Attribute\FromQuery;
use UXF\Core\Http\Request\NotSet;
use UXF\Gen\Bridge\UxfCoreInspectorPlugin;
use UXF\Gen\Bridge\UxfCoreTypeConverterPlugin;
use UXF\Gen\Generator\Http\EnumAsTsTypeGenerator;
use UXF\Gen\Generator\Http\EnumAsUnionGenerator;
use UXF\Gen\Generator\Http\EnumGenerator;
use UXF\Gen\Plugin\InspectorPlugin;
use UXF\Gen\Plugin\TypeConverterPlugin;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;

final class UXFGenBundle extends AbstractBundle
{
    protected string $extensionAlias = 'uxf_gen';

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
                ->arrayNode('config')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('prefix')->defaultValue('/api')->end()
                        ->scalarNode('disable_uxf_bridge')->defaultFalse()->end()
                        ->scalarNode('route_body_attribute')->defaultValue(FromBody::class)->end()
                        ->scalarNode('route_query_attribute')->defaultValue(FromQuery::class)->end()
                        ->scalarNode('route_header_attribute')->defaultValue(FromHeader::class)->end()
                        ->scalarNode('route_entity_attribute')->defaultValue(Entity::class)->end()
                        ->arrayNode('ignored_types')->scalarPrototype()->end()->defaultValue([NotSet::class])->end()
                        ->booleanNode('enum_as_union')->defaultFalse()->end()
                        ->arrayNode('typescript_types')
                            ->scalarPrototype()->defaultValue([])->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('open_api')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('authorization_header')->defaultValue('Authorization')->end()
                        ->arrayNode('areas')
                            ->useAttributeAsKey('name')
                            ->arrayPrototype()
                                ->children()
                                    ->scalarNode('path_pattern')->isRequired()->end()
                                    ->scalarNode('authorization_header')->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('hook')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('areas')
                            ->useAttributeAsKey('name')
                            ->arrayPrototype()
                                ->children()
                                    ->scalarNode('path_pattern')->isRequired()->end()
                                    ->booleanNode('with_enum_options')->defaultFalse()->end()
                                    ->enumNode('output')->values(['legacy', 'swr', 'empty'])->defaultValue('legacy')->end()
                                    ->arrayNode('destination')
                                        ->beforeNormalization()
                                            ->ifString()
                                            ->then(fn (string $v) => [$v])
                                        ->end()
                                        ->scalarPrototype()->isRequired()->end()
                                    ->end()
                                    ->arrayNode('prepends')
                                        ->scalarPrototype()->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('apollo')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('areas')
                            ->useAttributeAsKey('name')
                            ->arrayPrototype()
                                ->children()
                                    ->scalarNode('path_pattern')->isRequired()->end()
                                    ->arrayNode('destination')
                                        ->beforeNormalization()
                                            ->ifString()
                                            ->then(fn (string $v) => [$v])
                                        ->end()
                                        ->scalarPrototype()->isRequired()->end()
                                    ->end()
                                    ->arrayNode('prepends')
                                        ->scalarPrototype()->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import(__DIR__ . '/../config/services.php');

        $parameters = $container->parameters();
        $parameters->set('uxf_gen.config.global_prefix', $config['config']['prefix']);
        $parameters->set('uxf_gen.config.route_body_attribute', $config['config']['route_body_attribute']);
        $parameters->set('uxf_gen.config.route_query_attribute', $config['config']['route_query_attribute']);
        $parameters->set('uxf_gen.config.route_header_attribute', $config['config']['route_header_attribute']);
        $parameters->set('uxf_gen.config.route_entity_attribute', $config['config']['route_entity_attribute']);
        $parameters->set('uxf_gen.config.ignored_types', $config['config']['ignored_types']);
        $parameters->set('uxf_gen.config.typescript_types', $config['config']['typescript_types']);
        $parameters->set('uxf_gen.open_api.authorization_header', $config['open_api']['authorization_header']);
        $parameters->set('uxf_gen.open_api.areas', $config['open_api']['areas']);
        $parameters->set('uxf_gen.hook.areas', $config['hook']['areas']);
        $parameters->set('uxf_gen.apollo.areas', $config['apollo']['areas']);

        $enumGenerator = ($config['config']['enum_as_union'] ?? false) === true
            ? EnumAsUnionGenerator::class
            : EnumAsTsTypeGenerator::class;

        $services = $container->services();
        $services->set(EnumGenerator::class, $enumGenerator);

        $builder->registerForAutoconfiguration(TypeConverterPlugin::class)
            ->addTag('uxf_gen.type_converter_plugin');

        $builder->registerForAutoconfiguration(InspectorPlugin::class)
            ->addTag('uxf_gen.inspector_plugin');

        if ($config['config']['disable_uxf_bridge'] === false) {
            $services->set(UxfCoreInspectorPlugin::class)->autoconfigure();
            $services->set(UxfCoreTypeConverterPlugin::class)
                ->arg('$types', param('uxf_gen.config.typescript_types'))
                ->autoconfigure();
        }
    }
}
