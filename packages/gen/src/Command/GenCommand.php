<?php

declare(strict_types=1);

namespace UXF\Gen\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use UXF\Gen\Config\ApolloConfig;
use UXF\Gen\Config\HookConfig;
use UXF\Gen\Generator\Apollo\ApolloGenerator;
use UXF\Gen\Generator\Hook\HookApiGenerator;
use function Safe\file_put_contents;

#[AsCommand(name: 'uxf:gen')]
class GenCommand extends Command
{
    /**
     * @param HookConfig[] $hookAreas
     * @param ApolloConfig[] $apolloAreas
     */
    public function __construct(
        private readonly array $hookAreas,
        private readonly array $apolloAreas,
        private readonly HookApiGenerator $hookApiGenerator,
        private readonly ApolloGenerator $apolloGenerator,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->hookAreas as $config) {
            $out = $this->hookApiGenerator->generate($config);
            self::write($out, $config);
        }

        foreach ($this->apolloAreas as $config) {
            $out = $this->apolloGenerator->generate($config);
            self::write($out, $config);
        }

        return 0;
    }

    private static function write(string $out, HookConfig|ApolloConfig $config): void
    {
        foreach ($config->destinations as $destination) {
            (new Filesystem())->mkdir(pathinfo($destination, PATHINFO_DIRNAME));
            file_put_contents($destination, implode("\n", $config->prepends) . "\n\n$out");
        }
    }

    /**
     * @param array<string, mixed[]> $hookAreas
     * @param array<string, mixed[]> $apolloAreas
     */
    public static function create(
        array $hookAreas,
        array $apolloAreas,
        HookApiGenerator $hookApiGenerator,
        ApolloGenerator $apolloGenerator,
    ): self {
        $hookConfigs = [];
        foreach ($hookAreas as $name => $config) {
            $hookConfigs[] = new HookConfig(
                name: $name,
                destinations: $config['destination'],
                pathPattern: $config['path_pattern'],
                prepends: $config['prepends'] ?? [],
                withEnumLabels: $config['with_enum_options'] ?? false,
                output: $config['output'],
            );
        }

        $apolloConfigs = [];
        foreach ($apolloAreas as $name => $config) {
            $apolloConfigs[] = new ApolloConfig(
                $name,
                $config['destination'],
                $config['path_pattern'],
                $config['prepends'] ?? [],
            );
        }

        return new self(
            $hookConfigs,
            $apolloConfigs,
            $hookApiGenerator,
            $apolloGenerator,
        );
    }
}
