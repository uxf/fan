<?php

declare(strict_types=1);

namespace UXF\Gen\Plugin;

use UXF\Gen\Inspector\Schema\TypeSchema;

interface TypeConverterPlugin
{
    public function convertToTypescript(TypeSchema $typeSchema): ?TypescriptType;

    /**
     * @return array<string, mixed>
     */
    public function convertToOpenApi(TypeSchema $typeSchema): ?array;
}
