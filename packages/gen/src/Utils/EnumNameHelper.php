<?php

declare(strict_types=1);

namespace UXF\Gen\Utils;

final readonly class EnumNameHelper
{
    public static function getName(string $input): string
    {
        // ignore
        if (!str_contains($input, '_') && preg_match('/[a-z]+/', $input) === 1) {
            return $input;
        }

        return str_replace('_', '', ucwords(mb_strtolower($input), '_'));
    }
}
