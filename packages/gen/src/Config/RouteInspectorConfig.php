<?php

declare(strict_types=1);

namespace UXF\Gen\Config;

final readonly class RouteInspectorConfig
{
    /**
     * @param class-string $bodyAttribute
     * @param class-string $queryAttribute
     * @param class-string $headerAttribute
     * @param class-string $entityAttribute
     */
    public function __construct(
        public string $bodyAttribute,
        public string $queryAttribute,
        public string $headerAttribute,
        public string $entityAttribute,
    ) {
    }
}
