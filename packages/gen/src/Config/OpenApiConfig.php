<?php

declare(strict_types=1);

namespace UXF\Gen\Config;

final readonly class OpenApiConfig
{
    public function __construct(
        public string $name,
        public string $pathPattern,
        public ?string $authorizationHeader,
    ) {
    }

    /**
     * @param mixed[] $config
     */
    public static function create(array $config, string $name): self
    {
        return new self(
            $name,
            $config['path_pattern'],
            $config['authorization_header'] ?? null,
        );
    }
}
