<?php

declare(strict_types=1);

namespace UXF\Gen\Config;

final readonly class HookConfig
{
    /**
     * @param string[] $destinations
     * @param string[] $prepends
     */
    public function __construct(
        public string $name,
        public array $destinations,
        public string $pathPattern,
        public array $prepends,
        public bool $withEnumLabels,
        public string $output,
    ) {
    }
}
