<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector;

use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\Mapping\MappingException;
use Doctrine\Persistence\ObjectManager;
use Exception;
use ReflectionNamedType;

final readonly class DoctrineObjectInspector
{
    /**
     * @param ObjectManager[] $objectManagers
     */
    public function __construct(private array $objectManagers)
    {
    }

    public function inspectPropertyType(string $className, ?string $doctrineProperty): ?string
    {
        if (!class_exists($className)) {
            return null;
        }

        foreach ($this->objectManagers as $objectManager) {
            try {
                $metadata = $objectManager->getClassMetadata($className);
                assert($metadata instanceof ClassMetadata);

                if ($doctrineProperty !== null) {
                    $propertyName = $doctrineProperty;
                } else {
                    $identifiers = $metadata->getIdentifier();
                    if ($identifiers === []) {
                        continue;
                    }

                    if (count($identifiers) !== 1) {
                        throw new Exception("Object with multiple identifiers '$className'");
                    }

                    $propertyName = $identifiers[0];
                }

                $type = $metadata->reflFields[$propertyName]?->getType();
                assert($type instanceof ReflectionNamedType);

                // object as id
                if (isset($metadata->associationMappings[$propertyName])) {
                    return $this->inspectPropertyType($type->getName(), null);
                }

                return $type->getName();
            } catch (MappingException|ORMException) {
                // ignore problems
            }
        }

        return null;
    }
}
