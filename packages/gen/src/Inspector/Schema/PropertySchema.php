<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector\Schema;

final readonly class PropertySchema
{
    /**
     * @param array<string, TypeSchema> $types
     */
    public function __construct(
        public string $name,
        public array $types,
        public ArrayVariant $array,
        public bool $nullable,
        public bool $optional,
        public bool $deprecated = false,
        public mixed $defaultValue = null,
        public ?string $discriminatorValue = null,
        public bool $generic = false,
    ) {
    }
}
