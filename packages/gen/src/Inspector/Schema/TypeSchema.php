<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector\Schema;

final class TypeSchema
{
    /**
     * @param array<string, PropertySchema> $properties
     * @param array<string, TypeSchema> $children
     */
    public function __construct(
        public string $name,
        public TypeVariant $variant,
        public ?string $comment = null,
        public array $properties = [],
        public bool $deprecated = false,
        // union
        public ?string $discriminatorProperty = null,
        public array $children = [],
    ) {
    }
}
