<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector\Schema;

enum ArrayVariant
{
    case NONE;
    case LIST;
    case INDEXED;

    public static function from(bool $array, bool $indexed): self
    {
        if (!$array) {
            return self::NONE;
        }

        return $indexed ? self::INDEXED : self::LIST;
    }
}
