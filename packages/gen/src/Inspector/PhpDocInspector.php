<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector;

use Closure;
use Deprecated;
use JetBrains\PhpStorm\Deprecated as JetBrainsDeprecated;
use LogicException;
use phpDocumentor\Reflection\TypeResolver;
use phpDocumentor\Reflection\Types\ContextFactory;
use PHPStan\PhpDocParser\Ast\PhpDoc\ParamTagValueNode;
use PHPStan\PhpDocParser\Ast\PhpDoc\PhpDocTagNode;
use PHPStan\PhpDocParser\Ast\PhpDoc\ReturnTagValueNode;
use PHPStan\PhpDocParser\Ast\Type\ArrayTypeNode;
use PHPStan\PhpDocParser\Ast\Type\GenericTypeNode;
use PHPStan\PhpDocParser\Ast\Type\TypeNode;
use PHPStan\PhpDocParser\Ast\Type\UnionTypeNode;
use PHPStan\PhpDocParser\Lexer\Lexer;
use PHPStan\PhpDocParser\Parser\ConstExprParser;
use PHPStan\PhpDocParser\Parser\PhpDocParser;
use PHPStan\PhpDocParser\Parser\TokenIterator;
use PHPStan\PhpDocParser\Parser\TypeParser;
use PHPStan\PhpDocParser\ParserConfig;
use ReflectionAttribute;
use ReflectionException;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionUnionType;
use Reflector;
use UXF\Gen\Exception\GenException;
use UXF\Gen\Inspector\Schema\ArrayVariant;
use UXF\Gen\Inspector\Schema\PropertySchema;
use UXF\Gen\Inspector\Schema\TypeSchema;

final readonly class PhpDocInspector
{
    private PhpDocParser $phpDocParser;
    private Lexer $phpDocLexer;
    private TypeResolver $fqcnTypeResolver;
    private ContextFactory $contextFactory;

    /**
     * @param string[] $ignoredTypes
     */
    public function __construct(
        private string $entityAttribute,
        private array $ignoredTypes,
        private DoctrineObjectInspector $doctrineObjectInspector,
        private Closure $typeInspectorFn,
    ) {
        $config = new ParserConfig([]);
        $constExprParser = new ConstExprParser($config);
        $this->phpDocLexer = new Lexer($config);
        $this->phpDocParser = new PhpDocParser($config, new TypeParser($config, $constExprParser), $constExprParser);
        $this->fqcnTypeResolver = new TypeResolver();
        $this->contextFactory = new ContextFactory();
    }

    public function getPhpDocComment(?string $phpDoc, string $tag): ?string
    {
        if ($phpDoc === null) {
            return null;
        }

        $phpDocNode = $this->phpDocParser->parse(new TokenIterator($this->phpDocLexer->tokenize($phpDoc)));

        foreach ($phpDocNode->children as $tagNodeCandidate) {
            if ($tagNodeCandidate instanceof PhpDocTagNode && $tagNodeCandidate->name === $tag) {
                return (string) $tagNodeCandidate->value;
            }
        }

        return null;
    }

    public function inspect(
        TypeMap $typeMap,
        string $propertyName,
        ReflectionMethod|ReflectionParameter $reflection,
    ): PropertySchema {
        if ($reflection instanceof ReflectionParameter) {
            // parameter
            $reflectionParameter = $reflection;
            $reflectionType = $reflection->getType();
            $functionReflection = $reflection->getDeclaringFunction();
            $phpDoc = $functionReflection->getDocComment();
            $fileName = $functionReflection->getFileName();
            $optional = $reflection->isDefaultValueAvailable();
            $defaultValue = $optional ? $reflection->getDefaultValue() : null;
            $deprecated = $reflectionParameter->getAttributes(JetBrainsDeprecated::class) !== [] || $reflectionParameter->getAttributes(Deprecated::class) !== [];
            if (!$deprecated) {
                try {
                    $deprecated = str_contains((string) $reflectionParameter->getDeclaringClass()?->getProperty($propertyName)->getDocComment(), '@deprecated');
                } catch (ReflectionException) {
                }
            }
        } else {
            // method
            $reflectionParameter = null;
            $reflectionType = $reflection->getReturnType();
            $phpDoc = $reflection->getDocComment();
            $fileName = $reflection->getFileName();
            $optional = false;
            $defaultValue = null;
            $deprecated = false;
        }

        if (
            $reflectionType instanceof ReflectionUnionType &&
            !in_array('array', array_map(static fn ($t) => $t->getName(), $reflectionType->getTypes()), true)
        ) {
            $type = $reflectionType;
            $types = array_map(
                static fn (ReflectionNamedType $type) => $type->getName(),
                $type->getTypes(),
            ); // with prefix fix

            return new PropertySchema(
                name: $propertyName,
                types: $this->resolveTypeSchemas($typeMap, $types, $reflectionParameter),
                array: ArrayVariant::NONE,
                nullable: $type->allowsNull(),
                optional: $optional,
                deprecated: $deprecated,
            );
        }

        if ($phpDoc === false) {
            throw new GenException(
                "Missing phpDoc on property '$propertyName' in file '$fileName'",
            );
        }

        $reflectionClass = $reflection->getDeclaringClass();
        if (!$reflectionClass instanceof Reflector) {
            throw new LogicException();
        }

        $phpDocNode = $this->phpDocParser->parse(new TokenIterator($this->phpDocLexer->tokenize($phpDoc)));

        $primaryNode = null;
        if ($reflection instanceof ReflectionParameter) {
            // parameter
            foreach ($phpDocNode->children as $tagNodeCandidate) {
                if (
                    $tagNodeCandidate instanceof PhpDocTagNode &&
                    $tagNodeCandidate->value instanceof ParamTagValueNode &&
                    $tagNodeCandidate->value->parameterName === "\$$propertyName"
                ) {
                    $primaryNode = $tagNodeCandidate->value;
                    break;
                }
            }

            if ($primaryNode === null) {
                throw new GenException("Missing ParamTagValueNode '$propertyName'");
            }
        } else {
            // method
            foreach ($phpDocNode->children as $tagNodeCandidate) {
                if (
                    $tagNodeCandidate instanceof PhpDocTagNode &&
                    $tagNodeCandidate->value instanceof ReturnTagValueNode
                ) {
                    $primaryNode = $tagNodeCandidate->value;
                    break;
                }
            }

            if (!$primaryNode instanceof ReturnTagValueNode) {
                throw new GenException("Missing ReturnTagValueNode '$propertyName'");
            }
        }

        $array = false;
        $nullable = false;
        $indexed = false;
        $types = [];

        if ($primaryNode->type instanceof ArrayTypeNode) {
            $array = true;
            $arrayType = $primaryNode->type->type;
            if ($arrayType instanceof UnionTypeNode) {
                $types = array_map(static fn (TypeNode $node) => (string) $node, $arrayType->types);
            } else {
                $types[] = (string) $arrayType;
            }
        } elseif ($primaryNode->type instanceof UnionTypeNode) {
            foreach ($primaryNode->type->types as $unionType) {
                $type = (string) $unionType;
                if ($type === 'null') {
                    $nullable = true;
                } elseif ($unionType instanceof GenericTypeNode) {
                    $array = true;
                    $genericTypes = $unionType->genericTypes;
                    if (count($genericTypes) === 2) {
                        // array<string, Type>
                        $indexed = true;
                        $arrayType = $genericTypes[1];
                    } else {
                        // array<Type>
                        $arrayType = $genericTypes[0];
                    }
                    if ($arrayType instanceof UnionTypeNode) {
                        $types = array_map(static fn (TypeNode $node) => (string) $node, $arrayType->types);
                    } else {
                        $types[] = (string) $arrayType;
                    }
                } elseif ($unionType instanceof ArrayTypeNode) {
                    $array = true;
                    $arrayType = $unionType->type;
                    if ($arrayType instanceof UnionTypeNode) {
                        $types = array_map(static fn (TypeNode $node) => (string) $node, $arrayType->types);
                    } else {
                        $types[] = (string) $arrayType;
                    }
                } else {
                    $types[] = $type;
                }
            }
        } elseif ($primaryNode->type instanceof GenericTypeNode) {
            $array = true;
            $genericTypes = $primaryNode->type->genericTypes;
            if (count($genericTypes) === 2) {
                // array<string, Type>
                $indexed = true;
                $arrayType = $genericTypes[1];
            } else {
                // array<Type>
                $arrayType = $genericTypes[0];
            }
            if ($arrayType instanceof UnionTypeNode) {
                $types = array_map(static fn (TypeNode $node) => (string) $node, $arrayType->types);
            } else {
                $types[] = (string) $arrayType;
            }
        } else {
            $types[] = 'mixed';
        }

        $context = $this->contextFactory->createFromReflector($reflectionClass);

        $typeSchemas = $this->resolveTypeSchemas(
            $typeMap,
            array_map(fn (string $type) => ltrim((string) $this->fqcnTypeResolver->resolve($type, $context), '\\'), $types),
            $reflectionParameter,
        );

        return new PropertySchema(
            name: $propertyName,
            types: $typeSchemas,
            array: ArrayVariant::from($array, $indexed),
            nullable: $nullable,
            optional: $reflection instanceof ReflectionParameter ? $reflection->isDefaultValueAvailable() : false,
            deprecated: $deprecated,
            defaultValue: $defaultValue,
        );
    }

    /**
     * @param string[] $typeNames
     * @return array<string, TypeSchema>
     */
    public function resolveTypeSchemas(TypeMap $typeMap, array $typeNames, ?ReflectionParameter $reflectionParameter): array
    {
        $doctrineProperty = null;
        $entityAttribute = $reflectionParameter?->getAttributes($this->entityAttribute, ReflectionAttribute::IS_INSTANCEOF)[0] ?? null;
        if ($entityAttribute !== null && is_a($entityAttribute->getName(), $this->entityAttribute, true)) {
            $attr = $entityAttribute->newInstance();
            if (!property_exists($attr, 'property')) {
                throw new GenException("Missing {$this->entityAttribute}::\$property property");
            }
            $doctrineProperty = $attr->property;
        }

        $types = [];
        foreach ($typeNames as $typeName) {
            if ($typeName === 'null' || in_array($typeName, $this->ignoredTypes, true)) {
                continue;
            }

            if ($typeName === 'array') {
                throw new GenException("\$typeName === 'array'");
            }

            // register all!
            ($this->typeInspectorFn)()->inspect($typeMap, $typeName, $doctrineProperty);

            $propertyType = $this->doctrineObjectInspector->inspectPropertyType($typeName, $doctrineProperty);
            if ($propertyType !== null) {
                $newType = "$typeName#$propertyType";
                $types[$newType] = $typeMap[$newType] ?? throw new LogicException($newType);
            } else {
                $types[$typeName] = $typeMap[$typeName] ?? $typeMap['mixed'] ?? throw new LogicException();
            }
        }
        ksort($types);

        return $types;
    }

    public function inspectPropertyType(string $typeName, ?string $doctrineProperty = null): ?string
    {
        return $this->doctrineObjectInspector->inspectPropertyType($typeName, $doctrineProperty);
    }

    public function resolveFullClassName(ReflectionMethod $reflectionMethod, string $type): string
    {
        $context = $this->contextFactory->createFromReflector($reflectionMethod->getDeclaringClass());
        return ltrim((string) $this->fqcnTypeResolver->resolve($type, $context), '\\');
    }
}
