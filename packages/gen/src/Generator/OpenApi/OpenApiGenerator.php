<?php

declare(strict_types=1);

namespace UXF\Gen\Generator\OpenApi;

use LogicException;
use Nette\Utils\Strings;
use UXF\Gen\Config\OpenApiConfig;
use UXF\Gen\Generator\ClassNameConverter;
use UXF\Gen\Inspector\AppInspector;
use UXF\Gen\Inspector\Schema\PropertySchema;
use UXF\Gen\Inspector\Schema\RouteSchema;
use UXF\Gen\Inspector\Schema\TypeSchema;
use UXF\Gen\Inspector\Schema\TypeVariant;
use UXF\Gen\Utils\TypeConverter;

final readonly class OpenApiGenerator
{
    public function __construct(
        private string $authorizationHeader,
        private AppInspector $appInspector,
        private TypeConverter $typeConverter,
        private ClassNameConverter $classNameConverter,
    ) {
    }

    /**
     * @return mixed[]
     */
    public function generate(OpenApiConfig $config): array
    {
        $appSchema = $this->appInspector->inspect($config->name, $config->pathPattern);

        $schemas = [];
        foreach ($appSchema->types as $typeName => $typeSchema) {
            if ($typeSchema->variant === TypeVariant::DISCRIMINATOR_OBJECT) {
                $componentName = $this->classNameConverter->convert($typeName);
                $schemas[$componentName] = [
                    'oneOf' => array_values(array_map(fn (TypeSchema $ts) => [
                        '$ref' => '#/components/schemas/' . $this->classNameConverter->convert($ts->name),
                    ], $typeSchema->children)),
                    'discriminator' => [
                        'propertyName' => $typeSchema->discriminatorProperty,
                        'mapping' => array_map(
                            fn (TypeSchema $ts) => '#/components/schemas/' . $this->classNameConverter->convert($ts->name),
                            $typeSchema->children,
                        ),
                    ],
                ];

                continue;
            }

            if ($typeSchema->variant !== TypeVariant::OBJECT) {
                continue;
            }

            $properties = [];
            $required = [];
            foreach ($typeSchema->properties as $property) {
                $properties[$property->name] = $this->typeConverter->convertToOpenApi($property);
                if (!$property->optional) {
                    $required[] = $property->name;
                }
            }

            $componentName = $this->classNameConverter->convert($typeName);
            $schemas[$componentName] = [
                'properties' => $properties,
            ];

            if ($required !== []) {
                $schemas[$componentName]['required'] = $required;
            }
        }

        $paths = [];
        foreach ($appSchema->routes as $routeSchema) {
            // query
            $querySchemas = [];
            foreach ($routeSchema->requestQueries as $query) {
                $queryTypes = $query->types ?? [];
                if ($queryTypes !== []) {
                    $type = self::getFirstElement($queryTypes)->name;
                    if (isset($appSchema->types[$type])) {
                        $querySchemas += $appSchema->types[$type]->properties;
                    }
                }
            }

            // header
            $headerSchemas = [];
            foreach ($routeSchema->requestHeaders as $header) {
                $headerTypes = $header->types ?? [];
                if ($headerTypes !== []) {
                    $type = self::getFirstElement($headerTypes)->name;
                    if (isset($appSchema->types[$type])) {
                        $headerSchemas += $appSchema->types[$type]->properties;
                    }
                }
            }

            $pathInfo = array_filter([
                'operationId' => $routeSchema->name,
                'parameters' => $this->getParameters($routeSchema->pathParams, $querySchemas, $headerSchemas),
                'requestBody' => $this->getRequestBody($routeSchema->requestBody),
                'responses' => $this->getResponse($routeSchema->response),
                'tags' => $this->getTags($routeSchema),
                'description' => $routeSchema->description,
                'deprecated' => $routeSchema->deprecated,
            ], static fn (mixed $value) => $value !== null && $value !== [] && $value !== '' && $value !== false);

            foreach ($routeSchema->methods as $method => $secured) {
                if ($secured) {
                    $pathInfo['security'] = [[
                        'Auth' => [],
                    ]];
                }

                $paths[$routeSchema->path][$method] = $pathInfo;
            }
        }

        return [
            'openapi' => '3.0.0',
            'info' => [
                'title' => $config->name,
                'version' => '1',
            ],
            'paths' => $paths,
            'components' => [
                'schemas' => $schemas,
                'securitySchemes' => [
                    'Auth' => [
                        'type' => 'apiKey',
                        'in' => 'header',
                        'name' => $config->authorizationHeader ?? $this->authorizationHeader,
                    ],
                ],
            ],
        ];
    }

    /**
     * @param PropertySchema[] $pathSchemas
     * @param PropertySchema[] $querySchemas
     * @param PropertySchema[] $headerSchemas
     * @return mixed[]
     */
    private function getParameters(array $pathSchemas, array $querySchemas, array $headerSchemas): array
    {
        $parameters = [];

        foreach ($pathSchemas as $propertySchema) {
            $parameters[] = [
                'in' => 'path',
                'name' => $propertySchema->name,
                'schema' => $this->typeConverter->convertToOpenApi($propertySchema),
                'required' => true,
            ];
        }

        foreach ($querySchemas as $propertySchema) {
            $parameters[] = [
                'in' => 'query',
                'name' => $propertySchema->name,
                'schema' => $this->typeConverter->convertToOpenApi($propertySchema),
                'required' => !$propertySchema->optional,
            ];
        }

        foreach ($headerSchemas as $propertySchema) {
            $parameters[] = [
                'in' => 'header',
                'name' => self::camel2dashes($propertySchema->name),
                'schema' => $this->typeConverter->convertToOpenApi($propertySchema),
                'required' => !$propertySchema->optional,
            ];
        }

        return $parameters;
    }

    /**
     * @return mixed[]
     */
    private function getRequestBody(?PropertySchema $propertySchema): ?array
    {
        if ($propertySchema === null) {
            return null;
        }

        return [
            'content' => [
                'application/json' => [
                    'schema' => $this->typeConverter->convertToOpenApi($propertySchema),
                ],
            ],
        ];
    }

    /**
     * @return mixed[]
     */
    private function getResponse(?PropertySchema $propertySchema): array
    {
        if ($propertySchema === null) {
            return [
                '200' => [
                    'description' => '',
                ],
            ];
        }

        return [
            '200' => [
                'content' => [
                    'application/json' => [
                        'schema' => $this->typeConverter->convertToOpenApi($propertySchema),
                    ],
                ],
                'description' => '',
            ],
        ];
    }

    /**
     * @return string[]
     */
    private function getTags(RouteSchema $routeSchema): array
    {
        return [$this->classNameConverter->resolveTag($routeSchema->controller) ?? 'Default'];
    }

    /**
     * @param array<string, TypeSchema> $types
     */
    private static function getFirstElement(array $types): TypeSchema
    {
        $type = current($types);
        if ($type === false) {
            throw new LogicException('Empty array');
        }
        return $type;
    }

    private static function camel2dashes(string $propertyName): string
    {
        return ucwords(Strings::replace($propertyName, '/([A-Z])/', '-$1'));
    }
}
