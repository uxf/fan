<?php

declare(strict_types=1);

namespace UXF\Gen\Generator\Hook;

use BackedEnum;
use Nette\Utils\Json;
use ReflectionEnum;
use Traversable;
use UXF\CMS\Attribute\CmsLabel;
use UXF\Core\Attribute\Label;
use UXF\Core\Utils\ClassNameHelper;
use UXF\Gen\Config\HookConfig;
use UXF\Gen\Generator\Hook\Output\HookOutputGenerator;
use UXF\Gen\Generator\Http\HttpApiGenerator;
use UXF\Gen\Inspector\AppInspector;
use UXF\Gen\Inspector\Schema\TypeVariant;

final readonly class HookApiGenerator
{
    /**
     * @param iterable<string, HookOutputGenerator> $outputGenerators
     */
    public function __construct(
        private AppInspector $appInspector,
        private HttpApiGenerator $httpApiGenerator,
        private iterable $outputGenerators,
    ) {
    }

    public function generate(HookConfig $config): string
    {
        $appSchema = $this->appInspector->inspect($config->name, $config->pathPattern);

        $generators = $this->outputGenerators instanceof Traversable ? iterator_to_array($this->outputGenerators) : $this->outputGenerators;
        $hookOutputGenerator = $generators[$config->output];

        $output = $hookOutputGenerator->generateHeaderString();

        $output .= $this->httpApiGenerator->generateString($appSchema->types);

        if ($config->withEnumLabels) {
            // enums
            $output .= "const formOptions = <T extends Record<string, { label: string; }>>(o: T) =>
    Object.entries(o).map(([id, { label }]) => ({
        id: id as keyof T,
        label,
    }));\n\n";

            $output .= "const formIntOptions = <T extends Record<string, { label: string; }>>(o: T) =>
    Object.entries(o).map(([id, { label }]) => ({
        id: Number(id) as keyof T,
        label,
    }));\n\n";

            $used = [];
            foreach ($appSchema->types as $typeSchema) {
                if ($typeSchema->variant !== TypeVariant::ENUM || isset($used[$typeSchema->name])) {
                    continue;
                }
                $used[$typeSchema->name] = true;

                assert(is_a($typeSchema->name, BackedEnum::class, true));
                $refEnum = new ReflectionEnum($typeSchema->name);
                $fnName = $refEnum->getBackingType()?->getName() === 'int' ? 'formIntOptions' : 'formOptions';
                $options = [];
                foreach ($refEnum->getCases() as $caseRef) {
                    $labelAttr = $caseRef->getAttributes(Label::class)[0] ?? $caseRef->getAttributes(CmsLabel::class)[0] ?? null;
                    $options[$caseRef->getBackingValue()] = $labelAttr?->newInstance() ?? new Label($caseRef->getName());
                }

                $colorTypes = [];
                foreach ($options as $option) {
                    $colorTypes[] = $option->color === null ? 'null' : "\"{$option->color}\"";
                }
                $colorTypeString = implode(" | ", array_unique($colorTypes));
                $colorTypeString = $colorTypeString === '' ? 'any' : $colorTypeString;

                // export const enum_FinanceProductType_array: Array<{id: FinanceProductType, label: string, color: string | null}>  =  []
                // export const enum_FinanceProductType = (value: FinanceProductType) => FinanceProductTypeOptions[value];
                $shortName = ClassNameHelper::shortName($typeSchema->name);
                $output .= "// {$typeSchema->comment}\n";
                $output .= "export const {$shortName}Options: Record<{$shortName}, {label: string, color: $colorTypeString}> = " . Json::encode((object) $options, pretty: true) . ";\n";
                $output .= "export const enum_{$shortName}_array = $fnName({$shortName}Options);\n";
                $output .= "export const enum_{$shortName} = (value: {$shortName}) => {$shortName}Options[value];\n\n";
            }
        }

        foreach ($appSchema->routes as $rSchema) {
            $output .= $hookOutputGenerator->generateString($rSchema);
        }

        return $output;
    }
}
