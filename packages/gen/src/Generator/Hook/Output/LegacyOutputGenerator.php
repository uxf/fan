<?php

declare(strict_types=1);

namespace UXF\Gen\Generator\Hook\Output;

use Nette\Utils\Strings;
use UXF\Gen\Inspector\Schema\RouteSchema;
use UXF\Gen\Utils\TypeConverter;
use UXF\Gen\Utils\TypescriptHelper;

final readonly class LegacyOutputGenerator implements HookOutputGenerator
{
    public function __construct(
        private TypeConverter $typeConverter,
    ) {
    }

    public function generateHeaderString(): string
    {
        return '';
    }

    public function generateString(RouteSchema $rSchema): string
    {
        $output = '';

        foreach ($rSchema->methods as $method => $secured) {
            $routeName = $rSchema->name;
            $name = str_replace(' ', '', ucwords(Strings::replace($routeName, '/[^a-zA-Z0-9]/', ' ')));
            $name .= ucfirst($method);
            $urlHook = TypescriptHelper::generatePath($rSchema, 'config');

            $rb = $rSchema->requestBody !== null
                ? $this->typeConverter->convertToTypescript($rSchema->requestBody)
                : null;

            $rq = $rSchema->requestQueries !== []
                ? implode(' & ', array_map(fn ($query) => $this->typeConverter->convertToTypescript($query), $rSchema->requestQueries))
                : null;

            $rp = TypescriptHelper::resolvePathParams($this->typeConverter, $rSchema->pathParams);

            $r = $rSchema->response !== null
                ? $this->typeConverter->convertToTypescript($rSchema->response)
                : 'any';

            $rbt = $rb ?? 'null';
            $rqt = $rq ?? 'null';
            $rpt = $rp ?? 'null';
            $rt = $r;

            if ($rSchema->deprecated) {
                $output .= "/** @deprecated */\n";
            }

            $output .= "export const use$name = (config: RequestConfig<$rbt, $rqt, $rpt>) => " .
                "useAxiosRequest<$rt, $rbt, $rqt, $rpt>(`$urlHook`, '$method', config);\n\n";

            $params = ["ctx: any"];
            if ($rp !== null) {
                $params[] = "path: $rp";
            }
            if ($rq !== null) {
                $params[] = "query: $rq";
            }
            if ($rb !== null) {
                $params[] = "body: $rb";
            }

            if ($rSchema->deprecated) {
                $output .= "/** @deprecated */\n";
            }

            $urlCall = Strings::replace($rSchema->path, '/({)/', '\$$1path.');
            $output .= 'export const ' . lcfirst($name) . ' = (' . implode(', ', $params) . ') => ' .
                "axiosRequest<$r>(ctx, `$urlCall`, '$method', "
                . ($rb !== null ? 'body' : 'null') . ', '
                . ($rq !== null ? 'query' : 'null') . ");\n\n";
        }

        return $output;
    }

    public static function getDefaultTypeName(): string
    {
        return 'legacy';
    }
}
