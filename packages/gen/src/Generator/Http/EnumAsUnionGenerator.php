<?php

declare(strict_types=1);

namespace UXF\Gen\Generator\Http;

use UXF\Core\Utils\ClassNameHelper;
use UXF\Gen\Inspector\Schema\TypeSchema;
use UXF\Gen\Utils\EnumNameHelper;

final readonly class EnumAsUnionGenerator implements EnumGenerator
{
    public function generateString(string $typeName, TypeSchema $typeSchema): string
    {
        $shortName = ClassNameHelper::shortName($typeSchema->name);
        $output = "// {$typeSchema->comment}\n";
        $output .= "export const $shortName = {\n";

        $values = [];
        foreach ($typeName::cases() as $i => $item) {
            $value = $i;
            if (property_exists($item, 'value')) {
                $value = $item->value;
                if (is_string($value)) {
                    $value = "'" . addslashes($item->value) . "'";
                }
            }

            $values[] = $value;
            $name = EnumNameHelper::getName($item->name);
            $output .= "    $name: $value,\n";
        }
        $output .= "} as const;\n";

        $output .= "export type $shortName = " . implode(' | ', $values) . ";\n\n";

        return $output;
    }
}
