<?php

declare(strict_types=1);

namespace UXF\Gen\Generator\Apollo;

use Nette\Utils\Strings;
use UXF\Gen\Config\ApolloConfig;
use UXF\Gen\Generator\ClassNameConverter;
use UXF\Gen\Generator\Http\HttpApiGenerator;
use UXF\Gen\Inspector\AppInspector;
use UXF\Gen\Inspector\Schema\RouteSchema;
use UXF\Gen\Utils\TypeConverter;
use UXF\Gen\Utils\TypescriptHelper;

final readonly class ApolloGenerator
{
    public function __construct(
        private AppInspector $appInspector,
        private HttpApiGenerator $httpApiGenerator,
        private TypeConverter $typeConverter,
        private ClassNameConverter $tagResolver,
    ) {
    }

    public function generate(ApolloConfig $config): string
    {
        $appSchema = $this->appInspector->inspect($config->name, $config->pathPattern);

        $output = "import { BaseDataSource } from \"./BaseDataSource\";\n\n";
        $output .= $this->httpApiGenerator->generateString($appSchema->types);
        $output .= "\n\n";

        $grouped = [];
        foreach ($appSchema->routes as $routeSchema) {
            $tag = $this->tagResolver->resolveTag($routeSchema->controller) ?? 'Default';

            $grouped[$tag][] = $routeSchema;
        }

        foreach ($grouped as $zoneName => $routeSchemas) {
            $output .= $this->generateDataSource($zoneName, $routeSchemas);
        }

        return $output;
    }

    /**
     * @param RouteSchema[] $routeSchemas
     */
    public function generateDataSource(string $zoneName, array $routeSchemas): string
    {
        $output = "export class {$zoneName}DataSource extends BaseDataSource {\n";

        // camel case -> snake case
        $zoneName = mb_strtolower(Strings::replace($zoneName, '/([a-z])([A-Z])/', '$1_$2'));

        foreach ($routeSchemas as $rSchema) {
            foreach ($rSchema->methods as $method => $secured) {
                $routeName = $rSchema->name;

                // cleanup zone prefix
                $name = Strings::replace($routeName, "/^{$zoneName}_/");

                $name = str_replace(' ', '', ucwords(Strings::replace($name, '/[^a-zA-Z0-9]/', ' ')));
                $name = lcfirst($name);
                $urlHook = TypescriptHelper::generatePath($rSchema, 'input');

                $rb = $rSchema->requestBody !== null
                    ? $this->typeConverter->convertToTypescript($rSchema->requestBody)
                    : null;

                $rq = $rSchema->requestQueries !== []
                    ? implode(' & ', array_map(fn ($query) => $this->typeConverter->convertToTypescript($query), $rSchema->requestQueries))
                    : null;

                $rp = TypescriptHelper::resolvePathParams($this->typeConverter, $rSchema->pathParams);

                $r = $rSchema->response !== null
                    ? $this->typeConverter->convertToTypescript($rSchema->response)
                    : 'any';

                $input = [];
                $secondArg = $method !== 'delete' ? ', undefined' : '';

                if ($rp !== null) {
                    $input[] = "path: $rp";
                }
                if ($rq !== null) {
                    $input[] = "query: $rq";
                    $secondArg = ', input.query';
                }
                if ($rb !== null) {
                    $input[] = "body: $rb";
                    $secondArg = ', input.body';
                }

                $inputString = $input !== [] ? ('input: { ' . implode(', ', $input) . ' }') : '';

                $output .= "    public $name($inputString): Promise<$r> {\n";
                $output .= "        return this.$method(`$urlHook`$secondArg);\n";
                $output .= "    }\n\n";
            }
        }

        $output .= "}\n\n";

        return $output;
    }
}
