<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Gen\Command\GenCommand;
use UXF\Gen\Config\RouteInspectorConfig;
use UXF\Gen\Controller\OpenApiController;
use UXF\Gen\Generator\Apollo\ApolloGenerator;
use UXF\Gen\Generator\ClassNameConverter;
use UXF\Gen\Generator\Hook\HookApiGenerator;
use UXF\Gen\Generator\Hook\Output\EmptyOutputGenerator;
use UXF\Gen\Generator\Hook\Output\HookOutputGenerator;
use UXF\Gen\Generator\Hook\Output\LegacyOutputGenerator;
use UXF\Gen\Generator\Hook\Output\SWROutputGenerator;
use UXF\Gen\Generator\Http\HttpApiGenerator;
use UXF\Gen\Generator\OpenApi\OpenApiGenerator;
use UXF\Gen\Generator\SimpleClassNameConverter;
use UXF\Gen\Inspector\AppInspector;
use UXF\Gen\Inspector\DoctrineObjectInspector;
use UXF\Gen\Inspector\MethodInspector;
use UXF\Gen\Inspector\PhpDocInspector;
use UXF\Gen\Inspector\RouteInspector;
use UXF\Gen\Inspector\TypeInspector;
use UXF\Gen\Utils\TypeConverter;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service_closure;
use function Symfony\Component\DependencyInjection\Loader\Configurator\tagged_iterator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure()
        ->instanceof(HookOutputGenerator::class)->tag('uxf_gen.hook_output_generator');

    $services->set(OpenApiController::class)
        ->args(['%uxf_gen.open_api.areas%'])
        ->public();

    $services->set(GenCommand::class)
        ->factory([GenCommand::class, 'create'])
        ->args(['%uxf_gen.hook.areas%', '%uxf_gen.apollo.areas%']);

    $services->set(ApolloGenerator::class);
    $services->set(HttpApiGenerator::class);
    $services->set(HookApiGenerator::class)
        ->arg('$outputGenerators', tagged_iterator('uxf_gen.hook_output_generator', 'type'));
    $services->set(OpenApiGenerator::class)
        ->arg('$authorizationHeader', '%uxf_gen.open_api.authorization_header%');

    $services->set(LegacyOutputGenerator::class);
    $services->set(SWROutputGenerator::class);
    $services->set(EmptyOutputGenerator::class);

    $services->set(ClassNameConverter::class, SimpleClassNameConverter::class);

    $services->set(TypeInspector::class);
    $services->set(TypeConverter::class)
        ->arg('$types', param('uxf_gen.config.typescript_types'))
        ->arg('$plugins', tagged_iterator('uxf_gen.type_converter_plugin'));

    $services->set(AppInspector::class)
        ->arg('$plugins', tagged_iterator('uxf_gen.inspector_plugin'));

    $services->set(RouteInspectorConfig::class)
        ->arg('$bodyAttribute', param('uxf_gen.config.route_body_attribute'))
        ->arg('$queryAttribute', param('uxf_gen.config.route_query_attribute'))
        ->arg('$headerAttribute', param('uxf_gen.config.route_header_attribute'))
        ->arg('$entityAttribute', param('uxf_gen.config.route_entity_attribute'));

    $services->set(RouteInspector::class)
        ->arg('$globalPrefix', param('uxf_gen.config.global_prefix'))
        ->arg('$accessMap', service('security.access_map'));

    $services->set(MethodInspector::class);

    $services->set(DoctrineObjectInspector::class)
        ->arg('$objectManagers', [
            service('doctrine.orm.default_entity_manager')->ignoreOnInvalid(),
        ]);

    $services->set(PhpDocInspector::class)
        ->arg('$entityAttribute', param('uxf_gen.config.route_entity_attribute'))
        ->arg('$ignoredTypes', param('uxf_gen.config.ignored_types'))
        ->arg('$typeInspectorFn', service_closure(TypeInspector::class));
};
