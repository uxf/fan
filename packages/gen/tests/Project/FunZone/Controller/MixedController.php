<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Controller;

use UXF\Core\Attribute\FromBody;

class MixedController
{
    /**
     * @deprecated
     */
    public function __invoke(#[FromBody] mixed $body): mixed
    {
        return $body;
    }
}
