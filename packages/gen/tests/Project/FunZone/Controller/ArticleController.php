<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Controller;

use Nette\NotImplementedException;
use UXF\Core\Attribute\FromBody;
use UXF\Core\Attribute\FromHeader;
use UXF\Core\Attribute\FromQuery;
use UXF\GenTests\Project\FunZone\Entity\Article;
use UXF\GenTests\Project\FunZone\Http\Request\ArticleRequestBody;
use UXF\GenTests\Project\FunZone\Http\Request\ArticleRequestHeader;
use UXF\GenTests\Project\FunZone\Http\Request\ArticleRequestQuery;
use UXF\GenTests\Project\FunZone\Http\Request\JumpRequestQuery;
use UXF\GenTests\Project\FunZone\Http\Response\ArticleResponse;

class ArticleController
{
    /**
     * INTERNAL PRIVATE!!!
     * @description Super public description
     * multiline! wow!
     *
     * @return ArticleResponse[]
     */
    public function all(#[FromQuery] ArticleRequestQuery $query): array
    {
        throw new NotImplementedException();
    }

    public function doubleQuery(#[FromQuery] ArticleRequestQuery $query, #[FromQuery] JumpRequestQuery $query2): void
    {
        throw new NotImplementedException();
    }

    public function create(#[FromBody] ArticleRequestBody $body, #[FromHeader] ArticleRequestHeader $header): ArticleResponse
    {
        throw new NotImplementedException();
    }

    public function read(Article $article): ArticleResponse
    {
        throw new NotImplementedException();
    }

    public function update(Article $article, #[FromBody] ArticleRequestBody $body): ArticleResponse
    {
        throw new NotImplementedException();
    }

    public function delete(Article $article): void
    {
    }
}
