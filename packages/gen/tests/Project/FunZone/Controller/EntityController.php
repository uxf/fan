<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Controller;

use UXF\Core\Attribute\Entity;
use UXF\GenTests\Project\FunZone\Entity\File;

class EntityController
{
    public function __invoke(
        File $id,
        #[Entity('uuid')] File $uuid,
        #[Entity('string')] File $string,
        #[Entity('int')] File $int,
    ): void {
    }
}
