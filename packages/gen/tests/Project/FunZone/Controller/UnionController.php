<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Controller;

use UXF\Core\Attribute\FromBody;
use UXF\GenTests\Project\FunZone\Http\Request\Family\Activity;
use UXF\GenTests\Project\FunZone\Http\Request\Family\Orienteering;

class UnionController
{
    public function detail(#[FromBody] Activity $activity): Activity
    {
        return new Orienteering(0);
    }

    /**
     * @param Activity[] $activities
     * @return Activity[]
     */
    public function list(#[FromBody(Activity::class)] array $activities): array
    {
        return [];
    }
}
