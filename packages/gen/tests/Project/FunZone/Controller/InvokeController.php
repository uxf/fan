<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Controller;

use Symfony\Contracts\Cache\CacheInterface;
use UXF\GenTests\Project\FunZone\Entity\Type;
use UXF\GenTests\Project\FunZone\Http\Response\CategoryResponse;
use UXF\GenTests\Project\FunZone\Http\Response\TagResponse;

class InvokeController
{
    /**
     * @return CategoryResponse[]|TagResponse[]
     */
    public function __invoke(Type $type, CacheInterface $cache): array
    {
        return [];
    }
}
