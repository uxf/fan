<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Request;

use UXF\GenTests\Project\FunZone\Entity\Article;

final readonly class JumpRequestQuery
{
    public function __construct(
        public ?Article $article = null,
    ) {
    }
}
