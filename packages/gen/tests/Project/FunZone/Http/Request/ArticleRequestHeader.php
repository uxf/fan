<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Request;

final readonly class ArticleRequestHeader
{
    public function __construct(public string $acceptLanguage)
    {
    }
}
