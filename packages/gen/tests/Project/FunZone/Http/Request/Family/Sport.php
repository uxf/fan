<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Request\Family;

use UXF\Hydrator\Attribute\HydratorMap;

#[HydratorMap(property: 'type', matrix: [
    'o' => Orienteering::class,
    'p' => Paragliding::class,
])]
abstract class Sport implements Activity
{
}
