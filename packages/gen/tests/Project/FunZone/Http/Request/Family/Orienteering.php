<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Request\Family;

class Orienteering extends Sport
{
    public function __construct(
        public readonly int $card,
    ) {
    }
}
