<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Request;

use UXF\Core\Attribute\Entity;
use UXF\Core\Type\DateTime;
use UXF\GenTests\Project\FunZone\Entity\Category;
use UXF\GenTests\Project\FunZone\Entity\EntityWithEnumId;
use UXF\GenTests\Project\FunZone\Entity\EnumNumeric;
use UXF\GenTests\Project\FunZone\Entity\File;
use UXF\GenTests\Project\FunZone\Entity\Tag;
use UXF\GenTests\Project\FunZone\Entity\Type;

final readonly class ArticleRequestBody
{
    /**
     * @param Tag[] $tags
     * @param File[] $files
     */
    public function __construct(
        public string $title,
        public Type $type,
        public EnumNumeric $enumNumeric,
        public DateTime $publishedAt,
        public int $priority,
        public float $score,
        public bool $active,
        public Category $category,
        public EntityWithEnumId $enumEntity,
        public array $tags,
        #[Entity('uuid')] public File $file,
        #[Entity('uuid')] public array $files,
        public ?string $content = null,
    ) {
    }
}
