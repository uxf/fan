<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Request;

use JetBrains\PhpStorm\Deprecated;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\GenTests\Project\FunZone\Entity\Tag;

/**
 * @deprecated
 */
final readonly class PersonRequestBody
{
    /**
     * @param string[] $stringArray
     * @param list<string> $stringList
     * @param array<string, string> $stringIndexedArray
     * @param MacRequestBody[] $objArray
     * @param array<string, MacRequestBody> $objIndexedArray
     *
     * @param string[]|null $stringArrayNull
     * @param list<string>|null $stringListNull
     * @param array<string, string>|null $stringIndexedArrayNull
     * @param MacRequestBody[]|null $objArrayNull
     * @param array<string, MacRequestBody>|null $objIndexedArrayNull
     *
     * @param array<MacRequestBody|string> $arrayOfUnion
     * @param list<MacRequestBody|string> $listOfUnion
     * @param (MacRequestBody|string)[] $simpleArrayOfUnion
     *
     * @param string[] $stringArrayOptional
     * @param list<string> $stringListOptional
     * @param array<string, string> $stringIndexedArrayOptional
     * @param MacRequestBody[] $objArrayOptional
     * @param array<string, MacRequestBody> $objIndexedArrayOptional
     *
     * @param string[]|null $stringArrayNullOptional
     * @param list<string>|null $stringListNullOptional
     * @param array<string, string>|null $stringIndexedArrayNullOptional
     * @param MacRequestBody[]|null $objArrayNullOptional
     * @param array<string, MacRequestBody>|null $objIndexedArrayNullOptional
     *
     * @param string[]|null|NotSet $stringArrayNullPatch
     * @param list<string>|null|NotSet $stringListNullPatch
     * @param array<string, string>|null|NotSet $stringIndexedArrayNullPatch
     * @param MacRequestBody[]|null|NotSet $objArrayNullPatch
     * @param array<string, MacRequestBody>|null|NotSet $objIndexedArrayNullPatch
     */
    public function __construct(
        /** @deprecated */
        public string $string,
        #[Deprecated]
        public int $int,
        public float $float,
        public array $stringArray,
        public array $stringList,
        public array $stringIndexedArray,
        public MacRequestBody $obj,
        public Tag $entity,
        /** @deprecated */
        public array $objArray,
        #[Deprecated]
        public array $objIndexedArray,
        // null
        public ?string $stringNull,
        public ?int $intNull,
        public ?float $floatNull,
        public ?array $stringArrayNull,
        public ?array $stringListNull,
        public ?array $stringIndexedArrayNull,
        public ?MacRequestBody $objNull,
        public ?Tag $entityNull,
        public ?array $objArrayNull,
        public ?array $objIndexedArrayNull,
        // union
        public string|int $simpleUnion,
        public string|int|null $nullableSimpleUnion,
        public MacRequestBody|string|int $unionWithObject,
        public array $arrayOfUnion,
        public array $listOfUnion,
        public array $simpleArrayOfUnion,
        // optional
        public string $stringOptional = '',
        public int $intOptional = 0,
        public float $floatOptional = 0.0,
        public array $stringArrayOptional = [],
        public array $stringListOptional = [],
        public array $stringIndexedArrayOptional = [],
        public array $objArrayOptional = [],
        public array $objIndexedArrayOptional = [],
        // optional null
        public ?string $stringNullOptional = null,
        public ?int $intNullOptional = null,
        public ?float $floatNullOptional = null,
        public ?array $stringArrayNullOptional = null,
        public ?array $stringListNullOptional = null,
        public ?array $stringIndexedArrayNullOptional = null,
        public ?MacRequestBody $objNullOptional = null,
        public ?Tag $entityNullOptional = null,
        public ?array $objArrayNullOptional = null,
        public ?array $objIndexedArrayNullOptional = null,
        // optional patch
        public string | null | NotSet $stringNullPatch = new NotSet(),
        public int | null | NotSet $intNullPatch = new NotSet(),
        public float | null | NotSet $floatNullPatch = new NotSet(),
        public array | null | NotSet $stringArrayNullPatch = new NotSet(),
        public array | null | NotSet $stringListNullPatch = new NotSet(),
        public array | null | NotSet $stringIndexedArrayNullPatch = new NotSet(),
        public MacRequestBody | null | NotSet $objNullPatch = new NotSet(),
        public Tag | null | NotSet $entityNullPatch = new NotSet(),
        public array | null | NotSet $objArrayNullPatch = new NotSet(),
        public array | null | NotSet $objIndexedArrayNullPatch = new NotSet(),
        // uxf custom types
        public ?Date $date = null,
        public ?DateTime $dateTime = null,
        public ?Time $time = null,
        public ?Phone $phone = null,
        public ?Email $email = null,
        public ?NationalIdentificationNumberCze $nicCze = null,
        public ?BankAccountNumberCze $banCze = null,
        public ?Money $money = null,
        public ?Decimal $decimal = null,
        public ?Url $url = null,
    ) {
    }
}
