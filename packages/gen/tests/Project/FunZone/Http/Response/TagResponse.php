<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Response;

class TagResponse
{
    public function __construct(
        public int $id,
        public string $name,
        private readonly string $private,
    ) {
    }

    public function getPrivate(): string
    {
        return $this->private;
    }
}
