<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Response;

class CategoryResponse
{
    public function __construct(
        public int $id,
        public string $name,
    ) {
    }
}
