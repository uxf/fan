<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Response;

use Ramsey\Uuid\UuidInterface;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\GenTests\Project\FunZone\Entity\Type;

class ArticleResponse
{
    /**
     * @param TagResponse[] $tags
     * @param list<TagResponse> $tagList
     * @param array<string, TagResponse> $stringIndexedTags
     * @param array<int, TagResponse> $intIndexedTags
     * @param array<TagResponse|string> $arrayOfUnion
     * @param (TagResponse|string)[] $bracketArrayOfUnion
     * @param array<int, TagResponse|string> $indexedArrayOfUnion
     */
    public function __construct(
        public int $id,
        public string $title,
        public ?Type $type,
        public ?string $content,
        public Date $createdAt,
        public ?DateTime $publishedAt,
        public int $priority,
        public float $score,
        public bool $active,
        public CategoryResponse $category,
        public array $tags,
        public array $tagList,
        public array $stringIndexedTags,
        public array $intIndexedTags,
        public array $arrayOfUnion,
        public array $bracketArrayOfUnion,
        public array $indexedArrayOfUnion,
        public int|string $union,
        public UuidInterface $uuid,
    ) {
    }
}
