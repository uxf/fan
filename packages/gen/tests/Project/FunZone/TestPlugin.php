<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone;

use UXF\Gen\Inspector\Schema\AppSchema;
use UXF\Gen\Inspector\Schema\RouteSchema;
use UXF\Gen\Inspector\Schema\TypeSchema;
use UXF\Gen\Inspector\Schema\TypeVariant;
use UXF\Gen\Inspector\TypeMap;
use UXF\Gen\Plugin\InspectorPlugin;
use UXF\GenTests\Project\FunZone\Entity\EntityNames;

class TestPlugin implements InspectorPlugin
{
    public function pre(string $configName, TypeMap $typeMap): void
    {
        if ($configName === 'hook') {
            $typeMap[EntityNames::class] = new TypeSchema(EntityNames::class, TypeVariant::ENUM, EntityNames::class);
        }
    }

    public function post(string $configName, AppSchema $appSchema): void
    {
        if ($configName === 'apollo') {
            $appSchema->routes['plugin'] = new RouteSchema(
                name: 'plugin',
                path: '/plugin',
                controller: 'FunZone\Ok',
                requestBody: null,
                requestQueries: [],
                requestHeaders: [],
                pathParams: [],
                response: null,
                description: '',
                deprecated: false,
                methods: [
                    'get' => false,
                ],
            );
        }
    }
}
