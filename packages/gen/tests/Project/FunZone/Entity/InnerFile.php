<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class InnerFile
{
    #[ORM\Column, ORM\Id]
    public int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
