<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Entity;

enum Type: string
{
    case NEW = 'NEW';
    case OLD = 'OLD';
}
