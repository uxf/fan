<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class EntityWithEnumId
{
    #[ORM\Column, ORM\Id]
    public Type $type = Type::NEW;
}
