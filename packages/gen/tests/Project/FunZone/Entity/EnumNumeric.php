<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Entity;

enum EnumNumeric: int
{
    case ZERO = 0;
    case ONE = 1;
}
