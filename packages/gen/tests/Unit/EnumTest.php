<?php

declare(strict_types=1);

namespace UXF\GenTests\Unit;

use PHPUnit\Framework\TestCase;
use UXF\Gen\Generator\Http\EnumAsTsTypeGenerator;
use UXF\Gen\Generator\Http\EnumAsUnionGenerator;
use UXF\Gen\Inspector\Schema\TypeSchema;
use UXF\Gen\Inspector\Schema\TypeVariant;
use UXF\GenTests\Project\FunZone\Entity\EntityNames;

class EnumTest extends TestCase
{
    public function testEnumAsTsType(): void
    {
        self::assertSame(
            "// 
export enum EntityNames {
    Article = 'UXF\\\\GenTests\\\\Project\\\\FunZone\\\\Entity\\\\Article',
    Category = 'UXF\\\\GenTests\\\\Project\\\\FunZone\\\\Entity\\\\Category',
    Tag = 'UXF\\\\GenTests\\\\Project\\\\FunZone\\\\Entity\\\\Tag',
}

",
            (new EnumAsTsTypeGenerator())->generateString(EntityNames::class, new TypeSchema(EntityNames::class, TypeVariant::ENUM)),
        );
    }

    public function testEnumAsUnion(): void
    {
        self::assertSame(
            "// 
export const EntityNames = {
    Article: 'UXF\\\\GenTests\\\\Project\\\\FunZone\\\\Entity\\\\Article',
    Category: 'UXF\\\\GenTests\\\\Project\\\\FunZone\\\\Entity\\\\Category',
    Tag: 'UXF\\\\GenTests\\\\Project\\\\FunZone\\\\Entity\\\\Tag',
} as const;
export type EntityNames = 'UXF\\\\GenTests\\\\Project\\\\FunZone\\\\Entity\\\\Article' | 'UXF\\\\GenTests\\\\Project\\\\FunZone\\\\Entity\\\\Category' | 'UXF\\\\GenTests\\\\Project\\\\FunZone\\\\Entity\\\\Tag';

",
            (new EnumAsUnionGenerator())->generateString(EntityNames::class, new TypeSchema(EntityNames::class, TypeVariant::ENUM)),
        );
    }
}
