<?php

declare(strict_types=1);

namespace UXF\GenTests\Unit;

use PHPUnit\Framework\TestCase;
use UXF\Gen\Utils\EnumNameHelper;

class EnumNameHelperTest extends TestCase
{
    public function test(): void
    {
        self::assertSame('HelloKitty', EnumNameHelper::getName('HELLO_KITTY'));
        self::assertSame('HelloKitty', EnumNameHelper::getName('HelloKitty'));
        self::assertSame('HelloKitty0', EnumNameHelper::getName('HELLO_KITTY0'));
        self::assertSame('HelloKitty0', EnumNameHelper::getName('HelloKitty0'));
    }
}
