<?php

declare(strict_types=1);

namespace UXF\GenTests;

use Nette\Utils\Json;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use UXF\Gen\Config\ApolloConfig;
use UXF\Gen\Config\HookConfig;
use UXF\Gen\Config\OpenApiConfig;
use UXF\Gen\Generator\Apollo\ApolloGenerator;
use UXF\Gen\Generator\Hook\HookApiGenerator;
use UXF\Gen\Generator\OpenApi\OpenApiGenerator;

class GenTest extends KernelTestCase
{
    public function test(): void
    {
        self::bootKernel();

        /** @var OpenApiGenerator $generator */
        $generator = self::getContainer()->get(OpenApiGenerator::class);
        $config = new OpenApiConfig('app', '/^\/api/', 'X-Auth');
        $output = $generator->generate($config);
        // file_put_contents(__DIR__ . '/expected/open_api.json', Json::encode($output, Json::PRETTY) . "\n");
        self::assertStringEqualsFile(__DIR__ . '/expected/open_api.json', Json::encode($output, pretty: true) . "\n");

        /** @var HookApiGenerator $generator */
        $generator = self::getContainer()->get(HookApiGenerator::class);
        $config = new HookConfig('hook', [], '/^\/api/', [], true, 'legacy');
        $output = $generator->generate($config);
        // file_put_contents(__DIR__ . '/expected/hook.ts', $output);
        self::assertStringEqualsFile(__DIR__ . '/expected/hook.ts', $output);

        /** @var ApolloGenerator $generator */
        $generator = self::getContainer()->get(ApolloGenerator::class);
        $config = new ApolloConfig('apollo', [], '/^\/api/', []);
        $output = $generator->generate($config);
        // file_put_contents(__DIR__ . '/expected/apollo.ts', $output);
        self::assertStringEqualsFile(__DIR__ . '/expected/apollo.ts', $output);
    }

    public function testSwr(): void
    {
        self::bootKernel();

        /** @var HookApiGenerator $generator */
        $generator = self::getContainer()->get(HookApiGenerator::class);
        $config = new HookConfig('hook', [], '/^\/api/', [], true, 'swr');
        $output = $generator->generate($config);
        // file_put_contents(__DIR__ . '/expected/hook-swr.ts', $output);
        self::assertStringEqualsFile(__DIR__ . '/expected/hook-swr.ts', $output);
    }
}
