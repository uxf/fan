<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\GenTests\Project\FunZone\Controller\ArticleController;
use UXF\GenTests\Project\FunZone\Controller\EntityController;
use UXF\GenTests\Project\FunZone\Controller\FullController;
use UXF\GenTests\Project\FunZone\Controller\GenericController;
use UXF\GenTests\Project\FunZone\Controller\InvokeController;
use UXF\GenTests\Project\FunZone\Controller\MixedController;
use UXF\GenTests\Project\FunZone\Controller\PersonController;
use UXF\GenTests\Project\FunZone\Controller\UnionController;

return static function (RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->add('article_all', '/api/article')
        ->controller([ArticleController::class, 'all'])
        ->methods(['GET']);

    $routingConfigurator->add('article_double_query', '/api/article/double-query')
        ->controller([ArticleController::class, 'doubleQuery'])
        ->methods(['GET']);

    $routingConfigurator->add('article_create', '/api/article')
        ->controller([ArticleController::class, 'create'])
        ->methods(['POST']);

    $routingConfigurator->add('article_read', '/api/article/{article}')
        ->controller([ArticleController::class, 'read'])
        ->methods(['GET']);

    $routingConfigurator->add('article_update', '/api/article/{article}')
        ->controller([ArticleController::class, 'update'])
        ->methods(['PUT']);

    $routingConfigurator->add('article_delete', '/api/article/{article}')
        ->controller([ArticleController::class, 'delete'])
        ->methods(['DELETE']);

    $routingConfigurator->add('invoke', '/api/article/invoke/{type}')
        ->controller(InvokeController::class)
        ->methods(['GET']);

    $routingConfigurator->add('person_create', '/api/person')
        ->controller([PersonController::class, 'create'])
        ->methods(['POST']);

    $routingConfigurator->add('union_detail', '/api/union')
        ->controller([UnionController::class, 'detail'])
        ->methods(['POST']);

    $routingConfigurator->add('union_list', '/api/unions')
        ->controller([UnionController::class, 'list'])
        ->methods(['POST']);

    $routingConfigurator->add('entity', '/api/entity/{id}/{uuid}/{string}/{int}')
        ->controller(EntityController::class)
        ->methods(['GET']);

    $routingConfigurator->add('generic_one', '/api/gen-one')
        ->controller([GenericController::class, 'one'])
        ->methods(['GET']);

    $routingConfigurator->add('generic_two', '/api/gen-two')
        ->controller([GenericController::class, 'two'])
        ->methods(['GET']);

    $routingConfigurator->add('generic_three', '/api/gen-three')
        ->controller([GenericController::class, 'three'])
        ->methods(['GET']);

    $routingConfigurator->add('mixed', '/api/mixed')
        ->controller(MixedController::class)
        ->methods(['POST']);

    $routingConfigurator->add('full_read', '/api/full/{article}')
        ->controller(FullController::class)
        ->methods(['GET']);

    $routingConfigurator->add('full_write', '/api/full/{article}')
        ->controller(FullController::class)
        ->methods(['PATCH']);
};
