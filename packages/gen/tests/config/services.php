<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\GenTests\Project\FunZone\TestPlugin;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure()
        ->public();

    $services->load('UXF\GenTests\Project\FunZone\Controller\\', __DIR__ . '/../Project/FunZone/Controller')
        ->public();

    $services->set(TestPlugin::class);

    $containerConfigurator->extension('framework', [
        'test' => true,
        'validation' => [
            'email_validation_mode' => 'html5',
        ],
        'http_method_override' => false,
    ]);

    $containerConfigurator->extension('security', [
        'firewalls' => [
            'main' => [
                'pattern' => '^/',
                'security' => false,
            ],
        ],
        'access_control' => [
            [
                'path' => '^/api',
                'methods' => ['POST'],
                'roles' => 'ROLE_ADMIN',
            ],
            [
                'path' => '^/api/article',
                'roles' => 'PUBLIC_ACCESS',
            ],
        ],
    ]);

    $containerConfigurator->extension('doctrine', [
        'dbal' => [
            'url' => 'sqlite:///%kernel.project_dir%/var/db.sqlite',
        ],
        'orm' => [
            'report_fields_where_declared' => true,
            'enable_lazy_ghost_objects' => true,
            'mappings' => [
                'app' => [
                    'type' => 'attribute',
                    'dir' => '%kernel.project_dir%/tests/Project/FunZone/Entity',
                    'prefix' => 'UXF\GenTests\Project\FunZone\Entity',
                ],
            ],
        ],
    ]);

    $containerConfigurator->extension('uxf_gen', [
        'config' => [
            'enum_as_union' => true,
            'typescript_types' => [
                'DateTime' => '`${number}-${number}-${number}T${number}:${number}:${number}+${number}:${number}`',
                'Date' => '`${number}-${number}-${number}`',
                'Time' => '`${number}:${number}:${number}`',
            ],
        ],
        'open_api' => [
            'areas' => [
                'app' => [
                    'path_pattern' => '/^\/api\/article/',
                ],
            ],
        ],
        'hook' => [
            'areas' => [
                'admin' => [
                    'path_pattern' => '/^\/api\/article/',
                    'destination' => '%kernel.project_dir%/tests/generated/hook.ts',
                    'prepends' => [
                        '/* eslint-disable */',
                        'import { useAxiosRequest, axiosRequest, RequestConfig } from "@lib/api";',
                    ],
                ],
            ],
        ],
        'apollo' => [
            'areas' => [
                'app' => [
                    'path_pattern' => '/^\/api\/article/',
                    'destination' => [
                        '%kernel.project_dir%/tests/generated/apollo.ts',
                    ],
                ],
            ],
        ],
    ]);
};
