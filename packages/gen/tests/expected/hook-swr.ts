import useSWR from "swr";
import useSWRMutation from "swr/mutation";
import type { Key, SWRConfiguration } from "swr";
import type { SWRMutationConfiguration, SWRMutationResponse } from "swr/mutation";
import { stringify } from "qs";

export class ApiError extends Error {
    constructor(
        message: string,
        public readonly code: string,
        public readonly statusCode: number,
        public readonly info: {
            error: {
                code: string;
                message: string;
            },
            validationErrors: {
                field: string;
                message: string;
            }[]
        }
    ) {
        super(message);
    }
}

export const _url = (parts: readonly [string, any] | readonly [string]) => {
    const q = typeof parts[1] !== "undefined" ? stringify(parts[1], { skipNulls: true, addQueryPrefix: true }) : "";
    return `${parts[0]}${q}`;
}

export const _defaultFetch = (url: string, method: string = "GET", body: any = undefined) => fetch((process.env.API_URL ?? "") + url, { method, body: typeof body !== "undefined" ? JSON.stringify(body) : undefined }).then(async (res) => {
    const data = await res.json();

    if (!res.ok) {
        throw new ApiError(data.error.message, data.error.code, res.status, data);
    }

    return data;
});

declare global {
    const _customFetch: any;
}

const _fetch = typeof _customFetch === "function" ? _customFetch : _defaultFetch;

// 
export const Currency = {
    Aed: 'AED',
    Afn: 'AFN',
    All: 'ALL',
    Amd: 'AMD',
    Ang: 'ANG',
    Aoa: 'AOA',
    Ars: 'ARS',
    Aud: 'AUD',
    Awg: 'AWG',
    Azn: 'AZN',
    Bam: 'BAM',
    Bbd: 'BBD',
    Bdt: 'BDT',
    Bgn: 'BGN',
    Bhd: 'BHD',
    Bif: 'BIF',
    Bmd: 'BMD',
    Bnd: 'BND',
    Bob: 'BOB',
    Bov: 'BOV',
    Brl: 'BRL',
    Bsd: 'BSD',
    Btn: 'BTN',
    Bwp: 'BWP',
    Byn: 'BYN',
    Bzd: 'BZD',
    Cad: 'CAD',
    Cdf: 'CDF',
    Che: 'CHE',
    Chf: 'CHF',
    Chw: 'CHW',
    Clf: 'CLF',
    Clp: 'CLP',
    Cny: 'CNY',
    Cop: 'COP',
    Cou: 'COU',
    Crc: 'CRC',
    Cuc: 'CUC',
    Cup: 'CUP',
    Cve: 'CVE',
    Czk: 'CZK',
    Djf: 'DJF',
    Dkk: 'DKK',
    Dop: 'DOP',
    Dzd: 'DZD',
    Egp: 'EGP',
    Ern: 'ERN',
    Etb: 'ETB',
    Eur: 'EUR',
    Fjd: 'FJD',
    Fkp: 'FKP',
    Gbp: 'GBP',
    Gel: 'GEL',
    Ghs: 'GHS',
    Gip: 'GIP',
    Gmd: 'GMD',
    Gnf: 'GNF',
    Gtq: 'GTQ',
    Gyd: 'GYD',
    Hkd: 'HKD',
    Hnl: 'HNL',
    Htg: 'HTG',
    Huf: 'HUF',
    Idr: 'IDR',
    Ils: 'ILS',
    Inr: 'INR',
    Iqd: 'IQD',
    Irr: 'IRR',
    Isk: 'ISK',
    Jmd: 'JMD',
    Jod: 'JOD',
    Jpy: 'JPY',
    Kes: 'KES',
    Kgs: 'KGS',
    Khr: 'KHR',
    Kmf: 'KMF',
    Kpw: 'KPW',
    Krw: 'KRW',
    Kwd: 'KWD',
    Kyd: 'KYD',
    Kzt: 'KZT',
    Lak: 'LAK',
    Lbp: 'LBP',
    Lkr: 'LKR',
    Lrd: 'LRD',
    Lsl: 'LSL',
    Lyd: 'LYD',
    Mad: 'MAD',
    Mdl: 'MDL',
    Mga: 'MGA',
    Mkd: 'MKD',
    Mmk: 'MMK',
    Mnt: 'MNT',
    Mop: 'MOP',
    Mru: 'MRU',
    Mur: 'MUR',
    Mvr: 'MVR',
    Mwk: 'MWK',
    Mxn: 'MXN',
    Mxv: 'MXV',
    Myr: 'MYR',
    Mzn: 'MZN',
    Nad: 'NAD',
    Ngn: 'NGN',
    Nio: 'NIO',
    Nok: 'NOK',
    Npr: 'NPR',
    Nzd: 'NZD',
    Omr: 'OMR',
    Pab: 'PAB',
    Pen: 'PEN',
    Pgk: 'PGK',
    Php: 'PHP',
    Pkr: 'PKR',
    Pln: 'PLN',
    Pyg: 'PYG',
    Qar: 'QAR',
    Ron: 'RON',
    Rsd: 'RSD',
    Rub: 'RUB',
    Rwf: 'RWF',
    Sar: 'SAR',
    Sbd: 'SBD',
    Scr: 'SCR',
    Sdg: 'SDG',
    Sek: 'SEK',
    Sgd: 'SGD',
    Shp: 'SHP',
    Sle: 'SLE',
    Sll: 'SLL',
    Sos: 'SOS',
    Srd: 'SRD',
    Ssp: 'SSP',
    Stn: 'STN',
    Svc: 'SVC',
    Syp: 'SYP',
    Szl: 'SZL',
    Thb: 'THB',
    Tjs: 'TJS',
    Tmt: 'TMT',
    Tnd: 'TND',
    Top: 'TOP',
    Try: 'TRY',
    Ttd: 'TTD',
    Twd: 'TWD',
    Tzs: 'TZS',
    Uah: 'UAH',
    Ugx: 'UGX',
    Usd: 'USD',
    Usn: 'USN',
    Uyi: 'UYI',
    Uyu: 'UYU',
    Uyw: 'UYW',
    Uzs: 'UZS',
    Ved: 'VED',
    Ves: 'VES',
    Vnd: 'VND',
    Vuv: 'VUV',
    Wst: 'WST',
    Xaf: 'XAF',
    Xcd: 'XCD',
    Xof: 'XOF',
    Xpf: 'XPF',
    Yer: 'YER',
    Zar: 'ZAR',
    Zmw: 'ZMW',
    Zwl: 'ZWL',
} as const;
export type Currency = 'AED' | 'AFN' | 'ALL' | 'AMD' | 'ANG' | 'AOA' | 'ARS' | 'AUD' | 'AWG' | 'AZN' | 'BAM' | 'BBD' | 'BDT' | 'BGN' | 'BHD' | 'BIF' | 'BMD' | 'BND' | 'BOB' | 'BOV' | 'BRL' | 'BSD' | 'BTN' | 'BWP' | 'BYN' | 'BZD' | 'CAD' | 'CDF' | 'CHE' | 'CHF' | 'CHW' | 'CLF' | 'CLP' | 'CNY' | 'COP' | 'COU' | 'CRC' | 'CUC' | 'CUP' | 'CVE' | 'CZK' | 'DJF' | 'DKK' | 'DOP' | 'DZD' | 'EGP' | 'ERN' | 'ETB' | 'EUR' | 'FJD' | 'FKP' | 'GBP' | 'GEL' | 'GHS' | 'GIP' | 'GMD' | 'GNF' | 'GTQ' | 'GYD' | 'HKD' | 'HNL' | 'HTG' | 'HUF' | 'IDR' | 'ILS' | 'INR' | 'IQD' | 'IRR' | 'ISK' | 'JMD' | 'JOD' | 'JPY' | 'KES' | 'KGS' | 'KHR' | 'KMF' | 'KPW' | 'KRW' | 'KWD' | 'KYD' | 'KZT' | 'LAK' | 'LBP' | 'LKR' | 'LRD' | 'LSL' | 'LYD' | 'MAD' | 'MDL' | 'MGA' | 'MKD' | 'MMK' | 'MNT' | 'MOP' | 'MRU' | 'MUR' | 'MVR' | 'MWK' | 'MXN' | 'MXV' | 'MYR' | 'MZN' | 'NAD' | 'NGN' | 'NIO' | 'NOK' | 'NPR' | 'NZD' | 'OMR' | 'PAB' | 'PEN' | 'PGK' | 'PHP' | 'PKR' | 'PLN' | 'PYG' | 'QAR' | 'RON' | 'RSD' | 'RUB' | 'RWF' | 'SAR' | 'SBD' | 'SCR' | 'SDG' | 'SEK' | 'SGD' | 'SHP' | 'SLE' | 'SLL' | 'SOS' | 'SRD' | 'SSP' | 'STN' | 'SVC' | 'SYP' | 'SZL' | 'THB' | 'TJS' | 'TMT' | 'TND' | 'TOP' | 'TRY' | 'TTD' | 'TWD' | 'TZS' | 'UAH' | 'UGX' | 'USD' | 'USN' | 'UYI' | 'UYU' | 'UYW' | 'UZS' | 'VED' | 'VES' | 'VND' | 'VUV' | 'WST' | 'XAF' | 'XCD' | 'XOF' | 'XPF' | 'YER' | 'ZAR' | 'ZMW' | 'ZWL';

// UXF\GenTests\Project\FunZone\Entity\EntityNames
export const EntityNames = {
    Article: 'UXF\\GenTests\\Project\\FunZone\\Entity\\Article',
    Category: 'UXF\\GenTests\\Project\\FunZone\\Entity\\Category',
    Tag: 'UXF\\GenTests\\Project\\FunZone\\Entity\\Tag',
} as const;
export type EntityNames = 'UXF\\GenTests\\Project\\FunZone\\Entity\\Article' | 'UXF\\GenTests\\Project\\FunZone\\Entity\\Category' | 'UXF\\GenTests\\Project\\FunZone\\Entity\\Tag';

// UXF\GenTests\Project\FunZone\Entity\EnumNumeric
export const EnumNumeric = {
    Zero: 0,
    One: 1,
} as const;
export type EnumNumeric = 0 | 1;

// UXF\GenTests\Project\FunZone\Entity\Type
export const Type = {
    New: 'NEW',
    Old: 'OLD',
} as const;
export type Type = 'NEW' | 'OLD';

type UUID = string;

type BankAccountNumberCze = string;

type Date = `${number}-${number}-${number}`;

type DateTime = `${number}-${number}-${number}T${number}:${number}:${number}+${number}:${number}`;

type Decimal = string;

type Email = string;

// UXF\Core\Type\Money
export interface Money {
    amount: XString;
    currency: Currency;
}

type NationalIdentificationNumberCze = string;

type Phone = string;

type Time = `${number}:${number}:${number}`;

type Url = string;

// UXF\GenTests\Project\FunZone\Http\Request\ArticleRequestBody
export interface FunArticleRequestBody {
    title: XString;
    type: Type;
    enumNumeric: EnumNumeric;
    publishedAt: DateTime;
    priority: Int;
    score: Float;
    active: Bool;
    category: Int;
    enumEntity: Type;
    tags: Array<Int>;
    file: UUID;
    files: Array<UUID>;
    content?: XString | null;
}

// UXF\GenTests\Project\FunZone\Http\Request\ArticleRequestHeader
export interface FunArticleRequestHeader {
    acceptLanguage: XString;
}

// UXF\GenTests\Project\FunZone\Http\Request\ArticleRequestQuery
export interface FunArticleRequestQuery {
    title?: XString | null;
    type?: Type | null;
    publishedAt?: DateTime | null;
    priority?: Int | null;
    score?: Float | null;
    active?: Bool | null;
    category?: Int | null;
    enumEntity?: Type | null;
    tags?: Array<Int>;
}

// UXF\GenTests\Project\FunZone\Http\Request\Family\Activity
export type FunActivity = FunOrienteering | FunParagliding;

// UXF\GenTests\Project\FunZone\Http\Request\Family\Orienteering
export interface FunOrienteering {
    card: Int;
    type: "o";
}

// UXF\GenTests\Project\FunZone\Http\Request\Family\Paragliding
export interface FunParagliding {
    glider: XString;
    type: "p";
}

// UXF\GenTests\Project\FunZone\Http\Request\JumpRequestQuery
export interface FunJumpRequestQuery {
    article?: Int | null;
}

// UXF\GenTests\Project\FunZone\Http\Request\MacRequestBody
/** @deprecated */
export interface FunMacRequestBody {
    id: Int;
    name: XString;
}

// UXF\GenTests\Project\FunZone\Http\Request\PersonRequestBody
/** @deprecated */
export interface FunPersonRequestBody {
    /** @deprecated */
    string: XString;
    /** @deprecated */
    int: Int;
    float: Float;
    stringArray: Array<XString>;
    stringList: Array<XString>;
    stringIndexedArray: Record<string, XString>;
    obj: FunMacRequestBody;
    entity: Int;
    /** @deprecated */
    objArray: Array<FunMacRequestBody>;
    /** @deprecated */
    objIndexedArray: Record<string, FunMacRequestBody>;
    stringNull: XString | null;
    intNull: Int | null;
    floatNull: Float | null;
    stringArrayNull: Array<XString> | null;
    stringListNull: Array<XString> | null;
    stringIndexedArrayNull: Record<string, XString> | null;
    objNull: FunMacRequestBody | null;
    entityNull: Int | null;
    objArrayNull: Array<FunMacRequestBody> | null;
    objIndexedArrayNull: Record<string, FunMacRequestBody> | null;
    simpleUnion: Int | XString;
    nullableSimpleUnion: Int | XString | null;
    unionWithObject: FunMacRequestBody | Int | XString;
    arrayOfUnion: Array<FunMacRequestBody | XString>;
    listOfUnion: Array<FunMacRequestBody | XString>;
    simpleArrayOfUnion: Array<FunMacRequestBody | XString>;
    stringOptional?: XString;
    intOptional?: Int;
    floatOptional?: Float;
    stringArrayOptional?: Array<XString>;
    stringListOptional?: Array<XString>;
    stringIndexedArrayOptional?: Record<string, XString>;
    objArrayOptional?: Array<FunMacRequestBody>;
    objIndexedArrayOptional?: Record<string, FunMacRequestBody>;
    stringNullOptional?: XString | null;
    intNullOptional?: Int | null;
    floatNullOptional?: Float | null;
    stringArrayNullOptional?: Array<XString> | null;
    stringListNullOptional?: Array<XString> | null;
    stringIndexedArrayNullOptional?: Record<string, XString> | null;
    objNullOptional?: FunMacRequestBody | null;
    entityNullOptional?: Int | null;
    objArrayNullOptional?: Array<FunMacRequestBody> | null;
    objIndexedArrayNullOptional?: Record<string, FunMacRequestBody> | null;
    stringNullPatch?: XString | null;
    intNullPatch?: Int | null;
    floatNullPatch?: Float | null;
    stringArrayNullPatch?: Array<XString> | null;
    stringListNullPatch?: Array<XString> | null;
    stringIndexedArrayNullPatch?: Record<string, XString> | null;
    objNullPatch?: FunMacRequestBody | null;
    entityNullPatch?: Int | null;
    objArrayNullPatch?: Array<FunMacRequestBody> | null;
    objIndexedArrayNullPatch?: Record<string, FunMacRequestBody> | null;
    date?: Date | null;
    dateTime?: DateTime | null;
    time?: Time | null;
    phone?: Phone | null;
    email?: Email | null;
    nicCze?: NationalIdentificationNumberCze | null;
    banCze?: BankAccountNumberCze | null;
    money?: Money | null;
    decimal?: Decimal | null;
    url?: Url | null;
}

// UXF\GenTests\Project\FunZone\Http\Response\ArticleResponse
export interface FunArticleResponse {
    id: Int;
    title: XString;
    type: Type | null;
    content: XString | null;
    createdAt: Date;
    publishedAt: DateTime | null;
    priority: Int;
    score: Float;
    active: Bool;
    category: FunCategoryResponse;
    tags: Array<FunTagResponse>;
    tagList: Array<FunTagResponse>;
    stringIndexedTags: Record<string, FunTagResponse>;
    intIndexedTags: Record<string, FunTagResponse>;
    arrayOfUnion: Array<FunTagResponse | XString>;
    bracketArrayOfUnion: Array<FunTagResponse | XString>;
    indexedArrayOfUnion: Record<string, FunTagResponse | XString>;
    union: Int | XString;
    uuid: UUID;
}

// UXF\GenTests\Project\FunZone\Http\Response\CategoryResponse
export interface FunCategoryResponse {
    id: Int;
    name: XString;
}

// UXF\GenTests\Project\FunZone\Http\Response\Generic\GenericResponseBody
export interface FunGenericResponseBody<T> {
    success: Bool;
    data: T;
}

// UXF\GenTests\Project\FunZone\Http\Response\TagResponse
export interface FunTagResponse {
    id: Int;
    name: XString;
}

type Bool = boolean;

type Float = number;

type Int = number;

type Mixed = any;

type XString = string;

const formOptions = <T extends Record<string, { label: string; }>>(o: T) =>
    Object.entries(o).map(([id, { label }]) => ({
        id: id as keyof T,
        label,
    }));

const formIntOptions = <T extends Record<string, { label: string; }>>(o: T) =>
    Object.entries(o).map(([id, { label }]) => ({
        id: Number(id) as keyof T,
        label,
    }));

// 
export const CurrencyOptions: Record<Currency, {label: string, color: null}> = {
    "AED": {
        "label": "AED",
        "color": null
    },
    "AFN": {
        "label": "AFN",
        "color": null
    },
    "ALL": {
        "label": "ALL",
        "color": null
    },
    "AMD": {
        "label": "AMD",
        "color": null
    },
    "ANG": {
        "label": "ANG",
        "color": null
    },
    "AOA": {
        "label": "AOA",
        "color": null
    },
    "ARS": {
        "label": "ARS",
        "color": null
    },
    "AUD": {
        "label": "AUD",
        "color": null
    },
    "AWG": {
        "label": "AWG",
        "color": null
    },
    "AZN": {
        "label": "AZN",
        "color": null
    },
    "BAM": {
        "label": "BAM",
        "color": null
    },
    "BBD": {
        "label": "BBD",
        "color": null
    },
    "BDT": {
        "label": "BDT",
        "color": null
    },
    "BGN": {
        "label": "BGN",
        "color": null
    },
    "BHD": {
        "label": "BHD",
        "color": null
    },
    "BIF": {
        "label": "BIF",
        "color": null
    },
    "BMD": {
        "label": "BMD",
        "color": null
    },
    "BND": {
        "label": "BND",
        "color": null
    },
    "BOB": {
        "label": "BOB",
        "color": null
    },
    "BOV": {
        "label": "BOV",
        "color": null
    },
    "BRL": {
        "label": "BRL",
        "color": null
    },
    "BSD": {
        "label": "BSD",
        "color": null
    },
    "BTN": {
        "label": "BTN",
        "color": null
    },
    "BWP": {
        "label": "BWP",
        "color": null
    },
    "BYN": {
        "label": "BYN",
        "color": null
    },
    "BZD": {
        "label": "BZD",
        "color": null
    },
    "CAD": {
        "label": "CAD",
        "color": null
    },
    "CDF": {
        "label": "CDF",
        "color": null
    },
    "CHE": {
        "label": "CHE",
        "color": null
    },
    "CHF": {
        "label": "CHF",
        "color": null
    },
    "CHW": {
        "label": "CHW",
        "color": null
    },
    "CLF": {
        "label": "CLF",
        "color": null
    },
    "CLP": {
        "label": "CLP",
        "color": null
    },
    "CNY": {
        "label": "CNY",
        "color": null
    },
    "COP": {
        "label": "COP",
        "color": null
    },
    "COU": {
        "label": "COU",
        "color": null
    },
    "CRC": {
        "label": "CRC",
        "color": null
    },
    "CUC": {
        "label": "CUC",
        "color": null
    },
    "CUP": {
        "label": "CUP",
        "color": null
    },
    "CVE": {
        "label": "CVE",
        "color": null
    },
    "CZK": {
        "label": "CZK",
        "color": null
    },
    "DJF": {
        "label": "DJF",
        "color": null
    },
    "DKK": {
        "label": "DKK",
        "color": null
    },
    "DOP": {
        "label": "DOP",
        "color": null
    },
    "DZD": {
        "label": "DZD",
        "color": null
    },
    "EGP": {
        "label": "EGP",
        "color": null
    },
    "ERN": {
        "label": "ERN",
        "color": null
    },
    "ETB": {
        "label": "ETB",
        "color": null
    },
    "EUR": {
        "label": "EUR",
        "color": null
    },
    "FJD": {
        "label": "FJD",
        "color": null
    },
    "FKP": {
        "label": "FKP",
        "color": null
    },
    "GBP": {
        "label": "GBP",
        "color": null
    },
    "GEL": {
        "label": "GEL",
        "color": null
    },
    "GHS": {
        "label": "GHS",
        "color": null
    },
    "GIP": {
        "label": "GIP",
        "color": null
    },
    "GMD": {
        "label": "GMD",
        "color": null
    },
    "GNF": {
        "label": "GNF",
        "color": null
    },
    "GTQ": {
        "label": "GTQ",
        "color": null
    },
    "GYD": {
        "label": "GYD",
        "color": null
    },
    "HKD": {
        "label": "HKD",
        "color": null
    },
    "HNL": {
        "label": "HNL",
        "color": null
    },
    "HTG": {
        "label": "HTG",
        "color": null
    },
    "HUF": {
        "label": "HUF",
        "color": null
    },
    "IDR": {
        "label": "IDR",
        "color": null
    },
    "ILS": {
        "label": "ILS",
        "color": null
    },
    "INR": {
        "label": "INR",
        "color": null
    },
    "IQD": {
        "label": "IQD",
        "color": null
    },
    "IRR": {
        "label": "IRR",
        "color": null
    },
    "ISK": {
        "label": "ISK",
        "color": null
    },
    "JMD": {
        "label": "JMD",
        "color": null
    },
    "JOD": {
        "label": "JOD",
        "color": null
    },
    "JPY": {
        "label": "JPY",
        "color": null
    },
    "KES": {
        "label": "KES",
        "color": null
    },
    "KGS": {
        "label": "KGS",
        "color": null
    },
    "KHR": {
        "label": "KHR",
        "color": null
    },
    "KMF": {
        "label": "KMF",
        "color": null
    },
    "KPW": {
        "label": "KPW",
        "color": null
    },
    "KRW": {
        "label": "KRW",
        "color": null
    },
    "KWD": {
        "label": "KWD",
        "color": null
    },
    "KYD": {
        "label": "KYD",
        "color": null
    },
    "KZT": {
        "label": "KZT",
        "color": null
    },
    "LAK": {
        "label": "LAK",
        "color": null
    },
    "LBP": {
        "label": "LBP",
        "color": null
    },
    "LKR": {
        "label": "LKR",
        "color": null
    },
    "LRD": {
        "label": "LRD",
        "color": null
    },
    "LSL": {
        "label": "LSL",
        "color": null
    },
    "LYD": {
        "label": "LYD",
        "color": null
    },
    "MAD": {
        "label": "MAD",
        "color": null
    },
    "MDL": {
        "label": "MDL",
        "color": null
    },
    "MGA": {
        "label": "MGA",
        "color": null
    },
    "MKD": {
        "label": "MKD",
        "color": null
    },
    "MMK": {
        "label": "MMK",
        "color": null
    },
    "MNT": {
        "label": "MNT",
        "color": null
    },
    "MOP": {
        "label": "MOP",
        "color": null
    },
    "MRU": {
        "label": "MRU",
        "color": null
    },
    "MUR": {
        "label": "MUR",
        "color": null
    },
    "MVR": {
        "label": "MVR",
        "color": null
    },
    "MWK": {
        "label": "MWK",
        "color": null
    },
    "MXN": {
        "label": "MXN",
        "color": null
    },
    "MXV": {
        "label": "MXV",
        "color": null
    },
    "MYR": {
        "label": "MYR",
        "color": null
    },
    "MZN": {
        "label": "MZN",
        "color": null
    },
    "NAD": {
        "label": "NAD",
        "color": null
    },
    "NGN": {
        "label": "NGN",
        "color": null
    },
    "NIO": {
        "label": "NIO",
        "color": null
    },
    "NOK": {
        "label": "NOK",
        "color": null
    },
    "NPR": {
        "label": "NPR",
        "color": null
    },
    "NZD": {
        "label": "NZD",
        "color": null
    },
    "OMR": {
        "label": "OMR",
        "color": null
    },
    "PAB": {
        "label": "PAB",
        "color": null
    },
    "PEN": {
        "label": "PEN",
        "color": null
    },
    "PGK": {
        "label": "PGK",
        "color": null
    },
    "PHP": {
        "label": "PHP",
        "color": null
    },
    "PKR": {
        "label": "PKR",
        "color": null
    },
    "PLN": {
        "label": "PLN",
        "color": null
    },
    "PYG": {
        "label": "PYG",
        "color": null
    },
    "QAR": {
        "label": "QAR",
        "color": null
    },
    "RON": {
        "label": "RON",
        "color": null
    },
    "RSD": {
        "label": "RSD",
        "color": null
    },
    "RUB": {
        "label": "RUB",
        "color": null
    },
    "RWF": {
        "label": "RWF",
        "color": null
    },
    "SAR": {
        "label": "SAR",
        "color": null
    },
    "SBD": {
        "label": "SBD",
        "color": null
    },
    "SCR": {
        "label": "SCR",
        "color": null
    },
    "SDG": {
        "label": "SDG",
        "color": null
    },
    "SEK": {
        "label": "SEK",
        "color": null
    },
    "SGD": {
        "label": "SGD",
        "color": null
    },
    "SHP": {
        "label": "SHP",
        "color": null
    },
    "SLE": {
        "label": "SLE",
        "color": null
    },
    "SLL": {
        "label": "SLL",
        "color": null
    },
    "SOS": {
        "label": "SOS",
        "color": null
    },
    "SRD": {
        "label": "SRD",
        "color": null
    },
    "SSP": {
        "label": "SSP",
        "color": null
    },
    "STN": {
        "label": "STN",
        "color": null
    },
    "SVC": {
        "label": "SVC",
        "color": null
    },
    "SYP": {
        "label": "SYP",
        "color": null
    },
    "SZL": {
        "label": "SZL",
        "color": null
    },
    "THB": {
        "label": "THB",
        "color": null
    },
    "TJS": {
        "label": "TJS",
        "color": null
    },
    "TMT": {
        "label": "TMT",
        "color": null
    },
    "TND": {
        "label": "TND",
        "color": null
    },
    "TOP": {
        "label": "TOP",
        "color": null
    },
    "TRY": {
        "label": "TRY",
        "color": null
    },
    "TTD": {
        "label": "TTD",
        "color": null
    },
    "TWD": {
        "label": "TWD",
        "color": null
    },
    "TZS": {
        "label": "TZS",
        "color": null
    },
    "UAH": {
        "label": "UAH",
        "color": null
    },
    "UGX": {
        "label": "UGX",
        "color": null
    },
    "USD": {
        "label": "USD",
        "color": null
    },
    "USN": {
        "label": "USN",
        "color": null
    },
    "UYI": {
        "label": "UYI",
        "color": null
    },
    "UYU": {
        "label": "UYU",
        "color": null
    },
    "UYW": {
        "label": "UYW",
        "color": null
    },
    "UZS": {
        "label": "UZS",
        "color": null
    },
    "VED": {
        "label": "VED",
        "color": null
    },
    "VES": {
        "label": "VES",
        "color": null
    },
    "VND": {
        "label": "VND",
        "color": null
    },
    "VUV": {
        "label": "VUV",
        "color": null
    },
    "WST": {
        "label": "WST",
        "color": null
    },
    "XAF": {
        "label": "XAF",
        "color": null
    },
    "XCD": {
        "label": "XCD",
        "color": null
    },
    "XOF": {
        "label": "XOF",
        "color": null
    },
    "XPF": {
        "label": "XPF",
        "color": null
    },
    "YER": {
        "label": "YER",
        "color": null
    },
    "ZAR": {
        "label": "ZAR",
        "color": null
    },
    "ZMW": {
        "label": "ZMW",
        "color": null
    },
    "ZWL": {
        "label": "ZWL",
        "color": null
    }
};
export const enum_Currency_array = formOptions(CurrencyOptions);
export const enum_Currency = (value: Currency) => CurrencyOptions[value];

// UXF\GenTests\Project\FunZone\Entity\EntityNames
export const EntityNamesOptions: Record<EntityNames, {label: string, color: "red" | "blue" | null}> = {
    "UXF\\GenTests\\Project\\FunZone\\Entity\\Article": {
        "label": "Článek",
        "color": "red"
    },
    "UXF\\GenTests\\Project\\FunZone\\Entity\\Category": {
        "label": "Kategorie",
        "color": "blue"
    },
    "UXF\\GenTests\\Project\\FunZone\\Entity\\Tag": {
        "label": "Štítek",
        "color": null
    }
};
export const enum_EntityNames_array = formOptions(EntityNamesOptions);
export const enum_EntityNames = (value: EntityNames) => EntityNamesOptions[value];

// UXF\GenTests\Project\FunZone\Entity\EnumNumeric
export const EnumNumericOptions: Record<EnumNumeric, {label: string, color: null}> = {
    "0": {
        "label": "ZERO",
        "color": null
    },
    "1": {
        "label": "ONE",
        "color": null
    }
};
export const enum_EnumNumeric_array = formIntOptions(EnumNumericOptions);
export const enum_EnumNumeric = (value: EnumNumeric) => EnumNumericOptions[value];

// UXF\GenTests\Project\FunZone\Entity\Type
export const TypeOptions: Record<Type, {label: string, color: null}> = {
    "NEW": {
        "label": "NEW",
        "color": null
    },
    "OLD": {
        "label": "OLD",
        "color": null
    }
};
export const enum_Type_array = formOptions(TypeOptions);
export const enum_Type = (value: Type) => TypeOptions[value];

// GET /api/article
export const pickArticleAllKey = (config: { query: FunArticleRequestQuery }) => [`/api/article`, config.query] as const;
export const useArticleAllQuery = <T extends SWRConfiguration<Array<FunArticleResponse>, ApiError>>(config: { query: FunArticleRequestQuery } & { skip?: boolean; key?: Key }, options: T = {} as never) => useSWR<Array<FunArticleResponse>, ApiError, T>(config.skip !== true ? (config.key ?? pickArticleAllKey(config)) : null, () => _fetch(_url(pickArticleAllKey(config))), options);
export const getArticleAll = (config: { query: FunArticleRequestQuery }): Promise<Array<FunArticleResponse>> => _fetch(_url(pickArticleAllKey(config)), 'GET', undefined);

// GET /api/article/double-query
export const pickArticleDoubleQueryKey = (config: { query: FunArticleRequestQuery & FunJumpRequestQuery }) => [`/api/article/double-query`, config.query] as const;
export const useArticleDoubleQueryQuery = <T extends SWRConfiguration<Mixed, ApiError>>(config: { query: FunArticleRequestQuery & FunJumpRequestQuery } & { skip?: boolean; key?: Key }, options: T = {} as never) => useSWR<Mixed, ApiError, T>(config.skip !== true ? (config.key ?? pickArticleDoubleQueryKey(config)) : null, () => _fetch(_url(pickArticleDoubleQueryKey(config))), options);
export const getArticleDoubleQuery = (config: { query: FunArticleRequestQuery & FunJumpRequestQuery }): Promise<Mixed> => _fetch(_url(pickArticleDoubleQueryKey(config)), 'GET', undefined);

// POST /api/article
export const pickArticleCreateKey = () => [`/api/article`] as const;
export const useArticleCreateMutation = (config: { key?: Key } = {}, options: SWRMutationConfiguration<FunArticleResponse, ApiError, Key, FunArticleRequestBody> | undefined = undefined): SWRMutationResponse<FunArticleResponse, ApiError, Key, FunArticleRequestBody> => useSWRMutation(config.key ?? pickArticleCreateKey(), (_: Key, { arg }: { arg: FunArticleRequestBody}) => _fetch(pickArticleCreateKey()[0], 'POST', arg), options);
export const articleCreate = (config: { body: FunArticleRequestBody }): Promise<FunArticleResponse> => _fetch(pickArticleCreateKey()[0], 'POST', config.body);

// GET /api/article/{article}
export const pickArticleReadKey = (config: { path: { article: Int } }) => [`/api/article/${config.path.article}`] as const;
export const useArticleReadQuery = <T extends SWRConfiguration<FunArticleResponse, ApiError>>(config: { path: { article: Int } } & { skip?: boolean; key?: Key }, options: T = {} as never) => useSWR<FunArticleResponse, ApiError, T>(config.skip !== true ? (config.key ?? pickArticleReadKey(config)) : null, () => _fetch(_url(pickArticleReadKey(config))), options);
export const getArticleRead = (config: { path: { article: Int } }): Promise<FunArticleResponse> => _fetch(_url(pickArticleReadKey(config)), 'GET', undefined);

// PUT /api/article/{article}
export const pickArticleUpdateKey = (config: { path: { article: Int } }) => [`/api/article/${config.path.article}`] as const;
export const useArticleUpdateMutation = (config: { path: { article: Int } } & { key?: Key }, options: SWRMutationConfiguration<FunArticleResponse, ApiError, Key, FunArticleRequestBody> | undefined = undefined): SWRMutationResponse<FunArticleResponse, ApiError, Key, FunArticleRequestBody> => useSWRMutation(config.key ?? pickArticleUpdateKey(config), (_: Key, { arg }: { arg: FunArticleRequestBody}) => _fetch(_url(pickArticleUpdateKey(config)), 'PUT', arg), options);
export const articleUpdate = (config: { body: FunArticleRequestBody } & { path: { article: Int } }): Promise<FunArticleResponse> => _fetch(_url(pickArticleUpdateKey(config)), 'PUT', config.body);

// DELETE /api/article/{article}
export const pickArticleDeleteKey = (config: { path: { article: Int } }) => [`/api/article/${config.path.article}`] as const;
export const useArticleDeleteMutation = (config: { path: { article: Int } } & { key?: Key }, options: SWRMutationConfiguration<Mixed, ApiError, Key, never> | undefined = undefined): SWRMutationResponse<Mixed, ApiError, Key, never> => useSWRMutation(config.key ?? pickArticleDeleteKey(config), () => _fetch(_url(pickArticleDeleteKey(config)), 'DELETE'), options);
export const articleDelete = (config: { path: { article: Int } }): Promise<Mixed> => _fetch(_url(pickArticleDeleteKey(config)), 'DELETE', undefined);

// GET /api/article/invoke/{type}
export const pickInvokeKey = (config: { path: { type: Type } }) => [`/api/article/invoke/${config.path.type}`] as const;
export const useInvokeQuery = <T extends SWRConfiguration<Array<FunCategoryResponse | FunTagResponse>, ApiError>>(config: { path: { type: Type } } & { skip?: boolean; key?: Key }, options: T = {} as never) => useSWR<Array<FunCategoryResponse | FunTagResponse>, ApiError, T>(config.skip !== true ? (config.key ?? pickInvokeKey(config)) : null, () => _fetch(_url(pickInvokeKey(config))), options);
export const getInvoke = (config: { path: { type: Type } }): Promise<Array<FunCategoryResponse | FunTagResponse>> => _fetch(_url(pickInvokeKey(config)), 'GET', undefined);

// POST /api/person
export const pickPersonCreateKey = () => [`/api/person`] as const;
export const usePersonCreateMutation = (config: { key?: Key } = {}, options: SWRMutationConfiguration<Mixed, ApiError, Key, FunPersonRequestBody> | undefined = undefined): SWRMutationResponse<Mixed, ApiError, Key, FunPersonRequestBody> => useSWRMutation(config.key ?? pickPersonCreateKey(), (_: Key, { arg }: { arg: FunPersonRequestBody}) => _fetch(pickPersonCreateKey()[0], 'POST', arg), options);
export const personCreate = (config: { body: FunPersonRequestBody }): Promise<Mixed> => _fetch(pickPersonCreateKey()[0], 'POST', config.body);

// POST /api/union
export const pickUnionDetailKey = () => [`/api/union`] as const;
export const useUnionDetailMutation = (config: { key?: Key } = {}, options: SWRMutationConfiguration<FunActivity, ApiError, Key, FunActivity> | undefined = undefined): SWRMutationResponse<FunActivity, ApiError, Key, FunActivity> => useSWRMutation(config.key ?? pickUnionDetailKey(), (_: Key, { arg }: { arg: FunActivity}) => _fetch(pickUnionDetailKey()[0], 'POST', arg), options);
export const unionDetail = (config: { body: FunActivity }): Promise<FunActivity> => _fetch(pickUnionDetailKey()[0], 'POST', config.body);

// POST /api/unions
export const pickUnionListKey = () => [`/api/unions`] as const;
export const useUnionListMutation = (config: { key?: Key } = {}, options: SWRMutationConfiguration<Array<FunActivity>, ApiError, Key, Array<FunActivity>> | undefined = undefined): SWRMutationResponse<Array<FunActivity>, ApiError, Key, Array<FunActivity>> => useSWRMutation(config.key ?? pickUnionListKey(), (_: Key, { arg }: { arg: Array<FunActivity>}) => _fetch(pickUnionListKey()[0], 'POST', arg), options);
export const unionList = (config: { body: Array<FunActivity> }): Promise<Array<FunActivity>> => _fetch(pickUnionListKey()[0], 'POST', config.body);

// GET /api/entity/{id}/{uuid}/{string}/{int}
export const pickEntityKey = (config: { path: { id: Int, uuid: UUID, string: Type, int: EnumNumeric } }) => [`/api/entity/${config.path.id}/${config.path.uuid}/${config.path.string}/${config.path.int}`] as const;
export const useEntityQuery = <T extends SWRConfiguration<Mixed, ApiError>>(config: { path: { id: Int, uuid: UUID, string: Type, int: EnumNumeric } } & { skip?: boolean; key?: Key }, options: T = {} as never) => useSWR<Mixed, ApiError, T>(config.skip !== true ? (config.key ?? pickEntityKey(config)) : null, () => _fetch(_url(pickEntityKey(config))), options);
export const getEntity = (config: { path: { id: Int, uuid: UUID, string: Type, int: EnumNumeric } }): Promise<Mixed> => _fetch(_url(pickEntityKey(config)), 'GET', undefined);

// GET /api/gen-one
export const pickGenericOneKey = () => [`/api/gen-one`] as const;
export const useGenericOneQuery = <T extends SWRConfiguration<FunGenericResponseBody<FunTagResponse>, ApiError>>(config: { skip?: boolean; key?: Key } = {}, options: T = {} as never) => useSWR<FunGenericResponseBody<FunTagResponse>, ApiError, T>(config.skip !== true ? (config.key ?? pickGenericOneKey()) : null, () => _fetch(pickGenericOneKey()[0]), options);
export const getGenericOne = (config: {} = {}): Promise<FunGenericResponseBody<FunTagResponse>> => _fetch(pickGenericOneKey()[0], 'GET', undefined);

// GET /api/gen-two
export const pickGenericTwoKey = () => [`/api/gen-two`] as const;
export const useGenericTwoQuery = <T extends SWRConfiguration<FunGenericResponseBody<Array<FunTagResponse>>, ApiError>>(config: { skip?: boolean; key?: Key } = {}, options: T = {} as never) => useSWR<FunGenericResponseBody<Array<FunTagResponse>>, ApiError, T>(config.skip !== true ? (config.key ?? pickGenericTwoKey()) : null, () => _fetch(pickGenericTwoKey()[0]), options);
export const getGenericTwo = (config: {} = {}): Promise<FunGenericResponseBody<Array<FunTagResponse>>> => _fetch(pickGenericTwoKey()[0], 'GET', undefined);

// GET /api/gen-three
export const pickGenericThreeKey = () => [`/api/gen-three`] as const;
export const useGenericThreeQuery = <T extends SWRConfiguration<FunGenericResponseBody<Bool>, ApiError>>(config: { skip?: boolean; key?: Key } = {}, options: T = {} as never) => useSWR<FunGenericResponseBody<Bool>, ApiError, T>(config.skip !== true ? (config.key ?? pickGenericThreeKey()) : null, () => _fetch(pickGenericThreeKey()[0]), options);
export const getGenericThree = (config: {} = {}): Promise<FunGenericResponseBody<Bool>> => _fetch(pickGenericThreeKey()[0], 'GET', undefined);

// POST /api/mixed
/** @deprecated */
export const pickMixedKey = () => [`/api/mixed`] as const;
/** @deprecated */
export const useMixedMutation = (config: { key?: Key } = {}, options: SWRMutationConfiguration<Mixed, ApiError, Key, Mixed> | undefined = undefined): SWRMutationResponse<Mixed, ApiError, Key, Mixed> => useSWRMutation(config.key ?? pickMixedKey(), (_: Key, { arg }: { arg: Mixed}) => _fetch(pickMixedKey()[0], 'POST', arg), options);
/** @deprecated */
export const mixed = (config: { body: Mixed }): Promise<Mixed> => _fetch(pickMixedKey()[0], 'POST', config.body);

// GET /api/full/{article}
export const pickFullReadKey = (config: { query: FunArticleRequestQuery } & { path: { article: Int } }) => [`/api/full/${config.path.article}`, config.query] as const;
export const useFullReadQuery = <T extends SWRConfiguration<FunArticleResponse, ApiError>>(config: { body: FunArticleRequestBody } & { query: FunArticleRequestQuery } & { path: { article: Int } } & { skip?: boolean; key?: Key }, options: T = {} as never) => useSWR<FunArticleResponse, ApiError, T>(config.skip !== true ? (config.key ?? pickFullReadKey(config)) : null, () => _fetch(_url(pickFullReadKey(config))), options);
export const getFullRead = (config: { body: FunArticleRequestBody } & { query: FunArticleRequestQuery } & { path: { article: Int } }): Promise<FunArticleResponse> => _fetch(_url(pickFullReadKey(config)), 'GET', config.body);

// PATCH /api/full/{article}
export const pickFullWriteKey = (config: { query: FunArticleRequestQuery } & { path: { article: Int } }) => [`/api/full/${config.path.article}`, config.query] as const;
export const useFullWriteMutation = (config: { query: FunArticleRequestQuery } & { path: { article: Int } } & { key?: Key }, options: SWRMutationConfiguration<FunArticleResponse, ApiError, Key, FunArticleRequestBody> | undefined = undefined): SWRMutationResponse<FunArticleResponse, ApiError, Key, FunArticleRequestBody> => useSWRMutation(config.key ?? pickFullWriteKey(config), (_: Key, { arg }: { arg: FunArticleRequestBody}) => _fetch(_url(pickFullWriteKey(config)), 'PATCH', arg), options);
export const fullWrite = (config: { body: FunArticleRequestBody } & { query: FunArticleRequestQuery } & { path: { article: Int } }): Promise<FunArticleResponse> => _fetch(_url(pickFullWriteKey(config)), 'PATCH', config.body);

