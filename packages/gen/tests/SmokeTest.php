<?php

declare(strict_types=1);

namespace UXF\GenTests;

use LogicException;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class SmokeTest extends WebTestCase
{
    public function test(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/doc/app');
        self::assertResponseIsSuccessful();

        $client->request('GET', '/api/doc/app.json');
        self::assertResponseIsSuccessful();

        $application = new Application(self::$kernel ?? throw new LogicException());
        $command = $application->find('uxf:gen');
        $commandTester = new CommandTester($command);

        self::assertSame(0, $commandTester->execute([]));
    }
}
