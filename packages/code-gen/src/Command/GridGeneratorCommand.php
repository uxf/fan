<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UXF\CodeGen\Http\Request\GridRequestBody;

#[AsCommand(name: 'uxf:code-gen:grid', description: 'Generate Grid for entity in Zone')]
class GridGeneratorCommand extends GeneratorBase
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $zoneName = $this->selectZone($input, $output);
        $entityName = $this->selectEntity($zoneName, $input, $output);

        $this->gridService->generate(new GridRequestBody($zoneName, $entityName));

        return 0;
    }
}
