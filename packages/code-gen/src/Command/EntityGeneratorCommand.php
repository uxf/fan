<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use UXF\CodeGen\Http\Request\EntityColumnRequestBody;
use UXF\CodeGen\Http\Request\EntityRequestBody;
use UXF\CodeGen\Service\EntityService;

#[AsCommand(name: 'uxf:code-gen:entity', description: 'Generate Doctrine Entity in given Zone')]
class EntityGeneratorCommand extends GeneratorBase
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $dataTypes = EntityService::DATA_TYPES;

        // Select Zone name
        $zoneName = $this->selectZone($input, $output);

        // Enter entity name
        $helper = $this->getHelper('question');
        assert($helper instanceof QuestionHelper);
        $question = new Question('Please enter a entity name (eg. Profile): ', 'TestGenerator');
        $entityName = $helper->ask($input, $output, $question);

        // Enter entity label
        $question = new Question(
            'Please enter a entity label (eg. "Profil uživatele"): ',
            'Testovací entita',
        );
        $entityLabel = $helper->ask($input, $output, $question);

        // Enter columns
        $columns = [];
        while (true) {
            $output->writeln('');
            $question = new Question('Please enter a column name (eg. name; ENTER = end): ', '');
            $columnName = $helper->ask($input, $output, $question);

            // exit if column name is empty
            if (!(bool) $columnName) {
                break;
            }

            // enter column label
            $question = new Question('Please enter a column label (eg. Jméno): ', $columnName);
            $columnLabel = $helper->ask($input, $output, $question);

            // select column data type
            $helper = $this->getHelper('question');
            assert($helper instanceof QuestionHelper);
            $question = new ChoiceQuestion('Data type: ', array_keys($dataTypes), 0);
            $question->setErrorMessage('Data type %s is invalid.');
            $dbType = $helper->ask($input, $output, $question);

            $question = new ConfirmationQuestion('Column is nullable? (y/n) ', true);
            $nullable = $helper->ask($input, $output, $question);
            $output->writeln(
                "Column name: $columnName Column label: $columnLabel Data type: $dbType Nullable: $nullable",
            );

            // if Many to One, select zone and entity
            $m2oZoneName = null;
            $m2oEntityName = null;
            if ($dbType === 'ManyToOne') {
                // Select Zone name
                $m2oZoneName = $this->selectZone($input, $output);

                // Select entity name
                $m2oEntityName = $this->selectEntity($m2oZoneName, $input, $output);
            }

            $columns[] = new EntityColumnRequestBody(
                $columnName,
                $columnLabel,
                $dbType,
                $nullable,
                $m2oZoneName,
                $m2oEntityName,
            );
        }

        // end of collecting data

        // generate file
        $entityDTO = new EntityRequestBody(
            $zoneName,
            $entityName,
            $entityLabel,
            $columns,
        );

        $this->entityService->generate($entityDTO);

        return 0;
    }
}
