<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use UXF\CodeGen\Http\Request\EntityColumnRequestBody;
use UXF\CodeGen\Http\Request\EntityRequestBody;
use UXF\CodeGen\Http\Request\EnumRequestBody;
use UXF\CodeGen\Http\Request\ZoneRequestBody;
use function Safe\file;

#[AsCommand(name: 'uxf:code-gen:generate-from-csv', description: 'Generate from CSV file')]
class GenerateFromCSVCommand extends GeneratorBase
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Ask for csv file path
        $helper = $this->getHelper('question');
        assert($helper instanceof QuestionHelper);
        $question = new Question('Please enter a file path: ', '');

        $filePath = $helper->ask($input, $output, $question);

        $fileLines = file($filePath);

        $currentZone = null;
        $currentEntity = null;

        $zone = [];

        foreach ($fileLines as $line) {
            $line = str_replace(["\n", "\r"], '', $line);

            $csvCols = explode(',', $line);
            if ($csvCols[0] === '' && $csvCols[1] === '' && $csvCols[2] === '' && $csvCols[3] === '') {
                continue;
            }
            // ignore comments
            if (preg_match('/^\/\//', $csvCols[0]) === 1) {
                continue;
            }

            if ($csvCols[0] !== '') {
                $currentZone = $csvCols[0];
                echo "Current zone: $currentZone\n";

                $zone[$currentZone] = [
                    'name' => $currentZone,
                    'enums' => [],
                    'entities' => [],
                ];
            }
            if ($csvCols[0] === '' && $csvCols[1] !== '') {
                $currentEntity = $csvCols[1];
                $currentEntityLabel = $csvCols[2];
                echo "  Current Entity: $currentEntity - $currentEntityLabel\n";
                $zone[$currentZone]['entities'][$currentEntity] = [
                    'zoneName' => $currentZone,
                    'name' => $currentEntity,
                    'label' => $currentEntityLabel,
                    'columns' => [],
                ];
            }
            if ($csvCols[0] === '' && $csvCols[1] === '' && $csvCols[2] !== '') {
                $name = $csvCols[2];
                $label = $csvCols[3];
                $type = $csvCols[4];
                $nullable = $csvCols[5];
                $m2nZone = $csvCols[6];
                $m2nEntity = $csvCols[7];
                $enumClassName = $csvCols[8];
                $enumItems = $csvCols[9];
                echo "      $name, $type, $label\n";
                $zone[$currentZone]['entities'][$currentEntity]['columns'][] = [
                    'name' => $name,
                    'label' => $label,
                    'type' => $type,
                    'nullable' => $nullable,
                    'm2nZone' => $m2nZone,
                    'm2nEntity' => $m2nEntity,
                    'enumClassName' => $enumClassName,
                    'enumItems' => $enumItems,
                ];
            }
        }

        // create zones
        foreach ($zone as $key => $item) {
            $this->zoneService->generate(
                new ZoneRequestBody(
                    $key,
                    [
                        'Controller',
                        'Entity',
                        'Enum',
                        'Facade',
                        'Http/Response',
                        'Http/Request',
                        'Node',
                        'Repository',
                        'Service',
                    ],
                ),
            );
        }

        // create entities
        foreach ($zone as $zoneKey => $zoneItem) {
            $entities = [];
            foreach ($zoneItem['entities'] as $entityKey => $entityItem) {
                // prepare columns
                $columns = [];
                foreach ($zoneItem['entities'][$entityKey]['columns'] as $columnItem) {
                    if ($columnItem['type'] === 'enum') {
                        $this->enumService->generate(
                            new EnumRequestBody(
                                $zoneKey,
                                $columnItem['enumClassName'],
                                explode(';', $columnItem['enumItems']),
                            ),
                        );
                        $columnItem['type'] = $columnItem['enumClassName'];
                    }

                    $columns[] = new EntityColumnRequestBody(
                        $columnItem['name'],
                        $columnItem['label'],
                        $columnItem['type'],
                        $columnItem['nullable'] === 'nullable',
                        $columnItem['m2nZone'],
                        $columnItem['m2nEntity'],
                    );
                }

                $this->entityService->generate(
                    new EntityRequestBody(
                        $zoneKey,
                        $zoneItem['entities'][$entityKey]['name'],
                        $zoneItem['entities'][$entityKey]['label'],
                        $columns,
                    ),
                );
            }
        }

        return 0;
    }
}
