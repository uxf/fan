<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use Nette\Utils\Strings;
use ReflectionClass;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Routing\Route;
use UXF\CodeGen\Util\FileHelper;
use function Safe\mkdir;

#[AsCommand(name: 'uxf:code-gen:controller-test', description: 'Check Controller coverage')]
class ControllerTestGeneratorCommand extends GeneratorBase
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln(['<comment>Generate controller test', '=========================', '</comment>']);

        $appControllers = GeneratorBase::getAppControllers($this->router); // get all app controllers from router

        $output->writeln([
            '<comment>Please enter controller class full class name (FQN)',
            'e.g. App\ZeteoZone\Controller\Partner\PartnerGetDetailController',
            '</comment>',
        ]);
        $helper = $this->getHelper('question');
        assert($helper instanceof QuestionHelper);
        $question = new Question('Please enter a full controller FQN: ', '');
        $controllerClassName = $helper->ask($input, $output, $question);

        $this->generateTest($controllerClassName, $appControllers, $input, $output);

        return Command::SUCCESS;
    }

    /**
     * @param array<string, Route> $appControllers
     */
    private function generateTest(
        string $controllerClassName,
        array $appControllers,
        InputInterface $input,
        OutputInterface $output,
    ): void {
        /** @var Route $route */
        $route = $appControllers[$controllerClassName];
        $routePath = $route->getPath();
        $routeMethod = $route->getMethods()[0] ?? '';

        $output->writeln('Class: ' . $controllerClassName);
        $output->writeln('  Route details: ');
        $output->writeln("      - path: " . $routePath);
        $output->writeln("      - method: " . $routeMethod);
        $output->writeln("      - requirements: ");
        foreach ($route->getRequirements() as $requirementName => $requirementValue) {
            $output->writeln("          * $requirementName: $requirementValue");
        }

        if (!class_exists($controllerClassName)) {
            throw new RuntimeException("Class does not exist.");
        }
        $reflect = new ReflectionClass($controllerClassName);
        $filename = $reflect->getFileName();
        if ($filename === false) {
            throw new RuntimeException("Can't get filename from class.");
        }

        // find zone name
        $zoneMatches = Strings::matchAll($filename, '/.*\/(\w*Zone)/m', PREG_SET_ORDER);
        $testZoneName = $zoneMatches[0][1];
        // find sub part name
        $subPartMatches = Strings::matchAll($filename, '/.*Controller\/(.*)\//m', PREG_SET_ORDER);
        $testSubPartName = $subPartMatches[0][1] ?? null;
        // find test name
        $nameMatches = Strings::matchAll($filename, '/.*\/(.*)Controller\.php/m', PREG_SET_ORDER);
        $testPartName = $nameMatches[0][1];

        $testClassName = "{$testPartName}StoryTest";
        $testNameSpace = "App\Tests\\$testZoneName\Story" . ($testSubPartName ? "\\$testSubPartName" : null);
        $testFileName = $this->projectDirectory . "/tests/$testZoneName/Story" .
            ($testSubPartName ? "/$testSubPartName" : '') . "/$testClassName.php";
        $testDirName = $this->projectDirectory . "/tests/$testZoneName/Story" .
            ($testSubPartName ? "/$testSubPartName" : '');
        // end of collecting data

        // generate class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace($testNameSpace);
        $namespace->addUse('App\Tests\StoryTestCase');
        $class = $namespace->addClass($testClassName);
        $class->setExtends('\App\Tests\StoryTestCase');

        if ($routeMethod === 'GET') {
            $apiCallLine = "\$client->get('$routePath');";
        } elseif ($routeMethod === 'POST') {
            $apiCallLine = "\$client->post('$routePath', []);";
        } elseif ($routeMethod === 'PUT') {
            $apiCallLine = "\$client->put('$routePath', []);";
        } else {
            throw new RuntimeException('Unknown method');
        }

        // add test method
        $class->addMethod('test')
            ->setPublic()
            ->setReturnType('void')
            ->setBody("\$client = self::createWebClient();
$apiCallLine
self::assertResponseStatusCodeSame(200);
\$this->assertNewSnapshot(\$client->getResponseData(), ['id']);");

        // write file
        $printer = new PsrPrinter();
        $entityOut = $printer->printFile($phpFile);
        if (!file_exists($testDirName)) {
            mkdir($testDirName, 0755, true);
        }
        FileHelper::writeFile($testFileName, $entityOut);
        $output->writeln([
            '<comment>Generated test file:',
            $testFileName,
            '</comment>',
        ]);
    }
}
