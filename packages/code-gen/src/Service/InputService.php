<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use ReflectionClass;
use ReflectionNamedType;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Input;
use UXF\CodeGen\Http\Request\TypeRequestBody;
use UXF\CodeGen\Util\EntityHelper;
use UXF\CodeGen\Util\FileHelper;

use UXF\CodeGen\Util\ParseHelper;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Time;
use function Safe\mkdir;
use function Safe\preg_match;

final readonly class InputService
{
    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(TypeRequestBody $typeRequestBody): void
    {
        $zoneName = $typeRequestBody->zoneName;
        $entityName = $typeRequestBody->entityName;
        $entityData = $this->processEntityData($zoneName, $entityName);
        $className = "{$entityName}Input";

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\GQL\Input");

        $namespace
            ->addUse(DateTime::class)
            ->addUse(Date::class)
            ->addUse(Time::class)
            ->addUse(Field::class)
            ->addUse(Input::class);

        foreach ($entityData as $entityRecord) {
            $returnType = $entityRecord['returnType'];
            $isResponse = $entityRecord['isResponse'];
            if ($isResponse) {
                $namespace->addUse($returnType);
            }
        }

        $class = $namespace
            ->addClass($className)
            ->setReadOnly()
            ->addAttribute(Input::class);
        $class->setFinal();

        // create constructor with promoted parameters
        $constructor = $class
            ->addMethod('__construct')
            ->setPublic();

        foreach ($entityData as $entityRecord) {
            $propertyName = $entityRecord['propertyName'];
            $returnType = $entityRecord['returnType'];
            $returnNullable = $entityRecord['returnNullable'];
            $constructor
                ->addPromotedParameter($propertyName)
                ->setType($returnType)
                ->setNullable($returnNullable)
                ->addAttribute(Field::class);
        }

        // print class to file
        $printer = new PsrPrinter();
        $typeOut = $printer->printFile($phpFile);

        $inputDir = $this->srcDirectory . "{$zoneName}/GQL/Input";
        if (!is_dir($inputDir)) {
            mkdir($inputDir, 0755, true);
        }

        $dstFile = "{$inputDir}/{$entityName}Input.php";
        FileHelper::writeFile($dstFile, $typeOut);
    }

    /**
     * @return array<array{getterName: string, propertyName: string, returnType: string, returnNullable: bool, isResponse: bool}>
     */
    private function processEntityData(string $zoneName, string $entityName): array
    {
        /** @var class-string $className */
        $className = "App\\$zoneName\Entity\\$entityName";

        $rc = new ReflectionClass($className);

        $methods = $rc->getMethods();
        $methods = array_filter($methods, static fn ($method) => str_starts_with($method->getName(), 'get'));
        $entityData = [];
        foreach ($methods as $method) {
            $getterName = $method->getName();
            $propertyName = ParseHelper::convertGetterToPropertyName($getterName);
            $returnType = $method->getReturnType();
            if (!$returnType instanceof ReflectionNamedType) {
                continue;
            }

            if (EntityHelper::supportedReturnType($returnType) === false) {
                continue;
            }

            if ($getterName === 'getId') {
                continue;
            }

            if ($returnType->getName() === 'UXF\Storage\Entity\Image') {
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => "{$propertyName}Uuid",
                    'returnType' => "string",
                    'returnNullable' => $returnType->allowsNull(),
                    'isResponse' => false,
                ];
            } elseif ($returnType->getName() === 'UXF\Storage\Entity\File') {
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => "{$propertyName}Uuid",
                    'returnType' => "string",
                    'returnNullable' => $returnType->allowsNull(),
                    'isResponse' => false,
                ];
            } elseif (preg_match('/\\\Entity\\\/m', $returnType->getName()) === 1) {
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => "{$propertyName}Id",
                    'returnType' => "int",
                    'returnNullable' => $returnType->allowsNull(),
                    'isResponse' => false,
                ];
            } else {
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => $propertyName,
                    'returnType' => $returnType->getName(),
                    'returnNullable' => $returnType->allowsNull(),
                    'isResponse' => false,
                ];
            }
        }

        return $entityData;
    }
}
