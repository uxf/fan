<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use UXF\CodeGen\Http\Request\ZoneRequestBody;

use function Safe\mkdir;
use function Safe\preg_match;
use function Safe\scandir;

final readonly class ZoneService
{
    public function __construct(private string $srcDirectory)
    {
    }

    /**
     * @return string[]
     */
    public static function getAvailableSubDirs(): array
    {
        return [
            'Command',
            'Controller',
            'Entity',
            'Facade',
            'GQL/Type',
            'GQL/Input',
            'GQL/Query',
            'GQL/Mutation',
            'Http/Response',
            'Http/Request',
            'Node',
            'Repository',
            'Service',
            'Util',
        ];
    }

    public function generate(ZoneRequestBody $zoneRequestBody): void
    {
        $zoneName = $zoneRequestBody->zoneName;
        $dirs = $zoneRequestBody->subDirs;

        $this->createDirs($zoneName, $dirs);
    }

    /**
     * @param string[] $dirs
     */
    public function createDirs(string $zoneName, array $dirs): void
    {
        foreach ($dirs as $dir) {
            $newDir = $this->srcDirectory . "$zoneName/$dir";
            if (!is_dir($newDir)) {
                mkdir($newDir, 0755, true);
            }
        }
    }

    /**
     * @return string[]
     */
    public function getZones(): array
    {
        $dirs = scandir($this->srcDirectory);

        $zones = [];
        foreach ($dirs as $dir) {
            if (is_dir($this->srcDirectory . $dir) && preg_match('/^.*Zone$/', $dir) === 1) {
                $zones[] = $dir;
            }
        }

        return $zones;
    }
}
