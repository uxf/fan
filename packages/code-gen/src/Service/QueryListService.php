<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use TheCodingMachine\GraphQLite\Annotations\Query;
use UXF\CodeGen\Http\Request\QueryRequestBody;
use UXF\CodeGen\Util\FileHelper;

use function Safe\mkdir;

final readonly class QueryListService
{
    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(QueryRequestBody $queryRequestBody): void
    {
        $zoneName = $queryRequestBody->zoneName;
        $entityName = $queryRequestBody->entityName;

        // names
        $entityLc = lcfirst($entityName);
        $entityPlural = $entityName . 's';

        $repositoryName = "{$entityName}Repository";
        $repositoryClass = "\App\\$zoneName\Repository\\{$repositoryName}";
        $repositoryProperty = lcfirst($repositoryName);

        $typeName = "{$entityName}Type";

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\GQL\Query");

        $namespace->addUse("App\\$zoneName\Entity\\$entityName")
            ->addUse("App\\$zoneName\Repository\\{$repositoryName}")
            ->addUse("App\\$zoneName\GQL\Type\\{$typeName}")
            ->addUse(Query::class);

        $class = $namespace->addClass("{$entityPlural}Query")->setFinal()->setReadOnly();

        // constructor
        $class->addMethod('__construct')
            ->setPublic()
            ->addPromotedParameter($repositoryProperty)
            ->setType($repositoryClass)
            ->setPrivate();

        // index
        $class->addMethod('__invoke')
            ->setPublic()
            ->setReturnType('array')
            ->setBody("
                 return array_map(
    static fn ({$entityName} \${$entityLc}) => $typeName::createFromEntity(\$$entityLc),
    \$this->{$repositoryProperty}->findAll()
);")
            ->addComment("@return {$typeName}[]")
            ->addAttribute(Query::class, [
                'name' => lcfirst($entityPlural),
            ]);

        // print file
        $printer = new PsrPrinter();
        $queryOut = $printer->printFile($phpFile);

        $queryDir = $this->srcDirectory . "{$zoneName}/GQL/Query";
        if (!is_dir($queryDir)) {
            mkdir($queryDir, 0755, true);
        }

        $dstFile = "{$queryDir}/{$entityPlural}Query.php";
        FileHelper::writeFile($dstFile, $queryOut);
    }
}
