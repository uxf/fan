<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Doctrine\ORM\EntityManagerInterface;
use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use Override;
use ReflectionClass;
use ReflectionNamedType;
use UXF\CMS\DataGrid\CmsColumnTrait;
use UXF\CodeGen\Http\Request\GridRequestBody;
use UXF\CodeGen\Util\EntityHelper;
use UXF\CodeGen\Util\FileHelper;
use UXF\CodeGen\Util\ParseHelper;
use UXF\DataGrid\ColumnTrait;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridSortDir;
use UXF\DataGrid\DataGridType;
use UXF\DataGrid\DataSource\DataSource;
use UXF\DataGrid\DataSource\DoctrineDataSource;
use function Safe\mkdir;

final readonly class GridService
{
    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(GridRequestBody $gridRequestBody): void
    {
        $this->generateColumn();
        $zoneName = $gridRequestBody->zoneName;
        $entityName = $gridRequestBody->entityName;
        $className = "App\\$zoneName\Entity\\$entityName";

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\Grid");

        $namespace->addUse(DataGridBuilder::class)
            ->addUse(DataGridType::class)
            ->addUse(EntityManagerInterface::class)
            ->addUse(Override::class)
            ->addUse(DataGridBuilder::class)
            ->addUse(DataGridSortDir::class)
            ->addUse(DataGridType::class)
            ->addUse(DataSource::class)
            ->addUse(DoctrineDataSource::class)
            ->addUse($className)
            ->addUse("\App\CoreZone\Grid\Column")
        ;

        $class = $namespace->addClass("{$entityName}GridType")
            ->setFinal()
            ->setReadOnly()
            ->addComment("@template-implements DataGridType<{$entityName}>")
            ->addImplement(DataGridType::class);

        $entityNameDS = EntityService::entityNameToAlias($entityName);

        $class->addConstant('Name', $entityNameDS)->setPublic()->setType('string');

        // build constructor
        $class->addMethod('__construct')
            ->setPublic()
            ->addPromotedParameter('entityManager')
            ->setType(EntityManagerInterface::class)
            ->setPrivate();

        // build grid method
        $buildGridMethod = $class->addMethod('buildGrid')
            ->setPublic()
            ->setReturnType('void')
            ->addAttribute(Override::class)
            ->addComment("@inheritDoc -");

        $buildGridMethod = self::processEntityData($zoneName, $entityName, $buildGridMethod);


        $buildGridMethod->addParameter('builder')
            ->setType(DataGridBuilder::class);
        $buildGridMethod->addParameter('options')
            ->setDefaultValue([])
            ->setType('array');

        // getName method
        $getDataSourceMethod = $class->addMethod('getDataSource')
            ->setPublic()
            ->addComment("@inheritDoc -")
            ->addAttribute(Override::class)
            ->setReturnType(DataSource::class)
            ->setBody("
        \$qb = \$this->entityManager->createQueryBuilder()
    ->select('e')
    ->from({$entityName}::class, 'e');
return new DoctrineDataSource(\$qb);
");

        $getDataSourceMethod->addParameter('options')
            ->setDefaultValue([])
            ->setType('array');

        // getName method
        $class->addMethod('getName')
            ->setPublic()
            ->setStatic()
            ->addAttribute(Override::class)
            ->setReturnType('string')
            ->setBody("
                return self::Name;
");

        // print file
        $printer = new PsrPrinter();
        $repositoryOut = $printer->printFile($phpFile);

        $gridDir = $this->srcDirectory . "{$zoneName}/Grid";
        if (!is_dir($gridDir)) {
            mkdir($gridDir, 0755, true);
        }

        $dstFile = "{$gridDir}/{$entityName}GridType.php";
        FileHelper::writeFile($dstFile, $repositoryOut);
    }

    private static function processEntityData(string $zoneName, string $entityName, Method $buildGridMethod): Method
    {
        /** @var class-string $className */
        $className = "App\\$zoneName\Entity\\$entityName";

        $rc = new ReflectionClass($className);
        $methods = $rc->getMethods();
        $methods = array_filter($methods, static fn ($method) => str_starts_with($method->getName(), 'get'));
        foreach ($methods as $method) {
            $getterName = $method->getName();
            $propertyName = ParseHelper::convertGetterToPropertyName($getterName);
            $returnType = $method->getReturnType();
            if (!$returnType instanceof ReflectionNamedType) {
                continue;
            }

            if (EntityHelper::supportedReturnType($returnType) === false) {
                continue;
            }

            if ($getterName === 'getId') {
                $buildGridMethod->addBody("\$builder->setDefaultSort('id', DataGridSortDir::DESC);");
                $buildGridMethod->addBody("\$builder->addColumn(Column::id())->setHidden(true);");
            } elseif ($returnType->getName() === 'string') {
                $buildGridMethod->addBody("\$builder->addColumn(Column::string('{$propertyName}', '{$propertyName}', static fn ({$entityName} \$e) => \$e->{$getterName}()));");
            } elseif ($returnType->getName() === 'int') {
                $buildGridMethod->addBody("\$builder->addColumn(Column::int('{$propertyName}', '{$propertyName}', static fn ({$entityName} \$e) => \$e->{$getterName}()));");
            } elseif ($returnType->getName() === 'UXF\Core\Type\Email') {
                $buildGridMethod->addBody("\$builder->addColumn(Column::email('{$propertyName}', '{$propertyName}', static fn ({$entityName} \$e) => \$e->{$getterName}()));");
            } else {
                echo "unsupported return type: {$returnType->getName()}\n";
            }

            /*
            if ($returnType->getName() === 'UXF\Storage\Entity\Image') {
                // TODO currently not supported
            } elseif ($returnType->getName() === 'UXF\Storage\Entity\File') {
                // TODO currently not supported
            } elseif (preg_match('/\\\Entity\\\/m', $returnType->getName()) === 1) {
                // TODO currently not supported
            } else {
                // TODO currently not supported
            }
            */
        }
        return $buildGridMethod;
    }


    private function generateColumn(): void
    {
        $zoneName = "CoreZone";

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\Grid");

        $namespace->addUse(CmsColumnTrait::class)
            ->addUse(ColumnTrait::class);

        $class = $namespace->addClass("Column")
            ->setFinal()
            ->setReadOnly();
        $class->addTrait(ColumnTrait::class);
        $class->addTrait(CmsColumnTrait::class);

        // print file
        $printer = new PsrPrinter();
        $columnOut = $printer->printFile($phpFile);

        $gridDir = $this->srcDirectory . "{$zoneName}/Grid";
        if (!is_dir($gridDir)) {
            mkdir($gridDir, 0755, true);
        }

        $dstFile = "{$gridDir}/Column.php";
        FileHelper::writeFile($dstFile, $columnOut);
    }
}
