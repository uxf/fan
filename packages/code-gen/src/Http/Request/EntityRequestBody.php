<?php

declare(strict_types=1);

namespace UXF\CodeGen\Http\Request;

final readonly class EntityRequestBody
{
    /**
     * @param EntityColumnRequestBody[] $columns
     */
    public function __construct(
        public string $zoneName,
        public string $entityName,
        public string $entityLabel,
        public array $columns,
    ) {
    }
}
