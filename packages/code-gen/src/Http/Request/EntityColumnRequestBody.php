<?php

declare(strict_types=1);

namespace UXF\CodeGen\Http\Request;

final readonly class EntityColumnRequestBody
{
    public function __construct(
        public string $name,
        public string $label,
        public string $dbType,
        public bool $nullable,
        public ?string $many2oneZoneName = null,
        public ?string $many2oneEntityName = null,
    ) {
    }
}
