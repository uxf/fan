<?php

declare(strict_types=1);

namespace UXF\CodeGen\Util;

use Nette\Utils\Strings;

final readonly class ParseHelper
{
    public static function convertGetterToPropertyName(string $getterName): string
    {
        return lcfirst(Strings::replace($getterName, '/^get/'));
    }

    public static function convertSetterToPropertyName(string $setterName): string
    {
        return lcfirst(Strings::replace($setterName, '/^set/'));
    }

    public static function parseZoneNameFromClassName(string $className): string
    {
        return Strings::matchAll($className, "/.*\\\(\w*Zone)\\\.*/m")[0][1];
    }

    public static function parseEntityFromClassName(string $className): string
    {
        return Strings::matchAll($className, "/.*\\\(\w*)$/m")[0][1];
    }
}
