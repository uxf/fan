<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Mutation;

use TheCodingMachine\GraphQLite\Annotations\Mutation;
use UXF\GQLTests\Project\Input\GoofyInput;

class GoofyMutation
{
    #[Mutation(name: 'goofy')]
    public function __invoke(GoofyInput $goofy): bool
    {
        return true;
    }
}
