<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Input;

use Ramsey\Uuid\UuidInterface;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Input;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\GQL\Type\Json;
use UXF\GQLTests\Project\Type\GoofyEnum;
use UXF\GQLTests\Project\Type\PlutoEnum;

#[Input]
final readonly class GoofyInput
{
    /**
     * @param string[]|null $array
     */
    public function __construct(
        #[Field]
        public ?string $string,
        #[Field]
        public ?array $array,
        #[Field]
        public ?Date $date,
        #[Field]
        public ?DateTime $dateTime,
        #[Field]
        public ?Time $time,
        #[Field]
        public ?Phone $phone,
        #[Field]
        public ?Email $email,
        #[Field]
        public ?NationalIdentificationNumberCze $ninCze,
        #[Field]
        public ?BankAccountNumberCze $banCze,
        #[Field]
        public ?UuidInterface $uuid,
        #[Field]
        public ?Money $money,
        #[Field]
        public ?Decimal $decimal,
        #[Field]
        public ?Url $url,
        #[Field]
        public ?PlutoEnum $enum,
        #[Field]
        public ?GoofyEnum $enumInt,
        #[Field]
        public ?Json $json,
        #[Field(inputType: 'Long')]
        public ?int $long,
    ) {
    }
}
