<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Minnie
{
    public function __construct(
        #[ORM\Id, ORM\Column]
        public int $id,
    ) {
    }
}
