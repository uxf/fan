<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Query;

use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Right;
use UXF\GQLTests\Project\Input\PlutoInput;
use UXF\GQLTests\Project\Type\Goofy;

class GoofyQuery
{
    /**
     * @param PlutoInput[] $plutos
     * @return Goofy[]|null
     */
    #[Logged]
    #[Right('ROLE_ROOT')]
    #[Query(name: 'goofy')]
    public function __invoke(array $plutos): ?array
    {
        return null;
    }
}
