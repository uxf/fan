<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Query;

use TheCodingMachine\GraphQLite\Annotations\Query;
use UXF\GQL\Attribute\Inject;
use UXF\GQLTests\Project\Input\PlutoInput;
use UXF\GQLTests\Project\Staff\Coyote;
use UXF\GQLTests\Project\Type\Pluto;

class PlutoQuery
{
    #[Query(name: 'pluto')]
    public function __invoke(PlutoInput $pluto, #[Inject] Coyote $coyote): Pluto
    {
        return new Pluto(
            string: $pluto->string,
            array: $pluto->array,
            date: $pluto->date,
            dateTime: $pluto->dateTime,
            time: $pluto->time,
            phone: $pluto->phone,
            email: $pluto->email,
            ninCze: $pluto->ninCze,
            banCze: $pluto->banCze,
            uuid: $pluto->uuid,
            money: $pluto->money,
            decimal: $pluto->decimal,
            url: $pluto->url,
            enum: $pluto->enum,
            enumInt: $pluto->enumInt,
            json: $pluto->json,
            long: $pluto->long,
        );
    }
}
