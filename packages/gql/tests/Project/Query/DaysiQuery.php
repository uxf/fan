<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Query;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;
use TheCodingMachine\GraphQLite\Annotations\Query;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\GQL\Attribute\Inject;
use UXF\GQL\Type\Json;
use UXF\GQLTests\Project\Type\GoofyEnum;
use UXF\GQLTests\Project\Type\PlutoEnum;

final readonly class DaysiQuery
{
    /**
     * @param string[] $array
     */
    #[Query(name: 'daysi')]
    public function __invoke(
        #[Inject] Request $request,
        string $string,
        array $array,
        Date $date,
        DateTime $dateTime,
        Time $time,
        Phone $phone,
        Email $email,
        NationalIdentificationNumberCze $ninCze,
        BankAccountNumberCze $banCze,
        UuidInterface $uuid,
        Money $money,
        Decimal $decimal,
        Url $url,
        PlutoEnum $enum,
        GoofyEnum $enumInt,
        Json $json,
    ): bool {
        return true;
    }
}
