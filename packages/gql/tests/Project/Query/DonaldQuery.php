<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Query;

use TheCodingMachine\GraphQLite\Annotations\Query;
use UXF\Core\Attribute\Entity;
use UXF\GQLTests\Project\Entity\Donald as AppDonald;
use UXF\GQLTests\Project\Type\Donald;

class DonaldQuery
{
    /**
     * @param AppDonald[] $donalds
     * @param AppDonald[]|null $donaldsNullable
     * @param AppDonald[]|null $donaldsNullableOptional
     * @param AppDonald[] $donaldsOptional
     */
    #[Query(name: 'donald')]
    public function __invoke(
        #[Entity] AppDonald $donald,
        #[Entity] array $donalds,
        #[Entity] ?AppDonald $donaldNullable,
        #[Entity] ?array $donaldsNullable,
        #[Entity] ?AppDonald $donaldNullableOptional = null,
        #[Entity] ?array $donaldsNullableOptional = null,
        #[Entity] array $donaldsOptional = [],
    ): Donald {
        return new Donald($donald);
    }
}
