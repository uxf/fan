<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project;

class MickeyService
{
    public function get(): string
    {
        return 'mickey mouse';
    }
}
