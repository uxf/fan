<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Type;

use Ramsey\Uuid\UuidInterface;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;

#[Type]
class Goofy
{
    /**
     * @return int[]
     */
    #[Field]
    public function getInts(): array
    {
        return [];
    }

    /**
     * @return Date[]
     */
    #[Field]
    public function getDates(): array
    {
        return [];
    }

    /**
     * @return DateTime[]
     */
    #[Field]
    public function getDateTimes(): array
    {
        return [];
    }

    /**
     * @return Time[]
     */
    #[Field]
    public function getTimes(): array
    {
        return [];
    }

    /**
     * @return Phone[]
     */
    #[Field]
    public function getPhones(): array
    {
        return [];
    }

    /**
     * @return Email[]
     */
    #[Field]
    public function getEmails(): array
    {
        return [];
    }

    /**
     * @return NationalIdentificationNumberCze[]
     */
    #[Field]
    public function getNins(): array
    {
        return [];
    }

    /**
     * @return BankAccountNumberCze[]
     */
    #[Field]
    public function getBans(): array
    {
        return [];
    }

    /**
     * @return UuidInterface[]
     */
    #[Field]
    public function getUuids(): array
    {
        return [];
    }

    /**
     * @return Money[]
     */
    #[Field]
    public function getMoneys(): array
    {
        return [];
    }

    /**
     * @return Decimal[]
     */
    #[Field]
    public function getDecimals(): array
    {
        return [];
    }

    /**
     * @return Url[]
     */
    #[Field]
    public function getUrls(): array
    {
        return [];
    }

    /**
     * @return PlutoEnum[]
     */
    #[Field]
    public function getEnums(): array
    {
        return [];
    }

    /**
     * @return GoofyEnum[]
     */
    #[Field]
    public function getEnumInts(): array
    {
        return [];
    }
}
