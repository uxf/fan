<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Type;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use UXF\GQLTests\Project\Entity\Donald as AppDonald;

#[Type]
final readonly class Donald
{
    public function __construct(private AppDonald $donald)
    {
    }

    #[Field]
    public function getId(): int
    {
        return $this->donald->id;
    }

    #[Field]
    public function getName(): string
    {
        return $this->donald->name;
    }
}
