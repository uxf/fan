<?php

declare(strict_types=1);

namespace UXF\GQLTests\Project\Type;

use Ramsey\Uuid\UuidInterface;
use TheCodingMachine\GraphQLite\Annotations\Autowire;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\GQL\Type\Json;
use UXF\GQLTests\Project\MickeyService;

#[Type]
final readonly class Pluto
{
    /**
     * @param string[] $array
     */
    public function __construct(
        public string $string,
        public array $array,
        public Date $date,
        public DateTime $dateTime,
        public Time $time,
        public Phone $phone,
        public Email $email,
        public NationalIdentificationNumberCze $ninCze,
        public BankAccountNumberCze $banCze,
        public UuidInterface $uuid,
        public Money $money,
        public Decimal $decimal,
        public Url $url,
        public PlutoEnum $enum,
        public GoofyEnum $enumInt,
        public Json $json,
        public int $long,
    ) {
    }

    #[Field]
    public function getString(): string
    {
        return $this->string;
    }

    /**
     * @return string[]
     */
    #[Field]
    public function getArray(): array
    {
        return $this->array;
    }

    #[Field]
    public function getDate(): Date
    {
        return $this->date;
    }

    #[Field]
    public function getDateTime(): DateTime
    {
        return $this->dateTime;
    }

    #[Field]
    public function getTime(): Time
    {
        return $this->time;
    }

    #[Field]
    public function getPhone(): Phone
    {
        return $this->phone;
    }

    #[Field]
    public function getEmail(): Email
    {
        return $this->email;
    }

    #[Field]
    public function getNinCze(): NationalIdentificationNumberCze
    {
        return $this->ninCze;
    }

    #[Field]
    public function getBanCze(): BankAccountNumberCze
    {
        return $this->banCze;
    }

    #[Field]
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    #[Field]
    public function getMoney(): Money
    {
        return $this->money;
    }

    #[Field]
    public function getDecimal(): Decimal
    {
        return $this->decimal;
    }

    #[Field]
    public function getUrl(): Url
    {
        return $this->url;
    }

    #[Field]
    public function getEnum(): PlutoEnum
    {
        return $this->enum;
    }

    #[Field]
    public function getEnumInt(): GoofyEnum
    {
        return $this->enumInt;
    }

    #[Field]
    public function getJson(): Json
    {
        return $this->json;
    }

    #[Field(outputType: 'Long!')]
    public function getLong(): int
    {
        return $this->long;
    }

    /**
     * @return Goofy[]
     */
    #[Field]
    public function children(): array
    {
        return [];
    }

    /**
     * @return string[]|null
     */
    #[Field]
    public function nodes(#[Autowire] MickeyService $mickeyService): ?array
    {
        $tmp = $mickeyService->get();
        return $tmp !== '' ? [$mickeyService->get()] : null;
    }
}
