<?php

declare(strict_types=1);

namespace UXF\GQLTests\Functional;

use stdClass;
use UXF\Core\Test\Client;
use UXF\Core\Test\WebTestCase;
use UXF\GQLTests\Project\Type\GoofyEnum;
use UXF\GQLTests\Project\Type\PlutoEnum;

class SmokeTest extends WebTestCase
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = self::createClient();
    }

    public function test(): void
    {
        $this->gql(__DIR__ . '/query/Query.graphql', [
            'json' => [
                'hello' => [
                    'myFriend' => [1, 2, 3],
                ],
            ],
        ], operationName: 'Query');

        $this->gql(__DIR__ . '/query/Login.graphql');
        self::assertResponseHeaderSame('Auth-Token', '1');

        $this->gql(__DIR__ . '/query/Mutation.graphql', [
            'json' => [
                'hello' => [
                    'myFriend' => [1, 2, 3],
                ],
            ],
        ], true);

        $this->gql(__DIR__ . '/query/GoofyQuery.graphql', [], true);

        $this->gql(__DIR__ . '/query/DonaldQuery.graphql', [
            'id' => 1,
            'ids' => [1],
        ]);

        $this->gql(__DIR__ . '/query/DonaldQuery.graphql', [
            'id' => 2,
            'ids' => [2],
        ]);

        $this->gql(__DIR__ . '/query/GoofyMutation.graphql');

        $this->gql(__DIR__ . '/query/GoofyMutation2.graphql', [
            'goofy' => [],
        ]);

        $this->gql(__DIR__ . '/query/MickeyQuery.graphql', [
            'uuid' => '00000000-0000-0000-0000-000000000001',
            'minnie' => 2,
            'enum' => PlutoEnum::OK_WOW,
            'enumInt' => GoofyEnum::FIRST->name,
            'names' => ['WOW!'],
        ]);

        $this->gql(__DIR__ . '/query/DaisyQuery.graphql', [
            'string' => 'x',
            'array' => ['x'],
            'date' => '2020-01-02',
            'dateTime' => '2020-01-02T10:00:00',
            'time' => '10:00:00',
            'phone' => '777666777',
            'email' => 'info@uxf.cz',
            'ninCze' => '930201/3545',
            'banCze' => '123/0300',
            'uuid' => '00000000-0000-0000-0000-000000000001',
            'money' => [
                'amount' => '1',
                'currency' => 'CZK',
            ],
            'decimal' => '3.14',
            'url' => 'https://uxf.cz',
            'enum' => 'OK',
            'enumInt' => 'FIRST',
            'json' => new stdClass(),
        ]);
    }

    public function testInvalid(): void
    {
        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(date: '2010-01-50'),
        ]);

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(dateTime: '2010-01-50'),
        ]);

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(time: '25:00:00'),
        ]);

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(phone: 'xx'),
        ]);

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(uuid: 'b477e7cd-5b6f-46e5-aeda-a88467e9c7f'),
        ]);

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(money: [
                'amount' => '1,0',
                'currency' => 'CZK',
            ]),
        ]);

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(enum: 'XX'),
        ]);

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(long: 'XX'),
        ]);

        $this->gql(__DIR__ . '/query/Daisy2Query.graphql');

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(ninCze: 'xx'),
        ]);

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(banCze: 'xx'),
        ]);

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(email: 'xx'),
        ]);

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(decimal: 'xx'),
        ]);

        $this->gql(__DIR__ . '/query/InvalidInputQuery.graphql', [
            'pluto' => self::createPlutoInput(url: 'xx'),
        ]);
    }

    public function testJson(): void
    {
        $this->gql(__DIR__ . '/query/JsonQuery.graphql', [
            'json' => 1,
        ]);

        $this->gql(__DIR__ . '/query/JsonQuery.graphql', [
            'json' => [],
        ]);

        $this->gql(__DIR__ . '/query/JsonQuery.graphql', [
            'json' => new stdClass(),
        ]);

        $this->gql(__DIR__ . '/query/JsonQuery.graphql', [
            'json' => "hello",
        ]);

        $this->gql(__DIR__ . '/query/JsonQuery.graphql', [
            'json' => '{vrrr}',
        ]);
    }

    /**
     * @return array<string, mixed>
     */
    private static function createPlutoInput(
        mixed $string = 'hello',
        mixed $array = ['kitty'],
        mixed $date = '2010-01-28',
        mixed $dateTime = '2010-01-28T11:27:25+01:00',
        mixed $time = '15:30:50',
        mixed $phone = '+420777666777',
        mixed $email = 'info@uxf.cz',
        mixed $ninCze = '930201/3545',
        mixed $banCze = '123/0300',
        mixed $uuid = 'b477e7cd-5b6f-46e5-aeda-a88467e9c7fc',
        mixed $money = [
            'amount' => '123.45',
            'currency' => 'CZK',
        ],
        mixed $decimal = '3.14',
        mixed $url = 'https://uxf.cz',
        mixed $enum = 'OK',
        mixed $enumInt = 'FIRST',
        mixed $json = [],
        mixed $long = 5147483647,
    ): array {
        return [
            'string' => $string,
            'array' => $array,
            'date' => $date,
            'dateTime' => $dateTime,
            'time' => $time,
            'phone' => $phone,
            'email' => $email,
            'ninCze' => $ninCze,
            'banCze' => $banCze,
            'uuid' => $uuid,
            'money' => $money,
            'decimal' => $decimal,
            'url' => $url,
            'enum' => $enum,
            'enumInt' => $enumInt,
            'json' => $json,
            'long' => $long,
        ];
    }

    /**
     * @param array<mixed> $variables
     */
    private function gql(string $queryPath, array $variables = [], bool $logged = false, ?string $operationName = null): void
    {
        $headers = [];
        if ($logged) {
            $headers['HTTP_AUTH_TOKEN'] = 'testUser';
        }

        $gql = file_get_contents($queryPath);

        $this->client->post('/graphql', [
            'query' => $gql,
            'variables' => $variables,
            'operationName' => $operationName,
        ], $headers);
        self::assertResponseIsSuccessful();

        $this->assertSnapshot($this->client->getResponseData());
    }
}
