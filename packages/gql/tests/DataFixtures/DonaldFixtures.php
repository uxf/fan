<?php

declare(strict_types=1);

namespace UXF\GQLTests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use UXF\GQLTests\Project\Entity\Donald;
use UXF\GQLTests\Project\Entity\Minnie;
use UXF\GQLTests\Project\Type\GoofyEnum;
use UXF\GQLTests\Project\Type\PlutoEnum;

class DonaldFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $minnie = new Minnie(2);
        $manager->persist($minnie);
        $donald = new Donald(1, Uuid::fromInteger('1'), 'WOW!', $minnie, PlutoEnum::OK_WOW, GoofyEnum::FIRST);
        $manager->persist($donald);
        $manager->flush();
    }
}
