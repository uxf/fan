<?php

declare(strict_types=1);

use GraphQL\Server\ServerConfig;
use Psr\Container\ContainerInterface;
use Symfony\Component\Cache\Psr16Cache;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TheCodingMachine\GraphQLite\AggregateControllerQueryProviderFactory;
use TheCodingMachine\GraphQLite\Mappers\StaticClassListTypeMapperFactory;
use TheCodingMachine\GraphQLite\Mappers\StaticTypeMapper;
use TheCodingMachine\GraphQLite\Schema;
use TheCodingMachine\GraphQLite\SchemaFactory;
use TheCodingMachine\GraphQLite\Security\AuthenticationServiceInterface;
use TheCodingMachine\GraphQLite\Security\AuthorizationServiceInterface;
use UXF\GQL\Command\GenCommand;
use UXF\GQL\Controller\GQLController;
use UXF\GQL\Error\DefaultErrorHandler;
use UXF\GQL\Error\ErrorHandler;
use UXF\GQL\EventSubscriber\ProfilerEventSubscriber;
use UXF\GQL\EventSubscriber\SentryTracingOperationEventSubscriber;
use UXF\GQL\Factory\MoneyFactory;
use UXF\GQL\Mapper\EnumTypeMapperFactory;
use UXF\GQL\Mapper\Parameter\EntityParameterHandler;
use UXF\GQL\Mapper\Parameter\InjectParameterHandler;
use UXF\GQL\Mapper\UXFCoreTypeMapperFactory;
use UXF\GQL\Security\GQLAuthenticationService;
use UXF\GQL\Security\GQLAuthorizationService;
use UXF\GQL\Service\Injector\RequestInjector;
use UXF\GQL\Service\ResponseCallbackModifier;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $services->alias(ContainerInterface::class, 'service_container');

    $services->set(GQLController::class)->public();
    $services->set(GenCommand::class)
        ->arg('$destinations', '%uxf_gql.destinations%')
        ->public();

    $services->set(ErrorHandler::class, DefaultErrorHandler::class)
        ->arg('$debug', '%kernel.debug%');

    $services->set('uxf_gql.cache', Psr16Cache::class);

    // entity + inject parameter middleware
    $services->set(EntityParameterHandler::class);
    $services->set(InjectParameterHandler::class)
        ->arg('$map', '%uxf_gql.injected%');

    $services->set(AggregateControllerQueryProviderFactory::class);
    $services->set(StaticClassListTypeMapperFactory::class);

    $services->set(EnumTypeMapperFactory::class);
    $services->set(UXFCoreTypeMapperFactory::class);
    $services->set(MoneyFactory::class)->autowire(false)->public();

    // fix package bug
    $services->set('uxf_gql.money_mapper', StaticTypeMapper::class)
        ->factory([MoneyFactory::class, 'register'])
        ->arg('$allowMoney', param('uxf_gql.allow_money'));

    // security
    $services->set(AuthenticationServiceInterface::class, GQLAuthenticationService::class);
    $services->set(AuthorizationServiceInterface::class, GQLAuthorizationService::class);

    // injectors
    $services->set(RequestInjector::class)->public();

    // sentry
    $services->set(SentryTracingOperationEventSubscriber::class);

    // profiler
    if ($containerConfigurator->env() === 'dev') {
        $services->set(ProfilerEventSubscriber::class);
    }

    $services->set(SchemaFactory::class)
        ->arg('$cache', service('uxf_gql.cache'))
        ->call('addParameterMiddleware', [service(EntityParameterHandler::class)])
        ->call('addParameterMiddleware', [service(InjectParameterHandler::class)])
        ->call('addQueryProviderFactory', [service(AggregateControllerQueryProviderFactory::class)])
        ->call('addTypeMapperFactory', [service(StaticClassListTypeMapperFactory::class)])
        ->call('addRootTypeMapperFactory', [service(EnumTypeMapperFactory::class)])
        ->call('addRootTypeMapperFactory', [service(UXFCoreTypeMapperFactory::class)])
        ->call('setAuthenticationService', [service(AuthenticationServiceInterface::class)])
        ->call('setAuthorizationService', [service(AuthorizationServiceInterface::class)])
        ->call('addTypeMapper', [service('uxf_gql.money_mapper')]);

    $services->set(Schema::class)
        ->factory([service(SchemaFactory::class), 'createSchema']);

    $services->set(ServerConfig::class)
        ->call('setSchema', [service(Schema::class)])
        ->call('setDebugFlag', [param('uxf_gql.debug_flag')])
        ->call('setErrorsHandler', [service(ErrorHandler::class)]);

    // own clever functions
    $services->set(ResponseCallbackModifier::class);
};
