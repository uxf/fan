<?php

declare(strict_types=1);

namespace UXF\GQL;

use GraphQL\Error\DebugFlag;
use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use UXF\GQL\DependencyInjection\ProfilerCompilerPass;
use UXF\GQL\DependencyInjection\SchemaCompilerPass;
use UXF\GQL\Service\Injector\RequestInjector;

final class UXFGqlBundle extends AbstractBundle
{
    protected string $extensionAlias = 'uxf_gql';

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
                ->arrayNode('sources')
                    ->isRequired()
                    ->scalarPrototype()
                    ->end()
                ->end()
                ->arrayNode('injected')
                    ->scalarPrototype()
                    ->end()
                ->end()
            ->integerNode('debug_flag')->defaultValue(DebugFlag::INCLUDE_DEBUG_MESSAGE)->end()
            ->arrayNode('destination')
                ->beforeNormalization()
                    ->ifString()
                    ->then(fn (string $v) => [$v])
                ->end()
                ->scalarPrototype()->isRequired()->end()
            ->end()
            ->booleanNode('register_psr_17')->defaultTrue()->end()
            ->booleanNode('allow_money')->defaultFalse()->end();
    }

    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import(__DIR__ . '/../config/services.php');

        $parameters = $container->parameters();
        $parameters->set('uxf_gql.sources', $config['sources']);
        $parameters->set('uxf_gql.destinations', $config['destination']);
        $parameters->set('uxf_gql.debug_flag', $config['debug_flag']);
        $parameters->set('uxf_gql.allow_money', $config['allow_money']);

        $injected = $config['injected'] ?? [];
        $injected[Request::class] = RequestInjector::class;
        $parameters->set('uxf_gql.injected', $injected);

        if ($config['register_psr_17']) {
            $container->import(__DIR__ . '/../config/services_psr17.php');
        }
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new SchemaCompilerPass());
        $container->addCompilerPass(new ProfilerCompilerPass());
    }
}
