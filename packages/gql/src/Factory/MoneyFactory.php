<?php

declare(strict_types=1);

namespace UXF\GQL\Factory;

use Exception;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use TheCodingMachine\GraphQLite\Annotations\Factory;
use TheCodingMachine\GraphQLite\Mappers\Proxys\MutableObjectTypeAdapter;
use TheCodingMachine\GraphQLite\Mappers\StaticTypeMapper;
use UXF\Core\Type\Currency;
use UXF\Core\Type\Money;
use UXF\GQL\Mapper\EnumTypeMapper;

class MoneyFactory
{
    #[Factory]
    public function createMoney(string $amount, Currency $currency): Money
    {
        try {
            return Money::of($amount, $currency);
        } catch (Exception) {
            throw new UserError('Invalid money format');
        }
    }

    public static function register(bool $allowMoney): StaticTypeMapper
    {
        if (!$allowMoney) {
            return new StaticTypeMapper();
        }

        // TODO drop EnumTypeMapper dependency
        $type = new MutableObjectTypeAdapter(new ObjectType([
            'name' => 'Money',
            'fields' => [
                'amount' => Type::nonNull(Type::string()),
                'currency' => Type::nonNull(EnumTypeMapper::register(Currency::class)),
            ],
        ]));
        $type->description = null;

        $mapper = new StaticTypeMapper([
            Money::class => $type,
        ]);

        // BC with thecodingmachine/graphqlite 6.2
        // @phpstan-ignore-next-line
        if (method_exists($mapper, 'setTypes')) {
            $mapper->setTypes([
                Money::class => $type,
            ]);
        }

        return $mapper;
    }
}
