<?php

declare(strict_types=1);

namespace UXF\GQL\Mapper;

use GraphQL\Type\Definition\InputType;
use GraphQL\Type\Definition\NamedType;
use GraphQL\Type\Definition\OutputType;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Type\Definition\Type as GraphQLType;
use phpDocumentor\Reflection\DocBlock;
use phpDocumentor\Reflection\Type;
use phpDocumentor\Reflection\Types\Object_;
use Ramsey\Uuid\UuidInterface;
use ReflectionMethod;
use ReflectionProperty;
use TheCodingMachine\GraphQLite\Mappers\Root\RootTypeMapperInterface;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\GQL\Type\Definition\BanCzeType;
use UXF\GQL\Type\Definition\DateTimeType;
use UXF\GQL\Type\Definition\DateType;
use UXF\GQL\Type\Definition\DecimalType;
use UXF\GQL\Type\Definition\EmailType;
use UXF\GQL\Type\Definition\JsonType;
use UXF\GQL\Type\Definition\LongType;
use UXF\GQL\Type\Definition\NinCzeType;
use UXF\GQL\Type\Definition\PhoneType;
use UXF\GQL\Type\Definition\TimeType;
use UXF\GQL\Type\Definition\UrlType;
use UXF\GQL\Type\Definition\UuidType;
use UXF\GQL\Type\Json;
use UXF\GraphQL\Type\Json as NewJson;

final class UXFCoreTypeMapper implements RootTypeMapperInterface
{
    private static ?LongType $longType = null;
    private static ?DateTimeType $dateTimeType = null;
    private static ?DateType $dateType = null;
    private static ?DecimalType $decimalType = null;
    private static ?UrlType $urlType = null;
    private static ?TimeType $timeType = null;
    private static ?PhoneType $phoneType = null;
    private static ?EmailType $emailType = null;
    private static ?NinCzeType $ninCzeType = null;
    private static ?BanCzeType $banCzeType = null;
    private static ?UuidType $uuidType = null;
    private static ?JsonType $jsonType = null;

    public function __construct(
        private readonly RootTypeMapperInterface $next,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function toGraphQLOutputType(Type $type, ?OutputType $subType, ReflectionMethod|ReflectionProperty $reflector, DocBlock $docBlockObj): OutputType&GraphQLType
    {
        return $this->map($type) ?? $this->next->toGraphQLOutputType($type, $subType, $reflector, $docBlockObj);
    }

    /**
     * @inheritDoc
     */
    public function toGraphQLInputType(Type $type, ?InputType $subType, string $argumentName, ReflectionMethod|ReflectionProperty $reflector, DocBlock $docBlockObj): InputType&GraphQLType
    {
        return $this->map($type) ?? $this->next->toGraphQLInputType($type, $subType, $argumentName, $reflector, $docBlockObj);
    }

    public function mapNameToType(string $typeName): NamedType&GraphQLType
    {
        return match ($typeName) {
            'Long' => self::getLongType(),
            'DateTime' => self::getDateTimeType(),
            'Date' => self::getDateType(),
            'Decimal' => self::getDecimalType(),
            'Url' => self::getUrlType(),
            'Time' => self::getTimeType(),
            'Phone' => self::getPhoneType(),
            'Email' => self::getEmailType(),
            'NinCze' => self::getNinCzeType(),
            'BanCze' => self::getBanCzeType(),
            'UUID' => self::getUuidType(),
            'JSON' => self::getJsonType(),
            default => $this->next->mapNameToType($typeName),
        };
    }

    private function map(Type $type): ?ScalarType
    {
        if (!$type instanceof Object_) {
            return null;
        }

        $class = trim((string) $type->getFqsen(), '\\');

        return match ($class) {
            DateTime::class => self::getDateTimeType(),
            Date::class => self::getDateType(),
            Decimal::class => self::getDecimalType(),
            Url::class => self::getUrlType(),
            Time::class => self::getTimeType(),
            Phone::class => self::getPhoneType(),
            Email::class => self::getEmailType(),
            NationalIdentificationNumberCze::class => self::getNinCzeType(),
            BankAccountNumberCze::class => self::getBanCzeType(),
            UuidInterface::class => self::getUuidType(),
            Json::class, NewJson::class => self::getJsonType(),
            default => null,
        };
    }

    private static function getLongType(): LongType
    {
        return self::$longType ??= new LongType();
    }

    private static function getDateTimeType(): DateTimeType
    {
        return self::$dateTimeType ??= new DateTimeType();
    }

    private static function getDateType(): DateType
    {
        return self::$dateType ??= new DateType();
    }

    private static function getDecimalType(): DecimalType
    {
        return self::$decimalType ??= new DecimalType();
    }

    private static function getUrlType(): UrlType
    {
        return self::$urlType ??= new UrlType();
    }

    private static function getTimeType(): TimeType
    {
        return self::$timeType ??= new TimeType();
    }

    private static function getPhoneType(): PhoneType
    {
        return self::$phoneType ??= new PhoneType();
    }

    private static function getEmailType(): EmailType
    {
        return self::$emailType ??= new EmailType();
    }

    private static function getNinCzeType(): NinCzeType
    {
        return self::$ninCzeType ??= new NinCzeType();
    }

    private static function getBanCzeType(): BanCzeType
    {
        return self::$banCzeType ??= new BanCzeType();
    }

    public static function getUuidType(): UuidType
    {
        return self::$uuidType ??= new UuidType();
    }

    private static function getJsonType(): JsonType
    {
        return self::$jsonType ??= new JsonType();
    }
}
