<?php

declare(strict_types=1);

namespace UXF\GQL\Mapper;

use BackedEnum;
use GraphQL\Type\Definition\InputType;
use GraphQL\Type\Definition\NamedType;
use GraphQL\Type\Definition\OutputType;
use GraphQL\Type\Definition\Type as GraphQLType;
use Nette\Utils\Strings;
use phpDocumentor\Reflection\DocBlock;
use phpDocumentor\Reflection\Type;
use phpDocumentor\Reflection\Types\Object_;
use ReflectionMethod;
use ReflectionProperty;
use TheCodingMachine\GraphQLite\Mappers\Root\RootTypeMapperInterface;
use UXF\GQL\Type\Definition\EnumType;

final class EnumTypeMapper implements RootTypeMapperInterface
{
    /** @var array<string, EnumType> - indexed by gql type */
    private static array $types = [];

    /**
     * @param array<string, class-string<BackedEnum>> $typeMap - [gqlType => className]
     */
    public function __construct(
        private readonly RootTypeMapperInterface $next,
        private readonly array $typeMap,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function toGraphQLOutputType(Type $type, ?OutputType $subType, ReflectionMethod|ReflectionProperty $reflector, DocBlock $docBlockObj): OutputType&GraphQLType
    {
        return $this->map($type) ?? $this->next->toGraphQLOutputType($type, $subType, $reflector, $docBlockObj);
    }

    /**
     * @inheritDoc
     */
    public function toGraphQLInputType(Type $type, ?InputType $subType, string $argumentName, ReflectionMethod|ReflectionProperty $reflector, DocBlock $docBlockObj): InputType&GraphQLType
    {
        return $this->map($type) ?? $this->next->toGraphQLInputType($type, $subType, $argumentName, $reflector, $docBlockObj);
    }

    public function mapNameToType(string $typeName): NamedType&GraphQLType
    {
        $class = $this->typeMap[$typeName] ?? null;

        if ($class !== null) {
            return self::$types[$typeName] ??= new EnumType($typeName, $class);
        }

        return $this->next->mapNameToType($typeName);
    }

    private function map(Type $type): ?EnumType
    {
        if (!$type instanceof Object_) {
            return null;
        }

        $class = trim((string) $type->getFqsen(), '\\');
        if (is_a($class, BackedEnum::class, true)) {
            return self::register($class);
        }

        return null;
    }

    /**
     * @param class-string<BackedEnum> $phpTypeName
     */
    public static function register(string $phpTypeName): EnumType
    {
        $typeName = Strings::replace($phpTypeName, '/.*\\\\/');
        return self::$types[$typeName] ??= new EnumType($typeName, $phpTypeName);
    }
}
