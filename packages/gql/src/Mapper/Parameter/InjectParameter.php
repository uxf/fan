<?php

declare(strict_types=1);

namespace UXF\GQL\Mapper\Parameter;

use GraphQL\Type\Definition\ResolveInfo;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use TheCodingMachine\GraphQLite\Parameters\ParameterInterface;
use UXF\GQL\Service\Injector\InjectedArgument;

final readonly class InjectParameter implements ParameterInterface
{
    public function __construct(
        private ContainerInterface $container,
        private string $service,
        private InjectedArgument $argument,
        private Request $request,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function resolve(?object $source, array $args, mixed $context, ResolveInfo $info): ?object
    {
        return $this->container->get($this->service)($this->request, $this->argument);
    }
}
