<?php

declare(strict_types=1);

namespace UXF\GQL\Mapper\Parameter;

use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\InputType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\Type as GraphQLType;
use TheCodingMachine\GraphQLite\Parameters\InputTypeParameterInterface;

final readonly class EntityParameter implements InputTypeParameterInterface
{
    /**
     * @phpstan-param class-string $className
     */
    public function __construct(
        private EntityManagerInterface $entityManager,
        private string $className,
        private string $name,
        private bool $nullable,
        private bool $hasDefaultValue,
        private string $entityPropertyName,
        private ScalarType|EnumType $entityPropertyType,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function resolve(?object $source, array $args, mixed $context, ResolveInfo $info): ?object
    {
        $id = $args[$this->name] ?? null;
        if ($id === null) {
            return null;
        }

        return $this->entityManager->getRepository($this->className)->findOneBy([
            $this->entityPropertyName => $id,
        ]) ?? throw new UserError("$this->name:$id not found.");
    }

    public function getType(): InputType&GraphQLType
    {
        return $this->nullable ? $this->entityPropertyType : Type::nonNull($this->entityPropertyType);
    }

    public function hasDefaultValue(): bool
    {
        return $this->hasDefaultValue;
    }

    public function getDefaultValue(): mixed
    {
        return null;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return '';
    }
}
