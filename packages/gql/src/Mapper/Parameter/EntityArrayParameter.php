<?php

declare(strict_types=1);

namespace UXF\GQL\Mapper\Parameter;

use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\InputType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\Type as GraphQLType;
use TheCodingMachine\GraphQLite\Parameters\InputTypeParameterInterface;

final readonly class EntityArrayParameter implements InputTypeParameterInterface
{
    /**
     * @phpstan-param class-string $className
     */
    public function __construct(
        private EntityManagerInterface $entityManager,
        private string $className,
        private string $name,
        private bool $nullable,
        private bool $hasDefaultValue,
        private mixed $defaultValue,
        private string $entityPropertyName,
        private ScalarType|EnumType $entityPropertyType,
    ) {
    }

    /**
     * @inheritDoc
     * @phpstan-return object[]
     */
    public function resolve(?object $source, array $args, mixed $context, ResolveInfo $info): ?array
    {
        $ids = $args[$this->name];
        if ($ids === null) {
            return null;
        }

        return $this->entityManager->getRepository($this->className)->findBy([
            $this->entityPropertyName => $ids,
        ]);
    }

    public function getType(): InputType&GraphQLType
    {
        $type = Type::listOf(Type::nonNull($this->entityPropertyType));
        return $this->nullable ? $type : Type::nonNull($type);
    }

    public function hasDefaultValue(): bool
    {
        return $this->hasDefaultValue;
    }

    public function getDefaultValue(): mixed
    {
        return $this->defaultValue;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return '';
    }
}
