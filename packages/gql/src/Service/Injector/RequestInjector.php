<?php

declare(strict_types=1);

namespace UXF\GQL\Service\Injector;

use Symfony\Component\HttpFoundation\Request;

final readonly class RequestInjector
{
    public function __invoke(Request $request): Request
    {
        return $request;
    }
}
