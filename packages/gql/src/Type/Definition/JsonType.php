<?php

declare(strict_types=1);

namespace UXF\GQL\Type\Definition;

use GraphQL\Error\Error;
use GraphQL\Type\Definition\ScalarType;
use UXF\GQL\Type\Json;

final class JsonType extends ScalarType
{
    public const string NAME = 'JSON';

    public function __construct()
    {
        parent::__construct([
            'name' => self::NAME,
        ]);
    }

    public function serialize(mixed $value): mixed
    {
        return $value;
    }

    public function parseValue(mixed $value): ?Json
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof Json) {
            return $value;
        }

        return new Json($value);
    }

    /**
     * @inheritDoc
     */
    public function parseLiteral($valueNode, ?array $variables = null): mixed
    {
        throw new Error("JSON in query definition is not supported. Please use variables");
    }
}
