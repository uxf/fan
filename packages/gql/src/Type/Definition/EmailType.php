<?php

declare(strict_types=1);

namespace UXF\GQL\Type\Definition;

use Exception;
use GraphQL\Error\InvariantViolation;
use GraphQL\Error\UserError;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;
use TheCodingMachine\GraphQLite\GraphQLRuntimeException;
use UXF\Core\Type\Email;

final class EmailType extends ScalarType
{
    public const string NAME = 'Email';

    public function __construct()
    {
        parent::__construct([
            'name' => self::NAME,
        ]);
    }

    public function serialize(mixed $value): string
    {
        if (!$value instanceof Email) {
            throw new InvariantViolation('Email is not an instance of Email: ' . Utils::printSafe($value));
        }

        return $value->__toString();
    }

    public function parseValue(mixed $value): ?Email
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof Email) {
            return $value;
        }

        try {
            return Email::of($value);
        } catch (Exception) {
            throw new UserError('Invalid email format');
        }
    }

    /**
     * @inheritDoc
     */
    public function parseLiteral($valueNode, ?array $variables = null): mixed
    {
        if ($valueNode instanceof StringValueNode) {
            return $valueNode->value;
        }

        throw new GraphQLRuntimeException();
    }
}
