<?php

declare(strict_types=1);

namespace UXF\GQL\Type\Definition;

use Exception;
use GraphQL\Error\InvariantViolation;
use GraphQL\Error\UserError;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;
use TheCodingMachine\GraphQLite\GraphQLRuntimeException;
use UXF\Core\Type\Phone;

final class PhoneType extends ScalarType
{
    public const string NAME = 'Phone';

    public function __construct()
    {
        parent::__construct([
            'name' => self::NAME,
        ]);
    }

    public function serialize(mixed $value): string
    {
        if (!$value instanceof Phone) {
            throw new InvariantViolation('Phone is not an instance of Phone: ' . Utils::printSafe($value));
        }

        return $value->__toString();
    }

    public function parseValue(mixed $value): ?Phone
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof Phone) {
            return $value;
        }

        try {
            return Phone::of($value);
        } catch (Exception) {
            throw new UserError('Invalid phone number format');
        }
    }

    /**
     * @inheritDoc
     */
    public function parseLiteral($valueNode, ?array $variables = null): mixed
    {
        if ($valueNode instanceof StringValueNode) {
            return $valueNode->value;
        }

        throw new GraphQLRuntimeException();
    }
}
