<?php

declare(strict_types=1);

namespace UXF\GQL\Type\Definition;

use Exception;
use GraphQL\Error\InvariantViolation;
use GraphQL\Error\UserError;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;
use TheCodingMachine\GraphQLite\GraphQLRuntimeException;
use UXF\Core\Type\DateTime;

final class DateTimeType extends ScalarType
{
    public const string NAME = 'DateTime';

    public function __construct()
    {
        parent::__construct([
            'name' => self::NAME,
        ]);
    }

    public function serialize(mixed $value): string
    {
        if (!$value instanceof DateTime) {
            throw new InvariantViolation('DateTime is not an instance of DateTime: ' . Utils::printSafe($value));
        }

        return $value->format(DateTime::ATOM);
    }

    public function parseValue(mixed $value): ?DateTime
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof DateTime) {
            return $value;
        }

        try {
            return (new DateTime($value))->normalizeTz();
        } catch (Exception) {
            throw new UserError('Invalid date-time format');
        }
    }

    /**
     * @inheritDoc
     */
    public function parseLiteral($valueNode, ?array $variables = null): mixed
    {
        if ($valueNode instanceof StringValueNode) {
            return $valueNode->value;
        }

        throw new GraphQLRuntimeException();
    }
}
