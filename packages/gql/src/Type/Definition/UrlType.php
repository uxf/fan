<?php

declare(strict_types=1);

namespace UXF\GQL\Type\Definition;

use Exception;
use GraphQL\Error\InvariantViolation;
use GraphQL\Error\UserError;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;
use TheCodingMachine\GraphQLite\GraphQLRuntimeException;
use UXF\Core\Type\Url;

final class UrlType extends ScalarType
{
    public const string NAME = 'Url';

    public function __construct()
    {
        parent::__construct([
            'name' => self::NAME,
        ]);
    }

    public function serialize(mixed $value): string
    {
        if (!$value instanceof Url) {
            throw new InvariantViolation('Url is not an instance of Url: ' . Utils::printSafe($value));
        }

        return $value->__toString();
    }

    public function parseValue(mixed $value): ?Url
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof Url) {
            return $value;
        }

        try {
            return Url::of($value);
        } catch (Exception) {
            throw new UserError('Invalid url format');
        }
    }

    /**
     * @inheritDoc
     */
    public function parseLiteral($valueNode, ?array $variables = null): mixed
    {
        if ($valueNode instanceof StringValueNode) {
            return $valueNode->value;
        }

        throw new GraphQLRuntimeException();
    }
}
