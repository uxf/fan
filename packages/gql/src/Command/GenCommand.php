<?php

declare(strict_types=1);

namespace UXF\GQL\Command;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Utils\SchemaPrinter;
use ReflectionProperty;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use TheCodingMachine\GraphQLite\Schema;
use function Safe\file_put_contents;

#[AsCommand(name: 'uxf:gql-gen')]
class GenCommand extends Command
{
    /**
     * @param string[] $destinations
     */
    public function __construct(private readonly array $destinations, private readonly Schema $schema)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Trying to guarantee deterministic order
        $this->sortSchema();

        $out = SchemaPrinter::doPrint($this->schema, [
            'sortTypes' => true,
        ]);

        // cleanup unused descriptions
        $search = ['

  """
  
  
  """', '
  """
  
  
  """', '"
  
  
  
  """', '

  ""', '
  ""'];
        $out = str_replace($search, ['', '', '', '', ''], $out);

        foreach ($this->destinations as $destination) {
            (new Filesystem())->mkdir(pathinfo($destination, PATHINFO_DIRNAME));
            file_put_contents($destination, $out);
        }

        return 0;
    }

    private function sortSchema(): void
    {
        $config = $this->schema->getConfig();

        $refl = new ReflectionProperty(ObjectType::class, 'fields');
        $refl->setAccessible(true);

        if ($config->query instanceof ObjectType) {
            $fields = $config->query->getFields();
            ksort($fields);
            $refl->setValue($config->query, $fields);
        }

        if ($config->mutation instanceof ObjectType) {
            $fields = $config->mutation->getFields();
            ksort($fields);
            $refl->setValue($config->mutation, $fields);
        }
    }
}
