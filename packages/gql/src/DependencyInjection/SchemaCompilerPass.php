<?php

declare(strict_types=1);

namespace UXF\GQL\DependencyInjection;

use LogicException;
use ReflectionClass;
use ReflectionMethod;
use ReflectionNamedType;
use RuntimeException;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Request;
use TheCodingMachine\GraphQLite\AggregateControllerQueryProviderFactory;
use TheCodingMachine\GraphQLite\Annotations\Autowire;
use TheCodingMachine\GraphQLite\Annotations\ExtendType;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Input;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Mappers\StaticClassListTypeMapperFactory;
use UXF\Core\Type\Currency;
use UXF\Core\Utils\ClassFinder;
use UXF\GQL\Factory\MoneyFactory;
use UXF\GQL\Mapper\EnumTypeMapperFactory;

final readonly class SchemaCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        /** @var string[] $sources */
        $sources = $container->getParameter('uxf_gql.sources');
        /** @var bool $allowMoney */
        $allowMoney = $container->getParameter('uxf_gql.allow_money');

        $classNames = ClassFinder::findAllClassNames($sources);

        $controllers = [];
        $types = [];
        $enums = [];

        foreach ($classNames as $className) {
            $refClass = new ReflectionClass($className);
            if ($this->isController($refClass)) {
                $controllers[] = $className;
                $this->makePublic($container, $className, true);
                continue;
            }

            if ($this->isInput($refClass)) {
                $types[] = $className;
                continue;
            }

            if ($this->isType($refClass)) {
                $types[] = $className;
                $this->makeAutowirePublic($container, $refClass);
                continue;
            }

            if ($this->isEnum($refClass)) {
                $enums[$refClass->getShortName()] = $className;
                continue;
            }

            if ($this->isExtendType($refClass)) {
                $types[] = $className;
                $this->makePublic($container, $className, true);
            }
        }

        // money support
        if ($allowMoney) {
            $types[] = MoneyFactory::class;
            $enums['Currency'] = Currency::class;
        }

        $enumFactory = $container->getDefinition(EnumTypeMapperFactory::class);
        $enumFactory->setArgument(0, $enums);

        sort($controllers);
        $queryFactory = $container->getDefinition(AggregateControllerQueryProviderFactory::class);
        $queryFactory->setArgument(0, $controllers);
        $container->setParameter('uxf_gql.controller_names', $controllers);

        $typeFactory = $container->getDefinition(StaticClassListTypeMapperFactory::class);
        $typeFactory->setArgument(0, $types);

        // register injected services as public
        /** @var array<string, string> $injected */
        $injected = $container->getParameter('uxf_gql.injected');
        foreach ($injected as $serviceName) {
            $ref = new ReflectionMethod($serviceName, '__invoke');
            $params = $ref->getParameters();
            if (
                $params !== [] &&
                (!$params[0]->getType() instanceof ReflectionNamedType || $params[0]->getType()->getName() !== Request::class)
            ) {
                throw new LogicException("Invalid $serviceName::__invoke method definition.\nPlease change your code to __invoke(Request \$request, InjectedArgument \$argument)");
            }

            $this->makePublic($container, $serviceName);
        }
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     */
    private function isController(ReflectionClass $reflectionClass): bool
    {
        if (!$reflectionClass->hasMethod('__invoke')) {
            return false;
        }

        $refMethod = $reflectionClass->getMethod('__invoke');
        $attribute = $refMethod->getAttributes(Query::class)[0] ?? $refMethod->getAttributes(Mutation::class)[0] ?? null;
        return $attribute !== null;
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     */
    private function isInput(ReflectionClass $reflectionClass): bool
    {
        $attribute = $reflectionClass->getAttributes(Input::class)[0] ?? null;
        return $attribute !== null;
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     */
    private function isType(ReflectionClass $reflectionClass): bool
    {
        $attribute = $reflectionClass->getAttributes(Type::class)[0] ?? null;
        return $attribute !== null;
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     */
    private function isEnum(ReflectionClass $reflectionClass): bool
    {
        return $reflectionClass->isEnum();
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     */
    private function isExtendType(ReflectionClass $reflectionClass): bool
    {
        $attribute = $reflectionClass->getAttributes(ExtendType::class)[0] ?? null;
        return $attribute !== null;
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     */
    private function makeAutowirePublic(ContainerBuilder $container, ReflectionClass $reflectionClass): void
    {
        foreach ($reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC) as $reflectionMethod) {
            if (($reflectionMethod->getAttributes(Field::class)[0] ?? null) === null) {
                continue;
            }

            foreach ($reflectionMethod->getParameters() as $reflectionParameter) {
                if (($reflectionParameter->getAttributes(Autowire::class)[0] ?? null) === null) {
                    continue;
                }

                $reflectionType = $reflectionParameter->getType();
                if ($reflectionType instanceof ReflectionNamedType) {
                    $this->makePublic($container, $reflectionType->getName());
                }
            }
        }
    }

    private function makePublic(ContainerBuilder $container, string $className, bool $autoRegister = false): void
    {
        if ($container->hasAlias($className)) {
            $container->getAlias($className)->setPublic(true);
        } elseif ($container->hasDefinition($className)) {
            $container->getDefinition($className)->setPublic(true);
        } elseif ($autoRegister) {
            $container->register($className, $className)->setAutowired(true)->setPublic(true);
        } else {
            throw new RuntimeException("Service $className is not registered in container");
        }
    }
}
