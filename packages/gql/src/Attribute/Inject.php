<?php

declare(strict_types=1);

namespace UXF\GQL\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_PARAMETER)]
final readonly class Inject
{
}
