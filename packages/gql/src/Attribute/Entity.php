<?php

declare(strict_types=1);

namespace UXF\GQL\Attribute;

use Attribute;
use UXF\Core\Attribute\Entity as CoreEntity;

/**
 * @deprecated Please use UXF\Core\Attribute\Entity
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
final readonly class Entity extends CoreEntity
{
}
