<?php

declare(strict_types=1);

namespace UXF\DataGridTests\GQL;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\Core\Contract\Permission\PermissionChecker;
use UXF\Core\UXFCoreBundle;
use UXF\DataGrid\UXFDataGridBundle;
use UXF\GraphQL\Controller\GQLController;
use UXF\GraphQL\UXFGraphQLBundle;
use UXF\Hydrator\UXFHydratorBundle;

class KernelGQL extends BaseKernel
{
    use MicroKernelTrait;

    /**
     * @inheritDoc
     */
    public function registerBundles(): iterable
    {
        $bundles = [
            FrameworkBundle::class,
            SecurityBundle::class,
            DoctrineBundle::class,
            DoctrineFixturesBundle::class,
            MonologBundle::class,
            TwigBundle::class,
            UXFCoreBundle::class,
            UXFDataGridBundle::class,
            UXFHydratorBundle::class,
            UXFGraphQLBundle::class,
        ];

        foreach ($bundles as $bundle) {
            yield new $bundle();
        }
    }

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->import(__DIR__ . '/../config/services.php');
        $container->extension('uxf_graphql', [
            'sources' => [
                __DIR__ . '/../../src/GQL',
            ],
        ]);

        $container->extension('security', [
            'firewalls' => [
                'main' => [
                    'pattern' => '/',
                    'security' => false,
                ],
            ],
        ]);

        $container->services()->set(PermissionChecker::class, FakePermissionChecker::class);
    }

    public function getCacheDir(): string
    {
        return __DIR__ . '/../../var/cache/test-gql';
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import(__DIR__ . '/../../config/routes.php');
        $routes->add('gql', '/graphql')
            ->controller(GQLController::class)
            ->methods(['POST']);
    }
}
