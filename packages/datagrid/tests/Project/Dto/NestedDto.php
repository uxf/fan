<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Dto;

final readonly class NestedDto
{
    public function __construct(
        public string $nested,
    ) {
    }
}
