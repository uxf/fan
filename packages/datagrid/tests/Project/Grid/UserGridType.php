<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Grid;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\Column\ColumnConfig;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridSort;
use UXF\DataGrid\DataGridSortDir;
use UXF\DataGrid\DataGridTypeWithoutCustomDataSource;
use UXF\DataGrid\DataSource\DoctrineDataSource;
use UXF\DataGrid\Filter\EntitySelectFilter;
use UXF\DataGrid\Filter\StringFilter;
use UXF\DataGrid\Tab\Tab;
use UXF\DataGrid\Utils\DataGridMagicHelper;
use UXF\DataGridTests\Project\Entity\User;

/**
 * @template-implements DataGridTypeWithoutCustomDataSource<User>
 */
final readonly class UserGridType implements DataGridTypeWithoutCustomDataSource
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @inheritDoc
     */
    public function buildGrid(DataGridBuilder $builder, array $options = []): void
    {
        $qb = $this->entityManager->createQueryBuilder()->select('u')->from(User::class, 'u');

        $builder->setDataSource(new DoctrineDataSource($qb));

        $builder->addTab(Tab::qb('one', 'One', fn (QueryBuilder $qb) => $qb->andWhere('u.id = 1'), new DataGridSort(
            name: 'name',
            dir: DataGridSortDir::DESC,
        )));
        $builder->addTab(Tab::qb('all', 'All', fn () => null), 'one');

        $builder->addColumn(new Column('name', 'Name'))->setConfig(new ColumnConfig(isHidden: true));
        $builder->addColumn(new Column('id', 'ID'), 'name')->setHidden(true)->setConfig(new ColumnConfig(100));
        $builder->addColumn(new Column('role', 'Role', 'role.name'))
            ->setType('toOne')
            ->setCustomContentCallback(fn (User $user) => [
                'id' => $user->getRole()->getId(),
                'label' => $user->getRole()->getName(),
            ])
            ->setCustomExportCallback(fn (User $user) => $user->getRole()->getName())
            ->setSort(['role.name', 'role.id']);

        $builder->addColumn(new Column('fake', 'FAKE'))
            ->setCustomContentCallback(fn (User $user) => $user->getName() . ' CUSTOM')
            ->setCustomExportCallback(fn (User $user) => $user->getName() . ' EXPORT');

        $builder->addFilter(new EntitySelectFilter('role', 'Role name', 'role'));
        $builder->addFilter(new StringFilter('name', 'User name'), 'role');

        $builder->setFullTextFilterCallback(function (QueryBuilder $qb, string $value): void {
            DataGridMagicHelper::trySafelyApplyJoin($qb, 'u.role');

            $qb->andWhere('u.name LIKE :fullText OR role.name LIKE :fullText')
                ->setParameter('fullText', "%$value%");
        });
    }

    public static function getName(): string
    {
        return 'user';
    }
}
