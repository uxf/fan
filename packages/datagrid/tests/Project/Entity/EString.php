<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Entity;

enum EString: string
{
    case X = 'X';
    case Y = 'Y';
}
