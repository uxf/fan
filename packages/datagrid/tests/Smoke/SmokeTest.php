<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Smoke;

use UXF\Core\Test\WebTestCase;

class SmokeTest extends WebTestCase
{
    public function testGet(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/datagrid/user', [
            'f' => [[
                'name' => 'name',
                'value' => 'root',
            ]],
            'withTabCounts' => 1,
        ]);
        self::assertResponseIsSuccessful();

        $data = $client->getResponseData();

        self::assertArrayHasKey('result', $data);
        self::assertArrayHasKey('count', $data);
        self::assertArrayHasKey('totalCount', $data);
        self::assertArrayHasKey('tabCounts', $data);
        self::assertSame([
            'all' => 1,
            'one' => 1,
        ], $data['tabCounts']);
    }

    public function testSchema(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/datagrid/schema/user', [
            'f' => [[
                'name' => 'name',
                'value' => 'root',
            ]],
        ]);
        self::assertResponseIsSuccessful();

        $data = $client->getResponseData();
        self::assertSame([
            'name' => 'user',
            'columns' => [
                [
                    'name' => 'id',
                    'label' => 'ID',
                    'sort' => false,
                    'hidden' => true,
                    'type' => 'string',
                    'config' => [
                        'width' => 100,
                    ],
                ],
                [
                    'name' => 'name',
                    'label' => 'Name',
                    'sort' => false,
                    'hidden' => false,
                    'type' => 'string',
                    'config' => [
                        'isHidden' => true,
                    ],
                ],
                [
                    'name' => 'role',
                    'label' => 'Role',
                    'sort' => true,
                    'hidden' => false,
                    'type' => 'toOne',
                    'config' => [],
                ],
                [
                    'name' => 'fake',
                    'label' => 'FAKE',
                    'sort' => false,
                    'hidden' => false,
                    'type' => 'string',
                    'config' => [],
                ],
            ],
            'filters' => [
                [
                    'name' => 'name',
                    'label' => 'User name',
                    'type' => 'string',
                    'autocomplete' => null,
                    'options' => null,
                ],
                [
                    'name' => 'role',
                    'label' => 'Role name',
                    'type' => 'entitySelect',
                    'autocomplete' => 'role',
                    'options' => null,
                ],
            ],
            'tabs' => [
                [
                    'name' => 'all',
                    'label' => 'All',
                    'icon' => null,
                    's' => null,
                ],
                [
                    'name' => 'one',
                    'label' => 'One',
                    'icon' => null,
                    's' => [
                        'name' => 'name',
                        'dir' => 'desc',
                    ],
                ],
            ],
            'fullText' => true,
            's' => [
                'name' => 'id',
                'dir' => 'asc',
            ],
            'perPage' => 10,
            'sort' => 'id',
            'dir' => 'asc',
        ], $data);
    }

    public function testWithGetDataSource(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/datagrid/role');
        self::assertResponseIsSuccessful();
    }

    public function testWithoutPaginator(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/datagrid/user2', [
            'f' => [[
                'name' => 'name',
                'value' => 'X',
            ]],
        ]);
        self::assertResponseIsSuccessful();
    }

    public function testExportCsv(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/datagrid/export/user');
        self::assertResponseIsSuccessful();
        self::assertSame("﻿ID,Name,Role,FAKE\n1,Superman,ADMIN,\"Superman EXPORT\"\n", $client->getInternalResponse()->getContent());
    }

    public function testExportXlsx(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/datagrid/export/user?format=xlsx');
        self::assertResponseIsSuccessful();
        self::assertStringContainsString('Content_Types', $client->getInternalResponse()->getContent());
    }

    public function testFullText(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/datagrid/user', [
            'f' => [[
                'name' => 'name',
                'value' => 'Super',
            ]],
            'search' => 'a',
        ]);
        self::assertResponseIsSuccessful();

        $data = $client->getResponseData();
        self::assertSame(1, $data['count']);
    }

    public function testTab(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/datagrid/user', [
            'tab' => 'one',
        ]);
        self::assertResponseIsSuccessful();

        $data = $client->getResponseData();
        self::assertSame(1, $data['count']);
    }

    public function testArraySimple(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/datagrid/plane', [
            'f' => [
                [
                    'name' => 'name',
                    'value' => 'wo',
                ],
                [
                    'name' => 'date',
                    'value' => [
                        'from' => '2024-01-01',
                        'to' => '2024-01-20',
                    ],
                ],
                [
                    'name' => 'role',
                    'value' => '2',
                ],
                [
                    'name' => 'fake',
                    'value' => '2',
                ],
            ],
        ]);
        self::assertResponseIsSuccessful();

        $data = $client->getResponseData();
        self::assertSame(1, $data['count']);
    }

    public function testArrayOfObjects(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/datagrid/helicopter', [
            'f' => [
                [
                    'name' => 'name',
                    'value' => 'wo',
                ],
                [
                    'name' => 'date',
                    'value' => [
                        'from' => '2024-01-01',
                        'to' => '2024-01-20',
                    ],
                ],
                [
                    'name' => 'role',
                    'value' => '2',
                ],
                [
                    'name' => 'fake',
                    'value' => '2',
                ],
            ],
        ]);
        self::assertResponseIsSuccessful();

        $data = $client->getResponseData();
        self::assertSame(1, $data['count']);
    }
}
