<?php

declare(strict_types=1);

namespace UXF\DataGrid\Schema;

use UXF\DataGrid\DataGridSort;

final readonly class TabSchema
{
    public function __construct(
        public string $name,
        public string $label,
        public ?string $icon,
        public ?DataGridSort $s,
    ) {
    }
}
