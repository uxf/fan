<?php

declare(strict_types=1);

namespace UXF\DataGrid\Schema;

use UXF\DataGrid\Column\ColumnConfig;

final readonly class ColumnSchema
{
    public function __construct(
        public string $name,
        public string $label,
        public bool $sort,
        public bool $hidden,
        public string $type,
        public ColumnConfig $config,
    ) {
    }
}
