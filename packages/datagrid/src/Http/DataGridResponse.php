<?php

declare(strict_types=1);

namespace UXF\DataGrid\Http;

final readonly class DataGridResponse
{
    /**
     * @param mixed[] $result
     * @param array<string, int>|null $tabCounts
     */
    public function __construct(
        public array $result,
        public int $count,
        public int $totalCount,
        public ?array $tabCounts,
    ) {
    }
}
