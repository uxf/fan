<?php

declare(strict_types=1);

namespace UXF\DataGrid\Http;

final readonly class DataGridExportRequestQuery
{
    public function __construct(
        public string $format = 'csv',
    ) {
    }
}
