<?php

declare(strict_types=1);

namespace UXF\DataGrid\GQL\Query;

use TheCodingMachine\GraphQLite\Annotations\Query as LegacyQuery;
use UXF\Core\Contract\Permission\PermissionChecker;
use UXF\Core\Exception\BasicException;
use UXF\DataGrid\GQL\DataGridResponseProvider;
use UXF\DataGrid\GQL\Input\DataGridInput;
use UXF\DataGrid\GQL\Type\DataGridType;
use UXF\GraphQL\Attribute\Query;

final readonly class DataGridQuery
{
    public function __construct(
        private DataGridResponseProvider $responseProvider,
        private ?PermissionChecker $permissionChecker,
    ) {
    }

    #[LegacyQuery(name: 'dataGrid')]
    #[Query('dataGrid')]
    public function __invoke(string $name, DataGridInput $input): DataGridType
    {
        if ($this->permissionChecker?->isAllowed('grid', $name) !== true) {
            throw BasicException::forbidden();
        }

        return $this->responseProvider->get($name, $input);
    }
}
