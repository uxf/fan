<?php

declare(strict_types=1);

namespace UXF\DataGrid\GQL\Input;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Input as LegacyInput;
use UXF\DataGrid\DataGridSortDir;
use UXF\GraphQL\Attribute\Input;

#[LegacyInput]
#[Input(generateFake: false)]
final readonly class DataGridSortInput
{
    public function __construct(
        #[Field] public string $name,
        #[Field] public DataGridSortDir $dir,
    ) {
    }
}
