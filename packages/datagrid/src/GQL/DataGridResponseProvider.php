<?php

declare(strict_types=1);

namespace UXF\DataGrid\GQL;

use UXF\DataGrid\DataGridFactory;
use UXF\DataGrid\DataGridSort;
use UXF\DataGrid\GQL\Input\DataGridFilterInput;
use UXF\DataGrid\GQL\Input\DataGridInput;
use UXF\DataGrid\GQL\Type\DataGridType;
use UXF\DataGrid\Http\DataGridRequest;
use UXF\DataGrid\Http\FilterRequest;
use UXF\GraphQL\Type\Json;

final readonly class DataGridResponseProvider
{
    public function __construct(
        private DataGridFactory $dataGridFactory,
    ) {
    }

    /**
     * @param mixed[] $options
     */
    public function get(string $name, DataGridInput $input, array $options = []): DataGridType
    {
        $request = new DataGridRequest(
            f: array_map(static fn (DataGridFilterInput $i) => new FilterRequest($i->name, $i->value?->content, $i->op), $input->f),
            tab: $input->tab,
            search: $input->search,
            s: $input->s !== null ? new DataGridSort($input->s->name, $input->s->dir) : null,
            withTabCounts: $input->withTabCounts,
            page: $input->page,
            perPage: $input->perPage,
            sort: $input->sort,
            dir: $input->dir,
        );

        $result = $this->dataGridFactory->createDataGrid($name, $options)->getResult($request);

        return new DataGridType(
            result: array_map(static fn (mixed $row) => $row, $result->result),
            count: $result->count,
            totalCount: $result->totalCount,
            tabCounts: $result->tabCounts !== null ? new Json($result->tabCounts) : null,
        );
    }
}
