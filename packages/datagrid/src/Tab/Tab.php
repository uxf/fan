<?php

declare(strict_types=1);

namespace UXF\DataGrid\Tab;

use Closure;
use Doctrine\ORM\QueryBuilder;
use UXF\DataGrid\DataGridSort;

final readonly class Tab
{
    /**
     * @phpstan-param Closure(mixed $source): mixed $filterCallback
     */
    public function __construct(
        public string $name,
        public string $label,
        public Closure $filterCallback,
        public ?DataGridSort $s = null,
        public ?string $icon = null,
    ) {
    }

    /**
     * @phpstan-param Closure(QueryBuilder $qb): mixed $fn
     */
    public static function qb(string $name, string $label, Closure $fn, ?DataGridSort $s = null): self
    {
        return new self($name, $label, $fn, $s);
    }
}
