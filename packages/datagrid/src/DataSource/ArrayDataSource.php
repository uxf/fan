<?php

declare(strict_types=1);

namespace UXF\DataGrid\DataSource;

use Closure;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\DataGridSortDir;
use UXF\DataGrid\Filter\Filter;
use UXF\DataGrid\Filter\RangeFilter;
use UXF\DataGrid\Filter\StringFilter;
use UXF\DataGrid\Utils\PropertyPathHelper;

/**
 * @template T
 * @template-implements DataSource<T>
 */
final class ArrayDataSource implements DataSource
{
    /**
     * @param mixed[] $data
     */
    public function __construct(
        private readonly Closure $dataCallback,
        private readonly PropertyAccessorInterface $propertyAccessor,
        private array $data = [],
    ) {
    }

    public function init(): void
    {
        $this->data = call_user_func($this->dataCallback);
    }

    public function getTotalCount(?Closure $callback = null): int
    {
        return count($callback !== null ? array_filter($this->data, $callback) : $this->data);
    }

    public function applySort(Column $column, DataGridSortDir $direction): void
    {
        $path = $column->getSortColumnPaths()[0];

        usort($this->data, function ($item1, $item2) use ($path, $direction): int {
            $value1 = $this->accessValue($item1, $path);
            $value2 = $this->accessValue($item2, $path);

            if (is_string($value1)) {
                $value1 = mb_strtolower($value1);
            }

            if (is_string($value2)) {
                $value2 = mb_strtolower($value2);
            }

            return $direction === DataGridSortDir::ASC ? $value1 <=> $value2 : $value2 <=> $value1;
        });
    }

    public function applyFilter(Filter $filter, mixed $value, ?string $operand): void
    {
        $propertyPath = $filter->getColumnPath();
        $value = $filter->mapFilterValue($value);

        if ($filter instanceof RangeFilter) {
            $this->data = array_filter(
                $this->data,
                function (mixed $item) use ($propertyPath, $value) {
                    [$from, $to] = $value;
                    $itemValue = (string) $this->accessValue($item, $propertyPath);
                    return !(($from !== null && $itemValue < $from) || ($to !== null && $itemValue > $to));
                },
            );
        } elseif ($filter instanceof StringFilter) {
            $this->data = array_filter(
                $this->data,
                fn (mixed $item) => mb_stripos((string) $this->accessValue($item, $propertyPath), $value) !== false,
            );
        } else {
            $this->data = array_filter(
                $this->data,
                fn (mixed $item) => (string) $this->accessValue($item, $propertyPath) === (string)$value,
            );
        }
    }

    public function applyFullTextFilter(Closure $callback, string $value): void
    {
        $this->data = $callback($this->data, $value);
    }

    public function getFilteredCount(): int
    {
        return count($this->data);
    }

    public function applyLimitAndOffset(int $limit, int $offset): void
    {
        $this->data = array_slice($this->data, $offset, $limit);
    }

    /**
     * {@inheritdoc}
     */
    public function getData(): array
    {
        return $this->data;
    }

    private function accessValue(mixed $item, string $propertyPath): mixed
    {
        return $this->propertyAccessor->getValue($item, PropertyPathHelper::normalize($item, $propertyPath));
    }
}
