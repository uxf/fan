<?php

declare(strict_types=1);

namespace UXF\DataGrid\Column;

use Closure;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Core\Http\Request\NotSet;
use UXF\DataGrid\Utils\PropertyPathHelper;

/**
 * @template T
 */
class Column
{
    protected string $type = 'string';
    protected string $name;
    protected string $label;
    protected string $contentColumnPath;
    /** @var string[] */
    protected array $sortColumnPaths;
    protected bool $sort = false;
    protected bool $rawSort = false;
    protected int $order = 0;
    protected bool $hidden = false;
    protected bool $export = true;
    /** @var Closure(T $item): mixed|null */
    protected ?Closure $customContentCallback = null;
    /** @var Closure(T $item): mixed|null */
    protected ?Closure $customExportCallback = null;
    protected bool $embedded = false;
    protected ColumnConfig $config;

    /**
     * @param string[]|null $sortColumnPaths
     */
    public function __construct(
        string $name,
        string $label,
        ?string $contentColumnPath = null,
        ?array $sortColumnPaths = null,
    ) {
        $this->name = $name;
        $this->label = $label;
        $this->contentColumnPath = $contentColumnPath ?? $name;
        $this->sortColumnPaths = $sortColumnPaths ?? [$this->contentColumnPath];
        $this->config = new ColumnConfig();
    }

    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return self<T>
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return self<T>
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;
        return $this;
    }

    public function getContentColumnPath(): string
    {
        return $this->contentColumnPath;
    }

    /**
     * @return self<T>
     */
    public function setContentColumnPath(string $contentColumnPath): self
    {
        $this->contentColumnPath = $contentColumnPath;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getSortColumnPaths(): array
    {
        return $this->sortColumnPaths;
    }

    public function isSort(): bool
    {
        return $this->sort;
    }

    public function isRawSort(): bool
    {
        return $this->rawSort;
    }

    /**
     * @param bool|string[] $sort
     * @return self<T>
     */
    public function setSort(bool|array $sort = true): self
    {
        if (is_bool($sort)) {
            $this->sort = $sort;
        } else {
            $this->sort = true;
            $this->sortColumnPaths = $sort;
        }
        return $this;
    }

    /**
     * @param string[] $sort
     * @return self<T>
     */
    public function setRawSort(array $sort): self
    {
        $this->sort = true;
        $this->rawSort = true;
        $this->sortColumnPaths = $sort;
        return $this;
    }

    /**
     * @deprecated will be removed in 4.0
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @deprecated will be removed in 4.0
     * @return self<T>
     */
    public function setOrder(int $order): self
    {
        $this->order = $order;
        return $this;
    }

    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @return self<T>
     */
    public function setHidden(bool $hidden): self
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * @deprecated will be removed in 4.0
     */
    public function isExport(): bool
    {
        return $this->export;
    }

    /**
     * @deprecated will be removed in 4.0
     * @return self<T>
     */
    public function setExport(bool $export): self
    {
        $this->export = $export;
        return $this;
    }

    /**
     * @phpstan-param Closure(T $item): mixed|null $customContentCallback
     * @return self<T>
     */
    public function setCustomContentCallback(?Closure $customContentCallback): self
    {
        $this->customContentCallback = $customContentCallback;
        return $this;
    }

    /**
     * @deprecated will be removed in 4.0
     * @phpstan-param Closure(T $item): mixed|null $customExportCallback
     * @return self<T>
     */
    public function setCustomExportCallback(?Closure $customExportCallback): self
    {
        $this->customExportCallback = $customExportCallback;
        return $this;
    }

    /**
     * @internal
     * @param T $item
     */
    public function getContent(mixed $item, PropertyAccessorInterface $propertyAccessor): mixed
    {
        return is_callable($this->customContentCallback)
            ? call_user_func($this->customContentCallback, $item)
            : $propertyAccessor->getValue($item, PropertyPathHelper::normalize($item, $this->contentColumnPath));
    }

    /**
     * @internal
     * @deprecated will be removed in 4.0
     * @param T $item
     */
    public function getExportContent(mixed $item, PropertyAccessorInterface $propertyAccessor): mixed
    {
        return is_callable($this->customExportCallback)
            ? call_user_func($this->customExportCallback, $item)
            : $this->getContent($item, $propertyAccessor);
    }

    /**
     * @deprecated will be removed in 4.0
     */
    public function isEmbedded(): bool
    {
        return $this->embedded;
    }

    /**
     * @deprecated will be removed in 4.0 - use self::setRawSort()
     * @return self<T>
     */
    public function setEmbedded(bool $embedded): self
    {
        $this->embedded = $embedded;
        return $this;
    }

    public function getConfig(): ColumnConfig
    {
        return $this->config;
    }

    /**
     * @return self<T>
     */
    public function setConfig(ColumnConfig $config): self
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return self<T>
     */
    public function replaceConfig(int|string|NotSet|null $width = null, bool|NotSet|null $isHidden = null): self
    {
        $this->config = new ColumnConfig(
            width: $width ?? $this->config->width,
            isHidden: $isHidden ?? $this->config->isHidden,
        );
        return $this;
    }
}
