<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

/**
 * @extends Filter<string>
 */
final class StringFilter extends Filter
{
    private string $matchType = 'contains';

    public function getMatchType(): string
    {
        return $this->matchType;
    }

    public function setMatchType(string $matchType): self
    {
        $this->matchType = $matchType;
        return $this;
    }

    protected function getDefaultType(): string
    {
        return 'string';
    }

    public function mapFilterValue(mixed $value): string
    {
        return trim($value);
    }
}
