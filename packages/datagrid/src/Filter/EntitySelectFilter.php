<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

/**
 * @extends Filter<string|int>
 */
class EntitySelectFilter extends Filter implements FilterWithAutocomplete
{
    public function __construct(string $name, string $label, private readonly string $autocomplete)
    {
        parent::__construct($name, $label, "$name.id");
    }

    protected function getDefaultType(): string
    {
        return 'entitySelect';
    }

    public function mapFilterValue(mixed $value): string|int
    {
        return is_array($value) ? ($value['id'] ?? 0) : $value;
    }

    public function getAutocomplete(): string
    {
        return $this->autocomplete;
    }
}
