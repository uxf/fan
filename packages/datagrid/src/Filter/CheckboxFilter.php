<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

use UXF\Core\Utils\SH;

/**
 * @extends Filter<bool>
 */
final class CheckboxFilter extends Filter
{
    protected function getDefaultType(): string
    {
        return 'checkbox';
    }

    public function mapFilterValue(mixed $value): bool
    {
        if (is_bool($value)) {
            return $value;
        }

        if (is_string($value)) {
            return SH::s2b($value);
        }

        trigger_error('Invalid type: ' . get_debug_type($value));
        return false;
    }
}
