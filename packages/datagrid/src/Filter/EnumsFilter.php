<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

use BackedEnum;
use ReflectionEnum;
use UXF\CMS\Attribute\CmsLabel;
use UXF\Core\Attribute\Label;
use UXF\DataGrid\Schema\FilterOption;

/**
 * @template T of BackedEnum
 * @extends Filter<T[]>
 */
final class EnumsFilter extends Filter implements FilterWithOptions
{
    /** @var class-string<T> $enumClass */
    private string $enumClass;

    /**
     * @param class-string<T> $enumClass
     */
    public function __construct(string $name, string $label, string $enumClass, ?string $columnPath = null)
    {
        parent::__construct($name, $label, $columnPath);
        $this->enumClass = $enumClass;
    }

    protected function getDefaultType(): string
    {
        return 'multiSelect';
    }

    /**
     * @inheritDoc
     */
    public function mapFilterValue(mixed $value): array
    {
        $isInt = (new ReflectionEnum($this->enumClass))->getBackingType()?->getName() === 'int';

        $result = [];
        foreach ($value as $item) {
            $item = is_array($item) ? ($item['id'] ?? 0) : $item;
            $result[] = $this->enumClass::from($isInt ? (int) $item : (string) $item);
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getOptions(): array
    {
        $options = [];

        foreach ((new ReflectionEnum($this->enumClass))->getCases() as $caseRef) {
            $labelAttr = $caseRef->getAttributes(Label::class)[0] ?? $caseRef->getAttributes(CmsLabel::class)[0] ?? null;
            if ($labelAttr !== null) {
                $cmsLabel = $labelAttr->newInstance();
                $options[] = new FilterOption($caseRef->getBackingValue(), $cmsLabel->label);
            } else {
                $options[] = new FilterOption($caseRef->getBackingValue(), $caseRef->getName());
            }
        }

        return $options;
    }
}
