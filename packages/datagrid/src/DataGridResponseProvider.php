<?php

declare(strict_types=1);

namespace UXF\DataGrid;

use Symfony\Component\HttpFoundation\Response;
use UXF\DataGrid\Export\DataGridExporter;
use UXF\DataGrid\Http\DataGridRequest;
use UXF\DataGrid\Http\DataGridResponse;

final readonly class DataGridResponseProvider
{
    public function __construct(
        private DataGridFactory $dataGridFactory,
        private DataGridExporter $exporter,
    ) {
    }

    /**
     * @param mixed[] $options
     */
    public function get(string $name, DataGridRequest $request, array $options = []): DataGridResponse
    {
        return $this->dataGridFactory->createDataGrid($name, $options)->getResult($request);
    }

    /**
     * @param mixed[] $options
     */
    public function export(string $name, DataGridRequest $request, array $options = [], string $format = 'csv'): Response
    {
        return $this->exporter->export($this->dataGridFactory->createDataGrid($name, $options)->getExport($request), $format);
    }
}
