<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Content\Doctrine\SearchColumnEventSubscriber;
use UXF\Content\Service\ContentResponseProvider;
use UXF\Content\Service\ContentService;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $services->set(ContentService::class);
    $services->set(ContentResponseProvider::class);
    $services->set(SearchColumnEventSubscriber::class)
        ->autoconfigure();

    $services->load('UXF\Content\Controller\\', __DIR__ . '/../src/Controller')
        ->public();
};
