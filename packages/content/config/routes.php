<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\Content\Controller\Cms\CreateContentController;
use UXF\Content\Controller\Cms\DeleteContentController;
use UXF\Content\Controller\Cms\GetContentController;
use UXF\Content\Controller\Cms\UpdateContentController;
use UXF\Content\Controller\GetPublicContentController;
use UXF\Content\Controller\GetPublicContentsController;

return static function (RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->add('content_create', '/api/cms/content')
        ->controller(CreateContentController::class)
        ->methods(['POST']);

    $routingConfigurator->add('content_update', '/api/cms/content/{content}')
        ->controller(UpdateContentController::class)
        ->methods(['PUT']);

    $routingConfigurator->add('content_delete', '/api/cms/content/{content}')
        ->controller(DeleteContentController::class)
        ->methods(['DELETE']);

    $routingConfigurator->add('content_item', '/api/cms/content/{content}')
        ->controller(GetContentController::class)
        ->methods(['GET']);

    // public
    $routingConfigurator->add('public_content_item', '/api/app/content/{content}')
        ->controller(GetPublicContentController::class)
        ->methods(['GET']);

    $routingConfigurator->add('public_content_items', '/api/app/content')
        ->controller(GetPublicContentsController::class)
        ->methods(['GET']);
};
