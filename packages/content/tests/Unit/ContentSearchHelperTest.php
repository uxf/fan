<?php

declare(strict_types=1);

namespace UXF\ContentTests\Unit;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Content\Utils\ContentSearchHelper;
use UXF\Core\Utils\Lst;

class ContentSearchHelperTest extends TestCase
{
    /**
     * @param string[] $expectedParams
     * @param ContentSearchHelper::* $mode
     */
    #[DataProvider('getData')]
    public function test(string $expectedDQL, array $expectedParams, string $text, string $mode): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $qb = new QueryBuilder($em);

        $qb = ContentSearchHelper::apply($qb, 's', $text, $mode);

        self::assertSame($expectedDQL, $qb->getDQL());
        $params = Lst::from($qb->getParameters())->map(fn (Parameter $p) => $p->getValue())->getValues();
        self::assertSame($expectedParams, $params);
    }

    /**
     * @return mixed[]
     */
    public static function getData(): array
    {
        return [
            [
                'expectedDQL' => 'SELECT WHERE s LIKE UNACCENT(:search_0) AND s LIKE UNACCENT(:search_1) AND s LIKE UNACCENT(:search_2)',
                'expectedParams' => ['%hello%', '%world%', '%my%'],
                'text' => "hello world\nmy",
                'mode' => ContentSearchHelper::SIMPLE,
            ],
            [
                'expectedDQL' => 'SELECT',
                'expectedParams' => [],
                'text' => '',
                'mode' => ContentSearchHelper::SIMPLE,
            ],
            [
                'expectedDQL' => 'SELECT WHERE s LIKE UNACCENT(:search_0) AND s LIKE UNACCENT(:search_1)',
                'expectedParams' => ['% hello %', '% world %'],
                'text' => "hello world",
                'mode' => ContentSearchHelper::WORDS,
            ],
            [
                'expectedDQL' => 'SELECT WHERE s LIKE UNACCENT(:search_0) AND s LIKE UNACCENT(:search_1)',
                'expectedParams' => ['% hello %', '% world%'],
                'text' => "hello world",
                'mode' => ContentSearchHelper::WORDS_AUTOCOMPLETE,
            ],
        ];
    }
}
