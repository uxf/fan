<?php

declare(strict_types=1);

namespace UXF\Content\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\DBAL\Types\Type;

final class TsVector extends Type
{
    public const string NAME = 'tsvector';

    public function getName(): string
    {
        return self::NAME;
    }

    public function canRequireSQLConversion(): bool
    {
        return true;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @inheritdoc -
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform instanceof PostgreSQLPlatform ? 'TSVECTOR' : 'TEXT';
    }

    /**
     * @inheritdoc -
     */
    public function getMappedDatabaseTypes(AbstractPlatform $platform): array
    {
        return [self::NAME];
    }

    /**
     * @param string|mixed $sqlExpr - BC with DBAL 3
     */
    public function convertToDatabaseValueSQL(mixed $sqlExpr, AbstractPlatform $platform): string
    {
        return $platform instanceof PostgreSQLPlatform ? sprintf("to_tsvector('unaccented_cs', %s)", $sqlExpr) : $sqlExpr;
    }
}
