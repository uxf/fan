<?php

declare(strict_types=1);

namespace UXF\Content\Controller\Cms;

use UXF\Content\Entity\Content;
use UXF\Content\Service\ContentService;

final readonly class DeleteContentController
{
    public function __construct(
        private ContentService $contentService,
    ) {
    }

    public function __invoke(Content $content): void
    {
        $this->contentService->deleteContent($content);
    }
}
