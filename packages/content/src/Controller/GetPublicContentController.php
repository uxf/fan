<?php

declare(strict_types=1);

namespace UXF\Content\Controller;

use Symfony\Bundle\SecurityBundle\Security;
use UXF\Content\Entity\Content;
use UXF\Content\Entity\VisibilityLevel;
use UXF\Content\Http\ContentResponse;
use UXF\Core\Exception\BasicException;

final readonly class GetPublicContentController
{
    public function __construct(private Security $security)
    {
    }

    public function __invoke(Content $content): ContentResponse
    {
        if (
            $content->isHidden() ||
            ($content->getVisibilityLevel() === VisibilityLevel::PRIVATE && $this->security->getUser() === null)
        ) {
            throw BasicException::notFound();
        }

        return ContentResponse::create($content);
    }
}
