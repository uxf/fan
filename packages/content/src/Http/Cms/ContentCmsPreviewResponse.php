<?php

declare(strict_types=1);

namespace UXF\Content\Http\Cms;

use UXF\Content\Entity\Content;

final readonly class ContentCmsPreviewResponse
{
    public function __construct(
        public int $id,
        public string $name,
        public string $label,
    ) {
    }

    public static function createNullable(?Content $content): ?self
    {
        if ($content === null) {
            return null;
        }

        return new self(
            id: $content->getId(),
            name: $content->getName(),
            label: $content->getName(),
        );
    }
}
