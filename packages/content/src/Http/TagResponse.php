<?php

declare(strict_types=1);

namespace UXF\Content\Http;

use UXF\Content\Entity\Tag;

final readonly class TagResponse
{
    public function __construct(
        public int $id,
        public string $code,
        public string $label,
    ) {
    }

    public static function create(Tag $tag): self
    {
        return new self(
            id: $tag->getId(),
            code: $tag->getCode(),
            label: $tag->getLabel(),
        );
    }
}
