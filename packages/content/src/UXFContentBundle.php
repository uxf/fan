<?php

declare(strict_types=1);

namespace UXF\Content;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use UXF\Content\Doctrine\Func\TsQuery;
use UXF\Content\Doctrine\Func\TsRank;
use UXF\Content\Doctrine\Type\TsVector;

final class UXFContentBundle extends AbstractBundle
{
    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import(__DIR__ . '/../config/services.php');
    }

    public function prependExtension(ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        // doctrine
        $container->extension('doctrine', [
            'dbal' => [
                'types' => [
                    'tsvector' => TsVector::class,
                ],
            ],
            'orm' => [
                'dql' => [
                    'string_functions' => [
                        'TS_QUERY' => TsQuery::class,
                        'TS_RANK' => TsRank::class,
                    ],
                ],
            ],
        ]);
    }
}
