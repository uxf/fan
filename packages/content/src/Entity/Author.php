<?php

declare(strict_types=1);

namespace UXF\Content\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\Storage\Entity\Image;

#[ORM\Entity]
#[ORM\Table(schema: 'uxf_content')]
class Author
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = 0;

    #[ORM\Column]
    private string $firstName;

    #[ORM\Column]
    private string $surname;

    #[ORM\ManyToOne]
    private ?Image $avatar = null;

    public function __construct(string $firstName, string $surname)
    {
        $this->firstName = $firstName;
        $this->surname = $surname;
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    public function getAvatar(): ?Image
    {
        return $this->avatar;
    }

    public function setAvatar(?Image $avatar): void
    {
        $this->avatar = $avatar;
    }

    public function getFullName(): string
    {
        return implode(' ', [$this->surname, $this->firstName]);
    }
}
