<?php

declare(strict_types=1);

namespace UXF\Content\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\Storage\Entity\Image;

/**
 * @deprecated Please use Content::$parent
 */
#[ORM\Entity]
#[ORM\Table(schema: 'uxf_content')]
class Category
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = 0;

    #[ORM\Column]
    private string $name;

    #[ORM\Column]
    private string $titleSuffix = '';

    #[ORM\Column]
    private int $priority = 0;

    #[ORM\ManyToOne(cascade: ["persist"])]
    #[ORM\JoinColumn(name: "parent_id", referencedColumnName: "id", nullable: true)]
    private ?Category $parent = null;

    #[ORM\Column]
    private string $description = '';

    #[ORM\Column]
    private string $metaTitle = '';

    #[ORM\Column]
    private string $metaDescription = '';

    #[ORM\ManyToOne]
    private ?Image $image = null;

    #[ORM\Column]
    private bool $active = true;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getTitleSuffix(): string
    {
        return $this->titleSuffix;
    }

    public function setTitleSuffix(string $titleSuffix): void
    {
        $this->titleSuffix = $titleSuffix;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    public function getParent(): ?Category
    {
        return $this->parent;
    }

    public function setParent(?Category $parent): void
    {
        $this->parent = $parent;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getMetaTitle(): string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    public function getMetaDescription(): string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): void
    {
        $this->image = $image;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }
}
