<?php

declare(strict_types=1);

namespace UXF\Ares\Client;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use RuntimeException;
use UXF\Ares\Dto\AresRzpDataDto;
use UXF\Ares\Dto\AresStandardDataDto;
use UXF\Ares\Exception\AresDownloadingException;
use UXF\Ares\Exception\AresLimitException;
use function Safe\json_decode;
use function Safe\preg_match;

final class AresClientV2
{
    private const string ARES_URL = 'https://ares.gov.cz/ekonomicke-subjekty-v-be/rest';

    /** @var array<string, mixed> */
    private array $cache = [];

    public function __construct(
        private readonly ClientInterface $httpClient,
        private readonly RequestFactoryInterface $requestFactory,
    ) {
    }

    public function getStandardAresData(string $ico): AresStandardDataDto
    {
        $ico = $this->checkFormat($ico);
        $response = $this->getJson("/ekonomicke-subjekty/{$ico}");
        return AresStandardDataDto::createFromArrayV2($response);
    }

    public function getRzpAresData(string $ico): AresRzpDataDto
    {
        $ico = $this->checkFormat($ico);
        $response = $this->getJson("/ekonomicke-subjekty-rzp/{$ico}");
        return AresRzpDataDto::createFromArrayV2($response);
    }

    private function checkFormat(string $ico): string
    {
        $ico = trim($ico);
        if (preg_match('/^\d{8}$/', $ico) !== 1) {
            throw new AresDownloadingException("Invalid 'ico' format");
        }
        return $ico;
    }

    /**
     * @return mixed[]
     */
    private function getJson(string $url, bool $useCache = true): array
    {
        if ($useCache && isset($this->cache[$url])) {
            return $this->cache[$url];
        }

        $request = $this->requestFactory->createRequest('GET', self::ARES_URL . $url);
        $response = $this->httpClient->sendRequest($request);

        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        if ($statusCode === 404) {
            throw new AresDownloadingException("Record not found.");
        }
        if ($statusCode === 403) {
            throw new AresLimitException();
        }
        if ($statusCode !== 200) {
            throw new RuntimeException("$statusCode: $body");
        }

        $this->cache[$url] = json_decode($body, true);

        return $this->cache[$url];
    }
}
