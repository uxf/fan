<?php

declare(strict_types=1);

namespace UXF\Ares\Service;

use UXF\Ares\Dto\AresRzpDataDto;
use UXF\Ares\Dto\AresStandardDataDto;
use UXF\Ares\Http\Response\AresDataResponse;

interface AresDataDownloader
{
    public function downloadAresData(string $ico): AresDataResponse;

    public function downloadStandardAresData(string $ico): AresStandardDataDto;

    public function downloadRzpAresData(string $ico): ?AresRzpDataDto;
}
