<?php

declare(strict_types=1);

namespace UXF\Ares\Exception;

use UXF\Core\Exception\BasicException;

final class AresLimitException extends BasicException
{
    public function __construct()
    {
        parent::__construct('The request is blocked', 500, 'BLOCKED', 'critical');
    }
}
