<?php

declare(strict_types=1);

namespace UXF\Ares\Exception;

use UXF\Core\Exception\BasicException;

final class AresDownloadingException extends BasicException
{
    public function __construct(string $message)
    {
        parent::__construct($message, 404, 'NOT_FOUND', 'error');
    }
}
