<?php

declare(strict_types=1);

namespace UXF\Ares\Exception;

use UXF\Core\Exception\BasicException;
use function Safe\json_encode;

final class AresParsingException extends BasicException
{
    /**
     * @param mixed[] $aresArray
     */
    public function __construct(array $aresArray)
    {
        parent::__construct('Error parsing ares data: ' . json_encode($aresArray), 404, 'NOT_FOUND', 'error');
    }
}
