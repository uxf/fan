<?php

declare(strict_types=1);

namespace UXF\Ares\Exception;

use UXF\Core\Exception\BasicException;

final class AresSubjectTypeException extends BasicException
{
    public function __construct(string $aresCode)
    {
        parent::__construct("Unknown subject type $aresCode", 404, 'NOT_FOUND', 'error');
    }
}
