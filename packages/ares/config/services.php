<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Ares\Client\AresClientV2;
use UXF\Ares\Controller\AresDataByIcoController;
use UXF\Ares\Service\AresDataDownloader;
use UXF\Ares\Service\HttpAresDataDownloader;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $services->set(AresDataByIcoController::class)->public();
    $services->set(AresClientV2::class);
    $services->set(AresDataDownloader::class, HttpAresDataDownloader::class);
};
