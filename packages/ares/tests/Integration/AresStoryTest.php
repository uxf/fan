<?php

declare(strict_types=1);

namespace UXF\AresTests\Integration;

use Nette\Utils\Json;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use UXF\Ares\Exception\AresDownloadingException;
use UXF\Ares\Http\AresSubjectType;
use UXF\Ares\Http\Response\AresDataResponse;
use UXF\Ares\Service\AresDataDownloader;

class AresStoryTest extends WebTestCase
{
    public function test(): void
    {
        $client = self::createClient();

        $aresDataDownloader = self::getContainer()->get(AresDataDownloader::class);
        assert($aresDataDownloader instanceof AresDataDownloader);

        $responseDto = $aresDataDownloader->downloadAresData('87825317');

        self::assertEquals(
            new AresDataResponse(
                ico: '87825317',
                name: 'Bc. Martin Vik',
                smartAddressId: 16974131,
                vatPayer: true,
                vatNumber: 'CZ9202103856',
                firstname: 'Martin',
                surname: 'Vik',
                person: true,
                isForeign: false,
                subjectType: AresSubjectType::LOCAL_PERSON,
            ),
            $responseDto,
        );

        $responseDto = $aresDataDownloader->downloadAresData('06596193');
        self::assertEquals(
            new AresDataResponse(
                ico: '06596193',
                name: 'UX Fans s.r.o.',
                smartAddressId: 23713011,
                vatPayer: true,
                vatNumber: 'CZ06596193',
                firstname: null,
                surname: null,
                person: false,
                isForeign: false,
                subjectType: AresSubjectType::LOCAL_COMPANY,
            ),
            $responseDto,
        );

        $responseDto = $aresDataDownloader->downloadAresData('29140056');
        self::assertEquals(
            new AresDataResponse(
                ico: '29140056',
                name: 'Appio Digital s.r.o.',
                smartAddressId: 22075691,
                vatPayer: true,
                vatNumber: 'CZ29140056',
                firstname: null,
                surname: null,
                person: false,
                isForeign: false,
                subjectType: AresSubjectType::LOCAL_COMPANY,
            ),
            $responseDto,
        );

        $client->request('GET', '/api/app/public/ares?ico=09198849');

        self::assertSame(200, $client->getResponse()->getStatusCode());
        self::assertSame(
            [
                'ico' => '09198849',
                'name' => 'Tomáš Červený',
                'smartAddressId' => 23716584,
                'vatPayer' => false,
                'vatNumber' => null,
                'firstname' => 'Tomáš',
                'surname' => 'Červený',
                'person' => true,
                'isForeign' => false,
                'subjectType' => AresSubjectType::LOCAL_PERSON->value,
            ],
            Json::decode((string) $client->getResponse()->getContent(), 1),
        );

        $this->expectException(AresDownloadingException::class);
        $aresDataDownloader->downloadAresData('111');
    }
}
