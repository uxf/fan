# UXF aRES

## Install
```
$ composer req uxf/ares
```

## Config

```yaml
# config/routes/uxf_ares.yaml
uxf_gen:
  resource: '@UXFAresBundle/config/routes.php'
```
