<?php

declare(strict_types=1);

namespace UXF\Hydrator;

use ArrayAccess;

/**
 * @implements ArrayAccess<string, mixed>
 */
final class Options implements ArrayAccess
{
    /**
     * @param array<string, mixed> $values
     */
    public function __construct(
        public readonly string $name,
        private array $values = [],
    ) {
    }

    /**
     * @internal
     */
    public function merge(self $options): self
    {
        return new self($this->name, $this->values + $options->values);
    }

    public function offsetExists(mixed $offset): bool
    {
        return array_key_exists($offset, $this->values);
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->values[$offset];
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->values[$offset] = $value;
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->values[$offset]);
    }
}
