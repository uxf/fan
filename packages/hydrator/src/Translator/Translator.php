<?php

declare(strict_types=1);

namespace UXF\Hydrator\Translator;

interface Translator
{
    public function trans(string $key, ?ErrorInfo $info = null): string;
}
