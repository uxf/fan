<?php

declare(strict_types=1);

namespace UXF\Hydrator\Generator;

use DateTimeImmutable;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class DateTimeImmutableParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        $name = $definition->name;

        $body = '// ' . __CLASS__ . "\n";
        $body .= "if (\$data[\$name] instanceof \\" . DateTimeImmutable::class . ") {\n";
        $body .= "    \$_$name = \$data[\$name];\n";
        $body .= "} elseif (is_string(\$data[\$name])) {\n";
        $body .= "    try {\n";
        $body .= "        \$_$name = new \\" . DateTimeImmutable::class . "(\$data[\$name]);\n";
        $body .= "    } catch (\Exception) {\n";
        $body .= "        \$errors[\$path . \$name][] = \$this->translator->trans('date_time_immutable.invalid_format', new ErrorInfo(\$data[\$name], supportedFormat: 'ISO8601'));\n";
        $body .= "    }\n";

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";
        }

        $body .= "} else {\n";
        $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.invalid_value', new ErrorInfo(\$data[\$name]));\n";
        $body .= "}\n";
        return $body;
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        return !$definition->isUnion() && !$definition->array && $definition->getFirstType() === DateTimeImmutable::class;
    }

    public static function getDefaultPriority(): int
    {
        return 100;
    }
}
