<?php

declare(strict_types=1);

namespace UXF\Hydrator\Generator;

use LogicException;
use Nette\Utils\Strings;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class ScalarArrayParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        return match ($definition->getFirstType()) {
            'int' => $this->castInt($definition, $options),
            'float' => $this->castFloat($definition, $options),
            'bool' => $this->castBool($definition, $options),
            'string' => $this->castString($definition, $options),
            default => throw new LogicException(),
        };
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        return $definition->array &&
            !$definition->isUnion() &&
            in_array($definition->getFirstType(), ['int', 'float', 'bool', 'string'], true);
    }

    public static function getDefaultPriority(): int
    {
        return 100;
    }

    private function castInt(ParameterDefinition $definition, Options $options): string
    {
        $name = $definition->name;
        $xml = (bool) ($options['xml_mode'] ?? false);

        $body = '// ' . __CLASS__ . " - int\n";
        if ($xml) {
            $body .= "if (!is_array(\$data[\$name])) {\n";
            $body .= "    \$data[\$name] = \$data[\$name] !== '' ? [\$data[\$name]] : [];\n";
            $body .= "}\n";
        }
        $body .= "if (is_array(\$data[\$name])) {\n";
        $body .= "    \$_$name = [];\n";
        $body .= "    \$isList = array_is_list(\$data[\$name]);\n";
        $body .= "    foreach (\$data[\$name] as \$key => \$value) {\n";
        $body .= "        \$xPath = \$isList ? \"{\$name}[\$key]\" : \"{\$name}.\$key\";\n";

        if ($xml) {
            $body .= "        if (is_numeric(\$value)) {\n";
            $body .= "            \$_{$name}[\$key] = (int) \$value;\n";
        } else {
            $body .= "        if (is_int(\$value)) {\n";
            $body .= "            \$_{$name}[\$key] = \$value;\n";
        }

        $body .= "        } else {\n";
        $body .= "            \$errors[\$path . \$xPath][] = \$this->translator->trans('int.invalid_value', new ErrorInfo(\$value));\n";
        $body .= "        }\n";
        $body .= "    }\n";

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";
        }

        $body .= "} else {\n";
        $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.array_invalid_value', new ErrorInfo(\$data[\$name]));\n";
        $body .= "}\n";

        return $body;
    }

    private function castFloat(ParameterDefinition $definition, Options $options): string
    {
        $name = $definition->name;
        $xml = (bool) ($options['xml_mode'] ?? false);

        $body = '// ' . __CLASS__ . " - float\n";
        if ($xml) {
            $body .= "if (!is_array(\$data[\$name])) {\n";
            $body .= "    \$data[\$name] = \$data[\$name] !== '' ? [\$data[\$name]] : [];\n";
            $body .= "}\n";
        }
        $body .= "if (is_array(\$data[\$name])) {\n";
        $body .= "    \$_$name = [];\n";
        $body .= "    \$isList = array_is_list(\$data[\$name]);\n";
        $body .= "    foreach (\$data[\$name] as \$key => \$value) {\n";
        $body .= "        \$xPath = \$isList ? \"{\$name}[\$key]\" : \"{\$name}.\$key\";\n";
        $body .= "        if (is_numeric(\$value)) {\n";
        $body .= "            \$_{$name}[\$key] = (float) \$value;\n";
        $body .= "        } else {\n";
        $body .= "            \$errors[\$path . \$xPath][] = \$this->translator->trans('float.invalid_value', new ErrorInfo(\$value));\n";
        $body .= "        }\n";
        $body .= "    }\n";

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";
        }

        $body .= "} else {\n";
        $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.array_invalid_value', new ErrorInfo(\$data[\$name]));\n";
        $body .= "}";

        return $body;
    }

    private function castBool(ParameterDefinition $definition, Options $options): string
    {
        $name = $definition->name;
        $xml = (bool) ($options['xml_mode'] ?? false);

        $body = '// ' . __CLASS__ . " - bool\n";
        if ($xml) {
            $body .= "if (!is_array(\$data[\$name])) {\n";
            $body .= "    \$data[\$name] = [\$data[\$name]];\n";
            $body .= "}\n";
        }
        $body .= "if (is_array(\$data[\$name])) {\n";
        $body .= "    \$_$name = [];\n";
        $body .= "    \$isList = array_is_list(\$data[\$name]);\n";
        $body .= "    foreach (\$data[\$name] as \$key => \$value) {\n";
        $body .= "        \$xPath = \$isList ? \"{\$name}[\$key]\" : \"{\$name}.\$key\";\n";

        if ($xml) {
            $body .= "        if (true) {\n";
            $body .= "            \$_{$name}[\$key] = in_array(\$value, ['true', '', '1'], true);\n";
        } else {
            $body .= "        if (is_bool(\$value)) {\n";
            $body .= "            \$_{$name}[\$key] = \$value;\n";
        }

        $body .= "        } else {\n";
        $body .= "            \$errors[\$path . \$xPath][] = \$this->translator->trans('bool.invalid_value', new ErrorInfo(\$value));\n";
        $body .= "        }\n";
        $body .= "    }\n";

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";
        }

        $body .= "} else {\n";
        $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.array_invalid_value', new ErrorInfo(\$data[\$name]));\n";
        $body .= "}";

        return $body;
    }

    private function castString(ParameterDefinition $definition, Options $options): string
    {
        $name = $definition->name;
        $trim = (bool) ($options['allow_trim_string'] ?? false);
        $xml = (bool) ($options['xml_mode'] ?? false);

        $body = '// ' . __CLASS__ . " - string\n";
        if ($xml) {
            $body .= "if (!is_array(\$data[\$name])) {\n";
            $body .= "    \$data[\$name] = \$data[\$name] !== '' ? [\$data[\$name]] : [];\n";
            $body .= "}\n";
        }
        $body .= "if (is_array(\$data[\$name])) {\n";
        $body .= "    \$_$name = [];\n";
        $body .= "    \$isList = array_is_list(\$data[\$name]);\n";
        $body .= "    foreach (\$data[\$name] as \$key => \$value) {\n";
        $body .= "        \$xPath = \$isList ? \"{\$name}[\$key]\" : \"{\$name}.\$key\";\n";
        $body .= "        if (is_string(\$value)) {\n";
        $body .= $trim
            ? "            \$_{$name}[\$key] = \\" . Strings::class . "::trim(\$value);\n"
            : "            \$_{$name}[\$key] = \$value;\n";

        // allow lax string
        if (($options['allow_lax_string'] ?? false) === true) {
            $body .= "        } elseif (\$value !== null && (is_scalar(\$value) || \$value instanceof \Stringable)) {\n";
            $body .= $trim
                ? "            \$_{$name}[\$key] = \\" . Strings::class . "::trim((string) \$value);\n"
                : "            \$_{$name}[\$key] = (string) \$value;\n";
        }

        $body .= "        } else {\n";
        $body .= "            \$errors[\$path . \$xPath][] = \$this->translator->trans('string.invalid_value', new ErrorInfo(\$value));\n";
        $body .= "        }\n";
        $body .= "    }\n";

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";
        }

        $body .= "} else {\n";
        $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.array_invalid_value', new ErrorInfo(\$data[\$name]));\n";
        $body .= "}";

        return $body;
    }
}
