<?php

declare(strict_types=1);

namespace UXF\Hydrator\Generator;

use BackedEnum;
use Nette\PhpGenerator\Dumper;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class EnumArrayParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        $typeName = $definition->getFirstType();
        $phpType = is_string($typeName::cases()[0]->value) ? 'string' : 'int';
        $supportedValues = (new Dumper())->dump(array_map(static fn (BackedEnum $case) => $case->value, $typeName::cases()));

        $name = $definition->name;

        $body = '// ' . __CLASS__ . " - $phpType\n";
        $body .= "if (is_array(\$data[\$name])) {\n";
        $body .= "    \$_$name = [];\n";
        $body .= "    \$isList = array_is_list(\$data[\$name]);\n";
        $body .= "    foreach (\$data[\$name] as \$key => \$value) {\n";
        $body .= "        \$xPath = \$isList ? \"{\$name}[\$key]\" : \"{\$name}.\$key\";\n";
        $body .= "        if (\$value instanceof \\$typeName) {\n";
        $body .= "            \$_{$name}[\$key] = \$value;\n";
        $body .= "        } elseif (is_$phpType(\$value)) {\n";
        $body .= "            \$_{$name}[\$key] = \$item = \\$typeName::tryFrom(\$value);\n";
        $body .= "            if (\$item === null) {\n";
        $body .= "                \$errors[\$path . \$xPath][] = \$this->translator->trans('enum.invalid_value', new ErrorInfo(\$value, supportedValues: $supportedValues));\n";
        $body .= "            }\n";
        $body .= "        } else {\n";
        $body .= "            \$errors[\$path . \$xPath][] = \$this->translator->trans('core.invalid_value', new ErrorInfo(\$value));\n";
        $body .= "        }\n";
        $body .= "    }\n";

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";
        }

        $body .= "} else {\n";
        $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.array_invalid_value', new ErrorInfo(\$data[\$name]));\n";
        $body .= "}\n";

        return $body;
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        return !$definition->isUnion() && $definition->array && enum_exists($definition->getFirstType());
    }

    public static function getDefaultPriority(): int
    {
        return 200;
    }
}
