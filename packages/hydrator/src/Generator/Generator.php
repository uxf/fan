<?php

declare(strict_types=1);

namespace UXF\Hydrator\Generator;

use LogicException;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Dumper;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\PsrPrinter;
use Nette\Utils\Strings;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use UXF\Hydrator\Attribute\HydratorMap;
use UXF\Hydrator\Exception\HydratorException;
use UXF\Hydrator\Exception\UnsupportedParameterDefinitionException;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Inspector\ParameterDefinitionCreator;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;
use UXF\Hydrator\Translator\ErrorInfo;
use UXF\Hydrator\Translator\Translator;
use UXF\Hydrator\TypeCaster;
use UXF\Hydrator\TypeCasterProvider;
use UXF\Hydrator\Utils\ClassNameHelper;

final readonly class Generator
{
    /**
     * @param ParameterGenerator[] $parameterGenerators
     */
    public function __construct(
        private iterable $parameterGenerators,
        private ParameterDefinitionCreator $parameterDefinitionCreator,
        private Dumper $dumper = new Dumper(),
        private PsrPrinter $printer = new PsrPrinter(),
    ) {
    }

    /**
     * @phpstan-param class-string $className
     */
    public function generateFile(string $className, Options $options): string
    {
        $class = $this->generateClass($className, $options);

        $file = new PhpFile();
        $file->setStrictTypes();

        $namespace = new PhpNamespace("UxfGenerator\\$options->name");
        $namespace->add($class);
        $namespace->addUse(TypeCaster::class);
        $namespace->addUse(TypeCasterProvider::class);
        $namespace->addUse(HydratorException::class);
        $namespace->addUse(ContainerInterface::class);
        $namespace->addUse(Translator::class);
        $namespace->addUse(Options::class);
        $namespace->addUse(ErrorInfo::class);
        $file->addNamespace($namespace);

        return $this->printer->printFile($file);
    }

    /**
     * @phpstan-param class-string $className
     */
    public function generateClass(string $className, Options $options): ClassType
    {
        $reflectionClass = new ReflectionClass($className);

        $mapAttribute = $reflectionClass->getAttributes(HydratorMap::class)[0] ?? null;
        if ($mapAttribute === null) {
            return $this->innerGenerateClass($reflectionClass, $options);
        }

        /** @var HydratorMap $hydratorMap */
        $hydratorMap = $mapAttribute->newInstance();

        $body = "if (!array_key_exists('$hydratorMap->property', \$data)) {\n";
        $body .= "    throw new HydratorException([\$path . '$hydratorMap->property' => [\$this->translator->trans('core.missing_value')]]);\n";
        $body .= "}\n\n";

        $body .= "return match (\$data['$hydratorMap->property']) {\n";
        foreach ($hydratorMap->matrix as $property => $class) {
            $body .= "    '$property' => \$this->typeCasterProvider->get('$class', \$this->options)->cast(\$data, \$path),\n";
        }
        $supportedValues = $this->dumper->dump(array_keys($hydratorMap->matrix));
        $body .= "    default => throw new HydratorException([\$path . '$hydratorMap->property' => [\$this->translator->trans('core.invalid_value', new ErrorInfo(\$data['$hydratorMap->property'], supportedValues: $supportedValues))]]),\n";
        $body .= "};";

        return $this->generateClassType($className, $body);
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     */
    private function innerGenerateClass(ReflectionClass $reflectionClass, Options $options): ClassType
    {
        $className = $reflectionClass->getName();
        $constructor = $reflectionClass->getConstructor();
        if ($constructor === null) {
            throw new LogicException();
        }

        $definitions = [];
        foreach ($constructor->getParameters() as $reflectionParameter) {
            $definitions[] = $this->parameterDefinitionCreator->create($reflectionParameter);
        }

        $body = "\$errors = [];\n";

        foreach ($definitions as $definition) {
            $body .= $this->generateParameter($definition, $options);
        }

        // catch errors
        $body .= "\n";
        $body .= "if (\$errors !== []) {\n";
        $body .= "    throw new HydratorException(\$errors);\n";
        $body .= "}\n\n";

        // object constructor
        $body .= "return new \\$className(\n";
        foreach ($definitions as $definition) {
            $name = $definition->name;
            $body .= "    $name: \$_$name,\n";
        }
        $body .= ');';

        return $this->generateClassType($className, $body);
    }

    private function generateClassType(string $className, string $body): ClassType
    {
        $typeCasterName = ClassNameHelper::convertToTypeCasterName($className);

        $class = new ClassType($typeCasterName);
        $class->addImplement(TypeCaster::class);
        $construct = $class->addMethod('__construct');

        $construct->addPromotedParameter('typeCasterProvider')
            ->setType(TypeCasterProvider::class)
            ->setPrivate();

        $construct->addPromotedParameter('container')
            ->setType(ContainerInterface::class)
            ->setPrivate();

        $construct->addPromotedParameter('translator')
            ->setType(Translator::class)
            ->setPrivate();

        $construct->addPromotedParameter('options')
            ->setType(Options::class)
            ->setPrivate();

        $castMethod = $class->addMethod('cast')
            ->addComment('@param array<string, mixed> $data')
            ->setReturnType('object')
            ->setBody($body);
        $castMethod->addParameter('data')
            ->setType('array');
        $castMethod->addParameter('path')
            ->setType('string');

        return $class;
    }

    private function generateParameter(ParameterDefinition $definition, Options $options): string
    {
        $name = $definition->name;

        $body = "\n";
        if ($definition->optional) {
            $body .= "\$_$name = " . $this->dumper->dump($definition->defaultValue) . ";";
        } else {
            $body .= "\$_$name = null;";
        }

        $body .= "\n";

        if ($definition->propertyInfo !== null && $definition->propertyInfo->name !== '') {
            $name = $definition->propertyInfo->name;
        }

        $body .= "\$name = array_key_exists('$name', \$data) ? '$name' : null;\n";
        $body .= "if (\$name !== null) {\n";

        // retype section
        $handled = false;
        foreach ($this->parameterGenerators as $parameterGenerator) {
            if ($parameterGenerator->supports($definition, $options)) {
                $body .= Strings::indent($parameterGenerator->generate($definition, $options) . "\n", 4, ' ');
                $handled = true;
                break;
            }
        }

        if ($handled === false) {
            throw new UnsupportedParameterDefinitionException($definition, $options);
        }

        if (!$definition->optional && ($options['nullable_optional'] ?? false) !== true) {
            $body .= "} else {\n";
            $body .= "    \$errors[\$path . '$name'][] = \$this->translator->trans('core.missing_value');\n";
            $body .= "}\n";
        } else {
            $body .= "}\n";
        }

        return $body;
    }
}
