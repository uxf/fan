<?php

declare(strict_types=1);

namespace UXF\Hydrator\Generator;

use UXF\Hydrator\Inspector\ParameterDefinition;

final readonly class GeneratorHelper
{
    public static function new(
        string $generatorClass,
        ParameterDefinition $definition,
        string $errorMsgKey,
        string $errorArgs,
    ): string {
        $type = $definition->getFirstType();
        return self::gen(
            generatorClass: $generatorClass,
            definition: $definition,
            constructor: "new \\" . $type . "(\$data[\$name])",
            constructorArray: "new \\" . $type . "(\$value)",
            errorMsgKey: $errorMsgKey,
            errorArgs: $errorArgs,
        );
    }

    public static function of(
        string $generatorClass,
        ParameterDefinition $definition,
        string $errorMsgKey,
        string $errorArgs,
    ): string {
        $type = $definition->getFirstType();
        return self::gen(
            generatorClass: $generatorClass,
            definition: $definition,
            constructor: "\\" . $type . "::of(\$data[\$name])",
            constructorArray: "\\" . $type . "::of(\$value)",
            errorMsgKey: $errorMsgKey,
            errorArgs: $errorArgs,
        );
    }

    public static function gen(
        string $generatorClass,
        ParameterDefinition $definition,
        string $constructor,
        string $constructorArray,
        string $errorMsgKey,
        string $errorArgs,
    ): string {
        $name = $definition->name;
        $type = $definition->getFirstType();
        $body = '// ' . $generatorClass . "\n";

        if (!$definition->array) {
            $body .= 'if ($data[$name] instanceof \\' . $type . ") {\n";
            $body .= "    \$_$name = \$data[\$name];\n";
            $body .= "} elseif (is_string(\$data[\$name])) {\n";
            $body .= "    try {\n";
            $body .= "        \$_$name = $constructor;\n";
            $body .= "    } catch (\Exception \$e) {\n";
            $body .= "        \$errors[\$path . \$name][] = \$this->translator->trans('$errorMsgKey', new ErrorInfo(\$data[\$name], $errorArgs));\n";
            $body .= "    }\n";
        } else {
            $body .= "if (is_array(\$data[\$name])) {\n";
            $body .= "    \$_$name = [];\n";
            $body .= "    \$isList = array_is_list(\$data[\$name]);\n";
            $body .= "    foreach (\$data[\$name] as \$key => \$value) {\n";
            $body .= "        \$xPath = \$isList ? \"{\$name}[\$key]\" : \"{\$name}.\$key\";\n";
            $body .= "        if (\$value instanceof \\" . $type . ") {\n";
            $body .= "            \$_{$name}[\$key] = \$value;\n";
            $body .= "        } elseif (is_string(\$value)) {\n";
            $body .= "            try {\n";
            $body .= "                \$_{$name}[\$key] = $constructorArray;\n";
            $body .= "            } catch (\Exception \$e) {\n";
            $body .= "                \$errors[\$path . \$xPath][] = \$this->translator->trans('$errorMsgKey', new ErrorInfo(\$value, $errorArgs));\n";
            $body .= "            }\n";
            $body .= "        } else {\n";
            $body .= "            \$errors[\$path . \$xPath][] = \$this->translator->trans('core.invalid_value', new ErrorInfo(\$value));\n";
            $body .= "        }\n";
            $body .= "    }\n";
        }

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";
        }

        $msg = $definition->array ? 'array_invalid_value' : 'invalid_value';

        $body .= "} else {\n";
        $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.$msg', new ErrorInfo(\$data[\$name]));\n";
        $body .= "}\n";

        return $body;
    }
}
