<?php

declare(strict_types=1);

namespace UXF\Hydrator\Exception;

use Exception;
use function Safe\json_encode;

final class HydratorCollectionException extends Exception implements HydratorCoreException
{
    /**
     * @param array<int|string, HydratorException> $exceptions
     */
    public function __construct(public readonly array $exceptions)
    {
        $errors = array_map(static fn (HydratorException $e) => $e->getMessage(), $this->exceptions);
        parent::__construct(json_encode($errors));
    }
}
