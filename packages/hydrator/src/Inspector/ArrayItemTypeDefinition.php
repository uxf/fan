<?php

declare(strict_types=1);

namespace UXF\Hydrator\Inspector;

final readonly class ArrayItemTypeDefinition
{
    /**
     * @param string[] $types
     */
    public function __construct(
        public array $types,
        public bool $nullable,
    ) {
    }
}
