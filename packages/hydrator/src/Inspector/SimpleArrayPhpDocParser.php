<?php

declare(strict_types=1);

namespace UXF\Hydrator\Inspector;

use LogicException;
use Nette\Utils\Strings;
use phpDocumentor\Reflection\TypeResolver;
use phpDocumentor\Reflection\Types\Context;
use phpDocumentor\Reflection\Types\ContextFactory;
use ReflectionParameter;
use function Safe\preg_split;

// TODO replace by some tokenizer!
final readonly class SimpleArrayPhpDocParser implements ArrayPhpDocParser
{
    public function __construct(
        private TypeResolver $typeResolver = new TypeResolver(),
        private ContextFactory $contextFactory = new ContextFactory(),
    ) {
    }

    public function resolveTypes(ReflectionParameter $reflectionParameter): ?ArrayItemTypeDefinition
    {
        $name = $reflectionParameter->name;

        $phpDoc = $reflectionParameter->getDeclaringFunction()->getDocComment();
        if ($phpDoc === false) {
            throw new LogicException();
        }

        $types = $this->parsePhpDoc(
            $phpDoc,
            $this->contextFactory->createFromReflector($reflectionParameter->getDeclaringClass() ?? throw new LogicException()),
        );

        return $types[$name] ?? null;
    }

    /**
     * @return array<string, ArrayItemTypeDefinition>
     */
    public function parsePhpDoc(string $phpDoc, Context $context): array
    {
        $lines = preg_split("/\r\n|\n|\r/", $phpDoc);

        $result = [];
        foreach ($lines as $line) {
            $m = Strings::match($line, '/^.*@param (.*) \$([a-z0-9_]+).*$/i');
            if ($m !== null) {
                $result[$m[2]] = $this->parseTypes(trim($m[1]), $context);
            }
        }
        return $result;
    }

    private function parseTypes(string $string, Context $context): ArrayItemTypeDefinition
    {
        // remove null and other stuff
        $string = Strings::replace($string, '/\|null.*/');

        if (!str_starts_with($string, 'array') && !str_starts_with($string, 'list')) {
            // Type[]
            $string = Strings::replace($string, '/\[\]\|.*/'); // remove union
            $string = str_replace('[]', '', $string);
            return new ArrayItemTypeDefinition([$this->resolveFQCN($string, $context)], false);
        }

        // array<...>
        $string = Strings::replace($string, '/\>\|.*/'); // remove union

        // nested array - semi-support
        if (substr_count($string, 'array') > 1) {
            return new ArrayItemTypeDefinition(['mixed'], false);
        }

        $string = str_replace(['array<', 'list<', '>'], ['', '', ''], $string);
        if (!str_contains($string, ',')) {
            // array<Type> or list<Type>
            return new ArrayItemTypeDefinition([$this->resolveFQCN($string, $context)], false);
        }

        [, $string] = explode(',', $string);
        return new ArrayItemTypeDefinition([$this->resolveFQCN($string, $context)], false);
    }

    private function resolveFQCN(string $shortName, Context $context): string
    {
        return trim((string) $this->typeResolver->resolve($shortName, $context), '\\');
    }
}
