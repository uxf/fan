<?php

declare(strict_types=1);

namespace UXF\Hydrator\Inspector;

use ReflectionParameter;

interface ArrayPhpDocParser
{
    public function resolveTypes(ReflectionParameter $reflectionParameter): ?ArrayItemTypeDefinition;
}
