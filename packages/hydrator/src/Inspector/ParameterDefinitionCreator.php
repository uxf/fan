<?php

declare(strict_types=1);

namespace UXF\Hydrator\Inspector;

use LogicException;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionUnionType;
use UXF\Hydrator\Attribute\HydratorProperty;

final readonly class ParameterDefinitionCreator
{
    /**
     * @param string[] $ignoredTypes
     */
    public function __construct(
        private array $ignoredTypes,
        private ArrayPhpDocParser $arrayPhpDocParser,
    ) {
    }

    public function create(ReflectionParameter $reflectionParameter): ParameterDefinition
    {
        $type = $reflectionParameter->getType();
        $types = match (true) {
            $type instanceof ReflectionNamedType => [$type->getName()],
            $type instanceof ReflectionUnionType => array_map(static fn (ReflectionNamedType $t) => $t->getName(), $type->getTypes()),
            default => [],
        };

        $array = in_array('array', $types, true);
        $nullable = $reflectionParameter->allowsNull() || in_array('null', $types, true);
        $arrayItemNullable = false;
        $defaultValue = $reflectionParameter->isDefaultValueAvailable() ? $reflectionParameter->getDefaultValue() : null;
        $types = array_filter($types, fn (string $type) => $type !== 'null' && !in_array($type, $this->ignoredTypes, true));

        // if array -> start phpdoc inspection
        if ($array) {
            $typeDefinition = $this->arrayPhpDocParser->resolveTypes($reflectionParameter);
            if ($typeDefinition === null) {
                $className = $reflectionParameter->getDeclaringClass()?->name;
                throw new LogicException("$className::\$$reflectionParameter->name has no type");
            }

            $types = $typeDefinition->types;
            $arrayItemNullable = $typeDefinition->nullable;
        }

        if ($types === []) {
            $className = $reflectionParameter->getDeclaringClass()?->name;
            throw new LogicException("$className::\$$reflectionParameter->name has no type");
        }

        $attribute = $reflectionParameter->getAttributes(HydratorProperty::class)[0] ?? null;

        return new ParameterDefinition(
            name: $reflectionParameter->name,
            types: array_values($types),
            array: $array,
            optional: $reflectionParameter->isDefaultValueAvailable(),
            nullable: $nullable,
            arrayItemNullable: $arrayItemNullable,
            defaultValue: $defaultValue,
            propertyInfo: $attribute?->newInstance(),
            attributes: $reflectionParameter->getAttributes(),
        );
    }
}
