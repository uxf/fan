<?php

declare(strict_types=1);

namespace UXF\Hydrator;

/**
 * @template T of object
 */
interface TypeCaster
{
    /**
     * @param array<string, mixed> $data
     * @phpstan-return T
     */
    public function cast(array $data, string $path): object;
}
