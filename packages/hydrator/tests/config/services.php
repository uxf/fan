<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\HydratorTests\Project\FunnyService;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure()
        ->public();

    $services->set(FunnyService::class);

    $containerConfigurator->extension('framework', [
        'test' => true,
        'validation' => [
            'email_validation_mode' => 'html5',
        ],
        'http_method_override' => false,
    ]);

    $containerConfigurator->extension('uxf_hydrator', [
        'overwrite' => true,
        'default_options' => [
            'allow_fallback' => true,
        ],
    ]);
};
