<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects;

use Stringable;

final readonly class Ska implements Stringable
{
    public function __construct(
        public string $string,
        public ?string $null,
    ) {
    }

    public function __toString(): string
    {
        return $this->string;
    }
}
