<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects\Family;

use UXF\Hydrator\Attribute\HydratorMap;

#[HydratorMap(property: 'type', matrix: [
    'o' => Orienteering::class,
    'p' => Paragliding::class,
])]
interface Activity
{
}
