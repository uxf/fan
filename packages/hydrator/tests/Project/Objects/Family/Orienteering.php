<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects\Family;

class Orienteering extends Sport
{
    public function __construct(
        public readonly int $card,
    ) {
    }
}
