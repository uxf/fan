<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects;

class Reggae
{
    public function __construct(public float $float)
    {
    }
}
