<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects;

enum SwingEnum: int
{
    case ZERO = 0;
    case ONE = 1;
}
