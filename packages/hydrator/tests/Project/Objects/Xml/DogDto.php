<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects\Xml;

use UXF\Hydrator\Attribute\HydratorProperty;

final readonly class DogDto
{
    /**
     * @param string[] $items
     * @param CatDto[] $cats
     */
    public function __construct(
        public int $id,
        public string $name,
        public float $price,
        public bool $valid = false,
        #[HydratorProperty('item')]
        public array $items = [],
        #[HydratorProperty('cat')]
        public array $cats = [],
    ) {
    }
}
