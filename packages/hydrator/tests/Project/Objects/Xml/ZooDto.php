<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects\Xml;

final readonly class ZooDto
{
    public function __construct(
        public DogDto $dog,
        public ?DogDto $emptyDog,
        public DogsDto $oneDogs,
        public DogsDto $twoDogs,
        public ?DogsDto $emptyDogs,
        public DogsDto $naughtyDogs,
    ) {
    }
}
