<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects\Xml;

use UXF\Hydrator\Attribute\HydratorProperty;
use UXF\Hydrator\Attribute\HydratorXml;

#[HydratorXml('text')]
final readonly class CatDto
{
    public function __construct(
        #[HydratorProperty('#')]
        public string $text,
        #[HydratorProperty('@ID')]
        public ?int $id = null,
    ) {
    }
}
