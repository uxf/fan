<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects;

enum JazzEnum: string
{
    case ALPHA = 'A';
    case BRAVO = 'B';
}
