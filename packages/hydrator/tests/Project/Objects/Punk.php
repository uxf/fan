<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects;

class Punk
{
    /**
     * @param string[] $arrayX
     * @param Metal[] $arrayObjectX
     * @param array<string, JazzEnum> $arrayEnum
     * @param array<mixed> $arrayMixed
     * @param string[]|null $arrayNull
     * @param Metal[]|null $arrayObjectNull
     * @param JazzEnum[]|null $arrayEnumNull
     * @param array<mixed>|null $arrayMixedNull
     * @param string[]|null $arrayOptional
     * @param Metal[]|null $arrayObjectOptional
     * @param JazzEnum[]|null $arrayEnumOptional
     * @param string[] $arrayDefault
     * @param Metal[] $arrayObjectDefault
     * @param JazzEnum[] $arrayEnumDefault
     */
    public function __construct(
        public string $stringX,
        public int $intX,
        public float $floatX,
        public bool $boolX,
        public Metal $objectX,
        public array $arrayX,
        public array $arrayObjectX,
        public array $arrayEnum,
        public JazzEnum $stringEnumX,
        public SwingEnum $intEnumX,
        public int | string $unionX,
        public mixed $mixedX,
        public array $arrayMixed,
        //
        public ?string $stringNull,
        public ?int $intNull,
        public ?float $floatNull,
        public ?bool $boolNull,
        public ?Metal $objectNull,
        public ?array $arrayNull,
        public ?array $arrayObjectNull,
        public ?array $arrayEnumNull,
        public ?JazzEnum $stringEnumNull,
        public ?SwingEnum $intEnumNull,
        public int | string | null $unionNull,
        public ?array $arrayMixedNull,
        //
        public ?string $stringOptional = null,
        public ?int $intOptional = null,
        public ?float $floatOptional = null,
        public ?bool $boolOptional = null,
        public ?Metal $objectOptional = null,
        public ?array $arrayOptional = null,
        public ?array $arrayObjectOptional = null,
        public ?array $arrayEnumOptional = null,
        public ?JazzEnum $stringEnumOptional = null,
        public ?SwingEnum $intEnumOptional = null,
        public int | string | null $unionOptional = null,
        //
        public string $stringDefault = '',
        public int $intDefault = 0,
        public float $floatDefault = 0,
        public bool $boolDefault = true,
        public Metal $objectDefault = new Metal(0),
        public array $arrayDefault = [],
        public array $arrayObjectDefault = [],
        public array $arrayEnumDefault = [],
        public JazzEnum $stringEnumDefault = JazzEnum::ALPHA,
        public SwingEnum $intEnumDefault = SwingEnum::ZERO,
        public int | string | null $unionDefault = 0,
        //
        public int | null | IgnoredType $ignoredType = new IgnoredType(),
    ) {
    }
}
