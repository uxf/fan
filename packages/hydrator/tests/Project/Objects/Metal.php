<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects;

class Metal
{
    public function __construct(
        public int $id,
    ) {
    }
}
