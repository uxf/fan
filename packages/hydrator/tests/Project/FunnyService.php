<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project;

use UXF\Hydrator\ObjectHydrator;

class FunnyService
{
    public function __construct(private readonly ObjectHydrator $objectHydrator)
    {
    }

    public function getObjectHydrator(): ObjectHydrator
    {
        return $this->objectHydrator;
    }
}
