<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Hydrator\Generator\DateTimeImmutableParameterGenerator;
use UXF\Hydrator\Generator\EnumArrayParameterGenerator;
use UXF\Hydrator\Generator\EnumParameterGenerator;
use UXF\Hydrator\Generator\FallbackParameterGenerator;
use UXF\Hydrator\Generator\Generator;
use UXF\Hydrator\Generator\ObjectArrayParameterGenerator;
use UXF\Hydrator\Generator\ObjectParameterGenerator;
use UXF\Hydrator\Generator\ScalarArrayParameterGenerator;
use UXF\Hydrator\Generator\ScalarParameterGenerator;
use UXF\Hydrator\Inspector\ArrayPhpDocParser;
use UXF\Hydrator\Inspector\ParameterDefinitionCreator;
use UXF\Hydrator\Inspector\SimpleArrayPhpDocParser;
use UXF\Hydrator\ObjectHydrator;
use UXF\Hydrator\TypeCasterProvider;
use function Symfony\Component\DependencyInjection\Loader\Configurator\tagged_iterator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $parameters = $containerConfigurator->parameters();
    $parameters->set('uxf_hydrator.type_caster_dir', '%kernel.cache_dir%/hydrator');

    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $services->alias(ContainerInterface::class, 'service_container');

    $services->set(ArrayPhpDocParser::class, SimpleArrayPhpDocParser::class);

    $services->set(ObjectHydrator::class)
        ->factory([ObjectHydrator::class, 'create'])
        ->arg('$options', '%uxf_hydrator.default_options%');

    $services->set(ParameterDefinitionCreator::class)
        ->arg('$ignoredTypes', '%uxf_hydrator.ignored_types%')
        ->autowire();

    $services->set(TypeCasterProvider::class)
        ->arg('$overwrite', '%uxf_hydrator.overwrite%')
        ->arg('$typeCasterDir', '%uxf_hydrator.type_caster_dir%');

    $services->set(Generator::class)
        ->arg('$parameterGenerators', tagged_iterator('uxf_hydrator.parameter_generator'));

    $services->set(DateTimeImmutableParameterGenerator::class);
    $services->set(EnumArrayParameterGenerator::class);
    $services->set(EnumParameterGenerator::class);
    $services->set(FallbackParameterGenerator::class);
    $services->set(ObjectArrayParameterGenerator::class);
    $services->set(ObjectParameterGenerator::class);
    $services->set(ScalarArrayParameterGenerator::class);
    $services->set(ScalarParameterGenerator::class);
};
