<?php

declare(strict_types=1);

namespace UXF\CMS\GQL\Query;

use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Query as LegacyQuery;
use UXF\CMS\Repository\UserConfigRepository;
use UXF\CMS\Security\LoggedUserProvider;
use UXF\GraphQL\Attribute\Query;
use UXF\GraphQL\Type\Json;

final readonly class UserConfigQuery
{
    public function __construct(
        private UserConfigRepository $userConfigRepository,
        private LoggedUserProvider $loggedUserProvider,
    ) {
    }

    #[Logged]
    #[LegacyQuery(name: 'userConfig')]
    #[Query('userConfig')]
    public function __invoke(string $name): Json
    {
        return new Json($this->userConfigRepository->findByName($this->loggedUserProvider->getUser(), $name)?->getData());
    }
}
