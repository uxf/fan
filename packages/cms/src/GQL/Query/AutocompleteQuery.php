<?php

declare(strict_types=1);

namespace UXF\CMS\GQL\Query;

use TheCodingMachine\GraphQLite\Annotations\Query as LegacyQuery;
use UXF\CMS\GQL\AutocompleteResponseProvider;
use UXF\CMS\GQL\Type\Autocomplete;
use UXF\Core\Contract\Permission\PermissionChecker;
use UXF\Core\Exception\BasicException;
use UXF\GraphQL\Attribute\Query;

final readonly class AutocompleteQuery
{
    public function __construct(
        private AutocompleteResponseProvider $responseProvider,
        private ?PermissionChecker $permissionChecker,
    ) {
    }

    /**
     * @return Autocomplete[]
     */
    #[LegacyQuery(name: 'autocomplete')]
    #[Query('autocomplete')]
    public function __invoke(string $name, string $term = '', int $limit = 20): array
    {
        if ($this->permissionChecker?->isAllowed('autocomplete', $name) !== true) {
            throw BasicException::forbidden();
        }

        return $this->responseProvider->get($name, $term, $limit);
    }
}
