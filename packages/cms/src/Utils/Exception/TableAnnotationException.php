<?php

declare(strict_types=1);

namespace UXF\CMS\Utils\Exception;

use Exception;

final class TableAnnotationException extends Exception
{
}
