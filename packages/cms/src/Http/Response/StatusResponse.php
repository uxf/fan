<?php

declare(strict_types=1);

namespace UXF\CMS\Http\Response;

final readonly class StatusResponse
{
    public function __construct(
        public bool $success,
    ) {
    }

    public static function success(): self
    {
        return new self(true);
    }

    public static function error(): self
    {
        return new self(false);
    }
}
