<?php

declare(strict_types=1);

namespace UXF\CMS\Http\Request\RecoveryPassword;

use UXF\Core\Type\Email;

final readonly class ForgottenPasswordRequestBody
{
    public function __construct(
        public Email $email,
        public bool $cms = false,
    ) {
    }
}
