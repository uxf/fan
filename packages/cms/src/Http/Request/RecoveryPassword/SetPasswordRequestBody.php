<?php

declare(strict_types=1);

namespace UXF\CMS\Http\Request\RecoveryPassword;

use Symfony\Component\Validator\Constraints as Assert;

final readonly class SetPasswordRequestBody
{
    public function __construct(
        #[Assert\NotBlank]
        public string $password,
    ) {
    }
}
