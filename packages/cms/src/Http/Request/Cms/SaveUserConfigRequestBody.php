<?php

declare(strict_types=1);

namespace UXF\CMS\Http\Request\Cms;

use Symfony\Component\Validator\Constraints as Assert;

final readonly class SaveUserConfigRequestBody
{
    /**
     * @param mixed[] $data
     */
    public function __construct(
        #[Assert\NotBlank]
        public string $name,
        public array $data,
    ) {
    }
}
