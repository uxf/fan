<?php

declare(strict_types=1);

namespace UXF\CMS\Http\Request\Cms;

final readonly class ChangePasswordRequestBody
{
    public function __construct(
        public string $originalPassword,
        public string $newPassword,
        public string $newPasswordAgain,
    ) {
    }
}
