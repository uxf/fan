<?php

declare(strict_types=1);

namespace UXF\CMS\Attribute;

use Attribute;
use UXF\DataGrid\DataGridSortDir;

/**
 * @deprecated Please use GridType
 */
#[Attribute(Attribute::TARGET_CLASS)]
final readonly class CmsGridDefaults
{
    public function __construct(
        public string $sortColumnName = 'id',
        public DataGridSortDir $sortColumnDirection = DataGridSortDir::ASC,
        public int $perPage = 10,
    ) {
    }
}
