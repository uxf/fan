<?php

declare(strict_types=1);

namespace UXF\CMS\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Loggable
{
}
