<?php

declare(strict_types=1);

namespace UXF\CMS\Attribute;

use Attribute;

/**
 * @deprecated Please use AutocompleteFields
 */
#[Attribute(Attribute::TARGET_CLASS)]
final readonly class CmsMeta
{
    /**
     * @param string[] $actions
     * @param array<literal-string> $autocompleteFields
     */
    public function __construct(
        public string $alias,
        public string $title,
        public array $actions = ['add', 'edit', 'remove'],
        public array $autocompleteFields = [],
    ) {
    }
}
