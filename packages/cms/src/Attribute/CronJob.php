<?php

declare(strict_types=1);

namespace UXF\CMS\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
final readonly class CronJob
{
    /**
     * @param string[]|null $enabledStages
     */
    public function __construct(
        public string $crontab,
        public ?string $slug = null,
        public ?string $args = null,
        public ?array $enabledStages = null,
        public ?int $sentryMaxRuntime = null,
    ) {
    }
}
