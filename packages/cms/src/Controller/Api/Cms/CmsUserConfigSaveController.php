<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms;

use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Http\Request\Cms\SaveUserConfigRequestBody;
use UXF\CMS\Repository\UserConfigRepository;
use UXF\CMS\Security\LoggedUserProvider;
use UXF\CMS\Service\UserConfigService;
use UXF\Core\Attribute\FromBody;

final readonly class CmsUserConfigSaveController
{
    public function __construct(
        private UserConfigRepository $userConfigRepository,
        private UserConfigService $userConfigService,
        private LoggedUserProvider $loggedUserProvider,
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    #[Route('/api/cms/user-config', name: 'cms_user_config_save', methods: 'POST')]
    public function __invoke(#[FromBody] SaveUserConfigRequestBody $body): array
    {
        $loggedUser = $this->loggedUserProvider->getUser();

        $this->userConfigService->save($body->name, $body->data, $loggedUser);

        $configs = [];
        foreach ($this->userConfigRepository->getAllByUser($loggedUser) as $name => $config) {
            $configs[$name] = [
                'id' => $config->getId(),
                'name' => $config->getName(),
                'data' => $config->getData(),
                'type' => 'cms',
            ];
        }
        return $configs;
    }
}
