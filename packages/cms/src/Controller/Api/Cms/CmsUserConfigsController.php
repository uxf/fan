<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms;

use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Repository\UserConfigRepository;
use UXF\CMS\Security\LoggedUserProvider;

final readonly class CmsUserConfigsController
{
    public function __construct(
        private UserConfigRepository $userConfigRepository,
        private LoggedUserProvider $loggedUserProvider,
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    #[Route('/api/cms/user-config', name: 'cms_user_config_get', methods: 'GET')]
    public function __invoke(): array
    {
        $configs = [];
        foreach ($this->userConfigRepository->getAllByUser($this->loggedUserProvider->getUser()) as $name => $config) {
            $configs[$name] = [
                'id' => $config->getId(),
                'name' => $config->getName(),
                'data' => $config->getData(),
                'type' => 'cms',
            ];
        }
        return $configs;
    }
}
