<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms;

use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Http\Request\Cms\CreateUserRequestBody;
use UXF\CMS\Service\UserCreator;
use UXF\Core\Attribute\FromBody;

final readonly class CmsInviteUserController
{
    public function __construct(private UserCreator $userCreator)
    {
    }

    #[Route('/api/cms/invite-user', name: 'cms_invite_user', methods: 'POST')]
    public function __invoke(#[FromBody] CreateUserRequestBody $body): void
    {
        $this->userCreator->createUser(
            $body->email,
            $body->name,
            $body->surname,
            $body->roles,
            $body->sendInvitation ?? false,
        );
    }
}
