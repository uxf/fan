<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Email;

use Generator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;
use UXF\CMS\Mime\PreviewableEmail;
use UXF\CMS\Service\EmailPreviewService;

readonly class ListMailController
{
    public function __construct(
        private EmailPreviewService $emailPreviewService,
        private UrlGeneratorInterface $router,
        private Environment $twig,
    ) {
    }

    #[Route('/api/cms/email/list', name: 'cms_email_preview_list', methods: 'GET')]
    public function __invoke(): Response
    {
        $classNames = $this->emailPreviewService->getEmailClasses();

        $templates = iterator_to_array($this->prepareTemplates($classNames));
        $html = $this->twig->render(
            '@UXFCms/email-templates.twig',
            [
                'templates' => $templates,
            ],
        );

        return new Response($html);
    }

    /**
     * @param class-string<PreviewableEmail>[] $classNames
     * @return Generator<array{link: string, title: string}>
     */
    private function prepareTemplates(array $classNames): Generator
    {
        foreach ($classNames as $serializedClassName => $className) {
            foreach ($className::getPreviewData() as $variant => $data) {
                yield [
                    'link' => $this->router->generate('cms_email_preview', [
                        'email' => $serializedClassName,
                        'variant' => $variant,
                    ]),
                    'title' => "{$this->humanizeClassName($className)} - {$variant}",
                ];
            }
        }
    }

    private function humanizeClassName(string $class): string
    {
        $explode = explode('\\', $class);
        $length = count($explode);

        return $explode[$length - 1];
    }
}
