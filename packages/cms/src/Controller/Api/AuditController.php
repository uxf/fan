<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api;

use Composer\InstalledVersions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use UXF\Core\Exception\BasicException;

final readonly class AuditController
{
    public function __construct(private string $token)
    {
    }

    /**
     * @return array<string, mixed>
     */
    #[Route('/api/_cms-audit', name: 'cms_audit', methods: 'GET')]
    public function __invoke(Request $request): array
    {
        if ($request->query->get('token') !== $this->token) {
            throw BasicException::forbidden();
        }

        return [
            'php' => PHP_VERSION,
            'packages' => InstalledVersions::getAllRawData(),
        ];
    }
}
