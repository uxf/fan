<?php

declare(strict_types=1);

namespace UXF\CMS\DataGrid;

use BackedEnum;
use ReflectionEnum;
use UXF\CMS\Attribute\CmsLabel;
use UXF\Core\Attribute\Label;
use UXF\DataGrid\Filter\MultiSelectFilter;
use UXF\DataGrid\Filter\SelectFilter;
use UXF\DataGrid\Schema\FilterOption;

final readonly class GridFilterHelper
{
    /**
     * @param class-string<BackedEnum> $class
     * @return ($multi is true ? MultiSelectFilter : SelectFilter)
     */
    public static function createFromEnum(string $name, string $label, string $class, bool $multi = false): SelectFilter|MultiSelectFilter
    {
        $options = [];

        foreach ((new ReflectionEnum($class))->getCases() as $caseRef) {
            $labelAttr = $caseRef->getAttributes(Label::class)[0] ?? $caseRef->getAttributes(CmsLabel::class)[0] ?? null;
            if ($labelAttr !== null) {
                $cmsLabel = $labelAttr->newInstance();
                $options[] = new FilterOption($caseRef->getBackingValue(), $cmsLabel->label);
            } else {
                $options[] = new FilterOption($caseRef->getBackingValue(), $caseRef->getName());
            }
        }

        return $multi
            ? new MultiSelectFilter($name, $label, $options)
            : new SelectFilter($name, $label, $options);
    }
}
