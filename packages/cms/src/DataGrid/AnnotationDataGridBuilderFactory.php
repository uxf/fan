<?php

declare(strict_types=1);

namespace UXF\CMS\DataGrid;

use BackedEnum;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;
use UXF\CMS\Attribute\CmsGridDefaults;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\Column\LegacyColumnHelper;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridBuilderFactory;
use UXF\DataGrid\DataSource\DoctrineDataSource;
use UXF\DataGrid\Exception\DataGridException;
use UXF\DataGrid\Filter\DateRangeFilter;
use UXF\DataGrid\Filter\DateTimeRangeFilter;
use UXF\DataGrid\Filter\EntityMultiSelectFilter;
use UXF\DataGrid\Filter\EntitySelectFilter;
use UXF\DataGrid\Filter\Filter;
use UXF\DataGrid\Filter\SelectFilter;
use UXF\DataGrid\Filter\StringFilter;
use UXF\DataGrid\Schema\FilterOption;

/**
 * @template-implements DataGridBuilderFactory<mixed>
 */
final readonly class AnnotationDataGridBuilderFactory implements DataGridBuilderFactory
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private EntityAnnotationResolver $entityAnnotationResolver,
    ) {
    }

    public function tryCreateBuilder(string $name): ?DataGridBuilder
    {
        $metadata = $this->entityAnnotationResolver->findEntityMetadata($name);

        if ($metadata === null) {
            return null;
        }

        $builder = new DataGridBuilder($name);

        $reflectionClass = new ReflectionClass($metadata->getClassName());
        $defaultAttribute = $reflectionClass->getAttributes(CmsGridDefaults::class)[0] ?? null;
        if ($defaultAttribute !== null) {
            $defaults = $defaultAttribute->newInstance();
            $builder->setDefaultSort($defaults->sortColumnName, $defaults->sortColumnDirection);
            $builder->setDefaultPerPage($defaults->perPage);
        }

        $qb = $this->entityManager->createQueryBuilder()
            ->select('e')
            ->from($metadata->getClassName(), 'e');

        $builder->setDataSource(new DoctrineDataSource($qb));

        foreach ($metadata->getProperties() as $propertyInfo) {
            $name = $propertyInfo->name;
            $label = $propertyInfo->label;

            switch ($propertyInfo->relationType) {
                case EntityPropertyInfo::MANY_TO_ONE_RELATION:
                case EntityPropertyInfo::ONE_TO_ONE_RELATION:
                    self::check($propertyInfo, $metadata->getClassName());
                    $path = "$name.{$propertyInfo->targetColumn}";
                    $column = $builder->addColumn(LegacyColumnHelper::createToOne($name, $label, $path));
                    if ($propertyInfo->type !== '') {
                        $column->setType($propertyInfo->type);
                    }
                    break;
                case EntityPropertyInfo::MANY_TO_MANY_RELATION:
                case EntityPropertyInfo::ONE_TO_MANY_RELATION:
                    self::check($propertyInfo, $metadata->getClassName());
                    $path = $propertyInfo->targetColumn;
                    $column = $builder->addColumn(LegacyColumnHelper::createToMany($name, $label, $path));
                    if ($propertyInfo->type !== '') {
                        $column->setType($propertyInfo->type);
                    }
                    break;
                default:
                    $column = $builder->addColumn(new Column($name, $label));
                    $column->setType(self::resolveBasicType($propertyInfo));
                    $column->setEmbedded($propertyInfo->relationType === EntityPropertyInfo::EMBEDDED_RELATION);
            }

            $column
                ->setSort($propertyInfo->sortable)
                ->setOrder($propertyInfo->order)
                ->setHidden($propertyInfo->hidden);

            $filter = $this->resolveFilter($propertyInfo, $name, $label, $metadata->getClassName());
            if ($filter !== null) {
                $builder->addFilter($filter->setOrder($propertyInfo->order));
            }
        }

        return $builder;
    }

    /**
     * @return Filter<mixed>|null
     */
    private function resolveFilter(EntityPropertyInfo $propertyInfo, string $name, string $label, string $className): ?Filter
    {
        if (!$propertyInfo->filterable || $propertyInfo->hidden) {
            return null;
        }

        if (
            $propertyInfo->relationType !== EntityPropertyInfo::NONE_RELATION &&
            $propertyInfo->relationType !== EntityPropertyInfo::EMBEDDED_RELATION
        ) {
            if ($propertyInfo->autocomplete === '') {
                throw new DataGridException("Please fill autocomplete in $className::\$$propertyInfo->name or add autocompleteFields to target class");
            }

            return $propertyInfo->filterMulti
                ? new EntityMultiSelectFilter($name, $label, $propertyInfo->autocomplete)
                : new EntitySelectFilter($name, $label, $propertyInfo->autocomplete);
        }

        $type = self::resolveBasicType($propertyInfo);

        $filter = match ($type) {
            'boolean' => new SelectFilter($name, $label, [
                new FilterOption(1, 'Ano'),
                new FilterOption(0, 'Ne'),
            ]),
            'date' => new DateRangeFilter($name, $label),
            'datetime' => new DateTimeRangeFilter($name, $label),
            default => (new StringFilter($name, $label))->setMatchType($propertyInfo->matchType),
        };

        if ($propertyInfo->phpType !== null && is_a($propertyInfo->phpType, BackedEnum::class, true)) {
            $filter = GridFilterHelper::createFromEnum($name, $label, $propertyInfo->phpType, $propertyInfo->filterMulti);
        }

        $filter->setEmbedded($propertyInfo->relationType === EntityPropertyInfo::EMBEDDED_RELATION);

        return $filter;
    }

    private static function resolveBasicType(EntityPropertyInfo $propertyInfo): string
    {
        if ($propertyInfo->type !== '') {
            return $propertyInfo->type;
        }

        return match ($propertyInfo->ormType) {
            DateTime::class => 'datetime',
            Date::class => 'date',
            Time::class => 'time',
            Email::class => 'email',
            Phone::class => 'phone',
            BankAccountNumberCze::class => 'ban',
            NationalIdentificationNumberCze::class => 'nin',
            default => in_array($propertyInfo->ormType, ['boolean', 'date', 'datetime', 'integer', 'string', 'float'], true)
                ? $propertyInfo->ormType
                : 'string',
        };
    }

    private static function check(EntityPropertyInfo $propertyInfo, string $className): void
    {
        $property = $propertyInfo->name;
        $targetColumn = $propertyInfo->targetColumn;
        if ($targetColumn === '') {
            throw new DataGridException("Please fill targetColumn in $className::\$$property");
        }
    }
}
