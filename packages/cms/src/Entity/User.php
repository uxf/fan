<?php

declare(strict_types=1);

namespace UXF\CMS\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ReadableCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsGrid;
use UXF\CMS\Attribute\CmsMeta;
use UXF\Core\Attribute\AutocompleteFields;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;

#[ORM\Entity]
#[ORM\Table(name: 'users', schema: 'uxf_cms')]
#[ORM\HasLifecycleCallbacks]
#[CmsMeta(alias: 'user', title: 'Detail uživatele')]
#[AutocompleteFields(name: 'user', fields: ['name', 'email'])]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[CmsGrid(hidden: true)]
    #[CmsForm(label: 'Id', readOnly: true)]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    #[CmsGrid(label: 'Jméno')]
    #[CmsForm(label: 'Jméno')]
    private string $name;

    #[ORM\Column(length: 64)]
    private string $password;

    #[ORM\Column(type: Email::class, length: 60, unique: true)]
    #[CmsGrid(label: 'E-mail')]
    #[CmsForm(label: 'E-mail', editable: false)]
    private Email $email;

    #[ORM\Column]
    #[CmsGrid(label: 'Aktivní')]
    #[CmsForm(label: 'Aktivní', required: false)]
    private bool $active = true;

    #[ORM\Column]
    #[CmsForm(label: 'Ověřený', required: false)]
    private bool $verified = false;

    #[ORM\Column(unique: true, nullable: true)]
    private ?string $token = null;

    #[ORM\Column(type: DateTime::class)]
    #[CmsGrid(label: 'Čas registrace', type: 'datetime')]
    #[CmsForm(label: 'Čas registrace', readOnly: true)]
    private DateTime $createdAt;

    #[ORM\Column(type: DateTime::class, nullable: true)]
    #[CmsGrid(label: 'Čas poslední úpravy')]
    #[CmsForm(label: 'Čas poslední úpravy', readOnly: true)]
    private ?DateTime $updatedAt = null;

    #[ORM\Column(type: DateTime::class, nullable: true)]
    #[CmsGrid(label: 'Čas posledního přihlášení')]
    #[CmsForm(label: 'Čas posledního přihlášení', readOnly: true)]
    private ?DateTime $lastLoginAt = null;

    /** @var Collection<int, Role> */
    #[CmsForm(label: 'Uživatelské role', targetField: 'title', autocomplete: 'role')]
    #[ORM\ManyToMany(targetEntity: Role::class)]
    #[ORM\JoinTable(schema: 'uxf_cms'), ORM\JoinColumn(name: 'user_id'), ORM\InverseJoinColumn(name: 'role_id')]
    private Collection $userRoles;

    /**
     * @param Role[] $roles
     */
    public function __construct(Email $email, string $name, string $password = '', array $roles = [])
    {
        $this->email = $email;
        $this->name = $name;
        $this->password = $password;
        $this->userRoles = new ArrayCollection($roles);
        $this->createdAt = Clock::now();
    }

    #[ORM\PreUpdate]
    public function onPreUpdate(): void
    {
        $this->updatedAt = Clock::now();
    }

    public function getUserIdentifier(): string
    {
        return (string) $this->getId();
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function eraseCredentials(): void
    {
    }

    /**
     * @inheritDoc
     */
    public function getRoles(): array
    {
        return $this->userRoles->map(fn (Role $role) => $role->getName())->getValues();
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function setEmail(Email $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active = true): self
    {
        $this->active = $active;
        return $this;
    }

    public function isVerified(): bool
    {
        return $this->verified;
    }

    public function setVerified(bool $verified): void
    {
        $this->verified = $verified;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): void
    {
        $this->token = $token;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    public function getLastLoginAt(): ?DateTime
    {
        return $this->lastLoginAt;
    }

    public function setLastLoginAt(?DateTime $lastLoginAt = null): void
    {
        $this->lastLoginAt = $lastLoginAt ?? Clock::now();
    }

    /**
     * @return ReadableCollection<int, Role>
     */
    public function getUserRoles(): ReadableCollection
    {
        return $this->userRoles;
    }

    /**
     * @param array<Role>|Collection<int, Role> $roles
     */
    public function setUserRoles(Collection|array $roles): void
    {
        $this->userRoles->clear();
        array_map($this->userRoles->add(...), $roles instanceof Collection ? $roles->getValues() : $roles);
    }

    public function addUserRole(Role $role): self
    {
        if (!$this->userRoles->contains($role)) {
            $this->userRoles->add($role);
        }
        return $this;
    }

    public function removeUserRole(Role $role): self
    {
        $this->userRoles->removeElement($role);
        return $this;
    }
}
