<?php

declare(strict_types=1);

namespace UXF\CMS\Entity\Invitation;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class PasswordRecovery extends UserInvitation
{
}
