<?php

declare(strict_types=1);

namespace UXF\CMS\Entity\Invitation;

use Doctrine\ORM\Mapping as ORM;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsGrid;
use UXF\CMS\Attribute\CmsMeta;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\SystemProvider\Uuid;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;

#[ORM\Entity]
#[ORM\Table(schema: 'uxf_cms')]
#[ORM\InheritanceType(value: 'SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'discriminator', type: 'string')]
#[ORM\DiscriminatorMap(value: [
    'verification' => UserEmailVerification::class,
    'invitation' => UserInvitation::class,
    'passwordRecovery' => PasswordRecovery::class,
])]
#[CmsMeta(alias: 'user-invitation', title: 'Pozvánky uživatelům')]
class UserInvitation
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[CmsGrid(hidden: true)]
    #[CmsForm(label: 'Id', readOnly: true)]
    private ?int $id = null;

    #[ORM\Column(name: '`key`', length: 50, unique: true)]
    #[CmsGrid(label: 'Klíč')]
    #[CmsForm(label: 'Klíč')]
    private string $key;

    #[ORM\Column(type: Email::class, length: 60)]
    #[CmsGrid(label: 'E-mail')]
    #[CmsForm(label: 'E-mail', readOnly: true)]
    private Email $email;

    #[ORM\Column(type: DateTime::class)]
    #[CmsGrid(label: 'Platnost do', type: 'datetime')]
    #[CmsForm(label: 'Platnost do', readOnly: true)]
    private DateTime $validity;

    public function __construct(Email $email, ?string $key = null, ?DateTime $validity = null)
    {
        $this->email = $email;
        $this->key = $key ?? Uuid::uuid4()->toString();
        $this->validity = $validity ?? Clock::now()->modify('+2 days');
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getValidity(): DateTime
    {
        return $this->validity;
    }
}
