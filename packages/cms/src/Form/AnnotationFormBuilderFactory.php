<?php

declare(strict_types=1);

namespace UXF\CMS\Form;

use LogicException;
use ReflectionEnum;
use ReflectionEnumBackedCase;
use UXF\CMS\Attribute\CmsLabel;
use UXF\CMS\Form\Field\ContentField;
use UXF\CMS\Form\Field\FileCollectionField;
use UXF\CMS\Form\Field\FileField;
use UXF\CMS\Form\Field\ImageCollectionField;
use UXF\CMS\Form\Field\ImageField;
use UXF\Content\Entity\ContentLite;
use UXF\Core\Attribute\Label;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Form\Exception\FormException;
use UXF\Form\Field\BasicField;
use UXF\Form\Field\EmbeddedField;
use UXF\Form\Field\EnumField;
use UXF\Form\Field\Field;
use UXF\Form\Field\ManyToManyField;
use UXF\Form\Field\ManyToOneField;
use UXF\Form\Field\OneToManyField;
use UXF\Form\Field\OneToOneField;
use UXF\Form\Field\OneToOneFieldsetField;
use UXF\Form\FormBuilder;
use UXF\Form\FormBuilderFactory;
use UXF\Form\Schema\OptionSchema;
use UXF\Storage\Entity\File;
use UXF\Storage\Entity\Image;

/**
 * @implements FormBuilderFactory<object>
 */
final readonly class AnnotationFormBuilderFactory implements FormBuilderFactory
{
    public function __construct(private EntityAnnotationResolver $entityAnnotationResolver)
    {
    }

    public function tryCreateBuilder(string $entityAlias): ?FormBuilder
    {
        $metadata = $this->entityAnnotationResolver->findEntityMetadata($entityAlias);

        if ($metadata === null) {
            return null;
        }

        $builder = new FormBuilder();
        $builder->setClassName($metadata->getClassName());

        foreach ($metadata->getProperties() as $fieldAnnotation) {
            $builder->addField($this->createField($fieldAnnotation, $metadata));
        }

        return $builder;
    }

    private function createField(EntityPropertyInfo $propertyInfo, ?EntityInfo $metadata): Field
    {
        $name = $propertyInfo->name;
        $phpType = $propertyInfo->phpType ?? throw new LogicException();

        switch ($propertyInfo->relationType) {
            case EntityPropertyInfo::EMBEDDED_RELATION:
                if ($phpType === ContentLite::class) {
                    $field = new ContentField($name, $phpType, 'content');
                } else {
                    $fields = array_map(function (EntityPropertyInfo $info) {
                        return $this->createField($info, null);
                    }, $propertyInfo->childrenProperties);
                    assert(class_exists($phpType));
                    $field = new EmbeddedField($name, $phpType, $fields);
                }
                break;
            case EntityPropertyInfo::MANY_TO_ONE_RELATION:
                if ($phpType === File::class) {
                    $field = new FileField($name);
                } elseif ($phpType === Image::class) {
                    $field = new ImageField($name);
                } else {
                    self::check($propertyInfo, $metadata);
                    assert(class_exists($phpType));
                    $field = new ManyToOneField(
                        $name,
                        $phpType,
                        $propertyInfo->targetField,
                        $propertyInfo->autocomplete,
                    );
                }
                break;
            case EntityPropertyInfo::ONE_TO_ONE_RELATION:
                if ($propertyInfo->fieldset) {
                    $fields = array_map(function (EntityPropertyInfo $info) {
                        return $this->createField($info, null);
                    }, $propertyInfo->childrenProperties);
                    assert(class_exists($phpType));
                    $field = new OneToOneFieldsetField($name, $phpType, $fields);
                } else {
                    self::check($propertyInfo, $metadata);
                    assert(class_exists($phpType));
                    $field = new OneToOneField(
                        $name,
                        $phpType,
                        $propertyInfo->targetField,
                        $propertyInfo->autocomplete,
                    );
                }
                break;
            case EntityPropertyInfo::MANY_TO_MANY_RELATION:
                if ($phpType === File::class) {
                    $field = new FileCollectionField($name);
                } elseif ($phpType === Image::class) {
                    $field = new ImageCollectionField($name);
                } else {
                    self::check($propertyInfo, $metadata);
                    assert(class_exists($phpType));
                    $field = new ManyToManyField(
                        $name,
                        $phpType,
                        $propertyInfo->targetField,
                        $propertyInfo->autocomplete,
                    );
                }
                break;
            case EntityPropertyInfo::ONE_TO_MANY_RELATION:
                $fields = array_map(function (EntityPropertyInfo $info) {
                    return $this->createField($info, null);
                }, $propertyInfo->childrenProperties);
                assert(class_exists($phpType));
                $field = new OneToManyField($name, $phpType, $fields);
                break;
            default:
                if (enum_exists($phpType)) {
                    $options = [];
                    foreach ((new ReflectionEnum($phpType))->getCases() as $caseRef) {
                        assert($caseRef instanceof ReflectionEnumBackedCase);
                        $labelAttr = $caseRef->getAttributes(Label::class)[0] ?? $caseRef->getAttributes(CmsLabel::class)[0] ?? null;
                        if ($labelAttr !== null) {
                            $label = $labelAttr->newInstance();
                            $options[] = new OptionSchema($caseRef->getBackingValue(), $label->label, $label->color);
                        } else {
                            $options[] = new OptionSchema($caseRef->getBackingValue(), $caseRef->getName(), null);
                        }
                    }
                    $field = new EnumField($name, $phpType, $options);
                } else {
                    $type = match ($phpType) {
                        DateTime::class => 'datetime',
                        Date::class => 'date',
                        Time::class => 'time',
                        Email::class => 'email',
                        Phone::class => 'phone',
                        BankAccountNumberCze::class => 'ban',
                        NationalIdentificationNumberCze::class => 'nin',
                        default => $propertyInfo->type ?? $propertyInfo->ormType ?? 'string',
                    };
                    $field = new BasicField($name, $phpType, $type);
                }
        }

        $field->setLabel($propertyInfo->label);
        $field->setRequired($propertyInfo->required);
        $field->setReadOnly($propertyInfo->readOnly);
        $field->setEditable($propertyInfo->editable);
        $field->setHidden($propertyInfo->hidden);
        $field->setOrder($propertyInfo->order);

        if ($propertyInfo->type !== null && $propertyInfo->type !== '') {
            $field->setType($propertyInfo->type);
        }

        return $field;
    }

    private static function check(EntityPropertyInfo $propertyInfo, ?EntityInfo $metadata): void
    {
        $property = $propertyInfo->name;
        $targetField = $propertyInfo->targetField;
        $className = $metadata?->getClassName() ?? '???';

        if ($targetField === '') {
            throw new FormException("Please fill targetField in $className::\$$property");
        }

        $autocomplete = $propertyInfo->autocomplete;
        if ($autocomplete === '') {
            throw new FormException("Please fill autocomplete in $className::\$$property or add autocompleteFields to target class");
        }
    }
}
