<?php

declare(strict_types=1);

namespace UXF\CMS\Form;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\AssociationMapping;
use Doctrine\ORM\Mapping\ClassMetadata;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionNamedType;
use RuntimeException;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsMeta;
use UXF\CMS\Service\CmsMetaAnnotationResolver;
use UXF\Core\Attribute\AutocompleteFields;
use UXF\Form\Exception\FormException;

final class EntityAnnotationResolver
{
    /** @var array<string, EntityInfo>|null */
    private ?array $entitiesMetadata = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly CmsMetaAnnotationResolver $cmsMetaAnnotationResolver,
        private readonly ContainerInterface $container,
    ) {
    }

    public function findEntityMetadata(string $entityAlias): ?EntityInfo
    {
        return $this->getEntitiesMetadata()[$entityAlias] ?? null;
    }

    /**
     * @return array<string, EntityInfo>
     */
    public function getEntitiesMetadata(): array
    {
        if ($this->entitiesMetadata === null) {
            $entities = [];

            foreach ($this->entityManager->getMetadataFactory()->getAllMetadata() as $metadata) {
                $rc = $metadata->getReflectionClass();

                $attr = $rc->getAttributes(CmsMeta::class)[0] ?? null;
                $cmsMeta = $attr?->newInstance();

                if (!$cmsMeta instanceof CmsMeta) {
                    continue;
                }

                /** @var class-string<object> & literal-string $className */
                $className = $rc->getName();
                $entities[$cmsMeta->alias] = new EntityInfo($className, $this->getProperties($metadata));
            }

            $this->entitiesMetadata = $entities;
        }

        return $this->entitiesMetadata;
    }

    /**
     * @param ClassMetadata<object> $metadata
     * @return EntityPropertyInfo[]
     */
    private function getProperties(ClassMetadata $metadata): array
    {
        $propertyInfos = [];
        $orderCounter = 0;
        $rc = $metadata->getReflectionClass();

        foreach ($rc->getProperties() as $reflectionProperty) {
            $attr = $reflectionProperty->getAttributes(CmsForm::class)[0] ?? null;
            $cmsFormField = $attr?->newInstance();
            if (!$cmsFormField instanceof CmsForm) {
                continue;
            }

            $propertyName = $reflectionProperty->name;
            $propertyInfo = new EntityPropertyInfo($cmsFormField, $propertyName);

            // fix stable order
            $cmsFormField->order = ($cmsFormField->order !== 0 ? $cmsFormField->order : $orderCounter++) * 100;

            if (isset($metadata->embeddedClasses[$propertyName])) {
                // embedded
                $class = $metadata->embeddedClasses[$propertyName]['class'];
                $propertyInfo->phpType = $class;
                $propertyInfo->relationType = EntityPropertyInfo::EMBEDDED_RELATION;
                $propertyInfo->childrenProperties = $this->getEmbeddedProperties($class, $cmsFormField->order);
            } elseif (isset($metadata->fieldMappings[$propertyName])) {
                // basic
                $ref = $metadata->reflFields[$propertyName]?->getType();
                if (!$ref instanceof ReflectionNamedType) {
                    throw new FormException("Missing property type $metadata->name::\$$propertyName");
                }
                $propertyInfo->phpType = $ref->getName();
                $propertyInfo->ormType = $metadata->fieldMappings[$propertyName]['type'];
            } elseif (isset($metadata->associationMappings[$propertyName])) {
                // association
                $associationMapping = $metadata->associationMappings[$propertyName];
                $class = $associationMapping['targetEntity'];
                // @phpstan-ignore instanceof.alwaysTrue
                $type = $associationMapping instanceof AssociationMapping ? $associationMapping->type() : $associationMapping['type'];
                $propertyInfo->phpType = $class;

                switch ($type) {
                    case ClassMetadata::MANY_TO_ONE:
                        $relationType = EntityPropertyInfo::MANY_TO_ONE_RELATION;
                        break;
                    case ClassMetadata::MANY_TO_MANY:
                        $relationType = EntityPropertyInfo::MANY_TO_MANY_RELATION;
                        break;
                    case ClassMetadata::ONE_TO_MANY:
                        $relationType = EntityPropertyInfo::ONE_TO_MANY_RELATION;
                        $meta = $this->entityManager->getMetadataFactory()->getMetadataFor($class);
                        $propertyInfo->childrenProperties = $this->getProperties($meta);
                        break;
                    case ClassMetadata::ONE_TO_ONE:
                        $relationType = EntityPropertyInfo::ONE_TO_ONE_RELATION;
                        $meta = $this->entityManager->getMetadataFactory()->getMetadataFor($class);
                        $propertyInfo->childrenProperties = $this->getProperties($meta);
                        break;
                    default:
                        throw new RuntimeException();
                }

                if ($propertyInfo->autocomplete === '') {
                    $targetClassName = $metadata->associationMappings[$propertyName]['targetEntity'];

                    $autocomplete = ((new ReflectionClass($targetClassName))->getAttributes(AutocompleteFields::class)[0] ?? null)?->newInstance();
                    if ($autocomplete instanceof AutocompleteFields) {
                        $propertyInfo->autocomplete = $autocomplete->name;
                    } else {
                        $targetCmsMeta = $this->cmsMetaAnnotationResolver->findCmsMetaByClassName($targetClassName);
                        if (
                            $targetCmsMeta instanceof CmsMeta &&
                            ($targetCmsMeta->autocompleteFields !== [] || $this->container->has("autocomplete.$targetCmsMeta->alias"))
                        ) {
                            $propertyInfo->autocomplete = $targetCmsMeta->alias;
                        }
                    }
                }

                $propertyInfo->relationType = $relationType;
            } else {
                throw new RuntimeException();
            }

            $propertyInfos[] = $propertyInfo;
        }

        return $propertyInfos;
    }

    /**
     * @template T of object
     * @phpstan-param class-string<T> $className
     * @return EntityPropertyInfo[]
     */
    private function getEmbeddedProperties(string $className, int $parentOrder): array
    {
        $propertyInfos = [];

        $metadata = $this->entityManager->getMetadataFactory()->getMetadataFor($className);
        $rc = $metadata->getReflectionClass();

        $orderCounter = 0;
        foreach ($rc->getProperties() as $reflectionProperty) {
            $attr = $reflectionProperty->getAttributes(CmsForm::class)[0] ?? null;
            $field = $attr?->newInstance();
            if (!$field instanceof CmsForm) {
                continue;
            }

            $propertyName = $reflectionProperty->name;
            $propertyInfo = new EntityPropertyInfo($field, $propertyName);
            $ref = $metadata->reflFields[$propertyName]?->getType();
            assert($ref instanceof ReflectionNamedType);
            $propertyInfo->phpType = $ref->getName();
            $propertyInfo->ormType = $metadata->fieldMappings[$propertyName]['type'];

            // fix stable order
            $propertyInfo->order = ($field->order !== 0 ? $field->order : $orderCounter++) + $parentOrder;

            $propertyInfos[] = $propertyInfo;
        }

        return $propertyInfos;
    }
}
