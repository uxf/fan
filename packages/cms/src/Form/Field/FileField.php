<?php

declare(strict_types=1);

namespace UXF\CMS\Form\Field;

use UXF\CMS\Form\Mapper\FileMapper;
use UXF\Form\Field\Field;
use UXF\Storage\Entity\File;

final class FileField extends Field
{
    public function __construct(string $name, ?string $label = null)
    {
        parent::__construct($name, File::class, 'file', $label);
    }

    public function getDefaultMapperServiceId(): string
    {
        return FileMapper::SERVICE_ID;
    }
}
