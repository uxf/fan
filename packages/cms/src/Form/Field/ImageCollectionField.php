<?php

declare(strict_types=1);

namespace UXF\CMS\Form\Field;

use UXF\CMS\Form\Mapper\ImageCollectionMapper;
use UXF\Form\Field\Field;
use UXF\Storage\Entity\Image;

final class ImageCollectionField extends Field
{
    public function __construct(string $name, ?string $label = null)
    {
        parent::__construct($name, Image::class, 'images', $label);
    }

    public function getDefaultMapperServiceId(): string
    {
        return ImageCollectionMapper::SERVICE_ID;
    }
}
