<?php

declare(strict_types=1);

namespace UXF\CMS\Form\Field;

use UXF\CMS\Form\Mapper\ContentMapper;
use UXF\Form\Field\Field;

final class ContentField extends Field
{
    public function getDefaultMapperServiceId(): string
    {
        return ContentMapper::SERVICE_ID;
    }
}
