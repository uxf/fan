<?php

declare(strict_types=1);

namespace UXF\CMS\Form\Mapper;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Form\Field\Field;
use UXF\Form\Mapper\Mapper;
use UXF\Storage\Entity\Image;
use UXF\Storage\Http\Response\ImageResponse;

final readonly class ImageMapper implements Mapper
{
    public const string SERVICE_ID = 'uxf_form.image_mapper';

    public function __construct(
        private PropertyAccessorInterface $propertyAccessor,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function mapToEntityConstructor(Field $field, mixed $value): ?Image
    {
        if ($value === null) {
            return null;
        }

        return $this->entityManager->find(Image::class, is_array($value) ? $value['id'] : $value);
    }

    public function mapToEntity(Field $field, object $entity, mixed $value): void
    {
        if (!$this->propertyAccessor->isWritable($entity, $field->getName())) {
            return;
        }

        if ($value === null) {
            $this->propertyAccessor->setValue($entity, $field->getName(), null);
            return;
        }

        $image = $this->entityManager->find(Image::class, is_array($value) ? $value['id'] : $value);
        if ($image instanceof Image) {
            $this->propertyAccessor->setValue($entity, $field->getName(), $image);
        }
    }

    public function mapFromEntity(Field $field, object $entity): ?ImageResponse
    {
        if ($this->propertyAccessor->isReadable($entity, $field->getName())) {
            return ImageResponse::createNullable($this->propertyAccessor->getValue($entity, $field->getName()));
        }

        return null;
    }
}
