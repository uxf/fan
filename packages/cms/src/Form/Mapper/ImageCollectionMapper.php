<?php

declare(strict_types=1);

namespace UXF\CMS\Form\Mapper;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Form\Exception\FormException;
use UXF\Form\Field\Field;
use UXF\Form\Mapper\Mapper;
use UXF\Storage\Entity\Image;
use UXF\Storage\Http\Response\ImageResponse;

final readonly class ImageCollectionMapper implements Mapper
{
    public const string SERVICE_ID = 'uxf_form.image_collection_mapper';

    public function __construct(
        private PropertyAccessorInterface $propertyAccessor,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function mapToEntityConstructor(Field $field, mixed $value): mixed
    {
        throw new FormException('Unsupported method');
    }

    public function mapToEntity(Field $field, object $entity, mixed $value): void
    {
        if (is_array($value) && $this->propertyAccessor->isWritable($entity, $field->getName())) {
            $items = $this->entityManager->getRepository(Image::class)->findBy([
                'id' => array_map(static fn (array $item) => $item['id'], $value),
            ]);

            $this->propertyAccessor->setValue($entity, $field->getName(), new ArrayCollection($items));
        }
    }

    /**
     * @return ImageResponse[]
     */
    public function mapFromEntity(Field $field, object $entity): array
    {
        if ($this->propertyAccessor->isReadable($entity, $field->getName())) {
            $result = [];
            foreach ($this->propertyAccessor->getValue($entity, $field->getName()) as $image) {
                $result[] = ImageResponse::create($image);
            }
            return $result;
        }

        return [];
    }
}
