<?php

declare(strict_types=1);

namespace UXF\CMS\Form\Mapper;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Form\Field\Field;
use UXF\Form\Mapper\Mapper;
use UXF\Storage\Entity\File;
use UXF\Storage\Http\Response\StorageResponse;

final readonly class FileMapper implements Mapper
{
    public const string SERVICE_ID = 'uxf_form.file_mapper';

    public function __construct(
        private PropertyAccessorInterface $propertyAccessor,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function mapToEntityConstructor(Field $field, mixed $value): ?File
    {
        if ($value === null) {
            return null;
        }

        return $this->entityManager->find(File::class, is_array($value) ? $value['id'] : $value);
    }

    public function mapToEntity(Field $field, object $entity, mixed $value): void
    {
        if (!$this->propertyAccessor->isWritable($entity, $field->getName())) {
            return;
        }

        if ($value === null) {
            $this->propertyAccessor->setValue($entity, $field->getName(), null);
            return;
        }

        $file = $this->entityManager->find(File::class, is_array($value) ? $value['id'] : $value);
        if ($file instanceof File) {
            $this->propertyAccessor->setValue($entity, $field->getName(), $file);
        }
    }

    public function mapFromEntity(Field $field, object $entity): ?StorageResponse
    {
        if ($this->propertyAccessor->isReadable($entity, $field->getName())) {
            return StorageResponse::fromNullable($this->propertyAccessor->getValue($entity, $field->getName()));
        }

        return null;
    }
}
