<?php

declare(strict_types=1);

namespace UXF\CMS\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use UXF\CMS\Utils\Exception\TableAnnotationException;
use UXF\Core\Contract\Metadata\MetadataLoader;
use UXF\DataGrid\DataGridFactory;
use UXF\DataGrid\Exception\DataGridException;
use UXF\Form\Exception\FormException;
use UXF\Form\FormFactory;

#[AsCommand(name: 'lint:annotations')]
class LintAnnotationsCommand extends Command
{
    private const array IGNORED_FORMS = ['content'];

    public function __construct(
        private readonly Container $container,
        private readonly MetadataLoader $entityLoader,
        private readonly FormFactory $formFactory,
        private readonly DataGridFactory $dataGridFactory,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $metadatas = $this->entityLoader->getMetadata();
        } catch (TableAnnotationException $e) {
            $output->writeln("<error>Meta: {$e->getMessage()}</error>");
            return 1;
        }

        foreach ($metadatas as $entityAlias => $metadata) {
            if (!in_array($entityAlias, self::IGNORED_FORMS, true)) {
                try {
                    $this->formFactory->createForm($entityAlias);
                } catch (FormException $e) {
                    $output->writeln("<error>Form: {$e->getMessage()}</error>");
                    return 1;
                }
            }

            try {
                $this->dataGridFactory->createSchema($entityAlias);
            } catch (DataGridException $e) {
                $output->writeln("<error>DataGrid: {$e->getMessage()}</error>");
                return 1;
            }
        }

        foreach ($this->container->getServiceIds() as $serviceId) {
            if (str_starts_with($serviceId, 'datagrid.')) {
                try {
                    $this->dataGridFactory->createSchema(substr($serviceId, 9));
                } catch (DataGridException $e) {
                    $output->writeln("<error>DataGrid: {$e->getMessage()}</error>");
                    return 1;
                }
            } elseif (str_starts_with($serviceId, 'form.')) {
                try {
                    $this->formFactory->createForm(substr($serviceId, 5));
                } catch (FormException $e) {
                    $output->writeln("<error>Form: {$e->getMessage()}</error>");
                    return 1;
                }
            }
        }

        $output->writeln('<info>Valid</info>');

        return 0;
    }
}
