<?php

declare(strict_types=1);

namespace UXF\CMS\DependencyInjection;

use CompileError;
use Doctrine\ORM\Mapping\Embedded;
use Doctrine\ORM\Mapping\Entity;
use Override;
use ReflectionClass;
use ReflectionNamedType;
use ReflectionProperty;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use UXF\CMS\Attribute\Loggable;
use UXF\CMS\Service\ChangeLogSubscriber;
use UXF\Core\Utils\ClassFinder;

readonly class ChangeLogCompilerPass implements CompilerPassInterface
{
    /**
     * @param string[] $loggedProperties
     */
    private function processEntityProperty(ReflectionProperty $reflectionProperty, array &$loggedProperties, string $embeddedPrefix = ''): void
    {
        if ($reflectionProperty->getAttributes(Loggable::class) === []) {
            return;
        }

        if ($reflectionProperty->getAttributes(Embedded::class) !== []) {
            $reflectionType = $reflectionProperty->getType();
            assert($reflectionType instanceof ReflectionNamedType);
            $embeddedClassName = $reflectionType->getName();
            assert(class_exists($embeddedClassName));
            $embeddedReflectionClass = new ReflectionClass($embeddedClassName);
            foreach ($embeddedReflectionClass->getProperties() as $property) {
                $this->processEntityProperty($property, $loggedProperties, $embeddedPrefix . $reflectionProperty->getName() . '.');
            }
            return;
        }

        $loggedProperties[] = $embeddedPrefix . $reflectionProperty->getName();
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     * @param string[] $loggedProperties
     */
    private function processEntity(ReflectionClass $reflectionClass, array &$loggedProperties): void
    {
        if ($reflectionClass->getParentClass() instanceof ReflectionClass) {
            $this->processEntity($reflectionClass->getParentClass(), $loggedProperties);
        }

        foreach ($reflectionClass->getProperties() as $property) {
            $this->processEntityProperty($property, $loggedProperties);
        }
    }

    #[Override]
    public function process(ContainerBuilder $container): void
    {
        $sources = $container->getParameter('uxf_cms.loggable.sources');
        if (!is_array($sources)) {
            throw new CompileError('uxf_cms.loggable.sources is not array');
        }

        $entities = [];
        if ($sources !== []) {
            foreach (ClassFinder::findAllClassNames($sources) as $className) {
                $reflectionClass = new ReflectionClass($className);

                if ($reflectionClass->getAttributes(Entity::class) === []) {
                    continue;
                }

                $loggedProperties = [];
                $this->processEntity($reflectionClass, $loggedProperties);

                if ($loggedProperties !== []) {
                    $entities[$className] = $loggedProperties;
                }
            }
        }

        $container->getDefinition(ChangeLogSubscriber::class)->setArgument(0, $entities);
    }
}
