<?php

declare(strict_types=1);

namespace UXF\CMS\Service\PasswordRecovery;

final readonly class PasswordRecoveryConfig
{
    public function __construct(
        private string $invitationUrl,
        private string $recoveryUrl,
        private string $cmsRecoveryUrl,
        private string $emailFrom,
        private string $emailName,
    ) {
    }

    public function getRecoveryUrl(PasswordRecoveryType $type): string
    {
        return match ($type) {
            PasswordRecoveryType::INVITATION_TYPE => $this->invitationUrl,
            PasswordRecoveryType::RECOVERY_TYPE => $this->recoveryUrl,
            PasswordRecoveryType::RECOVERY_CMS_TYPE => $this->cmsRecoveryUrl,
        };
    }

    public function getRecoveryEmailSubject(PasswordRecoveryType $type): string
    {
        return match ($type) {
            PasswordRecoveryType::INVITATION_TYPE => 'Aktivace nového účtu',
            PasswordRecoveryType::RECOVERY_TYPE => 'Zapomenuté heslo',
            PasswordRecoveryType::RECOVERY_CMS_TYPE => 'Zapomenuté heslo',
        };
    }

    public function getEmailFrom(): string
    {
        return $this->emailFrom;
    }

    public function getEmailName(): string
    {
        return $this->emailName;
    }
}
