<?php

declare(strict_types=1);

namespace UXF\CMS\Service;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Entity\User;
use UXF\CMS\Repository\RoleRepository;

final readonly class UserService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private RoleRepository $roleRepository,
    ) {
    }

    public function addRole(User $user, string $name): void
    {
        $role = $this->roleRepository->get($name);
        $user->addUserRole($role);

        $this->entityManager->flush();
    }

    public function removeRole(User $user, string $name): void
    {
        $role = $this->roleRepository->get($name);
        $user->removeUserRole($role);

        $this->entityManager->flush();
    }
}
