<?php

declare(strict_types=1);

namespace UXF\CMS\Service\Storage;

final readonly class StorageConfig
{
    public function __construct(public string $defaultUploadNamespace)
    {
    }
}
