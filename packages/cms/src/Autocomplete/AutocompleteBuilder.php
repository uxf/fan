<?php

declare(strict_types=1);

namespace UXF\CMS\Autocomplete;

use UXF\Core\Contract\Autocomplete\Autocomplete;
use UXF\Core\Exception\BasicException;

final class AutocompleteBuilder
{
    public function __construct(private ?Autocomplete $autocomplete = null)
    {
    }

    public function setAutocomplete(?Autocomplete $autocomplete): void
    {
        $this->autocomplete = $autocomplete;
    }

    public function build(): Autocomplete
    {
        return $this->autocomplete ?? throw BasicException::badRequest();
    }
}
