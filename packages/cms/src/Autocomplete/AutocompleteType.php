<?php

declare(strict_types=1);

namespace UXF\CMS\Autocomplete;

interface AutocompleteType
{
    /**
     * @param mixed[] $options
     */
    public function buildAutocomplete(AutocompleteBuilder $builder, array $options = []): void;

    public static function getName(): string;
}
