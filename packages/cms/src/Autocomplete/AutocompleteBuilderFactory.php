<?php

declare(strict_types=1);

namespace UXF\CMS\Autocomplete;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Attribute\CmsMeta;
use UXF\CMS\Autocomplete\Adapter\DoctrineAutocomplete;
use UXF\Core\Attribute\AutocompleteFields;

final readonly class AutocompleteBuilderFactory
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function tryCreateBuilder(string $name): ?AutocompleteBuilder
    {
        foreach ($this->entityManager->getMetadataFactory()->getAllMetadata() as $metadata) {
            $rc = $metadata->getReflectionClass();

            $attr = $rc->getAttributes(AutocompleteFields::class)[0] ?? null;
            $attribute = $attr?->newInstance();
            if ($attribute instanceof AutocompleteFields && $attribute->name === $name) {
                /** @var class-string<object> & literal-string $className */
                $className = $metadata->name;
                return new AutocompleteBuilder(DoctrineAutocomplete::create($this->entityManager, $className, $attribute->fields));
            }

            // BC
            $attr = $rc->getAttributes(CmsMeta::class)[0] ?? null;
            $cmsMeta = $attr?->newInstance();
            if ($cmsMeta instanceof CmsMeta && $cmsMeta->alias === $name && $cmsMeta->autocompleteFields !== []) {
                /** @var class-string<object> & literal-string $className */
                $className = $metadata->name;
                return new AutocompleteBuilder(DoctrineAutocomplete::create($this->entityManager, $className, $cmsMeta->autocompleteFields));
            }
        }

        return null;
    }
}
