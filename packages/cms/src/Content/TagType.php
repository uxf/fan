<?php

declare(strict_types=1);

namespace UXF\CMS\Content;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Autocomplete\Adapter\DoctrineAutocomplete;
use UXF\CMS\Autocomplete\AutocompleteBuilder;
use UXF\CMS\Autocomplete\AutocompleteType;
use UXF\Content\Entity\Tag;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridSortDir;
use UXF\DataGrid\DataGridType;
use UXF\DataGrid\DataSource\DataSource;
use UXF\DataGrid\DataSource\DoctrineDataSource;
use UXF\DataGrid\Filter\StringFilter;
use UXF\Form\Field\BasicField;
use UXF\Form\FormBuilder;
use UXF\Form\FormType;

/**
 * @template-implements FormType<Tag>
 * @template-implements DataGridType<Tag>
 */
final readonly class TagType implements FormType, DataGridType, AutocompleteType
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function buildForm(FormBuilder $builder): void
    {
        $builder->setClassName(Tag::class);
        $builder->addField(new BasicField('code', 'string', 'string', 'Kód'));
        $builder->addField(new BasicField('label', 'string', 'string', 'Label'));
    }

    /**
     * @param mixed[] $options
     */
    public function buildGrid(DataGridBuilder $builder, array $options = []): void
    {
        $builder->addColumn(new Column('id', 'Id'))->setSort();
        $builder->addColumn(new Column('code', 'Kód'))->setSort();
        $builder->addColumn(new Column('label', 'Label'))->setSort();

        $builder->setDefaultSort('label', DataGridSortDir::ASC);

        $builder->addFilter(new StringFilter('code', 'Kód'));
        $builder->addFilter(new StringFilter('label', 'Label'));
    }

    /**
     * @param mixed[] $options
     */
    public function getDataSource(array $options = []): DataSource
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select('tag')
            ->from(Tag::class, 'tag');

        return new DoctrineDataSource($qb);
    }

    /**
     * @param mixed[] $options
     */
    public function buildAutocomplete(AutocompleteBuilder $builder, array $options = []): void
    {
        $builder->setAutocomplete(DoctrineAutocomplete::create($this->entityManager, Tag::class, ['label', 'code'], '{label} [{code}]'));
    }

    public static function getName(): string
    {
        return 'content-tag';
    }
}
