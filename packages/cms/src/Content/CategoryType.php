<?php

declare(strict_types=1);

namespace UXF\CMS\Content;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Autocomplete\Adapter\DoctrineAutocomplete;
use UXF\CMS\Autocomplete\AutocompleteBuilder;
use UXF\CMS\Autocomplete\AutocompleteType;
use UXF\CMS\Form\Field\ImageField;
use UXF\Content\Entity\Category;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridSortDir;
use UXF\DataGrid\DataGridType;
use UXF\DataGrid\DataSource\DataSource;
use UXF\DataGrid\DataSource\DoctrineDataSource;
use UXF\DataGrid\Filter\StringFilter;
use UXF\Form\Field\BasicField;
use UXF\Form\Field\ManyToOneField;
use UXF\Form\FormBuilder;
use UXF\Form\FormType;
use UXF\Storage\Http\Response\ImageResponse;

/**
 * @template-implements FormType<Category>
 * @template-implements DataGridType<Category>
 */
final readonly class CategoryType implements FormType, DataGridType, AutocompleteType
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function buildForm(FormBuilder $builder): void
    {
        $builder->setClassName(Category::class);
        $builder->addField(new BasicField('name', 'string', 'string', 'Název'));
        $builder->addField(new BasicField('titleSuffix', 'string', 'string', 'Suffix titulku'));
        $builder->addField(new BasicField('priority', 'int', 'integer', 'Priorita'));
        $builder->addField(new BasicField('description', 'string', 'string', 'Popis'));
        $builder->addField(new BasicField('metaTitle', 'string', 'string', 'Meta nadpis'));
        $builder->addField(new BasicField('metaDescription', 'string', 'string', 'Meta popis'));
        $builder->addField(new BasicField('active', 'bool', 'boolean', 'Aktivní'));
        $builder->addField(new ImageField('image', 'Avatar'));
        $builder->addField(new ManyToOneField('manyToOne', Category::class, 'name', 'content-category', 'Nadřazená kategorie'));
        // todo typy a kategorie
    }

    /**
     * @param mixed[] $options
     */
    public function buildGrid(DataGridBuilder $builder, array $options = []): void
    {
        $builder->addColumn(new Column('id', 'Id'))->setSort();
        $builder->addColumn(new Column('name', 'Název'))->setSort();
        $builder->addColumn(new Column('titleSuffix', 'Suffix titulku'))->setSort();
        $builder->addColumn(new Column('priority', 'Priorita'))->setSort();
        $builder->addColumn(new Column('description', 'Popis'))->setSort();
        $builder->addColumn(new Column('metaTitle', 'Meta nadpis'))->setSort();
        $builder->addColumn(new Column('metaDescription', 'Meta popis'))->setSort();
        $builder->addColumn(new Column('active', 'Aktivní'))->setSort()->setType('boolean');
        // todo typy a kategorie

        $builder->addColumn(new Column('image', 'Obrázek'))
            ->setType('image')
            ->setCustomContentCallback(fn (Category $category) => ImageResponse::createNullable($category->getImage()));

        $builder->setDefaultSort('name', DataGridSortDir::ASC);

        $builder->addFilter(new StringFilter('name', 'Název'));
    }

    /**
     * @param mixed[] $options
     */
    public function getDataSource(array $options = []): DataSource
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select('category')
            ->from(Category::class, 'category');

        return new DoctrineDataSource($qb);
    }

    /**
     * @param mixed[] $options
     */
    public function buildAutocomplete(AutocompleteBuilder $builder, array $options = []): void
    {
        $builder->setAutocomplete(DoctrineAutocomplete::create($this->entityManager, Category::class, ['name']));
    }

    public static function getName(): string
    {
        return 'content-category';
    }
}
