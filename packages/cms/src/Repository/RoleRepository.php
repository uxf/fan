<?php

declare(strict_types=1);

namespace UXF\CMS\Repository;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Entity\Role;
use UXF\Core\Exception\BasicException;

final readonly class RoleRepository
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function get(string $name): Role
    {
        return $this->find($name) ?? throw BasicException::notFound("Role '$name' not found");
    }

    public function find(string $name): ?Role
    {
        return $this->entityManager->getRepository(Role::class)->findOneBy([
            'name' => $name,
        ]);
    }
}
