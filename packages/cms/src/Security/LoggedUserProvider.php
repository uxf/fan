<?php

declare(strict_types=1);

namespace UXF\CMS\Security;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use UXF\CMS\Entity\User;

/**
 * @deprecated
 */
final readonly class LoggedUserProvider
{
    public function __construct(private TokenStorageInterface $tokenStorage)
    {
    }

    /**
     * @deprecated Please use GQL #[Inject] or Symfony ValueResolverInterface
     * @throws AccessDeniedHttpException
     */
    public function getUser(): User
    {
        $token = $this->tokenStorage->getToken();
        if ($token === null) {
            throw new AccessDeniedHttpException();
        }
        $user = $token->getUser();
        if (!$user instanceof User) {
            throw new AccessDeniedHttpException();
        }

        return $user;
    }
}
