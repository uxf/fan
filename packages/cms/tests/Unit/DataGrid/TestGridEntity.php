<?php

declare(strict_types=1);

namespace UXF\CMSTests\Unit\DataGrid;

use UXF\CMS\Attribute\CmsGridDefaults;
use UXF\DataGrid\DataGridSortDir;

#[CmsGridDefaults(sortColumnName: 'name', sortColumnDirection: DataGridSortDir::DESC, perPage: 100)]
class TestGridEntity
{
}
