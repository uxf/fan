<?php

declare(strict_types=1);

namespace UXF\CMSTests\Unit\DataGrid;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\TestCase;
use UXF\CMS\DataGrid\AnnotationDataGridBuilderFactory;
use UXF\CMS\DataGrid\EntityAnnotationResolver;
use UXF\CMS\DataGrid\EntityInfo;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridSortDir;

class AnnotationDataGridBuilderFactoryTest extends TestCase
{
    public function test(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->method('createQueryBuilder')
            ->willReturn(new QueryBuilder($entityManager));

        $entityAnnotationResolver = $this->createMock(EntityAnnotationResolver::class);
        $entityAnnotationResolver->method('findEntityMetadata')
            ->willReturn(new EntityInfo(TestGridEntity::class, []));

        $factory = new AnnotationDataGridBuilderFactory($entityManager, $entityAnnotationResolver);
        $builder = $factory->tryCreateBuilder('');
        self::assertInstanceOf(DataGridBuilder::class, $builder);
        self::assertSame('name', $builder->getDefaultSortColumnName());
        self::assertSame(DataGridSortDir::DESC, $builder->getDefaultSortColumnDirection());
        self::assertSame(100, $builder->getDefaultPerPage());
    }
}
