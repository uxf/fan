<?php

declare(strict_types=1);

namespace UXF\CMSTests\Unit\Autocomplete;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\CMS\Autocomplete\Adapter\DoctrineAutocomplete;

class DoctrineAutocompleteTest extends TestCase
{
    /**
     * @param literal-string $string
     */
    #[DataProvider('getData')]
    public function test(string $expected, string $string): void
    {
        self::assertSame($expected, DoctrineAutocomplete::generateQueryString($string, 'e'));
    }

    /**
     * @return string[][]
     */
    public static function getData(): array
    {
        return [
            ["COALESCE(CAST(e.a AS text), '')", '{a}'],
            ["COALESCE(CAST(e.a AS text), ''), ' ', COALESCE(CAST(e.b AS text), ''), ' [', COALESCE(CAST(e.c AS text), ''), ']'", '{a} {b} [{c}]'],
        ];
    }
}
