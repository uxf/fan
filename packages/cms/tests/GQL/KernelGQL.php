<?php

declare(strict_types=1);

namespace UXF\CMSTests\GQL;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\CMS\UXFCmsBundle;
use UXF\Content\UXFContentBundle;
use UXF\Core\Contract\Permission\PermissionChecker;
use UXF\Core\UXFCoreBundle;
use UXF\DataGrid\UXFDataGridBundle;
use UXF\Form\UXFFormBundle;
use UXF\GraphQL\Controller\GQLController;
use UXF\GraphQL\UXFGraphQLBundle;
use UXF\Hydrator\UXFHydratorBundle;
use UXF\Security\UXFSecurityBundle;
use UXF\Storage\UXFStorageBundle;

class KernelGQL extends BaseKernel
{
    use MicroKernelTrait;

    /**
     * @inheritDoc
     */
    public function registerBundles(): iterable
    {
        $bundles = [
            FrameworkBundle::class,
            SecurityBundle::class,
            TwigBundle::class,
            DoctrineBundle::class,
            DoctrineFixturesBundle::class,
            MonologBundle::class,
            UXFCoreBundle::class,
            UXFSecurityBundle::class,
            UXFCmsBundle::class,
            UXFContentBundle::class,
            UXFDataGridBundle::class,
            UXFFormBundle::class,
            UXFStorageBundle::class,
            UXFHydratorBundle::class,
            UXFGraphQLBundle::class,
        ];

        foreach ($bundles as $bundle) {
            yield new $bundle();
        }
    }

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->import(__DIR__ . '/../config/services.php');
        $container->extension('uxf_graphql', [
            'sources' => [
                __DIR__ . '/../../src/GQL',
            ],
        ]);

        $container->services()->set(PermissionChecker::class, FakePermissionChecker::class);
    }

    public function getCacheDir(): string
    {
        return __DIR__ . '/../../var/cache/test-gql';
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import('@UXFSecurityBundle/config/routes.php');
        $routes->import(__DIR__ . '/../../config/routes.php');
        $routes->add('gql', '/graphql')
            ->controller(GQLController::class)
            ->methods(['POST']);
    }
}
