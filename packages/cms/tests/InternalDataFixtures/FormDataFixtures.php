<?php

declare(strict_types=1);

namespace UXF\CMSTests\InternalDataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use UXF\CMSTests\Entity\ArticleUniqInfo;
use UXF\CMSTests\Entity\Tag;

class FormDataFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->persist(new Tag('Tag 1'));
        $manager->persist(new Tag('Tag 2'));
        $manager->persist(new Tag('Tag 3'));
        $manager->persist(new ArticleUniqInfo('Uniq top'));
        $manager->flush();
    }
}
