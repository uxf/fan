<?php

declare(strict_types=1);

namespace UXF\CMSTests\Project;

use UXF\CMS\DataGrid\CmsColumnTrait;

final readonly class CmsColumnFake
{
    use CmsColumnTrait;
}
