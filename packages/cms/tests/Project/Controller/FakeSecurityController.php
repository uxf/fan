<?php

declare(strict_types=1);

namespace UXF\CMSTests\Project\Controller;

use Symfony\Component\Routing\Attribute\Route;
use UXF\CMSTests\Project\UserRole;
use UXF\Core\Attribute\Security;

final readonly class FakeSecurityController
{
    #[Security(UserRole::ROLE_ROOT)]
    #[Route('/security', methods: 'GET')]
    public function __invoke(): void
    {
    }
}
