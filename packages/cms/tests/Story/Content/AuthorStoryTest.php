<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\Content;

use UXF\CMSTests\Story\StoryTestCase;

class AuthorStoryTest extends StoryTestCase
{
    public function test(): void
    {
        $client = self::createClient();
        $client->post('/api/cms/form/content-author', []);
        self::assertResponseStatusCodeSame(401);

        $client->login();

        $client->post('/api/cms/form/content-author', [
            "firstName" => "Markéta",
            "surname" => "Doležalová",
        ]);

        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/cms/form/content-author/1');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/cms/datagrid/content-author?sort=surname&dir=asc&perPage=10&page=0');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/cms/autocomplete/content-author?term=dolezal');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }
}
