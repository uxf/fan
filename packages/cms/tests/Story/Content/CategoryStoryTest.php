<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\Content;

use UXF\CMSTests\Story\StoryTestCase;

class CategoryStoryTest extends StoryTestCase
{
    public function test(): void
    {
        $client = self::createClient();
        $client->post('/api/cms/form/content-category', []);
        self::assertResponseStatusCodeSame(401);

        $client->login();

        $client->post('/api/cms/form/content-category', [
            "name" => "Kategorie obsahu",
        ]);

        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/cms/form/content-category/1');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/cms/datagrid/content-category?sort=name&dir=asc&perPage=10&page=0');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/cms/autocomplete/content-category?term=obsah');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }
}
