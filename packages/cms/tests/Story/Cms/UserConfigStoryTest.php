<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\Cms;

use UXF\CMSTests\Story\StoryTestCase;

class UserConfigStoryTest extends StoryTestCase
{
    public function testKV(): void
    {
        $client = self::createClient();

        $client->login();

        $client->put('/api/cms/user-config/test', [
            'hello' => 'world',
        ]);
        self::assertResponseIsSuccessful();
        self::assertSame([
            'hello' => 'world',
        ], $client->getResponseData());

        $client->get('/api/cms/user-config/test');
        self::assertResponseIsSuccessful();
        self::assertSame([
            'hello' => 'world',
        ], $client->getResponseData());

        $client->get('/api/cms/user-config/test2');
        self::assertResponseStatusCodeSame(404);
    }

    public function testEmpty(): void
    {
        $client = self::createClient();

        $client->login();

        $client->get('/api/cms/user-config');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }

    public function test(): void
    {
        $client = self::createClient();

        $client->login();

        // first config
        $client->post(
            '/api/cms/user-config',
            [
                'name' => 'first-config',
                'data' => [
                    'key1' => 'value1',
                    'key2' => 'value2',
                ],
            ],
        );
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        // second config
        $client->post(
            '/api/cms/user-config',
            [
                'name' => 'second-config',
                'data' => [
                    'keyA' => 'valueA',
                    'keyB' => 'valueB',
                ],
            ],
        );
        self::assertResponseIsSuccessful();

        // update first config
        $client->post(
            '/api/cms/user-config',
            [
                'name' => 'first-config',
                'data' => [
                    'key1' => 'value1',
                    'key2' => 'value2',
                    'key3' => 'value3',
                ],
            ],
        );
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }

    public function testValidation(): void
    {
        $client = self::createClient();

        $client->login();

        // first config
        $client->post(
            '/api/cms/user-config',
            [
                'name' => '',
                'data' => [
                    'key' => 'value',
                ],
            ],
        );
        self::assertResponseStatusCodeSame(400);
    }

    public function testCompatibilityWithOldRequests(): void
    {
        $client = self::createClient();

        $client->login();

        // first config
        $client->post(
            '/api/cms/user-config',
            [
                'id' => null,
                'name' => 'test',
                'data' => [
                    'key' => 'value',
                ],
                'user' => 1,
            ],
        );
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }
}
