<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story;

use Nette\Utils\Random;
use UXF\Core\Test\WebTestCase;
use function Safe\copy;
use function Safe\putenv;
use function Safe\unlink;

abstract class StoryTestCase extends WebTestCase
{
    private string $sqlitePath = '';

    protected function setUp(): void
    {
        parent::setUp();

        $this->sqlitePath = __DIR__ . '/../../var/db_' . Random::generate() . '.sqlite';
        copy(__DIR__ . '/../../var/db.sqlite', $this->sqlitePath);
        putenv('TEST_DATABASE_URL=' . $this->sqlitePath);
    }

    protected function tearDown(): void
    {
        unlink($this->sqlitePath);
        parent::tearDown();
    }
}
