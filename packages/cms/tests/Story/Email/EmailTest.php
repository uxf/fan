<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\Email;

use UXF\CMSTests\Story\StoryTestCase;

class EmailTest extends StoryTestCase
{
    public function testEmails(): void
    {
        $client = self::createClient();
        $client->login();

        $client->get('/api/cms/email/list');
        self::assertResponseIsSuccessful();
        self::assertStringEqualsFile(__DIR__ . '/excepted/list.html', (string) $client->getResponse()->getContent());

        $client->get('/api/cms/email/u-x-f_c-m-s-tests_email_sample-email/default');
        self::assertResponseIsSuccessful();
        self::assertStringEqualsFile(__DIR__ . '/excepted/detail.html', (string) $client->getResponse()->getContent());
    }
}
