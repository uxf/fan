<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api\Cms;

use Nette\Utils\Json;
use Nette\Utils\JsonException;
use UXF\CMSTests\Story\StoryTestCase;
use UXF\Core\Test\Client;

class CmsSetPasswordControllerTest extends StoryTestCase
{
    private Client $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->client->login();
    }

    /**
     * @throws JsonException
     */
    public function testSetPassword(): void
    {
        $this->client->request(
            'POST',
            '/api/cms/set-password/1',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            Json::encode([
                'password' => 'root',
            ]),
        );
        self::assertResponseIsSuccessful();

        $data = Json::decode((string) $this->client->getResponse()->getContent(), forceArrays: true);

        self::assertArrayHasKey('success', $data);
        self::assertTrue($data['success']);
    }
}
