<?php

declare(strict_types=1);

namespace UXF\CMSTests\Entity;

use UXF\Core\Attribute\Label;

enum Status: string
{
    #[Label('Nový', 'green')]
    case NEW = 'NEW';
    #[Label('Starý', 'red')]
    case OLD = 'OLD';
}
