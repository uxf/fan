<?php

declare(strict_types=1);

namespace UXF\CMSTests\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsMeta;

#[ORM\Entity]
#[CmsMeta(alias: 'test-comment', title: 'Comment')]
class Comment
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue]
    private ?int $id = 0;

    #[ORM\ManyToOne(inversedBy: 'oneToMany'), ORM\JoinColumn(nullable: false)]
    private Article $article;

    #[ORM\Column]
    #[CmsForm(label: 'TEXT')]
    private string $text;

    public function __construct(Article $parent, string $text)
    {
        $this->article = $parent;
        $this->text = $text;
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getArticle(): Article
    {
        return $this->article;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }
}
