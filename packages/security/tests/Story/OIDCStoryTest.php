<?php

declare(strict_types=1);

namespace UXF\SecurityTests\Story;

use UXF\Core\Test\WebTestCase;

class OIDCStoryTest extends WebTestCase
{
    public function test(): void
    {
        $client = self::createClient();

        // apple
        $client->get('/api/auth/oidc/apple/login?redirect=/ok1');
        self::assertResponseStatusCodeSame(302);

        $response = $client->getResponse();
        self::assertSame(
            'https://appleid.apple.com/auth/authorize?scope=openid+email&response_type=code+id_token&redirect_uri=https%3A%2F%2Fuxf.cz%2Fapi%2Fauth%2Foidc%2Fapple%2Fcallback&client_id=APPLE&nonce=00000000-0000-4000-0000-000000000001&response_mode=form_post',
            $response->headers->get('Location'),
        );
        self::assertSame('/ok1', $response->headers->getCookies()[0]->getValue());

        // facebook
        $client->get('/api/auth/oidc/facebook/login');
        self::assertResponseStatusCodeSame(302);

        $response = $client->getResponse();
        self::assertSame(
            'https://facebook.com/dialog/oauth/?scope=openid+email&response_type=code&redirect_uri=https%3A%2F%2Fuxf.cz%2Fapi%2Fauth%2Foidc%2Ffacebook%2Fcallback&client_id=FACEBOOK_ID&nonce=00000000-0000-4000-0000-000000000002&response_mode=form_post',
            $response->headers->get('Location'),
        );

        // gitlab
        $client->get('/api/auth/oidc/gitlab/login');
        self::assertResponseStatusCodeSame(302);

        $response = $client->getResponse();
        self::assertSame(
            'https://gitlab.com/oauth/authorize?scope=openid+email&response_type=code&redirect_uri=https%3A%2F%2Fuxf.cz%2Fapi%2Fauth%2Foidc%2Fgitlab%2Fcallback&client_id=GITLAB_ID&nonce=00000000-0000-4000-0000-000000000003&response_mode=form_post',
            $response->headers->get('Location'),
        );

        // google
        $client->get('/api/auth/oidc/google/login');
        self::assertResponseStatusCodeSame(302);

        $response = $client->getResponse();
        self::assertSame(
            'https://accounts.google.com/o/oauth2/v2/auth?scope=openid+email&response_type=code+id_token&redirect_uri=https%3A%2F%2Fuxf.cz%2Fapi%2Fauth%2Foidc%2Fgoogle%2Fcallback&client_id=GOOGLE&nonce=00000000-0000-4000-0000-000000000004&response_mode=form_post',
            $response->headers->get('Location'),
        );

        // microsoft
        $client->get('/api/auth/oidc/microsoft/login');
        self::assertResponseStatusCodeSame(302);

        $response = $client->getResponse();
        self::assertSame(
            'https://login.microsoftonline.com/common/oauth2/v2.0/authorize?scope=openid+email&response_type=id_token&redirect_uri=https%3A%2F%2Fuxf.cz%2Fapi%2Fauth%2Foidc%2Fmicrosoft%2Fcallback&client_id=MICROSOFT&nonce=00000000-0000-4000-0000-000000000005&response_mode=form_post',
            $response->headers->get('Location'),
        );

        // mojeid
        $client->get('/api/auth/oidc/mojeid/login');
        self::assertResponseStatusCodeSame(302);

        $response = $client->getResponse();
        self::assertSame(
            'https://mojeid.cz/oidc/authorization/?scope=openid+email&response_type=id_token&redirect_uri=https%3A%2F%2Fuxf.cz%2Fapi%2Fauth%2Foidc%2Fmojeid%2Fcallback&client_id=MOJEID&nonce=00000000-0000-4000-0000-000000000006&response_mode=form_post',
            $response->headers->get('Location'),
        );
    }

    public function testConnect(): void
    {
        $client = self::createClient();

        $client->get('/api/auth/oidc/apple/connect');
        self::assertResponseStatusCodeSame(400);
        self::assertStringContainsString('Login is required', (string) $client->getResponse()->getContent());
    }
}
