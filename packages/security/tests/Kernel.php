<?php

declare(strict_types=1);

namespace UXF\SecurityTests;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\Core\UXFCoreBundle;
use UXF\Hydrator\UXFHydratorBundle;
use UXF\Security\UXFSecurityBundle;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    /**
     * @inheritDoc -
     */
    public function registerBundles(): iterable
    {
        $bundles = [
            FrameworkBundle::class,
            DoctrineBundle::class,
            DoctrineFixturesBundle::class,
            SecurityBundle::class,
            MonologBundle::class,
            TwigBundle::class,
            UXFCoreBundle::class,
            UXFSecurityBundle::class,
            UXFHydratorBundle::class,
        ];

        foreach ($bundles as $bundle) {
            yield new $bundle();
        }
    }

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->import(__DIR__ . '/config/services.php');
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import(__DIR__ . '/../config/routes.php');
        $routes->import(__DIR__ . '/config/routes.php');
    }
}
