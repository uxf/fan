<?php

declare(strict_types=1);

use Symfony\Component\ErrorHandler\ErrorHandler;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\SystemProvider\Uuid;
use UXF\Core\Type\DateTime;

require __DIR__ . '/../../../vendor/autoload.php';

// https://github.com/symfony/symfony/issues/53812#issuecomment-1962311843
ErrorHandler::register(null, false);

Clock::$frozenTime = new DateTime('2022-01-01');
Uuid::$seq = 1;
