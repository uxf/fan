<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\SecurityTests\Project\Controller\Controller;
use UXF\SecurityTests\Project\Controller\FakeSecurityController;

return static function (RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->add('private', '/private/{x}')
        ->controller(Controller::class)
        ->defaults([
            'x' => '',
        ]);

    $routingConfigurator->add('public', '/public')
        ->controller(Controller::class);

    $routingConfigurator->add('secured', '/security')
        ->controller(FakeSecurityController::class);
};
