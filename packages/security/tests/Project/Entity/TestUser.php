<?php

declare(strict_types=1);

namespace UXF\SecurityTests\Project\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity]
readonly class TestUser implements UserInterface, PasswordAuthenticatedUserInterface
{
    public function __construct(
        #[ORM\Column, ORM\Id]
        private int $id,
        #[ORM\Column(unique: true)]
        private string $username,
    ) {
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): ?string
    {
        return 'pass';
    }

    /**
     * @inheritDoc
     */
    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    public function eraseCredentials(): void
    {
    }

    public function getUserIdentifier(): string
    {
        return (string) $this->id;
    }
}
