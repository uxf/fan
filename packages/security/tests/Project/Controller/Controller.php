<?php

declare(strict_types=1);

namespace UXF\SecurityTests\Project\Controller;

use Symfony\Component\HttpFoundation\Response;

final class Controller
{
    public function __invoke(): Response
    {
        return new Response();
    }
}
