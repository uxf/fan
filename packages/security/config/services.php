<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Core\Contract\Permission\RoleChecker;
use UXF\Security\Service\Authenticator;
use UXF\Security\Service\AuthService;
use UXF\Security\Service\Jwk\JwkProvider;
use UXF\Security\Service\Jwt\JwtDecoder;
use UXF\Security\Service\Jwt\JwtSigner;
use UXF\Security\Service\SecurityRoleChecker;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $services->load('UXF\Security\Service\\', __DIR__ . '/../src/Service');
    $services->load('UXF\Security\EventSubscriber\\', __DIR__ . '/../src/EventSubscriber');

    $services->load('UXF\Security\Controller\\', __DIR__ . '/../src/Controller')
        ->public();

    $services->set(AuthService::class)
        ->arg('$accessTokenLifetime', '%uxf_security.access_token_lifetime%')
        ->arg('$refreshTokenLifetime', '%uxf_security.refresh_token_lifetime%')
        ->arg('$refreshTokenCookiePath', '%uxf_security.refresh_token_cookie_path%')
        ->arg('$cookieName', '%uxf_security.cookie_name%')
        ->arg('$cookieSecured', '%uxf_security.cookie_secured%')
        ->arg('$cookieHttpOnly', '%uxf_security.cookie_http_only%');

    $services->set(JwkProvider::class)
        ->arg('$publicKey', '%uxf_security.public_key%');

    $services->set(JwtSigner::class)
        ->arg('$privateKey', '%uxf_security.private_key%')
        ->arg('$baseUrl', '%uxf_security.base_url%');

    $services->set(JwtDecoder::class)
        ->arg('$publicKey', '%uxf_security.public_key%');

    $services->set(Authenticator::class)
        ->arg('$cookieName', '%uxf_security.cookie_name%');

    // RoleChecker is checked in graphql SchemaCompilerPass
    $services->set(RoleChecker::class, SecurityRoleChecker::class);
};
