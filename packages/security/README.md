# UXF Security

## Install
```
$ composer req uxf/security
```

```php
// config/packages/uxf.php
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension('uxf_security', [
        'user_class' => User::class,                // required
        'base_url' => 'https://uxf.cz',             // required
        'public_key' => '%env(AUTH_PUBLIC_KEY)%',   // required
        'private_key' => '%env(AUTH_PRIVATE_KEY)%', // required
        // optional
        'access_token_lifetime' => 'P10Y',          // default 1 day
        'refresh_token_lifetime' => 'P20Y',         // default 1 month
        'refresh_token_cookie_path' => '/',         // default null (suggestion: /api/auth/refresh-token)
        'cookie_name' => 'Cookie-Name',             // default Authorization - used for header + cookie
        'cookie_secured' => false,                  // default true
        'cookie_http_only' => false,                // default true
        // OpenID Connect - optional
        'oidc' => [
            'apple' => [
                'client_id' => 'xxx',
            ],
            'facebook' => [
                'client_id' => 'xxx',
                'client_secret' => 'xxx',
            ],
            'gitlab' => [
                'client_id' => 'xxx',
                'client_secret' => 'xxx',
            ],
            'google' => [
                'client_id' => 'xxx',
            ],
            'microsoft' => [
                'client_id' => 'xxx',
            ],
            'mojeid' => [
                'client_id' => 'xxx',
            ],
        ],
    ]);
};
```

## Events

### LogoutUserEvent

```php
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use UXF\Security\Event\LogoutUserEvent;

#[AsEventListener(LogoutUserEvent::class)]
final readonly class LogoutUserListener
{
    public function __invoke(LogoutUserEvent $event): void
    {
        // edit response cookies/headers
        ...
    }
}
```

## OpenID Connect

### Create new user

```php
use Nette\Utils\Random;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use UXF\CMS\Entity\User;
use UXF\Security\Service\OIDC\NewUserEvent;

class NewUserEventSubscriber implements EventSubscriberInterface
{
    public function process(NewUserEvent $event): void
    {
        $event->user = new User($event->oidcInfo->email, '', Random::generate());
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            NewUserEvent::class => 'process',
        ];
    }
}
```

### Info about login

```php
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use UXF\Security\Service\OIDC\PreLoginUserEvent;

class PreLoginUserEventSubscriber implements EventSubscriberInterface
{
    /**
     * @throws OIDCFlowException
     */
    public function pre(PreLoginUserEvent $event): void
    {
        ...
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            PreLoginUserEvent::class => 'pre',
        ];
    }
}
```

### Info about connect

```php
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use UXF\Security\Service\OIDC\PreLoginUserEvent;

class PostConnectUserEventSubscriber implements EventSubscriberInterface
{
    /**
     * @throws OIDCFlowException
     */
    public function post(PostConnectUserEvent $event): void
    {
        ...
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            PostConnectUserEvent::class => 'post',
        ];
    }
}
```

### Info about new ExternalLogin

```php
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use UXF\Security\Service\OIDC\NewExternalLoginEvent;

class ExternalLoginEventSubscriber implements EventSubscriberInterface
{
    /**
     * @throws OIDCFlowException
     */
    public function pre(PreNewExternalLoginEvent $event): void
    {
        ...
    }

    /**
     * @throws OIDCFlowException
     */
    public function post(PostNewExternalLoginEvent $event): void
    {
        ...
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            PreExternalLoginEvent::class => 'pre',
            PostExternalLoginEvent::class => 'post',
        ];
    }
}
```

### Providers & URLs

- [apple](https://developer.apple.com/documentation/sign_in_with_apple/sign_in_with_apple_rest_api)
- [facebook](https://developers.facebook.com/docs/facebook-login/limited-login/token)
- [gitlab](https://docs.gitlab.com/ee/integration/openid_connect_provider.html)
- [google](https://developers.google.com/identity/protocols/oauth2/openid-connect)
- [microsoft](https://docs.microsoft.com/en-us/powerapps/maker/portals/configure/configure-openid-settings)
- [mojeid](https://www.mojeid.cz/dokumentace/html/ImplementacePodporyMojeid/OpenidConnect/Registrace/index.html)

```shell
# login
https://domain.com/api/auth/oidc/<provider>/login
# login with redirect (default is /)
https://domain.com/api/auth/oidc/<provider>/login?redirect=/some-path
# login callback
https://domain.com/api/auth/oidc/<provider>/callback


# connect
https://domain.com/api/auth/oidc/<provider>/connect
# connect with redirect (default is /)
https://domain.com/api/auth/oidc/<provider>/connect?redirect=/some-path
# connect callback
https://domain.com/api/auth/oidc/<provider>/connect-callback
```
