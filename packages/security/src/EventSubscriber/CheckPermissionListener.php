<?php

declare(strict_types=1);

namespace UXF\Security\EventSubscriber;

use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use UXF\Core\Attribute\Security;
use UXF\Security\Service\SecurityRoleChecker;

#[AsEventListener(ControllerEvent::class)]
final readonly class CheckPermissionListener
{
    public function __construct(
        private SecurityRoleChecker $checker,
    ) {
    }

    public function __invoke(ControllerEvent $event): void
    {
        $access = $event->getAttributes()[Security::class][0] ?? null;
        if (!$access instanceof Security) {
            return;
        }

        $this->checker->check($access);
    }
}
