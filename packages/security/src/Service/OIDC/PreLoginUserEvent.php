<?php

declare(strict_types=1);

namespace UXF\Security\Service\OIDC;

use UXF\Security\Entity\ExternalLogin;

final class PreLoginUserEvent
{
    public function __construct(
        public ExternalLogin $externalLogin,
        public OIDCInfo $oidcInfo,
        public bool $new,
    ) {
    }
}
