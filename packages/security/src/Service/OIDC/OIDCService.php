<?php

declare(strict_types=1);

namespace UXF\Security\Service\OIDC;

use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use UXF\Security\Entity\ExternalLogin;
use UXF\Security\Exception\OIDCException;
use UXF\Security\Exception\OIDCFlowException;
use UXF\Security\Http\AuthResponse;
use UXF\Security\Service\AuthService;
use UXF\Security\Service\AuthUserProvider;
use UXF\Security\Service\OIDC\Provider\OIDCProvider;

final readonly class OIDCService
{
    /**
     * @param array<string, OIDCProvider> $providers
     * @param AuthUserProvider<UserInterface> $authUserProvider
     */
    public function __construct(
        private array $providers,
        private EventDispatcherInterface $eventDispatcher,
        private EntityManagerInterface $entityManager,
        private AuthService $authService,
        private AuthUserProvider $authUserProvider,
    ) {
    }

    public function getRedirectUrl(OIDC $provider, bool $connect): string
    {
        $providerService = $this->providers[$provider->value] ?? throw new OIDCException("{$provider->value} is not supported.");
        return $providerService->getRedirectUrl($provider, $connect);
    }

    public function login(OIDC $provider, Request $request): AuthResponse
    {
        $providerService = $this->providers[$provider->value] ?? throw new OIDCException("{$provider->value} is not supported.");
        $oidcInfo = $providerService->authenticate($provider, $request, false);

        [$externalLogin, $new] = $this->getOrCreateExternalLogin($provider, $oidcInfo, null);

        $this->eventDispatcher->dispatch(new PreLoginUserEvent($externalLogin, $oidcInfo, $new));

        $user = $externalLogin->getUser();
        $this->authUserProvider->check($user, false);
        return $this->authService->generateAuthResponse($user, null);
    }

    public function connect(OIDC $provider, Request $request, UserInterface $user): void
    {
        $providerService = $this->providers[$provider->value] ?? throw new OIDCException("{$provider->value} is not supported.");
        $oidcInfo = $providerService->authenticate($provider, $request, true);

        [$externalLogin, $new] = $this->getOrCreateExternalLogin($provider, $oidcInfo, $user);

        $this->eventDispatcher->dispatch(new PostConnectUserEvent($externalLogin, $oidcInfo, $new));
    }

    /**
     * @return array{ExternalLogin, bool}
     */
    private function getOrCreateExternalLogin(OIDC $provider, OIDCInfo $oidcInfo, ?UserInterface $user): array
    {
        if ($oidcInfo->emailVerified === false) {
            throw new OIDCException('Email is not verified');
        }

        $externalLogin = $this->entityManager->createQueryBuilder()
            ->select('ext')
            ->from(ExternalLogin::class, 'ext')
            ->where('ext.type = :type')
            ->andWhere('ext.externalId = :externalId')
            ->setParameter('type', $provider->value)
            ->setParameter('externalId', $oidcInfo->sub)
            ->getQuery()
            ->getOneOrNullResult();

        if ($user !== null && $externalLogin !== null && $externalLogin->getUser() !== $user) {
            throw new OIDCFlowException('Account is already registered');
        }

        $new = false;
        if (!$externalLogin instanceof ExternalLogin) {
            // not exists -> create new user or link existing
            $user ??= $this->authUserProvider->findByUsername($oidcInfo->email->toString());
            if (!$user instanceof UserInterface) {
                // create new user
                $event = new NewUserEvent($oidcInfo);
                $this->eventDispatcher->dispatch($event);
                $user = $event->user;
                if (!$user instanceof UserInterface) {
                    throw new LogicException('Please handle ' . NewUserEvent::class);
                }
                $this->entityManager->persist($user);
            }

            $this->eventDispatcher->dispatch(new PreNewExternalLoginEvent($user, $oidcInfo));

            $externalLogin = new ExternalLogin($user, $provider, $oidcInfo->sub);
            $this->entityManager->persist($externalLogin);
            $this->entityManager->flush();

            $this->eventDispatcher->dispatch(new PostNewExternalLoginEvent($externalLogin, $oidcInfo));
            $new = true;
        }

        return [$externalLogin, $new];
    }
}
