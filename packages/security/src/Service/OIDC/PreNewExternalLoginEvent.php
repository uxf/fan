<?php

declare(strict_types=1);

namespace UXF\Security\Service\OIDC;

use Symfony\Component\Security\Core\User\UserInterface;

final readonly class PreNewExternalLoginEvent
{
    public function __construct(
        public UserInterface $user,
        public OIDCInfo $oidcInfo,
    ) {
    }
}
