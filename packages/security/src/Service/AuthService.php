<?php

declare(strict_types=1);

namespace UXF\Security\Service;

use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use UXF\Core\Exception\BasicException;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\SystemProvider\Uuid;
use UXF\Security\Entity\RefreshToken;
use UXF\Security\Http\AuthResponse;
use UXF\Security\Http\LoginRequestBody;
use UXF\Security\Service\Jwt\JwtSigner;

final readonly class AuthService
{
    public const string RefreshTokenCookieName = 'uxf_auth_refresh_token';
    public string $cookieExpireName;

    /**
     * @param AuthUserProvider<UserInterface> $userProvider
     */
    public function __construct(
        public string $accessTokenLifetime,
        public string $refreshTokenLifetime,
        public ?string $refreshTokenCookiePath,
        public string $cookieName,
        public bool $cookieSecured,
        public bool $cookieHttpOnly,
        private JwtSigner $jwtSigner,
        private AuthUserProvider $userProvider,
        private UserPasswordHasherInterface $passwordEncoder,
        private EntityManagerInterface $entityManager,
    ) {
        $this->cookieExpireName = $this->cookieName . '-Expire';
    }

    public function login(LoginRequestBody $body): AuthResponse
    {
        $user = $this->userProvider->findByUsername($body->username);

        if (!$user instanceof UserInterface) {
            throw BasicException::badRequest('User not found');
        }

        if (!$user instanceof PasswordAuthenticatedUserInterface) {
            throw new LogicException('$user has to be ' . PasswordAuthenticatedUserInterface::class);
        }

        if (!$this->passwordEncoder->isPasswordValid($user, $body->password)) {
            throw BasicException::badRequest('Invalid password');
        }

        $this->userProvider->check($user, false);

        return $this->generateAuthResponse($user, null);
    }

    public function refreshToken(UuidInterface $token): AuthResponse
    {
        $refreshToken = $this->entityManager->createQueryBuilder()
            ->select('refreshToken')
            ->from(RefreshToken::class, 'refreshToken')
            ->join('refreshToken.user', 'user')
            ->where('refreshToken.token = :refreshToken AND refreshToken.expiration > :now')
            ->setParameter('refreshToken', $token)
            ->setParameter('now', Clock::now())
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$refreshToken instanceof RefreshToken) {
            throw BasicException::unauthorized('Token not found or expired');
        }

        $user = $refreshToken->getUser();
        $this->userProvider->check($user, true);
        return $this->generateAuthResponse($user, $token);
    }

    public function generateAuthResponse(UserInterface $user, ?UuidInterface $refreshToken): AuthResponse
    {
        $now = Clock::now();
        $exp = $now->add(new DateInterval($this->accessTokenLifetime));

        $accessToken = $this->jwtSigner->sign([
            'uid' => (int) $user->getUserIdentifier(),
            'exp' => $exp->getTimestamp(),
            'iat' => $now->getTimestamp(),
        ]);

        $cookies = [
            new Cookie(
                name: $this->cookieName,
                value: $accessToken,
                expire: $exp,
                secure: $this->cookieSecured,
                httpOnly: $this->cookieHttpOnly,
                raw: true,
            ),
            new Cookie(
                name: $this->cookieExpireName,
                value: (string) $exp->getTimestamp(),
                expire: $exp,
                secure: $this->cookieSecured,
                httpOnly: false,
                raw: true,
            ),
        ];

        if ($refreshToken === null) {
            $refreshTokenEntity = new RefreshToken(Uuid::uuid4(), $user, $now->add(new DateInterval($this->refreshTokenLifetime)));
            $refreshToken = $refreshTokenEntity->getToken();
            $this->entityManager->persist($refreshTokenEntity);
            $this->entityManager->flush();

            if ($this->refreshTokenCookiePath !== null) {
                $cookies[] = new Cookie(
                    name: self::RefreshTokenCookieName,
                    value: $refreshToken->toString(),
                    expire: $refreshTokenEntity->getExpiration(),
                    path: $this->refreshTokenCookiePath,
                    secure: $this->cookieSecured,
                    raw: true,
                );
            }
        }

        return new AuthResponse($accessToken, $refreshToken, $cookies);
    }
}
