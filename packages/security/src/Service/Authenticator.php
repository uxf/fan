<?php

declare(strict_types=1);

namespace UXF\Security\Service;

use Nette\Utils\Strings;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use UXF\Security\Exception\JwtDecodeException;
use UXF\Security\Service\Jwt\JwtDecoder;

final class Authenticator extends AbstractAuthenticator
{
    public function __construct(
        private readonly string $cookieName,
        private readonly JwtDecoder $jwtDecoder,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function supports(Request $request): ?bool
    {
        if (!$request->headers->has($this->cookieName) && !$request->cookies->has($this->cookieName)) {
            return false;
        }

        return null;
    }

    public function authenticate(Request $request): Passport
    {
        $token = $request->headers->get($this->cookieName) ?? $request->cookies->get($this->cookieName) ?? '';

        // cleanup bearer
        $token = Strings::replace($token, '/^Bearer(%20| )/');

        try {
            $payload = $this->jwtDecoder->decode($token);
        } catch (JwtDecodeException $e) {
            $this->logger->info($e->getMessage(), [
                'exception' => $e,
            ]);
            throw new AuthenticationException();
        }

        // BC with old tokens - sub
        $userId = (string) ($payload['uid'] ?? $payload['sub']);

        return new SelfValidatingPassport(new UserBadge($userId));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return null;
    }
}
