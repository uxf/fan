<?php

declare(strict_types=1);

namespace UXF\Security\Service;

use BackedEnum;
use Symfony\Bundle\SecurityBundle\Security as SymfonySecurity;
use UXF\Core\Attribute\Security;
use UXF\Core\Contract\Permission\RoleChecker;
use UXF\Core\Exception\BasicException;

final readonly class SecurityRoleChecker implements RoleChecker
{
    public function __construct(
        private SymfonySecurity $security,
    ) {
    }

    public function check(Security $access): void
    {
        $roles = $access->getRoles();
        foreach ($roles as $role) {
            if ($role->value === 'PUBLIC') {
                return;
            }

            if ($role->value === 'LOGGED' && $this->security->getUser() !== null) {
                return;
            }

            if ($this->security->isGranted($role->value)) {
                return;
            }
        }

        if ($this->security->getUser() === null) {
            throw BasicException::unauthorized();
        }

        $info = implode(',', array_map(fn (BackedEnum $role) => $role->value, $roles));
        throw BasicException::forbidden("This action is forbidden ({$info})");
    }
}
