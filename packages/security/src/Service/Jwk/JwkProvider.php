<?php

declare(strict_types=1);

namespace UXF\Security\Service\Jwk;

use UXF\Security\Http\Keys\JwkResponseBody;
use UXF\Security\Http\Keys\JwksResponseBody;

final readonly class JwkProvider
{
    public const string KEY_ID = 'A';

    public function __construct(private string $publicKey)
    {
    }

    public function provide(): JwksResponseBody
    {
        $meta = KeyConverter::loadFromKey($this->publicKey);

        return new JwksResponseBody([new JwkResponseBody(
            'RS256',
            self::KEY_ID,
            $meta['kty'],
            $meta['n'],
            $meta['e'],
        )]);
    }
}
