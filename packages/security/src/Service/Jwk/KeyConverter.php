<?php

declare(strict_types=1);

namespace UXF\Security\Service\Jwk;

use InvalidArgumentException;
use RuntimeException;
use function extension_loaded;
use function in_array;
use function is_array;

/**
 * @see \Jose\Component\KeyManagement\KeyConverter\KeyConverter
 */
final readonly class KeyConverter
{
    private const array KEY_MAP = [
        'n' => 'n',
        'e' => 'e',
        'd' => 'd',
        'p' => 'p',
        'q' => 'q',
        'dp' => 'dmp1',
        'dq' => 'dmq1',
        'qi' => 'iqmp',
    ];

    /**
     * @return array<mixed>
     */
    public static function loadFromKey(string $pem): array
    {
        if (!extension_loaded('openssl')) {
            throw new RuntimeException('Please install the OpenSSL extension');
        }

        $res = openssl_pkey_get_public($pem);
        if ($res === false) {
            throw new InvalidArgumentException('Unable to load the key.');
        }

        $details = openssl_pkey_get_details($res);
        if (!is_array($details) || !isset($details['rsa'])) {
            throw new InvalidArgumentException('Unable to load the key.');
        }

        $values = [
            'kty' => 'RSA',
        ];

        foreach ($details['rsa'] as $key => $value) {
            if (in_array($key, self::KEY_MAP, true)) {
                $values[array_search($key, self::KEY_MAP, true)] = base64_encode($value);
            }
        }

        return $values;
    }
}
