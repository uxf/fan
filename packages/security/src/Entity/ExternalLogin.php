<?php

declare(strict_types=1);

namespace UXF\Security\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use UXF\Security\Service\OIDC\OIDC;

#[ORM\Entity]
#[ORM\Table(schema: 'uxf_security')]
#[ORM\UniqueConstraint(columns: ['external_id', 'type'])]
class ExternalLogin
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = 0;

    #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
    private UserInterface $user;

    #[ORM\Column]
    private OIDC $type;

    #[ORM\Column]
    private string $externalId;

    public function __construct(
        UserInterface $user,
        OIDC $type,
        string $externalId,
    ) {
        $this->user = $user;
        $this->type = $type;
        $this->externalId = $externalId;
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function getType(): OIDC
    {
        return $this->type;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }
}
