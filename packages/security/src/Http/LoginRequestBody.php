<?php

declare(strict_types=1);

namespace UXF\Security\Http;

final readonly class LoginRequestBody
{
    public function __construct(
        public string $username,
        public string $password,
    ) {
    }
}
