<?php

declare(strict_types=1);

namespace UXF\Security\Http\Keys;

final readonly class JwkResponseBody
{
    public function __construct(
        public string $alg,
        public string $kid,
        public string $kty,
        public string $n,
        public string $e,
    ) {
    }
}
