<?php

declare(strict_types=1);

namespace UXF\Security\Exception;

use RuntimeException;
use Throwable;

final class JwtDecodeException extends RuntimeException
{
    public static function createFromException(Throwable $previous): self
    {
        return new self($previous->getMessage(), $previous->getCode(), $previous);
    }
}
