<?php

declare(strict_types=1);

namespace UXF\Security\Exception;

use RuntimeException;

final class OIDCFlowException extends RuntimeException
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
