<?php

declare(strict_types=1);

namespace UXF\Security\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use UXF\Core\Attribute\FromBody;
use UXF\Core\Http\ResponseModifier;
use UXF\Security\Http\AuthResponse;
use UXF\Security\Http\LoginRequestBody;
use UXF\Security\Service\AuthService;

/**
 * @implements ResponseModifier<AuthResponse>
 */
final readonly class LoginController implements ResponseModifier
{
    public function __construct(private AuthService $authService)
    {
    }

    #[Route('/api/auth/login', name: 'auth_login', methods: 'POST')]
    public function __invoke(#[FromBody] LoginRequestBody $body): AuthResponse
    {
        return $this->authService->login($body);
    }

    public static function modifyResponse(Response $response, mixed $data): void
    {
        $data->applyCookies($response);
    }
}
