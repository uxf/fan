<?php

declare(strict_types=1);

namespace UXF\Security\Controller\OIDC;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Twig\Environment;
use UXF\Core\Attribute\FromQuery;
use UXF\Core\SystemProvider\Clock;
use UXF\Security\Service\OIDC\OIDC;
use UXF\Security\Service\OIDC\OIDCService;

final readonly class OIDCConnectController
{
    public function __construct(
        private OIDCService $service,
        private Security $security,
        private Environment $twig,
    ) {
    }

    #[Route('/api/auth/oidc/{provider}/connect', name: 'auth_oidc_connect', methods: 'GET')]
    public function __invoke(OIDC $provider, #[FromQuery] RedirectRequestQuery $query): Response
    {
        if ($this->security->getUser() === null) {
            $html = $this->twig->render('@UXFSecurity/oidc/error.html.twig', [
                'message' => 'Login is required',
            ]);
            return new Response($html, 400);
        }

        $response = new RedirectResponse($this->service->getRedirectUrl($provider, true));
        if ($query->redirect !== null) {
            $response->headers->setCookie(Cookie::create(
                name: 'oidc_redirect',
                value: $query->redirect,
                expire: Clock::now()->modify('+10 minutes'),
                secure: true,
                sameSite: Cookie::SAMESITE_NONE, // Secure + SameSite=None is required for POST CORS requests
            ));
        }
        return $response;
    }
}
