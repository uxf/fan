<?php

declare(strict_types=1);

namespace UXF\Security\Controller\OIDC;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use UXF\Core\Attribute\FromQuery;
use UXF\Core\SystemProvider\Clock;
use UXF\Security\Service\OIDC\OIDC;
use UXF\Security\Service\OIDC\OIDCService;

final readonly class OIDCLoginController
{
    public function __construct(private OIDCService $service)
    {
    }

    #[Route('/api/auth/oidc/{provider}/login', name: 'auth_oidc_login', methods: 'GET')]
    public function __invoke(OIDC $provider, #[FromQuery] RedirectRequestQuery $query): Response
    {
        $response = new RedirectResponse($this->service->getRedirectUrl($provider, false));
        if ($query->redirect !== null) {
            $response->headers->setCookie(Cookie::create(
                name: 'oidc_redirect',
                value: $query->redirect,
                expire: Clock::now()->modify('+10 minutes'),
                secure: true,
                sameSite: Cookie::SAMESITE_NONE, // Secure + SameSite=None is required for POST CORS requests
            ));
        }
        return $response;
    }
}
