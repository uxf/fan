<?php

declare(strict_types=1);

namespace UXF\Security\Controller\OIDC;

final readonly class RedirectRequestQuery
{
    public function __construct(public ?string $redirect = null)
    {
    }
}
