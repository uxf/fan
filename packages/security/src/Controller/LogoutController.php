<?php

declare(strict_types=1);

namespace UXF\Security\Controller;

use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use UXF\Core\Attribute\FromQuery;
use UXF\Security\Event\LogoutUserEvent;
use UXF\Security\Http\LogoutRequestQuery;
use UXF\Security\Service\AuthService;

final readonly class LogoutController
{
    public function __construct(
        private AuthService $authService,
        private EventDispatcherInterface $eventDispatcher,
    ) {
    }

    #[Route('/api/auth/logout', name: 'auth_logout', methods: 'GET')]
    public function __invoke(Request $request, #[FromQuery] LogoutRequestQuery $query): Response
    {
        $response = $query->redirect !== null && $query->redirect !== ''
            ? new RedirectResponse($query->redirect)
            : new Response();
        $response->headers->clearCookie($this->authService->cookieName);
        $response->headers->clearCookie($this->authService->cookieExpireName);
        $response->headers->clearCookie(AuthService::RefreshTokenCookieName);

        $this->eventDispatcher->dispatch(new LogoutUserEvent($request, $response));

        return $response;
    }
}
