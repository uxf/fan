<?php

declare(strict_types=1);

namespace UXF\Security\Controller;

use Symfony\Component\Routing\Attribute\Route;
use UXF\Security\Http\Keys\JwksResponseBody;
use UXF\Security\Service\Jwk\JwkProvider;

final readonly class KeysController
{
    public function __construct(private JwkProvider $jwkProvider)
    {
    }

    #[Route('/api/auth/keys', name: 'auth_keys', methods: 'GET')]
    public function __invoke(): JwksResponseBody
    {
        return $this->jwkProvider->provide();
    }
}
