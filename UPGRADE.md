# UPGRADE from 3.5 => 3.6

- All interfaces with `*Interface` suffix have been renamed (now without `Interface` suffix). 
- All deprecated CMS annotation have been removed.
- `RequestBodyInterface` and `RequestQueryInterface` have been removed.
- PHPStan `ImmutableRule` + `jetbrains/phpstorm-attributes` have been removed.
- Storage `dsn` parameter replaced by `filesystems`.

# UPGRADE from 1.x and 2.x => 3.0

## PHP + SF

- PHP 8.1 + Symfony 6.0 is required
- `MyCLabs\Enum\Enum` -> native enum `enum StringEnum: string` or `enum IntEnum: int`

```php
// Before

use MyCLabs\Enum\Enum;

/**
 * @method static self ACTIVE()
 * @method static self REQUEST()
 * @extends Enum<string>
 */
class MembershipStatus extends Enum
{
    protected const ACTIVE = 'ACTIVE'; // aktivni
    protected const REQUEST = 'REQUEST'; // pozadavek na vstup
}

// After
enum MembershipStatus: string
{
    case ACTIVE = 'ACTIVE'; // aktivni
    case REQUEST = 'REQUEST'; // pozadavek na vstup

    // BC support
    public static function ACTIVE(): self
    {
        return self::ACTIVE;
    }

    public static function REQUEST(): self
    {
        return self::REQUEST;
    }
}
```

## Core

### Types:

- `DateTime` -> `UXF\Core\Type\DateTime`
- `DateTimeImmutable` -> `UXF\Core\Type\DateTime`
- `UXF\Core\Http\Type\ApiDateTimeImmutable` -> `UXF\Core\Type\DateTime`
- new type `UXF\Core\Type\Date`
- `UXF\Core\Type\DateTime` + `UXF\Core\Type\Date` support json serialization, __toString, doctrine type, request body, query and path type by default
- `Ramsey\Uuid\UuidInterface` support in requests
- `enum` support in requests

```php
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;

// Entity
#[ORM\Entity]
class Test
{
    #[ORM\Column(type: DateTime::class)]
    private DateTime $dateTime;
    
    #[ORM\Column(type: Date::class)]
    private Date $date;
    ...
}

// Request body
class TestRequestBody implements RequestBodyInterface
{
    public function __construct(
        public readonly DateTime $dateTime,
        public readonly Date $date,
    ) {
    }
}

// Response
class TestResponseBody
{
    public function __construct(
        public readonly DateTime $dateTime,
        public readonly Date $date,
    ) {
    }
}

// Route path
class TestController
{
    // path /api/test/{date}
    public function __invoke(Date $date): void
    {
        ...
    }
}
```

### Shared

- `UXF\Core\Shared\Entity\FileInterface` removed -> use `UXF\Storage\Entity\File`
- `UXF\Core\Shared\Entity\ImageInterface` removed -> use `UXF\Storage\Entity\Image`
- `UXF\Core\Shared\Service\CreateFileServiceInterface` removed -> use `UXF\Storage\Service\StorageFileService`
- `UXF\Core\Shared\*` removed

### SystemProviders

- `UXF\Core\SystemProvider\Calendar::today(): Date`
- `UXF\Core\SystemProvider\Clock::now(): DateTime`
- `UXF\Core\SystemProvider\Uuid::uuid4(): UuidInterface`

### Request:

- `UXF\Core\Http\Request\PatchRequestInterface` -> removed, please use `UXF\Core\Http\Request\NotSet`
- `UXF\Core\Http\Request\RequestBodyInterface` -> deprecated, please migrate to `#[FromBody]` (`UXF\Core\Http\Request\FromBody`)
- `UXF\Core\Http\Request\RequestQueryInterface` -> deprecated, please migrate to `#[FromQuery]` (`UXF\Core\Http\Request\FromQuery`)
- `sensio/framework-extra-bundle` -> removed

```php
use UXF\Core\Http\Request\FromBody;

class TestPatchRequestBody
{
    public function __controller(
        #[FromBody] public readonly string | null | NotSet $name = new NotSet(),
    ) {
    }
}

/** @var TestPatchRequestBody $body */
if (!$body->name instanceof NotSet) {
    // update
}
```

### Response:

- Dropped Symfony Serializer -> please remove all response object getters and use public promoted readonly properties

```php
use UXF\Core\Type\DateTime;

class TestResponseBody
{
    public function __construct(
        public readonly DateTime $dateTime,
    ) {
    }
}
```

### Enums in doctrine

- You have to register doctrine custom type
- We are waiting for doctrine/orm 2.11 - https://github.com/doctrine/orm/pull/9304
- There is problem with doctrine setParameter method. `Enum::SOME()` -> `Enum::SOME->value` migration is required

```php
// usage
use App\BlogZone\Enum\SectionBannerWithButtonType;

#[ORM\Entity]
class Entity
{
    #[ORM\Column]
    #[CmsFormField(label: 'Type')]
    private SectionBannerWithButtonType $type;
```

### Exceptions + ErrorResponse

- `ForbiddenException` etc dropped, please migrate to `UXF\Core\Exception\BasicException`

## Cms

- Drop old entities (`Action`, `BlogArticle`, `Language`, `ListItem`, `Question`, `ShortText`, `Text`)
- User email (reset password, invitation) entities migrated to `Invitation` namespace
- All DateTime migrated to UXF type
- `user` table renamed to `users` + `is_active` column renamed to `active`
- Cms route names have been changed
- Add support for Form and DataGrid attributes

```php
#[ORM\Entity]
#[ORM\Table(name: 'users', schema: 'uxf_cms')]
#[ORM\HasLifecycleCallbacks]
#[CmsMeta(alias: 'user', title: 'User')]
class User
{
    #[ORM\Column(length: 60, unique: true)]
    #[CmsTableColumn(label: 'E-mail')]
    #[CmsFormField(label: 'E-mail', editable: false)]
    private string $email = '';
    ...
}
```

- New config/routes/uxf.php files

```php
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return static function (RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->import('@UXFCmsBundle/config/routes.php');
    $routingConfigurator->import('@UXFDataGridBundle/config/routes.php');
    $routingConfigurator->import('@UXFFormBundle/config/routes.php');
    $routingConfigurator->import('@UXFGenBundle/config/routes.php');
};
```

- `UXF\CMS\Controller\Api\OAuthController` -> `UXF\CMS\Security\SimpleAccessTokenProvider`

## Security provider

```php
// before
$containerConfigurator->extension('security', [
    'providers' => [
        'provider' => [
            'id' => UserRepository::class,
        ],
    ],
...

// after
$containerConfigurator->extension('security', [
    'providers' => [
        'provider' => [
            'entity' => [
                'class' => User::class,
                'property' => 'id',
            ],
        ],
    ],
...
```

## Gen

- Drop method inspection support -> pleases use only promoted public props
- Drop `sources`, `type_map` and `http` from configuration

```yaml
# Before
uxf_gen:
    type_map:
        \App\AuthZone\Enum\SocialAuthType: { name: SocialAuthType }
    open_api:
        authorization_header: '%env(AUTHORIZATION_HEADER)%'
        areas:
            app:
                path_pattern: '/^\/api\/app/'
                sources:
                    - '%kernel.project_dir%/src/*/Http'
                    - '%kernel.project_dir%/src/ServiceCompanyZone/*/Http'
```

```php
// After
return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension('uxf_gen', [
        'open_api' => [
            'areas' => [
                'app' => [
                    'path_pattern' => '/^\/api\/app/',
                ],
            ],
        ],
    ]);
};
```

- new TypeName generator logic: drop doubled zone names, uxf internal types short form

```ts
// Before
export interface UXF_CMS_Http_Request_Cms_SaveUserConfigRequestBody {
}

// After
export interface CMSSaveUserConfigRequestBody {
}
```

```ts
// Before
export interface BlogBlogArticleSaveRequestBody {
}

// After
export interface BlogArticleSaveRequestBody {
}
```

## CodeGen

- Generate attributes instead of annotations

## Form

- `BasicField` constructor `public function __construct(string $name, string $originalType, ?string $type = null)`

```php
$field = new BasicField('date', Date::class, 'date');
```

## OAuth2

- `cookie_name` removed -> please use `authorization_header`

## Storage

- `UXF\Storage\Http\Request\FileRequestBody` and `UXF\Storage\Http\Response\FileResponse` are final
- `uxf_storage.depth` config parameter removed -> default 2
- `UXF\Storage\Entity\File::$uuid` and `UXF\Storage\Entity\Image::$uuid` are `Ramsey\Uuid\UuidInterface` now (migrated from string)
- `UXF\Storage\Service\ImageGenerator\ImageGeneratorInterface` removed -> use nodejs alternative

## Hydrator

- New package with better performance and input validation - still WIP.

# Migration tools

## Rector

```php
use Rector\Php80\Rector\Class_\AnnotationToAttributeRector;
use Rector\Php80\ValueObject\AnnotationToAttribute;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $parameters = $containerConfigurator->parameters();
    $parameters->set(\Rector\Core\Configuration\Option::PATHS, [
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ]);

    $services = $containerConfigurator->services();
    $services->set(AnnotationToAttributeRector::class)
        ->configure([
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\Entity::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\Table::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\InheritanceType::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\DiscriminatorColumn::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\DiscriminatorMap::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\Id::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\Column::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\GeneratedValue::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\ManyToMany::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\ManyToOne::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\OneToMany::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\OneToOne::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\Embeddable::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\Embedded::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\JoinColumn::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\HasLifecycleCallbacks::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\PreUpdate::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\JoinTable::class),
            new AnnotationToAttribute(\Doctrine\ORM\Mapping\OrderBy::class),
            new AnnotationToAttribute(\UXF\CMS\Annotation\CmsFormField::class),
            new AnnotationToAttribute(\UXF\CMS\Annotation\CmsTableColumn::class),
            new AnnotationToAttribute(\UXF\CMS\Annotation\CmsMeta::class),
        ]);

    $services->set(\Rector\Renaming\Rector\Name\RenameClassRector::class)
        ->configure([
            'DateTimeImmutable' => \UXF\Core\Type\DateTime::class,
            \UXF\Core\Http\Type\ApiDateTimeImmutable::class => \UXF\Core\Type\DateTime::class,
        ]);
};
```
